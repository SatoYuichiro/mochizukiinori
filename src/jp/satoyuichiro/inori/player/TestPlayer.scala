package jp.satoyuichiro.inori.player

import java.util.{ List => JList }
import java.util.ArrayList
import org.ggp.base.util.gdl._
import org.ggp.base.util.gdl.grammar._
import org.ggp.base.util.gdl.factory.GdlFactory
import org.ggp.base.util.statemachine.implementation.prover.ProverStateMachine
import scala.io.Source
import java.io.File
import org.ggp.base.util.statemachine.Move

object TestPlayer {

  def main(args: Array[String]): Unit = {
    val description = readConnect4 map (l => GdlFactory.create(l))
    val sm = new ProverStateMachine()
    sm.initialize(toJavaList(description))
    val roles = sm.getRoles()

    val startTime = System.currentTimeMillis()
    for (i <- 0 to 100) {
      var current = sm.getInitialState()
      while (!sm.isTerminal(current)) {
        val moves = new ArrayList[Move]
        moves.add(sm.getLegalMoves(current, roles.get(0)).get(0))
        moves.add(sm.getLegalMoves(current, roles.get(1)).get(0))
        val nextState = sm.getNextState(current, moves)
//              println(nextState)
        current = nextState
      }
//          println(sm.getGoals(current))
    }
    val endTime = System.currentTimeMillis()
    println((endTime - startTime) / 1000)
  }

  def toJavaList(list: List[Gdl]): JList[Gdl] = {
    var jlist = new ArrayList[Gdl]
    list foreach {
      gdl => jlist.add(gdl)
    }
    jlist
  }

  def readTictactoe(): List[String] = {
    var lines = List.empty[String]
    val input = Source.fromFile(new File("./games/twoplayers/tictactoe.gdl"))
    for (line <- input.getLines) {
      lines = line :: lines
    }
    lines
  }

  def readConnect4(): List[String] = {
    var lines = List.empty[String]
    val input = Source.fromFile(new File("./games/twoplayers/connect4.gdl"))
    for (line <- input.getLines) {
        lines = cutComment(line) :: lines
    }
    cutter(lines.reverse.fold("")(_ + _))
  }

  def cutter(code: String): List[String] = {
    var lines = List.empty[String]
    var subCode = code
    while (1 <= subCode.size) {
//      println("subCode " + subCode)
      var i = 1
      var aCode = subCode.substring(0, 1)
      subCode = subCode.substring(1)
      while (i != 0 && !subCode.isEmpty()) {
        aCode += subCode.substring(0, 1)
        if (subCode.substring(0, 1) == "(") {
          i += 1
        } else if (subCode.substring(0, 1) == ")") {
          i -= 1
        }
        subCode = subCode.substring(1)
      }
//      println("aCode" + aCode)
      lines = aCode :: lines
//      println(lines)
    }
//    println(lines)
    lines.reverse
  }

  val spacer = List(" ", "\n", "\t")

  def cutComment(line: String): String = {
    val str = cutSpacer(line)
    if (str.startsWith(";"))
      cutTillEndOfLine(str)
    else
      str
  }

  def cutSpacer(line: String): String = {
    var subCode = line
    while (1 <= subCode.size && spacer.contains(subCode.substring(0, 1))) {
      subCode = subCode.substring(1)
    }
    subCode
  }

  def cutTillEndOfLine(line: String): String = {
    line.split("\n").tail.fold("")(_ + _)
  }
}