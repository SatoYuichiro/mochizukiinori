package jp.satoyuichiro.inori.player

import java.util.{ List => JList }
import java.util.ArrayList
import java.lang.{ Integer => JInteger }
import org.ggp.base.util.statemachine.MachineState
import jp.satoyuichiro.inori.cdl.Position
import org.ggp.base.util.gdl.grammar.GdlTerm
import org.ggp.base.util.gdl.grammar.GdlFunction
import org.ggp.base.util.gdl.grammar.GdlConstant
import jp.satoyuichiro.inori.cdl.Integer
import jp.satoyuichiro.inori.cdl.Piece
import jp.satoyuichiro.inori.cdl.Symbol
import jp.satoyuichiro.inori.cdl.Role
import jp.satoyuichiro.inori.cdl.Exp
import jp.satoyuichiro.inori.player._
import scala.io.Source
import org.ggp.base.util.gdl.grammar.Gdl
import org.ggp.base.util.gdl.factory.GdlFactory
import java.io.File
import org.ggp.base.util.gdl.grammar.GdlRelation
import org.ggp.base.util.gdl.grammar.GdlSentence

object PlayerUtil {

  def readGdl(filePath: String): List[String] = {
    var lines = List.empty[String]
    val input = Source.fromFile(new File(filePath))
    for (line <- input.getLines) {
      lines = cutComment(line) :: lines
    }
    cutter(lines.reverse.fold("")(_ + _))
  }

  def cutter(code: String): List[String] = {
    var lines = List.empty[String]
    var subCode = code
    while (1 <= subCode.size) {
      //      println("subCode " + subCode)
      var i = 1
      var aCode = subCode.substring(0, 1)
      subCode = subCode.substring(1)
      while (i != 0 && !subCode.isEmpty()) {
        aCode += subCode.substring(0, 1)
        if (subCode.substring(0, 1) == "(") {
          i += 1
        } else if (subCode.substring(0, 1) == ")") {
          i -= 1
        }
        subCode = subCode.substring(1)
      }
      //      println("aCode" + aCode)
      lines = aCode :: lines
      //      println(lines)
    }
    //    println(lines)
    lines.reverse
  }

  val spacer = List(" ", "\n", "\t")

  def cutComment(line: String): String = {
    val str = cutSpacer(line)
    if (str.startsWith(";"))
      cutTillEndOfLine(str)
    else
      str
  }

  def cutSpacer(line: String): String = {
    var subCode = line
    while (1 <= subCode.size && spacer.contains(subCode.substring(0, 1))) {
      subCode = subCode.substring(1)
    }
    subCode
  }

  def cutTillEndOfLine(line: String): String = {
    line.split("\n").tail.fold("")(_ + _)
  }

  def toDescription(description: List[String]): List[Gdl] = description map (r => GdlFactory.create(r))

  def toJavaList[A](scalaList: List[A]): ArrayList[A] = {
    var jList = new ArrayList[A]
    scalaList foreach {
      a => jList.add(a)
    }
    jList
  }

  def toScalaList[A](javaList: JList[A]): List[A] = {
    var scalaList = List.empty[A]
    for (i <- 0 to javaList.size - 1) {
      scalaList = javaList.get(i) :: scalaList
    }
    scalaList.reverse
  }

  def gdlTocdl(machineState: MachineState): Position = {
    val set = machineState.getContents()
    var list = List.empty[GdlFunction]
    val i = set.iterator()
    while (i.hasNext()) {
      list ::= i.next().getBody().get(0).asInstanceOf[GdlFunction]
    }
    //    println(list)
    val control = list filter (_.getName().getValue() == "control")
    val position = list filter (_.getName().getValue() != "control")
    //    println(position)

    if (control.size == 0) {
      Position(position map (c => gdlFunctionToPiece(c)), None)
    } else {
      Position(position map (c => gdlFunctionToPiece(c)), Some(controlTorole(control.head)))
    }
  }

  def controlTorole(control: GdlFunction): Role = {
    Role(Symbol(gdlToString(control.get(0).asInstanceOf[GdlTerm])))
  }

  def gdlFunctionToPiece(gdlFunction: GdlFunction): Piece = {
    val pieceName = Symbol(gdlFunction.getName.getValue)
    var tails = List.empty[Exp]
    var i = gdlFunction.getBody().iterator()
    while (i.hasNext) {
      val c = i.next() match {
        case i if (isInt(i)) => Integer(gdlToInt(i))
        case a => Symbol(gdlToString(a))
      }
      tails ::= c
    }
    Piece(pieceName :: tails.reverse)
  }

  def isInt(c: GdlTerm): Boolean = {
    try {
      JInteger.parseInt(c.asInstanceOf[GdlConstant].getValue)
      true
    } catch {
      case _: Throwable => false
    }
  }

  def gdlToInt(c: GdlTerm): Int = JInteger.parseInt(c.asInstanceOf[GdlConstant].getValue())

  def gdlToString(c: GdlTerm): String = c.asInstanceOf[GdlConstant].getValue()

  def printMachineState(machineState: MachineState): Unit = {
    val gdlFunctions = machineStateToGdlRelations(machineState)
    val typedPieces = gdlFunctions map (f => TypedPiece.create(f))
    val control = typedPieces filter (p => p.isInstanceOf[ZeroDimensionalCell]) map (_.asInstanceOf[ZeroDimensionalCell])
    val position = typedPieces filter (p => p.isInstanceOf[TwoDimensionalCell]) map (_.asInstanceOf[TwoDimensionalCell])

    control foreach { p => println(p.gdlFunction) }

    for (i <- 0 to 100) {
      val line = position filter (p => p.getX.value == i)
      val sorted = line.sortWith((t1, t2) => t1.getY < t2.getY)
      sorted foreach { c => print(c.gdlFunction + " ") }
      if (0 < sorted.size) println()
    }
    println
  }

  def printMachineState(position: Position): Unit = {
    val control = position.position map (_.asInstanceOf[Piece]) filter (_.args.size == 2)
    val cells = position.position map (_.asInstanceOf[Piece]) filter (_.args.size == 4)

    position.role match {
      case Some(r) => println("control" + r)
      case None =>
    }
    control foreach println

    for (i <- 0 to 100) {
      val line = cells filter (p => p.args.tail.head == Integer(i))
      val sorted = line.sortWith((t1, t2) => t1.args.drop(2).head.asInstanceOf[Integer] < t2.args.drop(2).head.asInstanceOf[Integer])
      sorted foreach { c => print("(" + c.toLisp.drop(7) + " ") }
      if (0 < sorted.size) println()
    }
    println()
  }

  def machineStateToGdlRelations(machineState: MachineState): List[GdlFunction] = {
    val i = machineState.getContents().iterator()
    var list = List.empty[GdlSentence]
    while (i.hasNext()) {
      list ::= i.next()
    }
    truesToGdlRelations(list)
  }

  def truesToGdlRelations(trues: List[GdlSentence]): List[GdlFunction] = {
    trues map (t => trueToGdlRelation(t.asInstanceOf[GdlRelation])) filter (_ != None) map (_.get)
  }

  def trueToGdlRelation(gdlRelation: GdlRelation): Option[GdlFunction] = {
    if (gdlRelation.getName().getValue() == "true")
      Some(gdlRelation.get(0).asInstanceOf[GdlFunction])
    else
      None
  }

  def makeBinaryPosition(position: Position): List[Position] = {
    var result = List.empty[Position]
    val arrayPieces = position.position.toArray
    for (i <- 0 to arrayPieces.length - 2) {
      result ::= Position(List(arrayPieces(i), arrayPieces(i + 1)), None)
    }
    result
  }

  def makeTrinaryPosition(position: Position): List[Position] = {
    var result = List.empty[Position]
    val arrayPieces = position.position.toArray
    for (i <- 0 to arrayPieces.length - 3) {
      result ::= Position(List(arrayPieces(i), arrayPieces(i + 1), arrayPieces(i + 2)), None)
    }
    result
  }

  def makeAllBinaryPosition(position: Position): List[Position] = {
    var result = List.empty[Position]
    val arrayPieces = position.position.toArray
    for (i <- 0 to arrayPieces.length - 1) {
      for (j <- 0 to arrayPieces.length - 1) {
        if (i != j)
          result ::= Position(List(arrayPieces(i), arrayPieces(j)), None)
      }
    }
    result
  }

  def makeAllTrinaryPosition(position: Position): List[Position] = {
    var result = List.empty[Position]
    val arrayPieces = position.position.toArray
    for (i <- 0 to arrayPieces.length - 1) {
      for (j <- 0 to arrayPieces.length - 1) {
        for (k <- 0 to arrayPieces.length - 1) {
          if (i != j && i != k && j != k)
            result ::= Position(List(arrayPieces(i), arrayPieces(j), arrayPieces(k)), None)
        }
      }
    }
    result
  }
}