package jp.satoyuichiro.inori.player

import java.util.{ List => JList }
import org.ggp.base.util.statemachine.MachineState
import org.ggp.base.util.statemachine.Role
import org.ggp.base.util.statemachine.Move
import org.ggp.base.util.statemachine.StateMachine
import java.util.ArrayList

abstract class Player(val stateMachine: StateMachine, val role: Role) {

  def chooseMove(state: MachineState): Move

}

class RandomPlayer(override val stateMachine: StateMachine, override val role: Role) extends Player(stateMachine, role) {

  def chooseMove(state: MachineState): Move = {
    val moves = stateMachine.getLegalMoves(state, role)
    moves.get((Math.random() * moves.size).asInstanceOf[Int])
  }
}

class AlphaBetaPlayer(override val stateMachine: StateMachine, override val role: Role, val searchDepth: Int = 3, val debugMode: Boolean = false) extends Player(stateMachine, role) {

  val roles = PlayerUtil.toScalaList(stateMachine.getRoles)
  val roleIndices = stateMachine.getRoleIndices()
  val myRoleIndex = roleIndices.get(role).asInstanceOf[Int]
  val opponent = (roles filter (_ != role)).head
  val opponentRoleIndex = roleIndices.get(opponent).asInstanceOf[Int]

  def chooseMove(state: MachineState): Move = {
    if (debugMode) {
      debugChooseMove(state, searchDepth)
    }
    chooseMove(state, searchDepth)
  }
  
  def chooseMove(state: MachineState, searchDepth: Int): Move = {
    val legal = stateMachine.getLegalMoves(state, role)
    if (legal.size == 1)
      legal.get(0)
    else {
      val noop = stateMachine.getLegalMoves(state, opponent).get(0)
      def makeNextState(state: MachineState, move: Move): MachineState = {
        stateMachine.getNextState(state, makeMoves(move, noop))
      }
      
      val evaluated = PlayerUtil.toScalaList(legal) map (move => (move, alphaBeta(makeNextState(state, move), searchDepth - 1, Double.MinValue, Double.MaxValue, false)))
      val maxMove = evaluated.max(Ordering.by[(Move, Double), Double](_._2))
      val maxs = evaluated filter (_._2 == maxMove._2)
      val choosen = maxs.drop((Math.random * maxs.size).asInstanceOf[Int]).head._1
      choosen
    }
  }
  
  def debugChooseMove(state: MachineState, searchDepth: Int): Move = {
    val legal = stateMachine.getLegalMoves(state, role)
    if (legal.size == 1)
      legal.get(0)
    else {
      val noop = stateMachine.getLegalMoves(state, opponent).get(0)
      def makeNextState(state: MachineState, move: Move): MachineState = {
        stateMachine.getNextState(state, makeMoves(move, noop))
      }
      
      val evaluated = PlayerUtil.toScalaList(legal) map (move => (move, alphaBeta(makeNextState(state, move), searchDepth - 1, Double.MinValue, Double.MaxValue, false)))
      evaluated foreach println
      val maxMove = evaluated.max(Ordering.by[(Move, Double), Double](_._2))
      val maxs = evaluated filter (_._2 == maxMove._2)
      val choosen = maxs.drop((Math.random * maxs.size).asInstanceOf[Int]).head._1
      println(choosen + " is choosen")
      choosen
    }
  }

  def alphaBeta(state: MachineState, depth: Int, alpha: Double, beta: Double, myTurn: Boolean): Double = {
    if (depth <= 0 || stateMachine.isTerminal(state))
      evaluate(state)
    else if (myTurn) {
      var a = alpha
      val myMoves = PlayerUtil.toScalaList(stateMachine.getLegalMoves(state, role))
      val noop = stateMachine.getLegalMoves(state, opponent).get(0)
      myMoves foreach {
        move =>
          a = List(a, alphaBeta(stateMachine.getNextState(state, makeMoves(move, noop)), depth - 1, a, beta, false)).max
//          println("depth " + depth + " alpha "+ a + " beta " + beta)
          if (beta <= a)
            return a
      }
      a
    }
    else {
      var b = beta
      val opponentMoves = PlayerUtil.toScalaList(stateMachine.getLegalMoves(state, opponent))
      val noop = stateMachine.getLegalMoves(state, role).get(0)
      opponentMoves foreach {
        move =>
          b = List(b, alphaBeta(stateMachine.getNextState(state, makeMoves(noop, move)), depth - 1, alpha, b, true)).min
//          println("depth " + depth + " alpha "+ alpha + " beta " + b)
          if (b <= alpha)
            return b
      }
      b
    }
  }
  
  def makeMoves(myMove: Move, opponentMove: Move): JList[Move] = {
    var moves = new ArrayList[Move]
    if (myRoleIndex < opponentRoleIndex) {
      moves.add(myMove)
      moves.add(opponentMove)
    }
    else {
      moves.add(opponentMove)
      moves.add(myMove)
    }
    moves
  }
  
  def evaluate(state: MachineState): Double = {
    if (stateMachine.isTerminal(state)) {
      val goals = stateMachine.getGoals(state)
      if (won(goals)) Double.MaxValue
      else if (lose(goals)) Double.MinValue
      else if (draw(goals)) 0.0
      else 0.0
    }
    else 0.0
  }

  def won(results: JList[Integer]): Boolean = results.get(opponentRoleIndex) < results.get(myRoleIndex)

  def lose(results: JList[Integer]): Boolean = results.get(myRoleIndex) < results.get(opponentRoleIndex)

  def draw(results: JList[Integer]): Boolean = results.get(myRoleIndex) == results.get(opponentRoleIndex)
  
  def isTerminalState(state: MachineState): Boolean = stateMachine.isTerminal(state)
}

class HeuristicsPlayer(val heuristics: Heuristics, override val stateMachine : StateMachine, override val role: Role,
    override val searchDepth: Int = 3, override val debugMode: Boolean = false) extends AlphaBetaPlayer(stateMachine, role, searchDepth, debugMode) {
  
  override def evaluate(state: MachineState): Double = {
    if (stateMachine.isTerminal(state)) {
      val goals = stateMachine.getGoals(state)
      if (won(goals)) Double.MaxValue
      else if (lose(goals)) Double.MinValue
      else if (draw(goals)) 0.0
      else 0.0
    }
    else heuristics.evaluate(state)
    
  }
}