package jp.satoyuichiro.inori.player

import jp.satoyuichiro.inori.cdl._
import jp.satoyuichiro.inori.learning.concept.generator.CompositeHeuristics
import org.ggp.base.util.statemachine.MachineState
import jp.satoyuichiro.inori.learning.concept.adapter.Adapter
import jp.satoyuichiro.inori.learning.concept.generator.PrimeHeuristics

class Heuristics(val heuristics: CompositeHeuristics, val adapter: Option[Adapter]) {

  var positionLookUpTable = Map.empty[MachineState, Double]
  var subPositionLookUpTable = Map.empty[Position, Double]
  
  val interpreter = heuristics.load(new Interpreter)

  def evaluate(position: MachineState): Double = {
    if (positionLookUpTable.contains(position)) {
      positionLookUpTable(position)
    }
    else {
      val evaluation = adapter match {
        case Some(a) => evaluate(a.convert(gdlToCdl(position)))
        case None => evaluate(gdlToCdl(position))
      }
      positionLookUpTable += position -> evaluation
      evaluation
    }
  }

  def gdlToCdl(position: MachineState): Position = PlayerUtil.gdlTocdl(position)

  def evaluate(position: Position): Double = {
    val subPos : List[Position] = PlayerUtil.makeBinaryPosition(position) ++
      PlayerUtil.makeAllTrinaryPosition(position)
    subPos.par.map(
      pos =>
        if (subPositionLookUpTable.contains(pos)) {
          subPositionLookUpTable(pos)
        }
        else {
          val evaluation = heuristics.primeHeuristics.map(
            prime => try {
              interpreter.interpret(prime.toQuery(pos)).asInstanceOf[Real].value
            } catch {
               case _: Throwable => 0.0
            }).fold(0.0)(_ + _)
            subPositionLookUpTable += pos -> evaluation
            evaluation
        }
      ).fold(0.0)(_ + _)
  }
}

object Heuristics {
  
  def apply(heuristics: CompositeHeuristics): Heuristics = new Heuristics(heuristics.labelAll, None)
  def apply(heuristics: CompositeHeuristics, adapter: Adapter): Heuristics = new Heuristics(heuristics.labelAll, Some(adapter))
  
  def apply(heuristics: PrimeHeuristics*): Heuristics = new Heuristics(CompositeHeuristics(heuristics.toList).labelAll, None)
  def apply(adapter: Adapter, heuristics: PrimeHeuristics*): Heuristics = new Heuristics(CompositeHeuristics(heuristics.toList).labelAll, Some(adapter))
}