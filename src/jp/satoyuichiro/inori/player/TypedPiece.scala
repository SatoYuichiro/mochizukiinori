package jp.satoyuichiro.inori.player

import jp.satoyuichiro.inori.cdl.Integer
import org.ggp.base.util.gdl.grammar.GdlFunction
import org.ggp.base.util.gdl.grammar.GdlConstant
import jp.satoyuichiro.inori.cdl.Piece

case class Argument(argumentType: Array[ArgumentType], arity: Int) {

  def getFirstIntIndex: Option[Int] = {
    for (i <- 0 to (arity - 1)) {
      if (argumentType(i).isInstanceOf[IntArgument])
        return Some(i)
    }
    None
  }

  def getSecondIntIndex: Option[Int] = {
    getFirstIntIndex match {
      case Some(i) =>
        for (j <- (i + 1) to (arity - 1)) {
          if (argumentType(j).isInstanceOf[IntArgument])
            return Some(j)
        }
        None
      case None => None
    }
  }

  def getFirstStringIndex: Option[Int] = {
    for (i <- 0 to (arity - 1)) {
      if (argumentType(i).isInstanceOf[StringArgument])
        return Some(i)
    }
    None
  }
}
class ArgumentType()
case class IntArgument() extends ArgumentType()
case class StringArgument() extends ArgumentType()
case class NullArgument() extends ArgumentType()

class TypedPiece(val gdlFunction: GdlFunction, val argument: Argument) {

  def getName: Symbol = Symbol(gdlFunction.getName.getValue)
  def getInteger(i: Int): Integer = Integer(gdlFunction.get(i).asInstanceOf[GdlConstant].getValue().toInt)
  def getSymbol(i: Int): Symbol = Symbol(gdlFunction.get(i).asInstanceOf[GdlConstant].getValue())
}

case class ZeroDimensionalCell(override val gdlFunction: GdlFunction, override val argument: Argument) extends TypedPiece(gdlFunction, argument) {

  def getMark: Symbol = getSymbol(0)
}

case class OneDimensionalCell(val x: Int, val mark: Int, override val gdlFunction: GdlFunction, override val argument: Argument)
  extends TypedPiece(gdlFunction, argument) {

  def getX: Integer = getInteger(x)
  def getMark: Symbol = getSymbol(mark)
}

case class TwoDimensionalCell(val x: Int, val y: Int, val mark: Int, override val gdlFunction: GdlFunction, override val argument: Argument)
  extends TypedPiece(gdlFunction, argument) {

  def getX: Integer = getInteger(x)
  def getY: Integer = getInteger(y)
  def getMark: Symbol = getSymbol(mark)
}

object TypedPiece {

  def create(gdlFunction: GdlFunction): TypedPiece = {
    val argument = checkArgumentType(gdlFunction)
    gdlFunction.arity() match {
      case 1 =>
        ZeroDimensionalCell(gdlFunction, argument)
      case 2 =>
        argument.getFirstIntIndex match {
          case Some(i) => OneDimensionalCell(i, argument.getFirstStringIndex.get, gdlFunction, argument)
          case None => null
        }
      case 3 =>
        argument.getSecondIntIndex match {
          case Some(j) => TwoDimensionalCell(argument.getFirstIntIndex.get, j, argument.getFirstStringIndex.get, gdlFunction, argument)
          case None => null
        }
    }
  }

  def checkArgumentType(gdlFunction: GdlFunction): Argument = {
    val body = gdlFunction.getBody()
    var array = new Array[ArgumentType](body.size())
    for (i <- 0 to body.size() - 1) {
      val arg = body.get(i)
      try {
        val int = arg.asInstanceOf[GdlConstant].getValue().toInt
        array(i) = new IntArgument()
      } catch {
        case _: Throwable =>
          try {
            arg.asInstanceOf[GdlConstant].getValue()
            array(i) = new StringArgument()
          } catch {
            case _: Throwable => array(i) = new NullArgument()
          }
      }
    }
    new Argument(array, array.size)
  }
}