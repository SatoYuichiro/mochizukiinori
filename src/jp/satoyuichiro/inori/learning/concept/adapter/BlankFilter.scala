package jp.satoyuichiro.inori.learning.concept.adapter

import org.ggp.base.util.statemachine.MachineState
import jp.satoyuichiro.inori.cdl.Position
import jp.satoyuichiro.inori.gdl.GdlConverter
import jp.satoyuichiro.inori.cdl.Piece
import jp.satoyuichiro.inori.cdl.Symbol
import jp.satoyuichiro.inori.cdl.Integer

class BlankFilter(val blank: Symbol, exception: List[(Integer, Integer)]) extends Adapter {

  def convert(machineState: MachineState): Position = {
    convert(GdlConverter.gdlTocdl(machineState))
  }
  
  def convert(position: Position): Position = {
    var result = List.empty[Piece]
    position.position foreach { piece => piece match {
      case p: Piece =>
        if (p.args.size == 4 && p.args.last != blank) {
          result ::= p
        }
        else if (p.args.size == 4 && p.args.last == blank) {
          exception.find(t => t._1 == p.args.tail.head && t._2 == p.args.tail.tail.head) match {
            case Some(t) => result ::= p
            case None =>
          }
        }
      case _ =>
      }
    }
    Position(result.reverse, None)
  }
}

object BlankFilter {
  
  def apply(blank: Symbol): BlankFilter = new BlankFilter(blank, List.empty[(Integer, Integer)])
  def apply(blank: Symbol, exceptions: (Integer,Integer)*): BlankFilter = new BlankFilter(blank, exceptions.toList)
}