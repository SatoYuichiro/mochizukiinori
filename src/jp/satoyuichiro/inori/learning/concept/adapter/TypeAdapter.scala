package jp.satoyuichiro.inori.learning.concept.adapter

import org.ggp.base.util.statemachine.MachineState
import jp.satoyuichiro.inori.cdl.Position
import jp.satoyuichiro.inori.gdl.GdlConverter
import jp.satoyuichiro.inori.learning.concept.generator.GeneratorUtil

class TypeAdapter(val typeRelations: List[TypeRelation], val succAdapter: Option[SuccAdapter]) extends Adapter {

  def convert(machineState: MachineState): Position = {
    succAdapter match {
      case Some(succ) => applyType(succ.convert(machineState))
      case None => applyType(GdlConverter.gdlTocdl(machineState))
    }
  }
  
  def convert(position: Position): Position = {
    succAdapter match {
      case Some(succ) => applyType(succ.convert(position))
      case None => applyType(position)
    }
  }
  
  def applyType(position: Position): Position = {
    if (0 < typeRelations.size) {
      val clojure = typeClojure(position)
      typeRelations.tail foreach {t => clojure(t) }
      clojure(typeRelations.head)
    }
    else
      position
  }
  
  def typeClojure(position: Position): TypeRelation => Position = {
    var pos = position
    (typeRelation: TypeRelation) => {
      pos = typeRelation.convert(pos)
      pos
    }
  }
}

object TypeAdapter {
  
  def apply(typeRelations: List[TypeRelation]): TypeAdapter = new TypeAdapter(typeRelations, None)
}