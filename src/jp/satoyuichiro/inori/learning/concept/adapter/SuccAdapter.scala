package jp.satoyuichiro.inori.learning.concept.adapter

import org.ggp.base.util.statemachine.MachineState
import jp.satoyuichiro.inori.learning.concept.generator.GeneratorUtil
import jp.satoyuichiro.inori.cdl.Position

class SuccAdapter(val succRelations: List[SuccRelation]) extends Adapter {

  val simpleAdapter = new SimpleAdapter
  
  def convert(machineState: MachineState): Position = {
    applySucc(simpleAdapter.convert(machineState))
  }
  
  def convert(position: Position): Position = applySucc(position)
  
  def applySucc(position: Position): Position = {
    if (0 < succRelations.size) {
      val clojure = succClojure(position)
      succRelations.tail foreach {s => clojure(s) }
      clojure(succRelations.head)
    }
    else
      position
  }
  
  def succClojure(position: Position): SuccRelation => Position = {
    var pos = position
    (succRelation: SuccRelation) => {
      pos = GeneratorUtil.Manipulator.replace(pos, succRelation.toReplace).asInstanceOf[Position]
      pos
    }
  }
}