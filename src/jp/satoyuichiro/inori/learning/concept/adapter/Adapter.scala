package jp.satoyuichiro.inori.learning.concept.adapter

import org.ggp.base.util.statemachine.MachineState
import jp.satoyuichiro.inori.cdl.Position

trait Adapter {

  def convert(machineState: MachineState): Position
  def convert(position: Position): Position
}