package jp.satoyuichiro.inori.learning.concept.adapter

import org.ggp.base.util.statemachine.MachineState
import jp.satoyuichiro.inori.gdl.GdlConverter
import jp.satoyuichiro.inori.cdl.Piece
import jp.satoyuichiro.inori.cdl.Integer
import jp.satoyuichiro.inori.cdl.Symbol
import scala.collection.mutable.Builder
import jp.satoyuichiro.inori.cdl.Exp
import jp.satoyuichiro.inori.cdl.Position

class TypeRelationGenerator {

  def generate(machineState: MachineState): List[TypeRelation] = {
    generate(GdlConverter.gdlTocdl(machineState))
  }
  
  def generate(position: Position): List[TypeRelation] = {
    generate(position.position map (_.asInstanceOf[Piece]))
  }
  
  def generate(position: List[Piece]): List[TypeRelation] = {
    val clojure = typeClojure
    position.tail foreach { p => clojure(p) }
    clojure(position.head)
  }

  def typeClojure(): Piece => List[TypeRelation] = {
    var typeRelations = List.empty[TypeRelation]
    (piece: Piece) => {
      val typeRelation = makeTypeRelation(piece)
      if (!typeRelation.isEmpty && !typeRelations.contains(typeRelation.head)) typeRelations ::= typeRelation.head
      typeRelations
    }
  }

  var intIndex = 0
  var strIndex = 0

  def makeTypeRelation(piece: Piece): List[TypeRelation] = {
    val pieceName = piece.args.head
    pieceName match {
      case name: Symbol =>
        val body = piece.args.toArray
        val builder = Map.newBuilder[Int, Int]
        intIndex = 1
        strIndex = piece.args.filter(_.isInstanceOf[Integer]).size + 1

        for (i <- 1 to body.length - 1) {
          typeCheck(body(i), i, builder)
        }
        intIndex = 1
        strIndex = 1
        List(new TypeRelation(name, builder.result))
      case _ => List.empty[TypeRelation]
    }
  }

  def typeCheck(exp: Exp, i: Int, builder: Builder[(Int, Int), Map[Int, Int]]): Unit = {
    exp match {
      case int: Integer =>
        builder += i -> intIndex
        intIndex += 1
      case sym: Symbol =>
        builder += i -> strIndex
        strIndex += 1
      case _ =>
    }
  }

}