package jp.satoyuichiro.inori.learning.concept.adapter

import org.ggp.base.util.statemachine.MachineState
import jp.satoyuichiro.inori.cdl.Position
import jp.satoyuichiro.inori.gdl.GdlConverter
import jp.satoyuichiro.inori.cdl.Piece
import jp.satoyuichiro.inori.cdl.Symbol
import jp.satoyuichiro.inori.cdl.Integer

class SuccBlankFilter(val succ: SuccAdapter, filter: BlankFilter) extends Adapter {

  def convert(machineState: MachineState): Position = {
    convert(GdlConverter.gdlTocdl(machineState))
  }
  
  def convert(position: Position): Position = {
    filter.convert(succ.convert(position))
  }
}