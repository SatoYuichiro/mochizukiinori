package jp.satoyuichiro.inori.learning.concept.adapter

import jp.satoyuichiro.inori.cdl._

import org.ggp.base.util.gdl.grammar.Gdl
import org.ggp.base.util.gdl.grammar.GdlRelation
import org.ggp.base.util.gdl.grammar.GdlConstant

class SuccRelationGenerator {

  def generate(description: List[Gdl]): List[SuccRelation] = {
    val relations = description filter (_.isInstanceOf[GdlRelation]) map (_.asInstanceOf[GdlRelation])
    val sameRelations = makeSameRelations(relations)
    (sameRelations map (r => makeSuccRelation(r)) filter (_ != None) map (_.get) map (a => List(a))).fold(List.empty[SuccRelation])((a,b) => a ++ b)
  }
  
  def makeSameRelations(relations: List[GdlRelation]): List[List[GdlRelation]] = {
    var result = List.empty[List[GdlRelation]]
    var names = List.empty[GdlConstant]
    relations foreach {
      r => if (!names.contains(r.getName())) names ::= r.getName()
    }
    names foreach {
      n =>
        var subResult = List.empty[GdlRelation]
        relations foreach {
          r => if (r.getName() == n) subResult ::= r
        }
        result ::= subResult
    }
    result
  }
  
  def isSuccRelation(relation: List[GdlRelation]): Boolean = {
    isAllBinary(relation) && hasOne(relation) && hasEnd(relation) && isAllHasUniquSucc(relation)
  }
  
  def isAllBinary(relation: List[GdlRelation]): Boolean = (relation map (r => isBinary(r))).fold(true)(_ && _)
  
  def isBinary(gdlRelation: GdlRelation): Boolean = gdlRelation.getBody().size == 2
  
  def hasOne(relation: List[GdlRelation]): Boolean = {
    findOne(relation) match {
      case Some(c) => true
      case None => false
    }
  }
  
  def findOne(relation: List[GdlRelation]): Option[GdlConstant] = {
    val firsts = getFirstArgs(relation)
    val seconds = getSecondArgs(relation)
    firsts filter (f => !seconds.contains(f)) match {
      case Nil => None
      case head :: Nil => Some(head)
      case head :: tail => None
    }
  }
  
  def getFirstArgs(relation: List[GdlRelation]): List[GdlConstant] = {
    try {
      relation map (_.getBody().get(0).asInstanceOf[GdlConstant])
    } catch {
      case _: Throwable => List.empty[GdlConstant]
    }
  }
  
  def getSecondArgs(relation: List[GdlRelation]): List[GdlConstant] = {
    try {
      relation map (_.getBody().get(1).asInstanceOf[GdlConstant])
    } catch {
      case _: Throwable => List.empty[GdlConstant]
    }
  }
  
  def hasEnd(relation: List[GdlRelation]): Boolean = {
    findEnd(relation) match {
      case Some(c) => true
      case None => false
    }
  }
  
  def findEnd(relation: List[GdlRelation]): Option[GdlConstant] = {
    val firsts = getFirstArgs(relation)
    val seconds = getSecondArgs(relation)
    seconds filter (f => !firsts.contains(f)) match {
      case Nil => None
      case head :: Nil => Some(head)
      case head :: tail => None
    }
  }
  
  def isAllHasUniquSucc(relation: List[GdlRelation]): Boolean = {
    (getFirstArgs(relation) map (a => hasUniquSucc(a, relation))).fold(true)(_ && _)
  }
  
  def hasUniquSucc(constant: GdlConstant, relation: List[GdlRelation]): Boolean = {
    findNext(constant, relation) match {
      case Nil => false
      case head :: Nil => true
      case head :: tail => false
    }
  }
  
  def findNext(constant: GdlConstant, relation: List[GdlRelation]): List[GdlConstant] = {
    val bodys = relation filter (_.getBody().get(0).asInstanceOf[GdlConstant] == constant)
    bodys map (_.getBody().get(1).asInstanceOf[GdlConstant])
  }
  
  def makeSuccRelation(relation: List[GdlRelation]): Option[SuccRelation] = {
    findOne(relation) match {
      case Some(one) =>
        findEnd(relation) match {
          case Some(end) =>
            val builder = Map.newBuilder[Symbol, Integer]
            var current = one
            var i = 1
            while(current != end) {
              builder += Symbol(current.getValue()) -> Integer(i)
              i += 1
              current = findNext(current, relation).head
            }
            builder += Symbol(end.getValue()) -> Integer(i)
            Some(new SuccRelation(relation.head.getName.getValue(), builder.result))
          case None => None
        }
      case None => None
    }
  }
}