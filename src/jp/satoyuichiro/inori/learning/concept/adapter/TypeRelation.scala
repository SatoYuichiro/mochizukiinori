package jp.satoyuichiro.inori.learning.concept.adapter

import org.ggp.base.util.statemachine.MachineState
import jp.satoyuichiro.inori.cdl.Position
import jp.satoyuichiro.inori.gdl.GdlConverter
import jp.satoyuichiro.inori.cdl.Piece
import jp.satoyuichiro.inori.cdl.Symbol
import jp.satoyuichiro.inori.cdl.Exp
import jp.satoyuichiro.inori.cdl.EvalPiece

case class TypeRelation(val cellName: Symbol, val typeMap: Map[Int, Int]) {

  def convert(machineState: MachineState): Position = {
    convert(GdlConverter.gdlTocdl(machineState))
  }
  
  def convert(position: Position): Position = {
    Position(position.position map (p => if (p.isInstanceOf[Piece]) applyRelation(p.asInstanceOf[Piece]) else p), None)
  }
  
  def applyRelation(piece: Piece): Piece = {
    if (piece.args.head == cellName) {
      val array = piece.args.toArray
      var result = new Array[Exp](array.length)
      for (i <- 0 to piece.args.size - 1) {
        typeMap.get(i) match {
          case Some(index) => result(index) = array(i)
          case None => result(i) = array(i)
        }
      }
      Piece(result.toList)
    }
    else
      piece
  }
  
}