package jp.satoyuichiro.inori.learning.concept.adapter

import jp.satoyuichiro.inori.learning.concept.generator.GeneratorUtil.Manipulator.Replace
import jp.satoyuichiro.inori.cdl.LeafExp
import jp.satoyuichiro.inori.cdl.Exp
import jp.satoyuichiro.inori.cdl.Integer
import jp.satoyuichiro.inori.cdl.Symbol

class SuccRelation(val name: String, val succ: Map[Symbol, Integer]) {

  def convertInt(s: Symbol): Integer =  {
    succ.get(s) match {
      case Some(i) => i
      case None => throw new IllegalArgumentException(s + " is not defined")
    }
  }
  
  def toReplace: Replace = {
    val builder = Map.newBuilder[LeafExp, Exp]
    succ foreach {
      map => builder += map._1.asInstanceOf[LeafExp] -> map._2.asInstanceOf[Exp] 
    }
    builder.result
  }
}