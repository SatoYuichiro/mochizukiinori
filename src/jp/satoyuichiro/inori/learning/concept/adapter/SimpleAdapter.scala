package jp.satoyuichiro.inori.learning.concept.adapter

import org.ggp.base.util.statemachine.MachineState
import jp.satoyuichiro.inori.cdl.Position
import jp.satoyuichiro.inori.player.PlayerUtil
import jp.satoyuichiro.inori.gdl.GdlConverter

class SimpleAdapter extends Adapter {

  def convert(machineState: MachineState): Position = {
    GdlConverter.gdlTocdl(machineState)
  }
  
  def convert(position: Position): Position = position
}