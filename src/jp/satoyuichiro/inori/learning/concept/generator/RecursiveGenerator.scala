package jp.satoyuichiro.inori.learning.concept.generator

import jp.satoyuichiro.inori.cdl._

class RecursiveGenerator {

  def generate(primeConcept : PrimeConcept): PrimeRecursive = { PrimeRecursive(primeConcept)
//    val order = GeneratorUtil.ArityCounter.count(concept)
//    val conceptLabel = Label("con1")
//    val positionLabel = Label("pos")
//    val FOR = makeFor(conceptLabel, positionLabel, order)
//    Concept(List(TypedLabel(positionLabel, PositionType())), FOR)
  }
  
//  def makeFor(conceptLabel : Label, positionLabel : Label, arity : Int): For = {
//    val i = Label("i")
//    val resultLabel = Label("res")
//    For(makeIndex(i, resultLabel), makeCondition(i, positionLabel, arity), makeIncriment(i), makeBody(i, resultLabel, positionLabel, conceptLabel, arity))
//  }
//  
//  def makeIndex(index : Label, resultLabel : Label): List[Substitution] = List(Substitution(index, Integer(0)), Substitution(resultLabel, Bool(true)))
//  
//  def makeCondition(index : Label, positionLabel : Label, arity : Int): Condition = Greater(index, Subtraction(Size(positionLabel), Integer(arity)))
//  
//  def makeIncriment(index : Label): Body = Body(Set(index, Addition(index, Integer(1))))
//  
//  def makeBody(index : Label, resultLabel : Label, positionLabel : Label, conceptLabel : Label, arity : Int): Body = {
//    val args = (0 to (arity - 1)).toList map (i => Arg(Addition(Integer(i), index), positionLabel))
//    Body(Set(resultLabel, And(resultLabel, ConceptCall(conceptLabel, List(TypedExp(Position(args, None), PositionType()))))), resultLabel)
//  }
}