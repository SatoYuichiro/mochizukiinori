package jp.satoyuichiro.inori.learning.concept.generator

import jp.satoyuichiro.inori.cdl._

class DisjunctionGenerator extends Generator {

  def generate(positions : List[Position]): CompositeDisjunction = {
    val conjunctionGenerator = new ConjunctionGenerator
    val newConceptName = GeneratorUtil.Labeler.makeLabeler("pattern")
    val primeConjunctions = positions map (p => conjunctionGenerator.generate(p).putLabel(newConceptName()))
    CompositeDisjunction(primeConjunctions)
  }
  
  def generate(concepts: List[PrimeConcept], positions: List[Position]): Option[CompositeDisjunction] = {
    var conceptList = List.empty[PrimeConcept]
    var tupleConcepts = concepts map (c => (c, c.toDefineConcept)) filter (_._2 != None) map (t => (t._1, t._2.get))
    if (0 < tupleConcepts.size) {
      var rest = positions
      while (0 < rest.size || 0 < tupleConcepts.size) {
        val counts = tupleConcepts map (t => (t, GeneratorUtil.countMatchable(t._2, rest)))
        counts foreach {c => println(c._1._1.toConcept.toLisp + " " + c._2) }
        if (counts.size == 0) return Some(CompositeDisjunction(conceptList.reverse))
        val maxConcept = counts.maxBy(c => c._2)
        println("max " + maxConcept._1._1.toConcept.toLisp + " " + maxConcept._2)
        println(rest.size)
        println()
        conceptList ::= maxConcept._1._1
        val buf = rest
        rest = rest filter (p => !GeneratorUtil.isMatchable(maxConcept._1._2, p))
        tupleConcepts = tupleConcepts filter (_ != maxConcept._1)
        if (rest.size == buf.size) return Some(CompositeDisjunction(conceptList.reverse))
      }
      Some(CompositeDisjunction(conceptList.reverse))
    } else {
      None
    }
  }
}