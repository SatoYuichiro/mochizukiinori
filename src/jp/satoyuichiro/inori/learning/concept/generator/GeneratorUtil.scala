package jp.satoyuichiro.inori.learning.concept.generator

import jp.satoyuichiro.inori.cdl._
import jp.satoyuichiro.inori.player.PlayerUtil

object GeneratorUtil {

  def generateUniquConcept(generator: Generator, positions: List[Position]): List[RichConcept] = {
    if (generator.isInstanceOf[ArithmeticConjunctionGenerator]) {
      val gen = generator.asInstanceOf[ArithmeticConjunctionGenerator]
      val duplicated = positions map (p => gen.generate(p))
      var result = List.empty[RichConcept]
      duplicated foreach {
        d => d match {
          case Some(concept) => if (!result.contains(concept)) result ::= concept
          case None =>
        }
      }
      result
    } else {
      List.empty[RichConcept]
    }
  }
  
  def countMatchable(defineConcept: DefineConcept, positions: List[Position]): Int = {
    val interpreter = new Interpreter
    interpreter.interpret(defineConcept)
    var count = 0
    positions foreach {
      position =>
        if (isMatchable(interpreter, defineConcept, position)) count += 1
    }
    count
  }
  
  def isMatchable(defineConcept: DefineConcept, position: Position): Boolean = {
    val interpreter = new Interpreter
    interpreter.interpret(defineConcept)
    PlayerUtil.makeAllBinaryPosition(position).par foreach {
      p => if (isMatch(interpreter, defineConcept, p)) return true
    }
    PlayerUtil.makeAllTrinaryPosition(position).par foreach {
      p => if (isMatch(interpreter, defineConcept, p)) return true
    }
    false
  }
  
  def isMatchable(interpreter: Interpreter, defineConcept: DefineConcept, position: Position): Boolean = {
    PlayerUtil.makeAllBinaryPosition(position).par foreach {
      p => if (isMatch(interpreter, defineConcept, position)) return true
    }
    PlayerUtil.makeAllTrinaryPosition(position).par foreach {
      p => if (isMatch(interpreter, defineConcept, position)) return true
    }
    false
  }
  
  def isMatch(interpreter: Interpreter, defineConcept: DefineConcept, position: Position): Boolean = {
    val query = ConceptCall(defineConcept.conceptName, List(TypedExp(position, PositionType())))
    try {
      val result = interpreter.interpret(query)
      result == Bool(true)
    } catch {
      case ne : NoVariableException => false
      case _ : Throwable => false
    }
  }

  def isMatch(interpreter: Interpreter, query: ConceptCall): Boolean = {
    try {
      val result = interpreter.interpret(query)
      result == Bool(true)
    } catch {
      case ne : NoVariableException => false
      case _ : Throwable => false
    }
  }

  object Labeler {

    def makeNewLabel(str: String): Int => Label = {
      i => { Label(str + i) }
    }

    def makeLabeler(str: String): Unit => Label = {
      val labeler = makeNewLabel(str)
      var i = -1
      Unit => {
        i += 1
        labeler(i)
      }
    }

    def indexMaker(c: Int): Unit => Integer = {
      var i = c - 1
      Unit => {
        i += 1
        Integer(i)
      }
    }

    def countNotLabel(exp: List[Exp]): Int = exp.filter(e => !e.isInstanceOf[Label]).size
  }

  object Manipulator {

    type Replace = Map[LeafExp, Exp]

    def replace(exp: Exp, rule: Replace): Exp = {
      exp match {
        case ari: Arithmetic => replaceArithmetic(ari, rule)
        case log: Logic => replaceLogic(log, rule)
        case cond: Condition => replaceCondition(cond, rule)
        case code: Code => replaceCode(code, rule)
        case elem: GameElement => replaceGameElement(elem, rule)
        case oper: GameOperation => replaceGameOperation(oper, rule)
        case cons: Concept => replaceConcept(cons, rule)
        case leaf: LeafExp => replace(leaf, rule)
        case _ => exp
      }
    }

    def replace(leafExp: LeafExp, rule: Replace): Exp =
      rule.get(leafExp) match {
        case Some(exp) => exp
        case None => leafExp
      }

    def replaceArithmetic(arithmetic: Arithmetic, rule: Replace): Arithmetic = {
      arithmetic match {
        case a: Addition => Addition(arithmetic.args map (e => replace(e, rule)))
        case s: Subtraction => Subtraction(arithmetic.args map (e => replace(e, rule)))
        case m: Multiplication => Multiplication(arithmetic.args map (e => replace(e, rule)))
        case d: Division => Division(arithmetic.args map (e => replace(e, rule)))
        case o: Modulo => Modulo(arithmetic.args map (e => replace(e, rule)))
      }
    }

    def replaceLogic(logic: Logic, rule: Replace): Logic = {
      logic match {
        case not: Not => Not(not.args map (e => replace(e, rule)))
        case and: And => And(and.args map (e => replace(e, rule)))
        case or: Or => Or(or.args map (e => replace(e, rule)))
        case eq: Eq => Eq(eq.args map (e => replace(e, rule)))
      }
    }

    def replaceCondition(condition: Condition, rule: Replace): Condition = {
      condition match {
        case gt: Greater => Greater(replaceEvalNumber(gt.arg1, rule), replaceEvalNumber(gt.arg2, rule))
        case lt: Less => Less(replaceEvalNumber(lt.arg1, rule), replaceEvalNumber(lt.arg2, rule))
        case eq: Equals => Equals(replace(eq.arg1, rule), replace(eq.arg2, rule))
      }
    }

    def replaceEvalNumber(evalNumber: EvalNumber, rule: Replace): EvalNumber = {
      evalNumber match {
        case leaf: LeafExp => replace(leaf, rule).asInstanceOf[EvalNumber]
        case ari: Arithmetic => replaceArithmetic(ari, rule)
      }
    }

    def replaceCode(code: Code, rule: Replace): Code = {
      code match {
        case lET: Let => null
        case bODY: Body => null
        case iF: If => null
        case fOR: For => null
        case sET: Set => null
      }
    }

    def replaceGameElement(gameElement: GameElement, rule: Replace): GameElement = {
      gameElement match {
        case piece: Piece => Piece(piece.args map (e => replace(e, rule)))
        case position: Position =>
          position.role match {
            case Some(r) => Position(position.position map (p => replaceEvalPiece(p, rule)),
              Some(replaceGameElement(r, rule).asInstanceOf[Role]))
            case None => Position(position.position map (p => replaceEvalPiece(p, rule)), None)
          }
        case role: Role => Role(replace(role.role, rule))
        case message: Message => null
      }
    }

    def replaceEvalPiece(evalPiece: EvalPiece, rule: Replace): EvalPiece = {
      evalPiece match {
        case piece: Piece => Piece(piece.args map (e => replace(e, rule)))
        case leaf: LeafExp => replace(leaf, rule).asInstanceOf[EvalPiece]
        case arg: Arg => replaceGameOperation(arg, rule).asInstanceOf[Arg]
      }
    }

    def replaceGameOperation(gameOperation: GameOperation, rule: Replace): GameOperation = {
      gameOperation match {
        case arg: Arg => Arg(replaceEvalInteger(arg.index, rule),
          replaceEvalGameElement(arg.gameElement, rule))
        case getRole: GetRole => null
        case size: Size => null
        case unificate: Unificate => Unificate(replaceEvalPiece(unificate.piece1, rule),
          replaceEvalPiece(unificate.piece2, rule))
      }
    }

    def replaceEvalGameElement(evalGameElement: EvalGameElement, rule: Replace): EvalGameElement = {
      evalGameElement match {
        case elem: GameElement => replaceGameElement(elem, rule)
        case leaf: LeafExp => replace(leaf, rule).asInstanceOf[EvalGameElement]
      }
    }

    def replaceEvalInteger(evalInteger: EvalInteger, rule: Replace): EvalInteger = {
      evalInteger match {
        case i: Integer => replace(i, rule).asInstanceOf[EvalInteger]
        case label: Label => replace(label, rule).asInstanceOf[EvalInteger]
      }
    }

    def replaceConcept(cons: Concept, rule: Replace): Concept = {
      new Concept(replaceArgs(cons.args, rule), replace(cons.body, rule))
    }
    
    def replaceArgs(args: List[TypedLabel], rule: Replace): List[TypedLabel] = {
      args map (a => TypedLabel(replace(a.label, rule).asInstanceOf[Label], a.t))
    }
  }

  object Formatter {

    def format(concept: Concept): Concept = {
      val formatter = formatterMaker("x")
      val ignore = ignoreLabel(concept.args)
      Concept(concept.args, formatExp(concept.body, formatter, ignore))
    }

    def formatExp(exp: Exp, formatter: Fom, ignore: Ignore): Exp = {
      exp match {
        case ari: Arithmetic => formatArithmetic(ari, formatter, ignore)
        case log: Logic => formatLogic(log, formatter, ignore)
        case cond: Condition => formatCondition(cond, formatter, ignore)
        case code: Code => formatCode(code, formatter, ignore)
        case elem: GameElement => formatGameElement(elem, formatter, ignore);
        case oper: GameOperation => formatGameOperation(oper, formatter, ignore);
        case label: Label => formatLabel(label, formatter, ignore)
        case _ => exp
      }
    }

    def formatLabel(label: Label, formatter: Fom, ignore: Ignore): Label = if (ignore(label)) label else formatter(label)

    def formatArithmetic(arithmetic: Arithmetic, formatter: Fom, ignore: Ignore): Arithmetic = {
      arithmetic match {
        case a: Addition => Addition(arithmetic.args map (e => formatExp(e, formatter, ignore)))
        case s: Subtraction => Subtraction(arithmetic.args map (e => formatExp(e, formatter, ignore)))
        case m: Multiplication => Multiplication(arithmetic.args map (e => formatExp(e, formatter, ignore)))
        case d: Division => Division(arithmetic.args map (e => formatExp(e, formatter, ignore)))
        case o: Modulo => Modulo(arithmetic.args map (e => formatExp(e, formatter, ignore)))
      }
    }

    def formatLogic(logic: Logic, formatter: Fom, ignore: Ignore): Logic = {
      logic match {
        case not: Not => Not(not.args map (e => formatExp(e, formatter, ignore)))
        case and: And => And(and.args map (e => formatExp(e, formatter, ignore)))
        case or: Or => Or(or.args map (e => formatExp(e, formatter, ignore)))
        case eq: Eq => Eq(eq.args map (e => formatExp(e, formatter, ignore)))
      }
    }

    def formatCondition(condition: Condition, formatter: Fom, ignore: Ignore): Condition = {
      condition match {
        case gt: Greater => Greater(formatEvalNumber(gt.arg1, formatter, ignore), formatEvalNumber(gt.arg2, formatter, ignore))
        case lt: Less => Less(formatEvalNumber(lt.arg1, formatter, ignore), formatEvalNumber(lt.arg2, formatter, ignore))
        case eq: Equals => Equals(formatExp(eq.arg1, formatter, ignore), formatExp(eq.arg2, formatter, ignore))
      }
    }

    def formatEvalNumber(evalNumber: EvalNumber, formatter: Fom, ignore: Ignore): EvalNumber = {
      evalNumber match {
        case n: Number => n
        case l: Label => formatLabel(l, formatter, ignore)
        case ari: Arithmetic => formatArithmetic(ari, formatter, ignore)
      }
    }

    def formatCode(code: Code, formatter: Fom, ignore: Ignore): Code = {
      code match {
        case lET: Let => null
        case bODY: Body => null
        case iF: If => null
        case fOR: For => null
        case sET: Set => null
      }
    }

    def formatGameElement(gameElement: GameElement, formatter: Fom, ignore: Ignore): GameElement = {
      gameElement match {
        case piece: Piece => Piece(piece.args map (e => formatExp(e, formatter, ignore)))
        case position: Position =>
          position.role match {
            case Some(r) => Position(position.position map (p => formatEvalPiece(p, formatter, ignore)),
              Some(formatGameElement(r, formatter, ignore).asInstanceOf[Role]))
            case None => Position(position.position map (p => formatEvalPiece(p, formatter, ignore)), None)
          }
        case role: Role => Role(formatExp(role.role, formatter, ignore))
        case message: Message => null
      }
    }

    def formatEvalPiece(evalPiece: EvalPiece, formatter: Fom, ignore: Ignore): EvalPiece = {
      evalPiece match {
        case piece: Piece => Piece(piece.args map (e => formatExp(e, formatter, ignore)))
        case label: Label => formatLabel(label, formatter, ignore)
        case arg: Arg => formatGameOperation(arg, formatter, ignore).asInstanceOf[Arg]
      }
    }

    def formatGameOperation(gameOperation: GameOperation, formatter: Fom, ignore: Ignore): GameOperation = {
      gameOperation match {
        case arg: Arg => Arg(formatEvalInteger(arg.index, formatter, ignore),
          formatEvalGameElement(arg.gameElement, formatter, ignore))
        case getRole: GetRole => GetRole(formatEvalPosition(getRole.position, formatter, ignore))
        case size: Size => Size(formatEvalGameElement(size.gameElement, formatter, ignore))
        case unificate: Unificate => Unificate(formatEvalPiece(unificate.piece1, formatter, ignore),
          formatEvalPiece(unificate.piece2, formatter, ignore))
      }
    }
    
    def formatEvalPosition(evalPosition: EvalPosition, formatter: Fom, ignore: Ignore): EvalPosition = {
      evalPosition match {
        case position : Position => formatGameElement(position, formatter, ignore).asInstanceOf[EvalPosition]
        case label : Label => formatLabel(label, formatter, ignore).asInstanceOf[EvalPosition]
      }
    }

    def formatEvalGameElement(evalGameElement: EvalGameElement, formatter: Fom, ignore: Ignore): EvalGameElement = {
      evalGameElement match {
        case elem: GameElement => formatGameElement(elem, formatter, ignore)
        case label: Label => formatLabel(label, formatter, ignore)
      }
    }

    def formatEvalInteger(evalInteger: EvalInteger, formatter: Fom, ignore: Ignore): EvalInteger = {
      evalInteger match {
        case i: Integer => i
        case label: Label => formatLabel(label, formatter, ignore)
      }
    }

    type Fom = Label => Label

    def formatterMaker(label: String): Fom = {
      val labeler = labelMaker(label)
      var replaceRule = Map.empty[Label, Label]
      (label: Label) => {
        replaceRule.get(label) match {
          case Some(l) => l
          case None =>
            val newLabel = labeler()
            replaceRule += label -> newLabel
            newLabel
        }
      }
    }

    def labelMaker(surfix: String): Unit => Label = {
      var i = -1
      Unit => {
        i += 1
        Label(surfix + i)
      }
    }

    type Ignore = Label => Boolean

    def ignoreLabel(labels: List[TypedLabel]): Ignore = {
      ignoreLabels(labels map (_.label))
    }

    def ignoreLabels(labels: List[Label]): Ignore = {
      (label: Label) => labels.contains(label)
    }

  }

  def relabel(piece: Piece, variableName: String): Piece = {
    var i = -1

    def replaceLabel(exp: Exp): Exp = {
      exp match {
        case label: Label =>
          i += 1
          Label(variableName + i)
        case _ => exp
      }
    }

    Piece(piece.args map replaceLabel)
  }

  def relabelWithout(label: Label, piece: Piece, variableName: String): Piece = {
    var i = -1

    def replaceLabel(exp: Exp): Exp = {
      exp match {
        case l: Label if (label != l) =>
          i += 1
          Label(variableName + i)
        case _ => exp
      }
    }

    Piece(piece.args map replaceLabel)
  }

  object ArityCounter {

    def count(concept: Concept): Int = {
      val countMax = maxCounter()

      concept.body match {
        case l: Logic => countLogic(l, countMax)
        case c: Condition => countCondition(c, countMax)
      }
      countMax(Int.MinValue)
    }

    def count(position: Position): Int = {
      val countMax = maxCounter()
      position.position foreach { p => countPiece(p, countMax) }
      countMax(Int.MinValue)
    }

    def count(piece: EvalPiece): Int = {
      val countMax = maxCounter()
      countPiece(piece, countMax)
      countMax(Int.MinValue)
    }

    def maxCounter(): Int => Int = {
      var i = 0
      (j: Int) => {
        if (i < j) {
          i = j; j
        } else i
      }
    }

    def countLogic(logic: Logic, counter: Int => Int): Int => Int = {
      logic.args foreach {
        l =>
          l match {
            case piece: EvalPiece => countPiece(piece, counter)
            case condition: Condition => countCondition(condition, counter)
            case _ =>
          }
      }
      counter
    }

    def countCondition(condition: Condition, counter: Int => Int): Int => Int = {
      condition match {
        case greater: Greater => counter
        case less: Less => counter
        case eq: Equals =>
          eq.arg1 match {
            case evalPiece: EvalPiece => countPiece(evalPiece, counter)
            case _ =>
          }
          eq.arg2 match {
            case evalPiece: EvalPiece => countPiece(evalPiece, counter)
            case _ =>
          }
      }
      counter
    }

    def countUnificate(unificate: Unificate, counter: Int => Int): Int => Int = countPiece(unificate.piece2, countPiece(unificate.piece1, counter))

    def countPiece(piece: EvalPiece, counter: Int => Int): Int => Int = {
      piece match {
        case p: Piece => counter
        case arg: Arg => countArg(arg, counter)
        case _ => counter
      }
    }

    def countArg(arg: Arg, counter: Int => Int): Int => Int = {
      arg.index match {
        case i: Integer => counter(i.value)
        case _ => counter(Int.MaxValue)
      }
      counter
    }
  }
}