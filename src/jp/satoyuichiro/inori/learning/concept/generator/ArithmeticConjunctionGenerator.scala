package jp.satoyuichiro.inori.learning.concept.generator

import jp.satoyuichiro.inori.cdl._

class ArithmeticConjunctionGenerator extends Generator {

  val labelMaker = GeneratorUtil.Labeler.makeLabeler("arithm")

  def generate(position: Position): Option[PrimeConjunction] = {
    val i = new Interpreter()
    val evaluated = i.interpret(position).asInstanceOf[Position]
    val conjunctionGenerator = new ConjunctionGenerator
    try {
      val replaced = conjunctionGenerator.generate(evaluated.position map (_.asInstanceOf[Piece]), evaluated.role).toConcept
      Some(generate(replaced.body.asInstanceOf[And], evaluated.position map (_.asInstanceOf[Piece]), evaluated.role))
    } catch {
      case e : Throwable => None
    }
  }

  def generate(and: And, original: List[Piece], role: Option[Role]): PrimeConjunction = {
    val arrayPieces = toArrayPieces(and)
    val arrayOriginal = toArrayPieces(original)
    role match {
      case Some(role) => makePrimeConjunctionWithRole(arrayToPiece(makeArithmetic(arrayPieces, arrayOriginal)))
      case None => makePrimeConjunction(arrayToPiece(makeArithmetic(arrayPieces, arrayOriginal)))
    }
  }

  def toArrayPieces(and: And): Array[Array[Exp]] = {
    val head = and.args.head.asInstanceOf[Unificate].piece1.asInstanceOf[Piece]
    val tail = and.args.tail map (_.asInstanceOf[Equals]) filter (_.arg1.isInstanceOf[Piece]) map (_.arg1.asInstanceOf[Piece])
    val pieces = head :: tail
    (pieces map (_.args.toArray)).toArray
  }
  
  def toArrayPieces(pieces: List[Piece]): Array[Array[Exp]] = (pieces map (_.args.toArray)).toArray

  def makeArithmetic(pieces: Array[Array[Exp]], original: Array[Array[Exp]]): Array[Array[Exp]] = {
    var result = Array.empty[Array[Exp]]
    
    for (i <- 0 to pieces(0).length - 1) {
      var column = List.empty[Exp]
      var originalColumn = List.empty[Exp]
      for (j <- 0 to pieces.length - 1) {
        column ::= pieces(j)(i)
        originalColumn ::= original(j)(i)
      }
      column = column.reverse
      originalColumn = originalColumn.reverse
      
      if (isIntegerColumn(originalColumn) && isDifferentIntegers(originalColumn))
        result = addArithmetic(originalColumn map (_.asInstanceOf[Integer]), result)
      else
        result = addColumn(column, result)
    }
    result
  }

  def isIntegerColumn(column: List[Exp]): Boolean = (column map (_.isInstanceOf[Integer])).fold(true)(_ && _)
  
  def isDifferentIntegers(column: List[Exp]): Boolean = (column map (c => column.count(_ == c)) map (_ == 1)).fold(true)(_ && _)

  def addArithmetic(column: List[Integer], result: Array[Array[Exp]]): Array[Array[Exp]] = addColumn(columnToArithmetic(column), result)

  def columnToArithmetic(column: List[Integer]): List[Exp] = {
    val label = labelMaker()
    val origin = column.head
    label :: (column.tail map (c => Addition(label, Integer(c.value - origin.value))))
  }

  def addColumn(column: List[Exp], result: Array[Array[Exp]]): Array[Array[Exp]] = {
    if (result.length == 0) {
      (column map (c => Array(c))).toArray
    } else {
      var newResult = Array.empty[Array[Exp]]
      val arrayColumn: Array[Array[Exp]] = (column map (c => Array(c))).toArray
      for (i <- 0 to result.length - 1) {
        val array: Array[Exp] = result(i)
        val concated = Array.concat(array, arrayColumn(i))
        newResult = Array.concat(newResult, Array(concated))
      }
      newResult
    }
  }

  def arrayToPiece(array: Array[Array[Exp]]): List[Piece] = array.toList.map(a => Piece(a.toList))

  def makePattern(pieces: List[Piece]): Concept = { 
    val index = GeneratorUtil.Labeler.indexMaker(0)
    val pos = Label("pos")

    val bodyHead = Unificate(pieces.head, Arg(index(), pos))
    val bodyTail = pieces.tail map (p => Equals(p, Arg(index(), pos)))
    val before = Concept(List(TypedLabel(pos, PositionType())), And(bodyHead :: bodyTail))
    val after = GeneratorUtil.Formatter.format(before)
    after
  }

  def makePatternWithRole(pieces: List[Piece]): Concept = {
    val index = GeneratorUtil.Labeler.indexMaker(0)
    val pos = Label("pos")

    val bodyHead = Unificate(pieces.head, Arg(index(), pos))
    val getRole = Equals(Role(Label("role-val")), GetRole(pos))
    val bodyTail = pieces.tail map (p => Equals(p, Arg(index(), pos)))
    val before = Concept(List(TypedLabel(pos, PositionType())), And(bodyHead :: getRole :: bodyTail))
    val after = GeneratorUtil.Formatter.format(before)
    after
  }
  
  def makePrimeConjunction(pieces: List[Piece]): PrimeConjunction = {
    val formatted = makePattern(pieces).body.asInstanceOf[And]
    val bodyHead = formatted.args.head.asInstanceOf[Unificate].piece1.asInstanceOf[Piece]
    val bodyTail = formatted.args.tail map (_.asInstanceOf[Equals]) map (_.arg1.asInstanceOf[Piece])
    PrimeConjunction(bodyHead :: bodyTail)
  }
  
  def makePrimeConjunctionWithRole(pieces: List[Piece]): PrimeConjunction = {
    val formatted = makePatternWithRole(pieces).body.asInstanceOf[And]
    val bodyHead = formatted.args.head.asInstanceOf[Unificate].piece1.asInstanceOf[Piece]
    val getRole = formatted.args.tail.head.asInstanceOf[Equals].arg1.asInstanceOf[Role]
    val bodyTail = formatted.args.tail.tail map (_.asInstanceOf[Equals]) map (_.arg1.asInstanceOf[Piece])
    PrimeConjunction(bodyHead :: bodyTail, getRole)
  }
}