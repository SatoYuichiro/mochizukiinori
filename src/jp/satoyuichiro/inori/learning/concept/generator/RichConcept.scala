package jp.satoyuichiro.inori.learning.concept.generator

import jp.satoyuichiro.inori.cdl._

abstract class RichConcept(val conceptName: Option[Label]) extends Serializable {
  val positionLabel = Label("pos")
  val argument = List(TypedLabel(positionLabel, PositionType()))
  val callArgument = List(TypedExp(positionLabel, PositionType()))

  def toConcept: Concept
  def putLabel(label: Label): RichConcept
  def load(interpreter: Interpreter): Interpreter

  def isLabeled: Boolean = {
    conceptName match {
      case Some(l) => true
      case None => false
    }
  }

  def toDefineConcept: Option[DefineConcept] = {
    conceptName match {
      case Some(label) => Some(DefineConcept(label, toConcept))
      case None => None
    }
  }
  
}

abstract class RichPattern(val arity: Int, val role: Option[Role], override val conceptName: Option[Label]) extends RichConcept(conceptName)

abstract class PrimeConcept(val pieces: List[Piece], override val arity: Int, override val role: Option[Role],
  override val conceptName: Option[Label]) extends RichPattern(arity, role, conceptName) {

  def load(interpreter: Interpreter): Interpreter = {
    toDefineConcept match {
      case Some(defineConcept) => interpreter.interpret(defineConcept)
      case None =>
    }
    interpreter
  }
  
  def makeHead: Unificate = Unificate(pieces.head, Arg(Integer(0), positionLabel))
  def makeTail: List[Equals] = {
    val indexMaker = GeneratorUtil.Labeler.indexMaker(1)
    pieces.tail map (p => Equals(p, Arg(indexMaker(), positionLabel)))
  }
  def makeRole: Equals = Equals(role.get, GetRole(positionLabel))
}

case class PrimeConjunction(override val pieces: List[Piece], override val arity: Int, override val role: Option[Role],
  override val conceptName: Option[Label]) extends PrimeConcept(pieces, arity, role, conceptName) with Serializable {

  def toConcept: Concept = {
    role match {
      case Some(r) => Concept(argument, And(makeHead :: makeRole :: makeTail))
      case None => Concept(argument, And(makeHead :: makeTail))
    }
  }

  def putLabel(label: Label): PrimeConjunction = PrimeConjunction(pieces, arity, role, Some(label))
}

object PrimeConjunction {
  def apply(pieces: List[Piece]): PrimeConjunction = PrimeConjunction(pieces, pieces.size, None, None)
  def apply(pieces: List[Piece], role: Role): PrimeConjunction = PrimeConjunction(pieces, pieces.size, Some(role), None)
}

case class PrimeDisjunction(override val pieces: List[Piece], override val arity: Int, override val role: Option[Role],
  override val conceptName: Option[Label]) extends PrimeConcept(pieces, arity, role, conceptName) with Serializable {

  def toConcept: Concept = {
    role match {
      case Some(r) => Concept(argument, Or(makeHead :: makeRole :: makeTail))
      case None => Concept(argument, Or(makeHead :: makeTail))
    }
  }

  def putLabel(label: Label): PrimeDisjunction = PrimeDisjunction(pieces, arity, role, Some(label))
}

object PrimeDisjunction {
  def apply(pieces: List[Piece]): PrimeDisjunction = PrimeDisjunction(pieces, pieces.size, None, None)
  def apply(pieces: List[Piece], role: Role): PrimeDisjunction = PrimeDisjunction(pieces, pieces.size, Some(role), None)
}

abstract class CompositeConcept(val primes: List[PrimeConcept], override val arity: Int, override val conceptName: Option[Label])
  extends RichPattern(arity, None, conceptName) {
  val defaultName = Label("defalut-concept-name")
  
  def load(interpreter: Interpreter): Interpreter = {
    toDefineConcept match {
      case Some(defineConcept) =>
        primes foreach { p => p.load(interpreter) }
        interpreter.interpret(defineConcept)
      case None =>
    }
    interpreter
  }

  def makeArgument(prime: PrimeConcept): ConceptCall = ConceptCall(prime.conceptName.getOrElse(defaultName), callArgument)
}

case class CompositeConjunction(override val primes: List[PrimeConcept], override val arity: Int, override val conceptName: Option[Label])
  extends CompositeConcept(primes, arity, conceptName) with Serializable {

  def toConcept: Concept = Concept(argument, And(primes map makeArgument))

  def putLabel(label: Label): CompositeConjunction = CompositeConjunction(primes, arity, Some(label))
}

object CompositeConjunction {
  def apply(primes: List[PrimeConcept]): CompositeConjunction = CompositeConjunction(primes, (primes map (_.arity)).max, None)
}

case class CompositeDisjunction(override val primes: List[PrimeConcept], override val arity: Int, override val conceptName: Option[Label])
  extends CompositeConcept(primes, arity, conceptName) with Serializable {

  def toConcept: Concept = Concept(argument, Or(primes map makeArgument))

  def putLabel(label: Label): CompositeDisjunction = CompositeDisjunction(primes, arity, Some(label))
}

object CompositeDisjunction {
  def apply(primes: List[PrimeConcept]): CompositeDisjunction = CompositeDisjunction(primes, (primes map (_.arity)).max, None)
}

case class PrimeRecursive(prime: PrimeConcept, override val conceptName: Option[Label]) extends RichConcept(conceptName) with Serializable {
  val indexLabel = Label("i")
  val resultLabel = Label("result")

  def toConcept: Concept = {
    Concept(argument, makeFor(prime.conceptName.get, positionLabel, prime.arity))
  }

  def makeFor(conceptLabel: Label, positionLabel: Label, arity: Int): For = {
    For(makeIndex(indexLabel, resultLabel),
      makeCondition(indexLabel, positionLabel, arity),
      makeIncriment(indexLabel),
      makeBody(indexLabel, resultLabel, positionLabel, conceptLabel, arity))
  }

  def makeIndex(index: Label, resultLabel: Label): List[Substitution] = List(Substitution(index, Integer(0)), Substitution(resultLabel, Bool(true)))

  def makeCondition(index: Label, positionLabel: Label, arity: Int): Condition = Greater(index, Subtraction(Size(positionLabel), Integer(arity - 1)))

  def makeIncriment(index: Label): Body = Body(Set(index, Addition(index, Integer(1))))

  def makeBody(index: Label, resultLabel: Label, positionLabel: Label, conceptLabel: Label, arity: Int): Body = {
    val args = (0 to (arity - 1)).toList map (i => Arg(Addition(Integer(i), index), positionLabel))
    Body(Set(resultLabel, And(resultLabel, ConceptCall(conceptLabel, List(TypedExp(Position(args, None), PositionType()))))), resultLabel)
  }

  def putLabel(label: Label): PrimeRecursive = PrimeRecursive(prime, Some(label))
  
  def load(interpreter: Interpreter): Interpreter = {
    toDefineConcept match {
      case Some(defineConcept) => interpreter.interpret(defineConcept)
      case None =>
    }
    interpreter
  }
}

object PrimeRecursive {
  def apply(prime: PrimeConcept): PrimeRecursive = PrimeRecursive(prime, None)
}

case class PrimeHeuristics(concept: RichPattern, evaluation: Double, heuristicsName: Option[Label])
  extends RichConcept(heuristicsName) with Serializable {

  def toConcept: Concept = {
    Concept(argument,
      If(ConceptCall(concept.conceptName.get, callArgument),
        Body(Real(evaluation)),
        Body(Real(0.0))))
  }

  def putLabel(label: Label): PrimeHeuristics = PrimeHeuristics(concept, evaluation, Some(label))
  
  def toQuery(position: Position): ConceptCall = ConceptCall(heuristicsName.get, List(TypedExp(position, PositionType())))
  
  def load(interpreter: Interpreter): Interpreter = {
    toDefineConcept match {
      case Some(defineConcept) =>
        concept.load(interpreter)
        interpreter.interpret(defineConcept)
      case None =>
    }
    interpreter
  }
}

object PrimeHeuristics {
  def apply(pattern: RichPattern, evaluation: Double): PrimeHeuristics = PrimeHeuristics(pattern, evaluation, None)
}

case class CompositeHeuristics(primeHeuristics: List[PrimeHeuristics]) extends RichConcept(None) with Serializable {

  def toConcept: Concept = null

  def labelAll: CompositeHeuristics = {
    val labeler = GeneratorUtil.Labeler.makeLabeler("heuristics")
    CompositeHeuristics(primeHeuristics map (_.putLabel(labeler())))
  }

  def load(interpreter: Interpreter): Interpreter = {
    primeHeuristics foreach {
      h => h.load(interpreter)
    }
    interpreter
  }

  def putLabel(label: Label): CompositeHeuristics = this
  
  def getPrimeLabels: List[Label] = primeHeuristics map (_.heuristicsName.get)
}