package jp.satoyuichiro.inori.learning.concept.generator

import jp.satoyuichiro.inori.cdl._

class HeuristicsGenerator {

  def generate(primeConcepts: List[RichPattern], positions: List[Position]): List[PrimeHeuristics] = {
    val prime = primeConcepts filter (_.isInstanceOf[PrimeConcept]) map (_.asInstanceOf[PrimeConcept])
    val comp = primeConcepts filter (_.isInstanceOf[CompositeConcept]) map (_.asInstanceOf[CompositeConcept])
    primeConceptHeuristics(prime, positions) ++ compositeComceptHeuristics(comp, positions)
  }

  def primeConceptHeuristics(primeConcepts: List[PrimeConcept], positions: List[Position]): List[PrimeHeuristics] = {
    val binaryConcepts = primeConcepts filter (_.arity == 2)
    val trinaryConcepts = primeConcepts filter (_.arity == 3)

    val binaryEvaluation = binaryConcepts map (b => (b, makeBinaryEvaluation(b, positions))) filter (_._2 != 0.0)
    //    binaryEvaluation foreach { p => println(p._1.toLisp + " " + p._2) }
    val trinaryEvaluation = trinaryConcepts map (b => (b, makeTrinaryEvaluation(b, positions))) filter (_._2 != 0.0)
    //    trinaryEvaluation foreach { p => println(p._1.toLisp + " " + p._2) }
    val hoge = (binaryEvaluation map (b => makeHeuristics(b._1, b._2)))
    (binaryEvaluation map (b => makeHeuristics(b._1, b._2))) ++ (trinaryEvaluation map (t => makeHeuristics(t._1, t._2)))
  }

  def compositeComceptHeuristics(compositeConcepts: List[CompositeConcept], positions: List[Position]): List[PrimeHeuristics] = {
    val binaryConcepts = compositeConcepts filter (_.arity == 2)
    val trinaryConcepts = compositeConcepts filter (_.arity == 3)

    val binaryEvaluation = binaryConcepts map (b => (b, makeCompositeBinaryEvaluation(b, positions))) filter (_._2 != 0.0)
    //    binaryEvaluation foreach { p => println(p._1.toLisp + " " + p._2) }
    val trinaryEvaluation = trinaryConcepts map (b => (b, makeCompositeTrinaryEvaluation(b, positions))) filter (_._2 != 0.0)
    //    trinaryEvaluation foreach { p => println(p._1.toLisp + " " + p._2) }
    val hoge = (binaryEvaluation map (b => makeHeuristics(b._1, b._2)))
    (binaryEvaluation map (b => makeHeuristics(b._1, b._2))) ++ (trinaryEvaluation map (t => makeHeuristics(t._1, t._2)))
  }

  def makeBinaryPosition(position: Position): List[Position] = {
    var result = List.empty[Position]
    val arrayPieces = position.position.toArray
    for (i <- 0 to arrayPieces.length - 2) {
      result ::= Position(List(arrayPieces(i), arrayPieces(i + 1)), None)
    }
    result
  }

  def makeBinaryEvaluation(concept: RichPattern, positions: List[Position]): Double = {
    val i = new Interpreter
    i.interpret(concept.toDefineConcept.get)
    var count = 0
    var eval = 0
    for (position <- positions) {
      val pairs = makeBinaryPosition(position)
      try {
        val tem = pairs map (p => i.interpret(ConceptCall(concept.conceptName.get, List(TypedExp(p, PositionType())))))
        val ev = (tem filter (_.isInstanceOf[Bool]) filter (_ == Bool(true))).size
        count += pairs.size
        eval += ev
      } catch {
        case _: Throwable => return 0.0
      }
    }
    eval.toDouble / count.toDouble
  }

  def makeTrinaryPosition(position: Position): List[Position] = {
    var result = List.empty[Position]
    val arrayPieces = position.position.toArray
    for (i <- 0 to arrayPieces.length - 3) {
      result ::= Position(List(arrayPieces(i), arrayPieces(i + 1), arrayPieces(i + 2)), None)
    }
    result
  }

  def makeTrinaryEvaluation(concept: RichPattern, positions: List[Position]): Double = {
    val i = new Interpreter
    i.interpret(concept.toDefineConcept.get)
    var count = 0
    var eval = 0
    for (position <- positions) {
      val pairs = makeTrinaryPosition(position)
      try {
        val tem = pairs map (p => i.interpret(ConceptCall(concept.conceptName.get, List(TypedExp(p, PositionType())))))
        val ev = (tem filter (_.isInstanceOf[Bool]) filter (_ == Bool(true))).size
        count += pairs.size
        eval += ev
      } catch {
        case _: Throwable => return 0.0
      }
    }
    eval.toDouble / count.toDouble
  }

  def makeCompositeBinaryEvaluation(concept: CompositeConcept, positions: List[Position]): Double = {
    val i = new Interpreter
    concept.primes foreach {
      c => i.interpret(c.toDefineConcept.get)
    }
    i.interpret(concept.toDefineConcept.get)
    var count = 0
    var eval = 0
    for (position <- positions) {
      val pairs = makeBinaryPosition(position)
      try {
        val tem = pairs map (p => i.interpret(ConceptCall(concept.conceptName.get, List(TypedExp(p, PositionType())))))
        val ev = (tem filter (_.isInstanceOf[Bool]) filter (_ == Bool(true))).size
        count += pairs.size
        eval += ev
      } catch {
        case _: Throwable => return 0.0
      }
    }
    eval.toDouble / count.toDouble
  }

  def makeCompositeTrinaryEvaluation(concept: CompositeConcept, positions: List[Position]): Double = {
    val i = new Interpreter
    concept.primes foreach {
      c => i.interpret(c.toDefineConcept.get)
    }
    i.interpret(concept.toDefineConcept.get)
    var count = 0
    var eval = 0
    for (position <- positions) {
      val pairs = makeTrinaryPosition(position)
      try {
        val tem = pairs map (p => i.interpret(ConceptCall(concept.conceptName.get, List(TypedExp(p, PositionType())))))
        val ev = (tem filter (_.isInstanceOf[Bool]) filter (_ == Bool(true))).size
        count += pairs.size
        eval += ev
      } catch {
        case _: Throwable => return 0.0
      }
    }
    eval.toDouble / count.toDouble
  }

  def makeHeuristics(richPattern: RichPattern, evaluation: Double): PrimeHeuristics = PrimeHeuristics(richPattern, evaluation)
}