package jp.satoyuichiro.inori.learning.concept.generator

import jp.satoyuichiro.inori.cdl._
import jp.satoyuichiro.inori.learning.concept.generator.GeneratorUtil.Manipulator._

class ConjunctionGenerator {

  def generate(position: Position): PrimeConjunction = {
    val i = new Interpreter()
    val evaluated = i.interpret(position).asInstanceOf[Position]
    generate(evaluated.position map (_.asInstanceOf[Piece]), evaluated.role)
  }

  def generate(pieces: List[Piece], role: Option[Role]): PrimeConjunction = {
    role match {
      case Some(role) => makePrimeConjunctionWithRole(replaceSameElementToVariableWithRole(pieces, role), pieces, role)
      case None => makePrimeConjunction(replaceSameElementToVariable(pieces))
    }
  }

  def replaceSameElementToVariableWithRole(pieces: List[Piece], role: Role): And = {
    val roleReplaced = replaceRoleToVariable(pieces, role)
    for (replace <- makeReplaceRule(roleReplaced.head)) {
      val i = new Interpreter()
      val replaced = roleReplaced map (p => GeneratorUtil.Manipulator.replace(p, replace).asInstanceOf[Piece])
      val query = makeQueryList(pieces, replaced)
      if (i.interpret(query) == Bool(true) && !hasUnusedUnification(replaced, pieces)) return query
    }
    And(List.empty[Exp])
  }

  def replaceRoleToVariable(pieces: List[Piece], role: Role): List[Piece] = {
    val rule = Map(role.role.asInstanceOf[LeafExp] -> Label("role-val"))
    pieces map (p => GeneratorUtil.Manipulator.replace(p, rule).asInstanceOf[Piece])
  }

  def replaceSameElementToVariable(pieces: List[Piece]): And = {
    for (replace <- makeReplaceRule(pieces.head)) {
      val i = new Interpreter()
      val replaced = pieces map (p => GeneratorUtil.Manipulator.replace(p, replace).asInstanceOf[Piece])
      val query = makeQueryList(pieces, replaced)
      if (i.interpret(query) == Bool(true) && !hasUnusedUnification(replaced, pieces)) return query
    }
    And(List.empty[Exp])
  }

  def makeReplaceRule(piece: Piece): List[Replace] = {
    (1 to piece.size).map(i => makerCaller(piece.args, i, List.empty[Replace]))
      .fold(List.empty[Replace])(_ ++ _)
      .sortWith((a, b) => a.size > b.size)
  }

  def makerCaller(exps: List[Exp], n: Int, result: List[Replace]): List[Replace] = {
    val labeler = GeneratorUtil.Labeler.makeLabeler("x")
    val labels = (0 to (if (n < exps.size) n else exps.size)) map (i => labeler())
    recursiveMaker(exps, labels.toList, List.empty[Replace])
  }

  def recursiveMaker(exps: List[Exp], labels: List[Label], result: List[Replace]): List[Replace] = {
    if (labels.isEmpty) {
      result.reverse
    } else if (GeneratorUtil.Labeler.countNotLabel(exps) <= labels.size) {
      val m = replaceAll(exps, labels)
      if (result.isEmpty) List(m) else result.map(r => r ++ m).reverse
    } else {
      if (!exps.head.isInstanceOf[Label]) {
        val replace = exps.head.asInstanceOf[LeafExp] -> labels.head
        val ls1 = recursiveMaker(exps.tail, labels.tail, result) map (r => r + replace)
        val ls2 = recursiveMaker(exps.tail, labels, result)
        ls1 ++ ls2
      } else {
        recursiveMaker(exps.tail, labels, result)
      }
    }
  }

  def replaceAll(exps: List[Exp], labels: List[Label]): Replace = {
    val builder = Map.newBuilder[LeafExp, Label]
    (exps.filter(e => !e.isInstanceOf[Label]) zip labels) foreach {
      el =>
        if (el._1.isInstanceOf[LeafExp] && !el._1.isInstanceOf[Label])
          builder += el._1.asInstanceOf[LeafExp] -> el._2
    }
    builder.result.asInstanceOf[Replace]
  }

  def makeQueryList(original: List[Piece], replaced: List[Piece]): And = {
    val uni = Unificate(replaced.head, original.head)
    val tail = (original zip replaced).tail map (t => Equals(t._2, t._1))
    And(uni :: tail)
  }

  def hasUnusedUnification(replaced: List[Piece], pieces: List[Piece]): Boolean = {
    var res = true
    val rules = (replaced.head.args zip pieces.head.args) filter (_._1.isInstanceOf[Label]) map (t => Map(t._1.asInstanceOf[LeafExp] -> t._2.asInstanceOf[Exp]))
    val hoge = for (rule <- rules) {
      val eq = replaced.tail map (p => GeneratorUtil.Manipulator.replace(p, rule).asInstanceOf[Piece] != p)
      res = eq.fold(res)(_ && _)
    }
    !res
  }

  def makePattern(and: And): Concept = {
    if (and == And())
      return null

    val index = GeneratorUtil.Labeler.indexMaker(0)
    val pos = Label("pos")

    val bodyHead = Unificate(and.args.head.asInstanceOf[Unificate].piece1, Arg(index(), pos))
    val bodyTail = and.args.tail map (_.asInstanceOf[Equals]) map (eq => Equals(eq.arg1, Arg(index(), pos)))
    val before = Concept(List(TypedLabel(pos, PositionType())), And(bodyHead :: bodyTail))
    val after = GeneratorUtil.Formatter.format(before)
    after
  }

  def makePatternWithRole(and: And, originalPieces: List[Piece], role: Role): Concept = {
    if (and == And())
      return null

    val index = GeneratorUtil.Labeler.indexMaker(0)
    val pos = Label("pos")

    if (GeneratorUtil.Manipulator.replace(and, Map(Label("role-val").asInstanceOf[LeafExp] -> role)) == and) {
      val bodyHead = Unificate(and.args.head.asInstanceOf[Unificate].piece1, Arg(index(), pos))
      val getRole = Equals(role, GetRole(pos))
      val bodyTail = and.args.tail map (_.asInstanceOf[Equals]) map (eq => Equals(eq.arg1, Arg(index(), pos)))
      val before = Concept(List(TypedLabel(pos, PositionType())), And(bodyHead :: getRole :: bodyTail))
      val after = GeneratorUtil.Formatter.format(before)
      after
    } else {
      val bodyHead = Unificate(and.args.head.asInstanceOf[Unificate].piece1, Arg(index(), pos))
      val getRole = Equals(Role(Label("role-val")), GetRole(pos))
      val bodyTail = and.args.tail map (_.asInstanceOf[Equals]) map (eq => Equals(eq.arg1, Arg(index(), pos)))
      val before = Concept(List(TypedLabel(pos, PositionType())), And(bodyHead :: getRole :: bodyTail))
      val after = GeneratorUtil.Formatter.format(before)
      after
    }
  }

  def makePrimeConjunction(and: And): PrimeConjunction = {
    if (and == And())
      return null

    val formatted = makePattern(and).body.asInstanceOf[And]
    val bodyHead = formatted.args.head.asInstanceOf[Unificate].piece1.asInstanceOf[Piece]
    val bodyTail = formatted.args.tail map (_.asInstanceOf[Equals]) map (_.arg1.asInstanceOf[Piece])
    PrimeConjunction(bodyHead :: bodyTail)
  }

  def makePrimeConjunctionWithRole(and: And, original: List[Piece], role: Role): PrimeConjunction = {
    if (and == And())
      return null

    val formatted = makePatternWithRole(and, original, role).body.asInstanceOf[And]
    val bodyHead = formatted.args.head.asInstanceOf[Unificate].piece1.asInstanceOf[Piece]
    val getRole = formatted.args.tail.head.asInstanceOf[Equals].arg1.asInstanceOf[Role]
    val bodyTail = formatted.args.tail.tail map (_.asInstanceOf[Equals]) map (_.arg1.asInstanceOf[Piece])
    PrimeConjunction(bodyHead :: bodyTail, getRole)
  }

}