package jp.satoyuichiro.inori.learning.concept.specifier

import jp.satoyuichiro.inori.learning.concept.generator.RichConcept
import jp.satoyuichiro.inori.cdl.Label
import jp.satoyuichiro.inori.cdl.Concept
import jp.satoyuichiro.inori.learning.concept.generator.GeneratorUtil.Manipulator._
import jp.satoyuichiro.inori.cdl.LeafExp
import jp.satoyuichiro.inori.cdl.Exp
import jp.satoyuichiro.inori.learning.concept.generator.GeneratorUtil
import jp.satoyuichiro.inori.cdl.DefineConcept
import jp.satoyuichiro.inori.learning.concept.generator._
import jp.satoyuichiro.inori.cdl.Piece
import jp.satoyuichiro.inori.player.Heuristics

class SpecifiedConcept(val concept: RichPattern, val specialization: Specialization) {

  def toConcept: Concept = {
    GeneratorUtil.Manipulator.replace(concept.toConcept, specialization.toReplase).asInstanceOf[Concept]
  }
  
  def toRichConcept: RichPattern = {
    concept match {
      case prime: PrimeConcept => replacePrimeConcept(prime)
      case comp: CompositeConcept =>
        comp match {
          case conj: CompositeConjunction => new CompositeConjunction(conj.primes map replacePrimeConcept, conj.arity, conj.conceptName)
          case disj: CompositeDisjunction => new CompositeDisjunction(disj.primes map replacePrimeConcept, disj.arity, disj.conceptName)
        }
      case _ => concept
    }
  }
  
  def replacePrimeConcept(primeConcept: PrimeConcept): PrimeConcept = {
    primeConcept match {
      case conj: PrimeConjunction => new PrimeConjunction(conj.pieces map replacePiece, conj.arity, conj.role, conj.conceptName)
      case disj: PrimeDisjunction => new PrimeDisjunction(disj.pieces map replacePiece, disj.arity, disj.role, disj.conceptName)
    }
  }
  
  def replacePiece(piece: Piece): Piece = {
    GeneratorUtil.Manipulator.replace(piece, specialization.toReplase).asInstanceOf[Piece]
  }
  
  def toDefineConcept: Option[DefineConcept] = {
    concept.toDefineConcept match {
      case Some(defineConcept) => Some(GeneratorUtil.Manipulator.replace(defineConcept, specialization.toReplase).asInstanceOf[DefineConcept])
      case None => None
    }
  }
  
  def putLabel(label: Label): SpecifiedConcept = {
    new SpecifiedConcept(concept.putLabel(label).asInstanceOf[RichPattern], specialization)
  }
}

case class SpecifiedHeuristics(val concept: RichPattern, val specialization: Specialization, val evaluation: Double) {
  
  def toHeuristics: PrimeHeuristics = {
    val specified = (new SpecifiedConcept(concept, specialization)).toRichConcept.asInstanceOf[RichPattern]
    PrimeHeuristics(specified, evaluation)
  }
}

case class Specialization(val specialization: Map[Label, LeafExp]) {
  
  def toReplase: Replace = {
    specialization.asInstanceOf[Map[LeafExp, Exp]]
  }
}

object Specialization {
  
  def apply(specializations: List[Specialization]): Specialization = {
    val builder = Map.newBuilder[Label, LeafExp]
    specializations foreach {
      specialization => builder ++= specialization.specialization
    }
    new Specialization(builder.result)
  }
}