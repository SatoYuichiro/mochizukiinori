package jp.satoyuichiro.inori.learning.concept.specifier

import jp.satoyuichiro.inori.learning.concept.generator.RichPattern
import jp.satoyuichiro.inori.cdl._
import jp.satoyuichiro.inori.learning.concept.generator.PrimeConcept
import jp.satoyuichiro.inori.learning.concept.generator.CompositeConcept

class ConceptSpecifier {

  def specify(concept: RichPattern, positiveExample: List[Position], negativeExample: List[Position]): SpecifiedConcept = {
    val realPositiveExample = filterExample(concept, positiveExample)
    val realNegativeExample = filterExample(concept, negativeExample)

    val positiveSpecializations = makeSpecialization(concept, specializationCandidate(realPositiveExample))
    makeSpecifiedConcept(concept, positiveExample, negativeExample, positiveSpecializations)
  }

  def filterExample(concept: RichPattern, example: List[Position]): List[Position] = {
    example
  }

  def specializationCandidate(positiveExample: List[Position]): List[LeafExp] = {
    var candidates = List.empty[LeafExp]
    positiveExample foreach {
      p =>
        p.position foreach {
          piece =>
            piece.asInstanceOf[Piece].args foreach {
              exp =>
                if (!candidates.contains(exp) && exp.isInstanceOf[LeafExp]) candidates ::= exp.asInstanceOf[LeafExp]
            }
        }
    }
    candidates
  }

  def makeSpecialization(concept: RichPattern, candidates: List[LeafExp]): List[Specialization] = {
    val labels = pickUpLabel(concept)
    var substitutions = List.empty[Map[Label, LeafExp]]
    labels foreach {
      label =>
        candidates foreach {
          candidate =>
            substitutions ::= Map(label -> candidate)
        }
    }
    //todo generalize
    substitutions map (Specialization(_))
  }

  def pickUpLabel(concept: RichPattern): List[Label] = {
    concept match {
      case prime: PrimeConcept => pickUpLabelFromPrimeConcept(prime)
      case comp: CompositeConcept => pickUpLabelFromCompositeConcept(comp)
    }
  }

  def pickUpLabelFromPrimeConcept(prime: PrimeConcept): List[Label] = {
    var labels = List.empty[Label]
    prime.pieces foreach {
      p =>
        p match {
          case piece: Piece => piece.args foreach {
            a => if (a.isInstanceOf[Label] && !labels.contains(a)) labels ::= a.asInstanceOf[Label]
          }
          case _ =>
        }
    }
    labels
  }

  def pickUpLabelFromCompositeConcept(comp: CompositeConcept): List[Label] = {
    (comp.primes flatMap (p => pickUpLabelFromPrimeConcept(p))).toSet.toList
  }

  def makeSpecifiedConcept(concept: RichPattern, positiveExample: List[Position], negativeExample: List[Position], candidates: List[Specialization]): SpecifiedConcept = {
    var buf = 0
    var specializations = candidates
    var result = List.empty[Specialization]
    val order = Ordering.by[Tuple2[Object, Int], Int](t => t._2)
    while (0 < specializations.size) {
      val max = (specializations zip (specializations map (s => benefit(s :: result, concept, positiveExample, negativeExample)))).max(order)
      if (max._2 - buf <= 0) return new SpecifiedConcept(concept, Specialization(result))
      specializations = specializations filter (_ != max._1)
      buf = max._2
      result ::= max._1
    }
    new SpecifiedConcept(concept, Specialization(result))
  }

  def benefit(specializations: List[Specialization], concept: RichPattern, positiveExample: List[Position], negativeExample: List[Position]): Int = {
    val specifiedConcept = new SpecifiedConcept(concept, Specialization(specializations))
    countMatch(specifiedConcept, positiveExample) - countMatch(specifiedConcept, negativeExample)
  }
  
  def countMatch(specifiedConcept: SpecifiedConcept, example:List[Position]): Int = {
    val conceptName = Label("hogehogehogehoge")
    val concept = specifiedConcept.toRichConcept.putLabel(conceptName)
    val interpreter = concept.load(new Interpreter)
    var n = 0
    example foreach {
      position =>
        try {
          if(interpreter.interpret(makeQuery(position, conceptName)) == Bool(true)) n += 1
        } catch {
          case _: Throwable =>
        }
    }
    n
  }
  
  def makeQuery(position: Position, conceptName: Label) = {
    ConceptCall(conceptName, List(TypedExp(position, PositionType())))
  }
}