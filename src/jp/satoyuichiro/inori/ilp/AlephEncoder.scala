package jp.satoyuichiro.inori.ilp

import jp.satoyuichiro.inori.cdl.Position
import jp.satoyuichiro.inori.cdl.Piece
import jp.satoyuichiro.inori.cdl.Exp
import jp.satoyuichiro.inori.cdl._
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.Files

class AlephEncoder(val dir: String = "./ILPworkspace/") {

  type IndexedPosition = Tuple2[Position, Int]
  
  implicit def toArrayChar(str: String): Array[Byte] = {
    str.toCharArray() map (_.toByte)
  }
  
  def encode(positiveExample: List[Position], negativeExample: List[Position], filename:String): Unit = {
    val positions = positiveExample ++ negativeExample
    backGroundKnowledge(positions, filename)
    positiveExamples(positiveExample.size, filename)
    negativeExamples(positiveExample.size, positions.size, filename)
  }
  
  def backGroundKnowledge(positions: List[Position], filename: String): Unit = {
    var result = makeHeader(positions) + "\n"
    val indexed = putIndex(positions)
    //result += encodeSuccRelation(positions) + "\n"
    result += encode(indexed)
    println(result)
    Files.write(Paths.get(dir + filename + ".b"), result)
  }
  
  def positiveExamples(lastIndex: Int, filename: String): Unit = {
    var result = ""
    for (i <- 1 to lastIndex) {
      result += "example(position_" + i +").\n"
    }
    println(result)
    Files.write(Paths.get(dir + filename + ".f"), result)
  }
  
  def negativeExamples(startIndex: Int, lastIndex: Int, filename: String): Unit = {
    var result = ""
    for (i <- startIndex + 1 to lastIndex) {
      result += "example(position_" + i +").\n"
    }
    println(result)
    Files.write(Paths.get(dir + filename + ".n"), result)
  }
  
  def makeHeader(positions: List[Position]): String = {
    val maxArgSize = countMaxArgSize(positions) - 1
    val modes = makeMode(positions, maxArgSize)
    val detms = makeDetermination(positions, maxArgSize)
    val types = encodeSymbols(positions)
    modes + "\n" + detms + "\n" + types
  }
  
  def makeMode(positions: List[Position], maxArgSize: Int): String = {
    var result = ":- modeh(1, example(+position)).\n"
    result += ":- modeb(*, has_piece(+position,-piece)).\n"

    val argi = ":- modeb(1, argi_"
    val args = ":- modeb(1, args_"
    for (i <- 0 to maxArgSize) {
      result += argi + i + "(+piece,#int)).\n"
      result += args + i + "(+piece,#symbol)).\n"
    }
    result
  }
  
  def makeDetermination(positions: List[Position], maxArgSize: Int): String = {
    val det = ":- determination(example/1,"
    var result = det + "has_piece/2).\n"

    //val argi = det + "argi_"
    //val args = det + "args_"
    //for (i <- 0 to maxArgSize) {
    //  result += argi + i + "/2).\n"
    //  result += args + i + "/2).\n"
    //}
    result
  }
  
  def countMaxArgSize(positions: List[Position]): Int = {
    var max = 0
    positions foreach {
      position => position.position collect { case p: Piece => p } foreach {
        piece => if (max < piece.args.size) max = piece.args.size
      }
    }
    max
  }
  
  def putIndex(positions: List[Position]): List[IndexedPosition] = {
    var i = 1
    var indexed = List.empty[IndexedPosition]
    positions foreach {
      p =>
        indexed ::= (p,i)
        i += 1
    }
    indexed
  }
  
  def encode(positions: List[IndexedPosition]): String = {
    (positions map (p => encode(p))).fold("")(_ + _)
  }
  
  def encode(position: IndexedPosition): String = {
    "position(position_" + position._2 + ").\n" + encode(position._1.position collect { case p: Piece => p }, position._2) + "\n"
  }
  
  def encode(pieces: List[Piece], positionIndex: Int): String = {
    var pieceIndex = 1
    var result = ""
    pieces foreach {
      p => 
        result += encode(p, positionIndex, pieceIndex)
        pieceIndex += 1
    }
    result
  }
  
  def encode(piece: Piece, positionIndex: Int, pieceIndex: Int): String = {
    var argIndex = 0
    val pieceString = "piece_" + positionIndex + "_" + pieceIndex
    var result = "piece(" + pieceString + ").\n"
    result += "has_piece(position_" + positionIndex + "," + pieceString + ").\n"
    piece.args foreach {
      arg =>
        result += encode(arg, argIndex, pieceString)
        argIndex += 1
    }
    result
  }
  
  def encode(exp: Exp, argIndex: Int, pieceString: String): String = {
    exp match {
      case i: Integer => "argi_" + argIndex + "(" + pieceString + "," + i.toLisp + ").\n"
      case s: Symbol => "args_" + argIndex + "(" + pieceString + "," + s.value + ").\n"
      case _ => ""
    }
  }
  
  def encodeSymbols(positions: List[Position]): String = {
    var symbols = List.empty[Symbol]
    positions foreach {
      position => position.position collect { case p: Piece => p } foreach {
        piece => piece.args foreach {
          arg => arg match {
            case s: Symbol => if (!symbols.contains(s)) symbols ::= s
            case _ =>
          }
        }
      }
    }
    (symbols map (s => "symbol(" + s.value + ").\n")).fold("")(_ + _)
  }
  
  def encodeSuccRelation(positions: List[Position]): String = {
    var result = ""
    for (i <- 0 to (countMaxArgSize(positions) - 1)) {
      val col = getArg(i, positions)
      if (isSuccRelation(col))
        result += makeSucc(i)
    }
    result
  }
  
  def getArg(i: Int, positions: List[Position]): List[Exp] = {
    val piecels = positions flatMap (_.position) collect { case p: Piece => p }
    piecels filter (p => i < p.args.size) map (a => a.args.drop(i).head)
  }
  
  def isSuccRelation(exps: List[Exp]): Boolean = (exps map (_.isInstanceOf[Integer])).fold(false)(_ || _)
  
  def makeSucc(i: Int): String = {
    ":- modeb(1, next_piece_" + i + "(+piece,+piece)).\n" +
    ":- determination(example/1,next_piece_" + i + "/2).\n" +
    "next_piece_" + i + "(P1,P2):-argi_" + i + "(P1,X),argi_" + i + "(P2,Y),Y is X+1.\n"
  }
}