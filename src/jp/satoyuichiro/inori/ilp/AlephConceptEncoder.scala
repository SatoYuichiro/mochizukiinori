package jp.satoyuichiro.inori.ilp

import jp.satoyuichiro.inori.learning.concept.generator.RichConcept
import java.io.File
import jp.satoyuichiro.inori.learning.concept.generator.PrimeConcept
import jp.satoyuichiro.inori.learning.concept.generator.CompositeConcept
import jp.satoyuichiro.inori.learning.concept.generator.PrimeConjunction
import jp.satoyuichiro.inori.learning.concept.generator.CompositeDisjunction
import jp.satoyuichiro.inori.cdl._
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardOpenOption
import java.nio.file.Path
import jp.satoyuichiro.inori.learning.concept.generator.GeneratorUtil

class AlephConceptEncoder(val typeArray: Array[Exp], val dir: String = "./ILPworkspace/") {

  def encode(concepts: List[RichConcept], filename: String): Unit = {
    val path = Paths.get(dir + filename + ".b")
    concepts foreach { concept =>
        concept match {
          case p: PrimeConcept => appendString(encodePrimeConcept(p), path)
          case c: CompositeConcept => appendString(encodeCompositeConcept(c), path)
        }
      }
  }
  
  def appendString(str: String, path: Path): Unit = {
    Files.write(path, AlephUtil.toArrayChar(str), StandardOpenOption.APPEND)
  }
  
  def encodePrimeConcept(primeConcept: PrimeConcept): String = {
    primeConcept match {
      case conj: PrimeConjunction =>
        encodeMode(conj) + "\n" +
        encodePrimeConjunction(conj) + "\n"
    }
  }
  
  def encodeMode(conj: PrimeConjunction): String = {
    val arity = conj.arity + conj.pieces.head.args.size
    val name = conj.conceptName.getOrElse(Label("hoge")).label
    val piece = List.fill(conj.arity)("+piece").fold("")(_ + _ + ",")
    val variables = "#symbol,#int,#int,#symbol"
    ":- modeb(1, " + name + "(" + piece + variables + ")). \n" +
    ":- determination(example/1," + name + "/" + arity + ")."
  }
  
  def encodePrimeConjunction(conj: PrimeConjunction): String = {
    val head = encodeHead(conj)
    val body = encodeBody(conj)
    println(head ++ body)
    encodeHead(conj) + encodeBody(conj)
  }
  
  def encodeHead(conj: PrimeConjunction): String = {
    var head = conj.conceptName.getOrElse(Label("hoge")).label + "("
    for (i <- 0 to conj.arity - 1) {
      head += "P" + i + ","
    }
    val variables = conj.pieces.head.args collect { case v: Label => v.label.toString.toUpperCase() }
    variables foreach { v =>
      head += v + ","
    }
    head.dropRight(1) + ") :- "    
  }
  
  def encodeBody(conj: PrimeConjunction): String = {
    val index = GeneratorUtil.Labeler.indexMaker(0)
    val pairs = makePieceLabelPair(conj.pieces)
    val strlsls = pairs map (pair => preEncodePiece(pair._1, pair._2, index))
    encodePiece(strlsls)
  }
  
  def makePieceLabelPair(pieces: List[Piece]): List[(Piece, String)] = {
    val labels = for (i <- 0 to pieces.size - 1) yield "P" + i.toString
    pieces zip labels
  }
  
  def preEncodePiece(piece: Piece, pieceName: String, index: Unit => Integer): List[Either[String, (String, String, String)]] = {
    var c = 0
    var result = List.empty[Either[String, (String,String,String)]]
    piece.args foreach { arg =>
      arg match {
        case i: Integer => result ::= Left(AlephUtil.Enco.intArg(c) + pieceName + "," + i.value.toString + ")")
        case s: Symbol => result ::= Left(AlephUtil.Enco.symbolArg(c) + pieceName + "," + s.value + ")")
        case v: Label => typeArray(c) match {
          case i: Integer => result ::= Left(AlephUtil.Enco.intArg(c) + pieceName + "," + v.label.toUpperCase() + ")")
          case s: Symbol => result ::= Left(AlephUtil.Enco.symbolArg(c) + pieceName + "," + v.label.toUpperCase() + ")")
        }
        case a: Addition =>
          result ::= Right(encodeAddition(a, c, pieceName, index().value))
      }
      c += 1
    }
    result.reverse
  }
  
  def encodeAddition(a: Addition, c: Int, pieceName: String, i: Int): (String, String, String) = {
    val y = AlephUtil.Enco.intArg(c) + pieceName + ",Y" + i + ")"
    val str = a.args collect { case e: Exp => e.toLisp.toUpperCase() }
    var res = ""
    str foreach { s =>
      res += s + " + "
    }
    (res.dropRight(3), y, "Y"+i)
  }
  
  def encodePiece(preEncode: List[List[Either[String, (String,String,String)]]]): String = {
    var res = List.empty[String]
    preEncode foreach { enc =>
      enc foreach { p =>
        p match {
          case Left(s) =>
          case Right(s) => res ::= s._3 + " is " + s._1 + ", "
        }
      }
      enc foreach { p =>
        p match {
          case Left(s) => res ::= s + ", "
          case Right(s) => res ::= s._2 + ", "
          }
        }
    }
    res.reverse.fold("")(_ + _).dropRight(2) + "."
  }
  
  def encodeCompositeConcept(compositeConcept: CompositeConcept): String = {
    compositeConcept match {
      case disj: CompositeDisjunction => 
    }
  ""
  }
}