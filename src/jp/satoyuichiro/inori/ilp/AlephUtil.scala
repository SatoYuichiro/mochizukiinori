package jp.satoyuichiro.inori.ilp

import jp.satoyuichiro.inori.learning.concept.generator.RichConcept
import java.io.FileOutputStream
import java.io.ObjectOutputStream
import java.io.File
import java.io.FileInputStream
import java.io.ObjectInputStream
import jp.satoyuichiro.inori.cdl.Position

object AlephUtil {
  
  implicit def toArrayChar(str: String): Array[Byte] = {
    str.toCharArray() map (_.toByte)
  }
  
  object Enco {
    val modeh = ":- modeh"
    val modeb = ":- modeb"
    val concept = ""
    def intArg(i: Int): String = "argi_" + i + "("
    def symbolArg(i: Int): String = "args_" + i +"("
  }
    
  def positionSerialize(positions: List[Position], fileName: String): Unit = {
    try {
      val cFile = new FileOutputStream(fileName)
	  val cStream = new ObjectOutputStream(cFile)
	  cStream.writeObject(positions)

	  cStream.close()
	  cFile.close()
    } catch {
      case _: Throwable => println("something wrong!")
    }
  }
  
  def positionDeserialize(file: File): List[Position] = {
    if (file.exists()) {
      val cFile = new FileInputStream(file)
	  val cStream = new ObjectInputStream(cFile)

      try {
        val cmap = cStream.readObject().asInstanceOf[List[Position]]
            
        cmap
      } catch {
        case _: Throwable => List.empty[Position]
      } finally {
        cStream.close()
        cFile.close()
      }
    } else {
      println("no file")
      List.empty[Position]
    }
  }

  def positionDeserialize(fileName: String): List[Position] = {
    positionDeserialize(new File(fileName))
  }

  def conceptSerialize(concepts: List[RichConcept], fileName: String): Unit = {
    try {
      val cFile = new FileOutputStream(fileName)
	  val cStream = new ObjectOutputStream(cFile)
	  cStream.writeObject(concepts)

	  cStream.close()
	  cFile.close()
    } catch {
      case _: Throwable => println("something wrong!")
    }
  }
  
  def conceptDeserialize(file: File): List[RichConcept] = {
    if (file.exists()) {
      val cFile = new FileInputStream(file)
	  val cStream = new ObjectInputStream(cFile)

      try {
        val cmap = cStream.readObject().asInstanceOf[List[RichConcept]]
            
        cmap
      } catch {
        case e: Throwable =>
          e.printStackTrace()
          List.empty[RichConcept]
      } finally {
        cStream.close()
        cFile.close()
      }
    } else {
      println("no file")
      List.empty[RichConcept]
    }  }

  def conceptDeserialize(fileName: String): List[RichConcept] = {
    conceptDeserialize(new File(fileName));
  }
}