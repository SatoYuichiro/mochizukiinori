package jp.satoyuichiro.inori.weka

import jp.satoyuichiro.inori.learning.concept.generator.RichConcept
import jp.satoyuichiro.inori.simulator.GameSimulator
import jp.satoyuichiro.inori.cdl._
import jp.satoyuichiro.inori.player.PlayerUtil
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardOpenOption
import java.io.File
import jp.satoyuichiro.inori.learning.concept.generator.PrimeConcept
import jp.satoyuichiro.inori.learning.concept.generator.GeneratorUtil
import jp.satoyuichiro.inori.learning.concept.specifier.SpecifiedConcept
import jp.satoyuichiro.inori.learning.concept.specifier.Specialization
import jp.satoyuichiro.inori.ilp.AlephUtil
import jp.satoyuichiro.inori.simulator.Episode
import org.ggp.base.util.statemachine.implementation.prover.ProverStateMachine
import jp.satoyuichiro.inori.gdl.Description
import jp.satoyuichiro.inori.gdl.GdlUtil
import jp.satoyuichiro.inori.player.RandomPlayer
import jp.satoyuichiro.inori.learning.concept.generator.PrimeConjunction
import jp.satoyuichiro.inori.learning.concept.generator.PrimeDisjunction
import java.nio.file.Path
import jp.satoyuichiro.inori.cdl.Interpreter
import java.nio.charset.Charset

case class WekaData(val position: Position, val label: String)

class WekaLabel
object WekaLabel {
  case object Win extends WekaLabel
  case object Lose extends WekaLabel
  case object Draw extends WekaLabel
  case object Even extends WekaLabel
}

trait WekaTree {
  
  def eval(interpreter: Interpreter, position: Position): WekaLabel
}

trait WekaNode {
  
  def eval(interpreter: Interpreter, positions: List[Position]): WekaLabel
}

case class CompositeTree(node: WekaNode) extends WekaTree {

  def eval(interpreter: Interpreter, position: Position): WekaLabel = {
    val subPos = PlayerUtil.makeBinaryPosition(position) ++ PlayerUtil.makeAllTrinaryPosition(position)
    node.eval(interpreter, subPos)
  }
}

case class ClassifyTree(node: WekaNode) extends WekaTree {
  
  def eval(interpreter: Interpreter, position: Position): WekaLabel = {
    val subPos = PlayerUtil.makeBinaryPosition(position) ++ PlayerUtil.makeAllTrinaryPosition(position)
    node.eval(interpreter, subPos)
  }
}

object ClassifyTree {
  
  def toMap(position: Position): Map[Piece, Exp] = {
    val hoge = position.position map (_.asInstanceOf[Piece]) map (p => (Piece(p.args.dropRight(1)), p.args.last))
    var builder = Map.newBuilder[Piece, Exp]
    hoge foreach { t =>
      builder += t._1 -> t._2
    }
    builder.result()
  }
}

case class BoolTree(node: BoolNode) extends WekaTree {
  
  def eval(interpreter: Interpreter, position: Position): WekaLabel = {
    val subPos = PlayerUtil.makeBinaryPosition(position) ++ PlayerUtil.makeAllTrinaryPosition(position)
    node.eval(interpreter, subPos)
  }
}

case class BoolNode(concept: Label, trueCase: WekaNode, falseCase: WekaNode) extends WekaNode {
  
  def eval(interpreter: Interpreter, positions: List[Position]): WekaLabel = {
    if (WekaUtil.isMatch(positions, interpreter, concept)) {
      trueCase.eval(interpreter, positions)
    } else {
      falseCase.eval(interpreter, positions)
    }
  }
}

case class BoolLeaf(concept: Label, trueCase: WekaLabel, falseCase: WekaLabel) extends WekaNode {
  
  def eval(interpreter: Interpreter, positions: List[Position]): WekaLabel = {
    if (WekaUtil.isMatch(positions, interpreter, concept)) {
      trueCase
    } else {
      falseCase
    }
  }
}

case class NumberTree(node: NumberNode) extends WekaTree {
  
  def eval(interpreter: Interpreter, position: Position): WekaLabel = {
    val subPos = PlayerUtil.makeBinaryPosition(position) ++ PlayerUtil.makeAllTrinaryPosition(position)
    node.eval(interpreter, subPos)
  }
}

case class NumberNode(concept: Label, threshold: Int, gtOrEqCase: WekaNode, ltCase: WekaNode) extends WekaNode {
  
  def eval(interpreter: Interpreter, positions: List[Position]): WekaLabel = {
    if (threshold <= WekaUtil.countMatch(positions, interpreter, concept)) {
      gtOrEqCase.eval(interpreter, positions)
    } else {
      ltCase.eval(interpreter, positions)
    }
  }
}

case class NumberLeaf(concept: Label, threshold: Int, gtOrEqCase: WekaLabel, ltCase: WekaLabel) extends WekaNode {
  
  def eval(interpreter: Interpreter, positions: List[Position]): WekaLabel = {
    if (threshold <= WekaUtil.countMatch(positions, interpreter, concept)) {
      gtOrEqCase
    } else {
      ltCase
    }
  }
}

abstract class ClassifyNode extends WekaNode {
  
  def eval(interpreter: Interpreter, positions: List[Position]): WekaLabel = {
    eval(ClassifyTree.toMap(positions.head))
  }
  
  def eval(positions: Map[Piece, Exp]): WekaLabel
}

case class ClassifyNodes(cell: String, x: Int, y: Int, nodes: Map[Exp, ClassifyNode]) extends ClassifyNode {
  
  val target = Piece(Symbol(cell), Integer(x), Integer(y))
  
  def eval(position: Map[Piece, Exp]): WekaLabel = {
    position.get(target) match {
      case Some(e) => nodes.get(e) match {
        case Some(n) => n.eval(position)
        case None => throw new RuntimeException("unkown label")
      }
      case None => throw new RuntimeException("unknown piece")
    }
  }
}

case class ClassifyLeaf(cell: String, x: Int, y: Int, cases: Map[Exp, WekaLabel]) extends ClassifyNode {
  
  val target = Piece(Symbol(cell), Integer(x), Integer(y))
  
  def eval(positions: Map[Piece, Exp]): WekaLabel = {
    positions.get(target) match {
      case Some(e) => cases.get(e) match {
        case Some(c) => c
        case None => throw new RuntimeException("unkown label")
      }
      case None => throw new RuntimeException("unknown piece")
    }
  }
}
case class WinLeaf() extends WekaNode {
  
  def eval(interpreter: Interpreter, positions: List[Position]): WekaLabel = WekaLabel.Win
}

case class LoseLeaf() extends WekaNode {
  
  def eval(interpreter: Interpreter, positions: List[Position]): WekaLabel = WekaLabel.Lose
}

case class DrawLeaf() extends WekaNode {
  
  def eval(interpreter: Interpreter, positions: List[Position]): WekaLabel = WekaLabel.Draw
}

case class EvenLeaf() extends WekaNode {
  
  def eval(interpreter: Interpreter, positions: List[Position]): WekaLabel = WekaLabel.Even
}

object WekaUtil {
  
  def getConcept(file: File): List[PrimeConcept] = putName(specialize(putName(AlephUtil.conceptDeserialize(file))))
  
  def getConceptFromDefineConcept(path: Path): List[PrimeConcept] = putName(specialize(putName(getFromDefineConcept(path))))
  
  def getFromDefineConcept(path: Path): List[PrimeConcept] = {
    val lines = Files.readAllLines(path, Charset.defaultCharset()).iterator()
    var res = List.empty[Exp]
    while(lines.hasNext()) {
      res ::= Compiler.compile(Parser.parse(lines.next()))
    }
    defineConceptToRichConcept(res map (_.asInstanceOf[DefineConcept]))
  }
  
  def defineConceptToRichConcept(defineConcepts: List[DefineConcept]): List[PrimeConcept] = {
    defineConcepts map ( con =>
      con.concept.body match {
        case and : And => PrimeConjunction(and.args map (arg => argToRichConcept(arg)))
        case or : Or => PrimeDisjunction(or.args map (arg => argToRichConcept(arg)))
      })
  }
  
  def argToRichConcept(exp: Exp): Piece = {
    exp match {
      case u: Unificate => u.piece1.asInstanceOf[Piece]
      case e: Equals => e.arg1.asInstanceOf[Piece]
    }
  }
  
  def putName(rich: List[RichConcept]): List[PrimeConcept] = {
    val labeler = GeneratorUtil.Labeler.makeLabeler("concept")
    rich map (_.putLabel(labeler())) collect { case c: PrimeConcept => c }
  }
  
  def specialize(rich: List[PrimeConcept]): List[PrimeConcept] = {
    rich flatMap (r => {
      val last = r.pieces.head.args.last
      if (last.isInstanceOf[Label]) {
        val label = last.asInstanceOf[Label]
        val hasInteger = (r.pieces.map(p => p.args.map(_.isInstanceOf[Integer]).fold(false)(_ || _))).fold(false)(_ || _)
        if (label.label == "x3" && !hasInteger) {
          val sp1 = Specialization(Map(label -> Symbol("r")))
          val sp2 = Specialization(Map(label -> Symbol("w")))
          List((new SpecifiedConcept(r, sp1)).toRichConcept.asInstanceOf[PrimeConcept],
              (new SpecifiedConcept(r, sp2)).toRichConcept.asInstanceOf[PrimeConcept])
        } else {
          List.empty[PrimeConcept]
        }
      } else {
        List.empty[PrimeConcept]
      }
    })
  }
  
  def simulateFirstPlayerWin(gdlFile: String, n: Int): List[Episode] = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(gdlFile).getDescription)
    val roles = GdlUtil.toScalaList(stateMachine.getRoles())
    val players = List(new RandomPlayer(stateMachine, roles.head),
        new RandomPlayer(stateMachine, roles.tail.head))
    val simulator = GameSimulator(stateMachine, players)
    
    simulator.simulateWinEpisode(players.head, n) map (ep => Episode(ep map PlayerUtil.gdlTocdl))
  }
  
  def simulateSecondPlayerWin(gdlFile: String, n: Int): List[Episode] = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(gdlFile).getDescription)
    val roles = GdlUtil.toScalaList(stateMachine.getRoles())
    val players = List(new RandomPlayer(stateMachine, roles.head),
        new RandomPlayer(stateMachine, roles.tail.head))
    val simulator = GameSimulator(stateMachine, players)
    
    simulator.simulateWinEpisode(players.tail.head, n) map (ep => Episode(ep map PlayerUtil.gdlTocdl))
  }
  
  def makeArff(fileName: String, concepts: List[RichConcept], data: List[WekaData]): Unit = {
    val path = Paths.get("./weka/" + fileName + ".arff")
    Files.write(path, makeAttribute(fileName, concepts).getBytes())
    val dataLines = data.par map (d => makeData(load(new Interpreter(), concepts), concepts, d.position, d.label))
    dataLines foreach { str => Files.write(path, str.getBytes(), StandardOpenOption.APPEND) }
  }
    
  def makeArffTrueOrFalse(fileName: String, concepts: List[RichConcept], data: List[WekaData]): Unit = {
    val path = Paths.get("./weka/" + fileName + ".arff")
    Files.write(path, makeAttributeTrueOrFalse(fileName, concepts).getBytes())
    val dataLines = data.par map (d => makeDataTrueOrFalse(load(new Interpreter(), concepts), concepts, d.position, d.label))
    dataLines foreach { str => Files.write(path, str.getBytes(), StandardOpenOption.APPEND) }
  }
  
  def makeArffXYCell(fileName: String, data: List[WekaData]): Unit = {
    val path = Paths.get("./weka/" + fileName + ".arff")
    val sorted = sortXYCell(data)
    Files.write(path, makeAttributeXYCell(fileName, sorted).getBytes())
    val dataLines = sorted map (d => makeDataXYCell(d.position, d.label))
    dataLines foreach { str => Files.write(path, str.getBytes(), StandardOpenOption.APPEND) }
  }
  
  def load(interpreter: Interpreter, concepts: List[RichConcept]): Interpreter = {
    concepts map (_.toDefineConcept) foreach { s => 
      s match {
        case Some(d) => interpreter.interpret(d)
        case None => 
      }
    }
    interpreter
  }
  
  def makeAttribute(fileName: String, concepts: List[RichConcept]): String = {
    "@RELATION " + fileName + "\n" +
    "\n" +
    (concepts map makeReal).fold("")(_ + _) +
    makeClass +
    "\n\n" +
    "@DATA\n"
  }
  
  def makeAttributeTrueOrFalse(fileName: String, concepts: List[RichConcept]): String = {
    "@RELATION " + fileName + "\n" +
    "\n" +
    (concepts map makeBoolean).fold("")(_ + _) +
    makeClass +
    "\n\n" +
    "@DATA\n"
  }
  
  def sortXYCell(data: List[WekaData]): List[WekaData] = {
    data map (d => WekaData(sortCell(d.position), d.label))
  }
  
  object CellOrder extends Ordering[Piece] {
    def compare(p1: Piece, p2: Piece): Int = {
      val cell1 = p1.args.head.asInstanceOf[Symbol]
      val cell2 = p2.args.head.asInstanceOf[Symbol]
      val x1 = p1.args.drop(1).head.asInstanceOf[Integer]
      val x2 = p2.args.drop(1).head.asInstanceOf[Integer]
      val y1 = p1.args.drop(2).head.asInstanceOf[Integer]
      val y2 = p2.args.drop(2).head.asInstanceOf[Integer]
      
      val c1 = cell1.value.compare(cell2.value)
      if (c1 == 0) {
        val c2 = x1.value.compare(x2.value)
        if (c2 == 0) {
          y1.value.compare(y2.value)
        } else {
          c2
        }
      } else {
        c1
      }
    }
  }
  
  def sortCell(position: Position): Position = {
    val pieces = position.position.map(_.asInstanceOf[Piece])
    Position(pieces.sorted(CellOrder), None)
  }
  
  def makeAttributeXYCell(fileName: String, data: List[WekaData]): String = {
    "@RELATION " + fileName + "\n" +
    "\n" +
    (makeXYCell(data)) +
    makeClass +
    "\n\n" +
    "@DATA\n"
  }
  
  def makeReal(concept: RichConcept): String = {
    concept.conceptName match {
      case Some(name) => "@ATTRIBUTE " + name.label + " REAL\n"
      case None => ""
    }
  }
  
  def makeBoolean(concept: RichConcept): String = {
    concept.conceptName match {
      case Some(name) => "@ATTRIBUTE " + name.label + " INTEGER\n"
      case None => ""
    }
  }
  
  def makeXYCell(data: List[WekaData]): String = {
    var ls = List.empty[String]
    val symbols = "{" + possibleSymbol(data).fold("")(_ + _ + ",").dropRight(1) + "}"
    data foreach { d =>
      d.position.position foreach { p =>
        val cellXY = p.asInstanceOf[Piece].args.take(3).map(a => makeXYCellSymbol(a)).fold("")(_ + _)
        if (!ls.contains(cellXY)) {
          ls ::= cellXY
        }
      }
    }
    ls.map(str => "@ATTRIBUTE " + str + " " + symbols + "\n").fold("")(_ + _)
  }
  
  def possibleSymbol(data: List[WekaData]): List[String] = {
    val symbols = data flatMap (d => d.position.position map (p => makeXYCellSymbol(p.asInstanceOf[Piece].args.last)))
    var res = List.empty[String]
    symbols foreach { s =>
      if (!res.contains(s)) {
        res ::= s
      }
    }
    res.reverse
  }
  
  def makeXYCellSymbol(exp: Exp): String = {
    exp match {
      case i: Integer => i.value.toString
      case s: Symbol => s.value
      case _ => ""
    }
  }
  
  def makeClass(): String = "@ATTRIBUTE class {win,draw,lose,even}\n"
  
  def makeData(interpreter: Interpreter, concepts: List[RichConcept], position: Position, label: String): String = {
    val subPositions = PlayerUtil.makeAllBinaryPosition(position) ++ PlayerUtil.makeAllTrinaryPosition(position)
    (concepts map (c => count(interpreter, subPositions, c).toString)).fold("")(_ + _ + ",") + label + "\n"
  }
  
  def makeDataTrueOrFalse(interpreter: Interpreter, concepts: List[RichConcept], position: Position, label: String): String = {
    val subPositions = PlayerUtil.makeAllBinaryPosition(position) ++ PlayerUtil.makeAllTrinaryPosition(position)
    (concepts map (c => trueOrFalse(interpreter, subPositions, c).toString)).fold("")(_ + _ + ",") + label + "\n"
  }
  
  def makeDataXYCell(position: Position, label: String): String = {
    (position.position map (a => makeXYCellSymbol(a.asInstanceOf[Piece].args.last))).fold("")(_ + _ + ",") + label + "\n"
  }
  
  def toQuery(concept: RichConcept, position: Position): ConceptCall = ConceptCall(concept.conceptName.get, List(TypedExp(position, PositionType())))
  
  def count(interpreter: Interpreter, positions: List[Position], concept: RichConcept): Double = {
    positions map (p => interpreter.interpret(toQuery(concept, p)).asInstanceOf[Bool]) count (_.value) toDouble
  }
  
  def trueOrFalse(interpreter: Interpreter, positions: List[Position], concept: RichConcept): Int = {
    positions.foreach{ p =>
      if (interpreter.interpret(toQuery(concept, p)).asInstanceOf[Bool].value) {
        return 1
      }
    }
    0
  }
  
  def isMatch(subPositions: List[Position], interpreter: Interpreter, name: Label): Boolean = {
    var res = false
    for (pos <- subPositions) {
      if (res) {
        return true
      }
      
      try {
        res = interpreter.interpret(makeQuery(name, pos)).asInstanceOf[Bool].value
      } catch {
        case e: Throwable => e.printStackTrace()
      }
    }
    res
  }
  
  def countMatch(subPositions: List[Position], interpreter: Interpreter, name: Label): Int = {
    subPositions map (pos => interpreter.interpret(makeQuery(name, pos)).asInstanceOf[Bool].value) count (_ == true)
  }
  
  def makeQuery(name: Label, subPosition: Position): ConceptCall = ConceptCall(name, List(TypedExp(subPosition, PositionType())))

  def precision(tp: Int, fp: Int): Double = tp.toDouble / (tp.toDouble + fp.toDouble)
  
  def recall(tp: Int, fn: Int): Double = tp.toDouble / (tp.toDouble + fn.toDouble)
  
  def fMeasure(tp: Int, fp: Int, fn: Int, tn: Int): Double = 2 * precision(tp, fp) * recall(tp, fn) / (precision(tp, fp) + recall(tp, fn))
}