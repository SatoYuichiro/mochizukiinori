package jp.satoyuichiro.inori.weka

import scala.util.parsing.combinator._
import jp.satoyuichiro.inori.cdl.Label
import jp.satoyuichiro.inori.cdl.Exp

class WekaParseTree(val indent: Int, val conceptName: String, val operator: String, val value: String)
case class NodeParseTree(override val indent: Int, override val conceptName: String, override val operator: String, override val value: String)
  extends WekaParseTree(indent, conceptName, operator, value)
case class LeafParseTree(override val indent: Int, override val conceptName: String, override val operator: String, override val value: String, result: String)
  extends WekaParseTree(indent, conceptName, operator, value)

object WekaParser extends RegexParsers {

  override val skipWhitespace = false
  
  def conceptName: Parser[String] = "[a-zA-Z0-9]+".r
  
  def value: Parser[String] = "[a-zA-Z0-9]+".r
  
  def tabCount: Parser[String] = "[0-9]+".r

  def op: Parser[String] = "<=" | "=" | "<" | ">"
  
  def result: Parser[String] = "[a-zA-Z]+".r
  
  def eol: Parser[Unit] = opt('\r') <~ '\n' ^^ {
    case _ => Unit
  }
  
  def hoge: Parser[String] = "(" ~ "[0-9./]+".r ~ ")" ^^ {
    case "("~h~")" => h
  }
  
  def leaf: Parser[LeafParseTree] = tabCount ~ " " ~ conceptName ~ " " ~ op ~ " " ~ value ~ ": " ~ result ~ " " ~ hoge ~ eol ^^ {
    case t~" "~name~" "~o~" "~v~": "~res~" "~hoge~eol => LeafParseTree(t.toInt, name, o, v, res)
  }
  
  def node: Parser[NodeParseTree] = tabCount ~ " " ~ conceptName ~ " " ~ op ~ " " ~ value ~ eol ^^ {
    case t~" "~name~" "~o~" "~v~eol => NodeParseTree(t.toInt, name, o, v)
  }
  
  def nodeOrLeaf: Parser[WekaParseTree] = node | leaf
  
  def top: Parser[List[WekaParseTree]] = rep(nodeOrLeaf)
  
  val tabSymbol = "|   "
  
  def tabToCount(lines: List[String]): List[String] = {
    val trimed = lines.map(_.trim()).filter(_.size > 0)
    trimed map (t => cutAndCountTab(t)) map (stri => stri._2 + " " + stri._1)
  }
    
  def cutAndCountTab(line: String): (String, Int) = {
    var res = line
    var i = 0
    while(res.startsWith(tabSymbol)) {
      res = res.drop(tabSymbol.size)
      i += 1
    }
    (res, i)
  }
  
  def parse(input: String) = {
    val treated = tabToCount(input.split("\n").toList).fold("")(_ + _ + "\n")
    parseAll(top, treated)
  }
}

object WekaTreeCompiler {
  
  class AST(val args: List[WekaParseTree])
  case class ASTNode(override val args: List[WekaParseTree], val childs: List[AST]) extends AST(args)
  case class ASTLeaf(override val args: List[LeafParseTree]) extends AST(args)
  
  def toBinaryWekaTree(ls: List[WekaParseTree]): WekaTree = {
    toAST(ls) match {
      case Some(ast: AST) => CompositeTree(blanchNodeLeaf2(ast))
      case None => throw new RuntimeException("this is not binary tree")
    }
  }
  
  def toTrinaryWekaTree(ls: List[WekaParseTree]): WekaTree = {
    toClasifyWekaTree(ls, 3)
  }
  
  def toFourClassWekaTree(ls: List[WekaParseTree]): WekaTree = {
    toClasifyWekaTree(ls, 4)
  }
  
  def toClasifyWekaTree(ls: List[WekaParseTree], argSize: Int): WekaTree = {
    toAST(ls) match {
      case Some(ast: AST) => CompositeTree(blanchNodeLeafEq(ast, argSize))
      case None => throw new RuntimeException("this is not binary tree")
    }
  }
  
  def toAST(ls: List[WekaParseTree]): Option[AST] = {
    if (ls.size == 0) {
      None
    } else {
      var args = List.empty[WekaParseTree]
      var subTrees = List.empty[List[WekaParseTree]]
      var workingLs = ls
      
      val indent = ls.head.indent
      val conceptName = ls.head.conceptName
      
      while(!workingLs.isEmpty) {
        val p = workingLs.head
        if (p.indent == indent && p.conceptName == conceptName) {
          args ::= p
          workingLs = workingLs.tail
        } else {
          val sub = workingLs.takeWhile(_.indent != indent)
          subTrees ::= sub
          workingLs = workingLs.drop(sub.size)
        }
      }
      args = args.reverse
      subTrees = subTrees.reverse
      
      val childs = subTrees map (sub => toAST(sub)) map (_.get)
      if (childs.size == 0) {
        Some(ASTLeaf(args map (_.asInstanceOf[LeafParseTree])))
      } else {
        Some(ASTNode(args, childs))
      }
    }
   }
  
  def printAST(ast: AST): Unit = {
    ast match {
      case n: ASTNode => {
        printTab(n)
        println("Node " + n.args.map(_.toString).fold("")(_ + _ + ", "))
        n.childs foreach { child =>
          printAST(child)
        }
      }
      case l: ASTLeaf => {
        printTab(l)
        println("Leaf " + l.args.map(_.toString).fold("")(_ + _ + ", "))
      }
    }
  }
  
  def printTab(ast: AST): Unit = {
    ast.args.headOption match {
      case Some(p: WekaParseTree) => {
        for (_ <- 1 to p.indent) {
          print("  ")
        }
      }
      case None =>
    }
  }
  
  def blanchNodeLeaf2(ast: AST): WekaNode = {
    ast match {
      case n: ASTNode => makeNode2(n)
      case l: ASTLeaf => makeLeaf2(l)
    }
  }
  
  def makeNode2(ast: ASTNode): WekaNode = {
    if (ast.args.size != 2) {
      throw new RuntimeException("This is not binary tree")
    }
    val trueCase = ast.args.head
    val falseCase = ast.args.tail.head
    
    if (trueCase.operator != "<=") {
      throw new RuntimeException("Unknown operator: " + trueCase.operator)
    }
    nodeErrerCheck2(ast)
    
    trueCase match {
      case n1: NodeParseTree => falseCase match {
        case n2: NodeParseTree => NumberNode(Label(n1.conceptName), n1.value.toInt, blanchNodeLeaf2(ast.childs.head), blanchNodeLeaf2(ast.childs.tail.head))
        case l2: LeafParseTree => NumberNode(Label(n1.conceptName), n1.value.toInt, blanchNodeLeaf2(ast.childs.head), toLabelLeaf(toWekaLabel(l2.result)))
        }
      case l1: LeafParseTree => falseCase match {
        case n2: NodeParseTree => NumberNode(Label(l1.conceptName), l1.value.toInt, toLabelLeaf(toWekaLabel(l1.result)), blanchNodeLeaf2(ast.childs.head))
        case l2: LeafParseTree => throw new RuntimeException("Node do not have node")
      }
    }
  }
  
  def nodeErrerCheck2(ast: ASTNode): Unit = {
    val trueCase = ast.args.head
    val falseCase = ast.args.tail.head
    
    trueCase match {
      case n1: NodeParseTree => falseCase match {
        case n2: NodeParseTree => if (ast.childs.size != 2) throw new RuntimeException("Number of child is invalid")
        case l2: LeafParseTree => if (ast.childs.size != 1) throw new RuntimeException("Number of child is invalid")
        }
      case l1: LeafParseTree => falseCase match {
        case n2: NodeParseTree => if (ast.childs.size != 1) throw new RuntimeException("Number of child is invalid")
        case l2: LeafParseTree => throw new RuntimeException("Node do not have node")
      }
    }
  }
  
  def makeLeaf2(ast: ASTLeaf): WekaNode = {
    if (ast.args.size != 2) {
      throw new RuntimeException("This is not binary tree")
    }
    val trueCase = ast.args.head
    val falseCase = ast.args.tail.head
    
    trueCase.operator match {
      case "<=" => if (trueCase.value == 0) {
        toBoolLeaf(trueCase, falseCase)
      } else {
        toNumberLeaf(trueCase, falseCase)
      }
      case str => throw new RuntimeException("Unknown operator: " + str)
    }
  }
  
  def blanchNodeLeafEq(ast: AST, argSize: Int): WekaNode = {
    ast match {
      case n: ASTNode => makeNodeEq(n, argSize)
      case l: ASTLeaf => makeLeafEq(l, argSize)
    }
  }
  
  def makeNodeEq(ast: ASTNode, argSize: Int): ClassifyNode = {
    nodeErrerCheckEq(ast, argSize)
    
    var labels = List.empty[(Exp, ClassifyNode)]
    var args = ast.args
    var childs = ast.childs
    
    while(!args.isEmpty) {
      args.head match {
        case n: NodeParseTree =>
        case l: LeafParseTree => (Symbol(l.value).asInstanceOf[Exp], leafParseTreeToClassifyLeaf(List(l))) :: labels
      }
    }
    
    val cell = ast.args.head.conceptName.dropRight(2)
    val x = ast.args.head.conceptName.takeRight(2).head
    val y = ast.args.head.conceptName.last
    ClassifyNodes(cell, x.toInt, y.toInt, labels.toMap)
//    val trueCase = ast.args.head
//    val falseCase = ast.args.tail.head
//    
//    trueCase match {
//      case n1: NodeParseTree => falseCase match {
//        case n2: NodeParseTree => NumberNode(Label(n1.conceptName), n1.value.toInt, blanchNodeLeaf2(ast.childs.head), blanchNodeLeaf2(ast.childs.tail.head))
//        case l2: LeafParseTree => NumberNode(Label(n1.conceptName), n1.value.toInt, blanchNodeLeaf2(ast.childs.head), toLabelLeaf(toWekaLabel(l2.result)))
//        }
//      case l1: LeafParseTree => falseCase match {
//        case n2: NodeParseTree => NumberNode(Label(l1.conceptName), l1.value.toInt, toLabelLeaf(toWekaLabel(l1.result)), blanchNodeLeaf2(ast.childs.head))
//        case l2: LeafParseTree => throw new RuntimeException("Node do not have node")
//      }
//    }
  }
  
  def nodeErrerCheckEq(ast: ASTNode, argSize: Int): Unit = {
    if (ast.args.size != argSize) {
      throw new RuntimeException("This is not " + argSize + " tree")
    }
    if (!ast.args.map(_.operator == "=").fold(true)(_ || _)) {
      throw new RuntimeException("This is not clasify tree")
    }
  }
  
  def makeLeafEq(ast: ASTLeaf, argSize: Int): ClassifyLeaf = {
    leafErrorCheckEq(ast, argSize)
    leafParseTreeToClassifyLeaf(ast.args)
//    val labels = ast.args map (arg => (arg.value, arg.result)) map (t => (Symbol(t._1).asInstanceOf[Exp], toWekaLabel(t._2)))
//    val cell = ast.args.head.conceptName.dropRight(2)
//    val x = ast.args.head.conceptName.takeRight(2).head
//    val y = ast.args.head.conceptName.last
//    ClassifyLeaf(cell, x.toInt, y.toInt, labels.toMap)
  }
  
  def leafParseTreeToClassifyLeaf(args: List[LeafParseTree]): ClassifyLeaf = {
    val labels = args map (arg => (arg.value, arg.result)) map (t => (Symbol(t._1).asInstanceOf[Exp], toWekaLabel(t._2)))
    val cell = args.head.conceptName.dropRight(2)
    val x = args.head.conceptName.takeRight(2).head
    val y = args.head.conceptName.last
    ClassifyLeaf(cell, x.toInt, y.toInt, labels.toMap)    
  }
  
  def leafErrorCheckEq(ast: ASTLeaf, argSize: Int): Unit = {
    if (ast.args.size != argSize) {
      throw new RuntimeException("This is not " + argSize + " tree")
    }
    if (!ast.args.map(_.operator == "=").fold(true)(_ || _)) {
      throw new RuntimeException("This is not clasify tree")
    }
  }
  
  def toBoolLeaf(trueCase: LeafParseTree, falseCase: LeafParseTree): BoolLeaf = {
    BoolLeaf(Label(trueCase.conceptName), toWekaLabel(trueCase.result), toWekaLabel(falseCase.result))
  }
  
  def toNumberLeaf(trueCase: LeafParseTree, falseCase: LeafParseTree): NumberLeaf = {
    NumberLeaf(Label(trueCase.conceptName), trueCase.value.toInt, toWekaLabel(trueCase.result), toWekaLabel(falseCase.result))
  }
  
  def toLabelLeaf(wekaLabel: WekaLabel): WekaNode = {
    wekaLabel match {
      case WekaLabel.Win => WinLeaf()
      case WekaLabel.Lose => LoseLeaf()
      case WekaLabel.Draw => DrawLeaf()
      case WekaLabel.Even => EvenLeaf()
    }
  }
  
  def toWekaLabel(str: String): WekaLabel = {
    str match {
      case "win" => WekaLabel.Win
      case "lose" => WekaLabel.Lose
      case "draw" => WekaLabel.Draw
      case "even" => WekaLabel.Even
      case str => throw new RuntimeException("Unknown label: " + str)
    }
  }  
}