package jp.satoyuichiro.inori.weka

import jp.satoyuichiro.inori.cdl.Position
import org.ggp.base.util.statemachine.implementation.prover.ProverStateMachine
import jp.satoyuichiro.inori.gdl.Description
import jp.satoyuichiro.inori.gdl.GdlUtil
import jp.satoyuichiro.inori.player.RandomPlayer
import jp.satoyuichiro.inori.simulator.GameSimulator
import org.ggp.base.util.statemachine.MachineState
import jp.satoyuichiro.inori.player.PlayerUtil

object GeneralizationUtil {
  
  def simulateWinEpisode(gdl: String, n: Int): List[List[Position]] = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(gdl).getDescription)
    val roles = GdlUtil.toScalaList(stateMachine.getRoles())
    val players = List(new RandomPlayer(stateMachine, roles.head),
        new RandomPlayer(stateMachine, roles.tail.head))
    val simulator = GameSimulator(stateMachine, players)
    
    var episodes = List.empty[List[Position]]
    while (episodes.size < n) {
      val tem = simulator.simulateWinEpisode(players.head, (n - episodes.size)) filter (ep => 20 < ep.size)
      episodes ++= (tem map (pls => pls map gdlToCdl))
    }
    episodes
  }
  
  def simulateLoseEpisode(gdl: String, n: Int): List[List[Position]] = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(gdl).getDescription)
    val roles = GdlUtil.toScalaList(stateMachine.getRoles())
    val players = List(new RandomPlayer(stateMachine, roles.head),
        new RandomPlayer(stateMachine, roles.tail.head))
    val simulator = GameSimulator(stateMachine, players)
    
    var episodes = List.empty[List[Position]]
    while (episodes.size < n) {
      val tem = simulator.simulateWinEpisode(players.tail.head, (n - episodes.size)) filter (ep => 20 < ep.size)
      episodes ++= (tem map (pls => pls map gdlToCdl))
    }
    episodes
  }
  
  def gdlToCdl(position: MachineState): Position = PlayerUtil.gdlTocdl(position)

  def getPositionAt(episode: List[Position], n: Int): Position = {
    episode.drop(n - 1).head
  }
  
  def getLast(episode: List[Position]): Position = episode.last

}