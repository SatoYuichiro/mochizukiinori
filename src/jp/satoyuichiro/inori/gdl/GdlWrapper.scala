package jp.satoyuichiro.inori.gdl

import java.util.{List => JList}
import org.ggp.base.util.gdl.grammar.Gdl
import org.ggp.base.util.gdl.factory.GdlFactory
import java.io.File
import jp.satoyuichiro.inori.player.Player

object GdlWrapper {

}

case class Description(val description: List[Gdl]) {
  
  def getDescription: JList[Gdl] = GdlUtil.toJavaList(description)
}

object Description {
  
  def apply(filePath: String): Description = new Description(GdlReader.readGdl(filePath) map (r => GdlFactory.create(r)))
  def apply(file: File): Description = new Description(GdlReader.readGdl(file) map (r => GdlFactory.create(r)))
}

case class PlayerInfo(val roleIndex: Map[Player, Int]) {
  
  def isWinner(player: Player, result: JList[Integer]): Boolean = {
    roleIndex.get(player) match {
      case Some(i) =>
        val score = result.get(i)
        (GdlUtil.toScalaList(result) filter (_ >= score)).size == 1
      case None => false
    }
  }
}