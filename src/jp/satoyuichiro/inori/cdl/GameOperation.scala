package jp.satoyuichiro.inori.cdl

abstract class GameOperation extends Exp

case class Arg(index : EvalInteger, gameElement : EvalGameElement) extends GameOperation with EvalPiece {
  
  def eval(env : Environment): Exp = gameElement.evalGameElement(env).arg(env, index.evalInteger(env))
  def evalPiece(env : Environment): Piece = eval(env).asInstanceOf[Piece]
  
  override def toLisp: String = "(arg " + index.toLisp + " " + gameElement.toLisp + ")"
}

case class GetRole(position : EvalPosition) extends GameOperation {
  
  def eval(env : Environment): Exp =
    position.evalPosition(env).role match {
      case Some(role : Role) => role
      case _ => NullExp()
    }
  
  override def toLisp: String = "(role " + position.toLisp + ")"
}

case class Size(gameElement : EvalGameElement) extends GameOperation with EvalInteger with EvalNumber {
  
  def eval(env : Environment): Exp = evalInteger(env)
  def evalNumber(env : Environment): Number = evalInteger(env)
  def evalInteger(env : Environment): Integer = Integer(gameElement.evalGameElement(env).size)
  
  override def toLisp: String = "(size " + gameElement.toLisp + ")"
}

case class Unificate(piece1 : EvalPiece, piece2 : EvalPiece) extends GameOperation with EvalBool{
  
  def eval(env : Environment): Exp = evalBool(env)
  
  def evalBool(env : Environment): Bool = {
    val p1 =
      piece1 match {
      case p : Piece => p
      case a : Arg => a.evalPiece(env)
      case l : Label => l.evalPiece(env)
    }
    
    val p2 =
      piece2 match {
      case p : Piece => p
      case a : Arg => a.evalPiece(env)
      case l : Label => l.evalPiece(env)
    }

    if (p1.args.size != p2.args.size)
      return Bool(false)
    else {
      def single(t : Tuple2[Exp, Exp]): Boolean =
        t match {
        case (Label("_"), exp : Exp) => true
        case (exp : Exp, Label("_")) => true
        case (label1 : Label, label2 : Label) => label1.eval(env) == label2.eval(env)
        case (label : Label, exp : Exp) => eqput(label, exp)
        case (exp : Exp, label : Label) => eqput(label, exp)
        case (exp1 : Exp, exp2 : Exp) => exp1 == exp2
      }
      
      def eqput(label : Label, exp : Exp): Boolean = {
          try {
            label.eval(env) == exp
          } catch {
            case e : Exception =>
              env.put(label, exp)
              true
          }
      }
      Bool((p1.args.zip(p2.args) map single).fold(true)(_ && _))
    }
  }
  
  override def toLisp: String = "(unificate " + piece1.toLisp + " " + piece2.toLisp + ")"
}