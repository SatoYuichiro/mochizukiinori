package jp.satoyuichiro.inori.cdl

abstract class Code extends Exp
case class Substitution(label : Label, value : Exp) extends Lisp {
  
  override def toLisp: String = label.toLisp + " " + value.toLisp
}

case class Let(variableList : List[Substitution], body : Body) extends Code {
  
  def eval(env : Environment): Exp = body.eval(env.set(variableList))
  
  override def toLisp: String = {
    val variableListstr = (for (exp <- variableList) yield exp.toLisp + " ").fold("")(_ + _)
    "(let (" + variableListstr.substring(0, variableListstr.length - 1) + ") " + body.toLisp + ")"
  }
}

case class Body(body : List[Exp]) extends Code {
  
  def eval(env : Environment): Exp = {
    if (0 < body.size) {
      body.take(body.size - 1) foreach {
        e => e.eval(env)
      }
    }
    
    body.last.eval(env)
  }
  
  override def toLisp: String = {
    body.length match {
      case 1 => body.head.toLisp
      case _ =>
        val bodystr = (for (exp <- body) yield exp.toLisp + " ").fold("")(_ + _)
        "(" + bodystr.substring(0, bodystr.length - 1) + ")"
    }
  }
}

object Body {
  
  def apply(exps : Exp*): Body = Body(exps.toList)
}

case class If(evalBool : EvalBool, trueBody : Body, falseBody : Body) extends Code {
  
  def eval(env : Environment): Exp = if (evalBool.evalBool(env).value) trueBody.eval(env) else falseBody.eval(env)
  
  override def toLisp: String = {
    val cond =
      evalBool match {
      case b : Bool => "(" + b.toLisp + ")"
      case _ => evalBool.toLisp
    }
    "(if " + cond + " " + trueBody.toLisp + " " + falseBody.toLisp + ")"
  }
}

case class For(index : List[Substitution], condition : EvalBool, incriment : Body, body : Body) extends Code {
  
  def eval(env : Environment): Exp = {
    val newEnv = env.set(index)
    var returnValue : Exp = NullExp()
    while(condition.evalBool(newEnv).value) {
      returnValue = body.eval(newEnv)
      incriment.eval(newEnv)
    }
    returnValue
  }
  
  override def toLisp: String = {
    val indexstr = (for (exp <- index) yield exp.toLisp + " ").fold("")(_ + _)
    val cond =
      condition match {
      case b : Bool => "(" + b.toLisp + ")"
      case _ => condition.toLisp
    }
    "(for (" + indexstr.substring(0, indexstr.length - 1) + ") " + cond + " " + incriment.toLisp + " " +body.toLisp + ")"
  }
}

case class Set(variable : Label, value : Exp) extends Code {
  
  def eval(env : Environment): Exp = {
    env.put(variable, value.eval(env))
    NullExp()
  }
  
  override def toLisp: String = "(set " + variable.toLisp + " " + value.toLisp + ")"
}