package jp.satoyuichiro.inori.cdl

object Parser {
  
  val startParenthesis = '('
  val endParenthesis = ')'
  
  def parse(code : String): ParseTree = {
    if (checkParenthesis(code))
      makeParseTree(code)
    else
      throw new ParseErrorException("parenthesis error")
  }
  
  def checkParenthesis(code : String): Boolean = {
    if (code.charAt(0) == startParenthesis) {
      var i = 0
      for (c <- code.toCharArray()) {
        i = countParenthesis(c, i)
        if (i < 0)
          return false
      }
      return i == 0
    }
    false
  }
  
  def countParenthesis(c : Char, i : Int): Int = {
    if (c == startParenthesis)
      i + 1
    else if (c == endParenthesis)
      i - 1
    else
      i
  }
  
  def makeParseTree(code : String): ParseTree =
    if (Syntax.isLispBlock(code)) makeNode(code) else makeLeaf(code)
  
  def makeNode(code : String): ParseTree =
    Cutter.lispSplit(code) match {
        case head :: tail =>
          head match {
            case op if (Syntax.isStatementTag(head)) => StatementMaker.make(op, tail)
            case code if (Syntax.isCodeKeywordTag(head)) => CodeMaker.make(code, tail)
            case gamep if (Syntax.isGamePositionTag(head)) => GamePositionMaker.make(gamep, tail)
            case gameop if (Syntax.isGameOperationTag(head)) => GameOperationMaker.make(gameop, tail)
            case concept if (Syntax.isConcpetTag(head)) => ConceptMaker.make(concept, tail)
            case label if (Syntax.isLabelLeaf(label)) => ConceptMaker.makeConceptCall(label, tail)
            case _ => throw new ParseErrorException(head + " is not supported")
          }
        case _ => throw new ParseErrorException("something wrong under cutting lisp block")
      }
  
  object StatementMaker {
    
    def make(head : String, tail : List[String]): StatementParseTree =
      head match {
        case a if (Syntax.isArithmeticTag(head)) => makeArithmetic(a, tail)
        case l if (Syntax.isLogicTag(head)) => makeLogic(l, tail)
        case c if (Syntax.isConditionTag(head)) => makeCondition(c, tail)
      }

    def makeArithmetic(a : String, tail : List[String]): ArithmeticParseTree =
      ArithmeticParseTree(Tag(a), tail map makeParseTree)
  
    def makeLogic(l : String, tail : List[String]): LogicParseTree =
      LogicParseTree(Tag(l), tail map makeParseTree)
  
    def makeCondition(c : String, tail : List[String]): ConditionParseTree =
      (tail map makeParseTree) match {
        case ls if (ls.size == 2) => ConditionParseTree(Tag(c), ls.head, ls.tail.head)
        case _ => throw new ParseErrorException("number of arguments is wrong")
    }
  }

  object CodeMaker {
    
    def make(code : String, tail : List[String]): CodeParseTree =
      code match {
        case "let" => makeLet(tail)
        case "if" => makeIf(tail)
        case "for" => makeFor(tail)
        case "set" => makeSet(tail)
        case _ => throw new ParseErrorException(code + " is not a keyword or argument is wrong")
    }
  
    def makeLet(codeList : List[String]): LetParseTree =
      codeList.headOption match {
        case Some(env) =>
          codeList.tail.headOption match {
            case Some(body) => LetParseTree(makeSubstitution(env), makeBody(body))
            case None => throw new ParseErrorException("let has no body")
        }
        case None => throw new ParseErrorException("let has no enviromnent")
      }
    
    def makeSubstitution(env : String): List[SubstitutionParseTree] = {
      def rec(codeList : List[String], result : List[SubstitutionParseTree]): List[SubstitutionParseTree] =
        if (codeList.size == 0)
          result.reverse
          else
            codeList.headOption match {
            case Some(label) =>
              codeList.tail.headOption match {
                case Some(exp) => rec(codeList.tail.tail, SubstitutionParseTree(LabelLeaf(label), makeParseTree(exp)) :: result)
                case None => throw new ParseErrorException("missing expression")
              }
            case None => result.reverse
          }
      rec(Cutter.lispSplit(env), List.empty[SubstitutionParseTree])
    }
    
    def makeIf(codeList : List[String]): IfParseTree =
      codeList.headOption match {
        case Some(str) => Cutter.lispSplit(str) match {
          case c :: ctail if (Syntax.isEvalToBoolTag(c)) =>
            codeList.tail.headOption match {
              case Some(trueBody) =>
                codeList.tail.tail.headOption match {
                  case Some(falseBody) => IfParseTree(makeEvalToBool(c, ctail), makeBody(trueBody), makeBody(falseBody))
                  case _ => throw new ParseErrorException("if does not have false body")
                }
              case _ => throw new ParseErrorException("if does not have true body")
            }
          case _ => throw new ParseErrorException(str + " is not condition")
        }
        case _ => throw new ParseErrorException("if does not have condition")
      }
  
    def makeEvalToBool(c : String, tail : List[String]): EvalToBoolParseTree =
      c match {
        case bool if (Syntax.isBooleanLeaf(bool)) => BooleanLeaf(bool.toBoolean)
        case logc if (Syntax.isLogicTag(logc)) => StatementMaker.makeLogic(logc, tail)
        case conp if (Syntax.isConditionTag(conp)) => StatementMaker.makeCondition(conp, tail)
        case label if (Syntax.isLabelLeaf(label)) => LabelLeaf(label)
        case _ => throw new ParseErrorException("")
      }
    
    def makeFor(codeList : List[String]): ForParseTree =
      codeList.headOption match {
        case Some(code1) =>
          codeList.tail.headOption match {
            case Some(code2) =>
              Cutter.lispSplit(code2) match {
                case c :: ctail if (Syntax.isEvalToBoolTag(c)) =>
                  codeList.tail.tail.headOption match {
                    case Some(code3) =>
                      codeList.tail.tail.tail.headOption match {
                        case Some(code4) => ForParseTree(makeIndex(code1), makeEvalToBool(c, ctail), makeBody(code3), makeBody(code4))
                        case None => throw new ParseErrorException("for body is none")
                      }
                    case None => throw new ParseErrorException("for incrimental body is none")
                  }
                case _ => throw new ParseErrorException("for condition is wrong")
              }
            case None => throw new ParseErrorException("for conditon is none")
          }
        case _ => throw new ParseErrorException("for index is none")
      }
  
    def makeIndex(code : String): IndexParseTree =
      Cutter.lispSplit(code) match {
        case list if (list.size % 2 == 0) => IndexParseTree(makeSubstitution(code))
        case _ => throw new ParseErrorException("index part is wrong")
      }
    
    def makeSet(tail : List[String]): SetParseTree =
      SetParseTree(LabelLeaf(tail.head), makeParseTree(tail.tail.head))
    
    def makeBody(body : String): BodyParseTree =
      if (Syntax.isLispBody(body))
        BodyParseTree(Cutter.lispSplit(body) map makeParseTree)
      else
        makeSingleBody(body)
    
    def makeSingleBody(body : String): BodyParseTree =
      BodyParseTree(List(makeParseTree(body)))
  }
  
  object GamePositionMaker {
    
    def make(gamep : String, tail : List[String]): GameParseTree =
      gamep match {
      case "piece" => makePiece(tail)
      case "position" => makePosition(tail)
      case "role" => makeRole(tail)
      case "message" => makeMessage(tail)
    }
    
    def makeEvalToGameElement(gamep : String, tail : List[String]): EvalToGameElementParseTree =
      gamep match {
      case "piece" => makePiece(tail)
      case "position" => makePosition(tail)
      case "role" => makeRole(tail)
      case "message" => makeMessage(tail)
      case _ => LabelLeaf(gamep)
    }
    
    def makePiece(tail : List[String]): PieceParseTree =
      PieceParseTree(tail map makeParseTree)
    
    def makePosition(tail : List[String]): PositionParseTree = {
      val trees = tail map makeParseTree
      val pieces = trees filter (_.isInstanceOf[EvalToPieceParseTree]) map (_.asInstanceOf[EvalToPieceParseTree])
      val role = trees filter (_.isInstanceOf[RoleParseTree]) map (_.asInstanceOf[RoleParseTree])
      PositionParseTree(pieces, role.headOption)
    }
    
    def makeRole(tail : List[String]): RoleParseTree =
      tail.headOption match {
      case Some(str) => RoleParseTree(makeParseTree(str))
      case None => throw new ParseErrorException("role has no argument")
    }
    
    def makeMessage(tail : List[String]): MessageParseTree =
      MessageParseTree(tail map makeParseTree)
  }
  
  object GameOperationMaker {
    
    def make(gameop : String, tail : List[String]): GameOperationParseTree =
      gameop match {
        case "arg" => makeGetArg(tail)
        case "get-role" => makeGetRole(tail)
        case "size" => makeGetSize(tail)
        case "unificate" => makeUnificate(tail)
        case _ => throw new ParseErrorException(gameop + " is not a game operation")
    }
    
    def makeGetArg(codeList : List[String]): GetArgParseTree =
      codeList.headOption match {
      case Some(code) =>
        if (Syntax.isLispBlock(code))
        Cutter.lispSplit(code) match {
          case i :: itail if (Syntax.isEvalToIntTag(i)) => makeGetArg2(makeEvalToInt(i, itail), codeList)
          case _ => throw new ParseErrorException("arg index is wrong")
        }
        else if (Syntax.isIntLeaf(code))
          makeGetArg2(IntLeaf(code.toInt), codeList)
        else if (Syntax.isLabelLeaf(code))
          makeGetArg2(LabelLeaf(code), codeList)
        else throw new ParseErrorException("arg index is none")
      case None => throw new ParseErrorException("arg index is none")
    }
    
    def makeGetArg2(index : EvalToIntParseTree, codeList : List[String]): GetArgParseTree = {
      codeList.tail.headOption match {
        case Some(code) =>
          if (Syntax.isLispBlock(code))
             Cutter.lispSplit(code) match {
               case head :: tail if (Syntax.isGamePositionTag(head)) => GetArgParseTree(index, GamePositionMaker.makeEvalToGameElement(head, tail))
               case head :: Nil if (Syntax.isLabelLeaf(head)) => GetArgParseTree(index, LabelLeaf(head))
               case _ => throw new ParseErrorException("arg targe is wrong")
             }
          else if (Syntax.isLabelLeaf(code))
            GetArgParseTree(index, LabelLeaf(code))
          else throw new ParseErrorException("arg target is none")
        case None => throw new ParseErrorException("arg target is none")
      }
    }
    
    def makeEvalToInt(i : String, tail : List[String]): EvalToIntParseTree =
      i match {
        case int if (Syntax.isIntLeaf(int)) => IntLeaf(int.toInt)
        case ari if (Syntax.isArithmeticTag(ari)) => StatementMaker.makeArithmetic(ari, tail)
        case label if (Syntax.isLabelLeaf(label)) => LabelLeaf(label)
        case _ => throw new ParseErrorException("")
      }
    
    def makeGetSize(codeList : List[String]): GetSizeParseTree =
      codeList.headOption match {
        case Some(code) =>
          if (Syntax.isLispBlock(code)) {
            Cutter.lispSplit(code) match {
              case head :: tail => GetSizeParseTree(GamePositionMaker.makeEvalToGameElement(head, tail))
              case _ => throw new ParseErrorException("size target is wrong")
            }
          } else if (Syntax.isLabelLeaf(code)){
            GetSizeParseTree(LabelLeaf(code))
          }
          else throw new ParseErrorException("size index is none")
        case None => throw new ParseErrorException("size index is none")
      }
      
    def makeGetRole(tail : List[String]): GetRoleParseTree =
      tail.headOption match {
        case Some(code) =>
          if (Syntax.isLispBlock(code)) {
            Cutter.lispSplit(code) match {
              case "position" :: tail => GetRoleParseTree(GamePositionMaker.makePosition(tail))
              case _ => throw new ParseErrorException("get-role target is wrong")
            }
          }
          else if (Syntax.isLabelLeaf(code)) {
            GetRoleParseTree(LabelLeaf(code))
          }
          else throw new ParseErrorException("get-role target is wrong")
        case None => throw new ParseErrorException("get-role target is none")
      }
    
    def makeUnificate(codeList : List[String]): UnificateParseTree =
      codeList.headOption match {
        case Some(code1) =>
          codeList.tail.headOption match {
            case Some(code2) => UnificateParseTree(makeUnificateArgument(code1), makeUnificateArgument(code2))
            case None => throw new ParseErrorException("unificate second target is none")
          }
        case None => throw new ParseErrorException("unificate first target is none")
      }
    
    def makeUnificateArgument(code : String): EvalToPieceParseTree =
      if (Syntax.isLispBlock(code))
        Cutter.lispSplit(code) match {
        case "piece" :: tail => GamePositionMaker.makePiece(tail)
        case "arg" :: tail => makeGetArg(tail)
        case _ => throw new ParseErrorException("unificate target is wrong")
      }
      else if (Syntax.isLabelLeaf(code)) LabelLeaf(code)
      else throw new ParseErrorException("unificate target is wrong")
  }
  
  object ConceptMaker {
    
    def make(concept : String, tail : List[String]): ConceptParseTree =
      concept match {
      case "pattern" => makeConcept(new PatternParseTree, tail)
      case "update" => makeConcept(new UpdateParseTree, tail)
      case "transition" => makeConcept(new TransitionParseTree, tail)
      case "heuristics" => makeConcept(new HeuristicsParseTree, tail)
      case "recognition" => makeConcept(new RecognitionParseTree, tail)
      case "representation" => makeConcept(new RepresentationParseTree, tail)
      case "concept" => makeConcept(new HigherParseTree, tail)
    }
    
    def makeConceptCall(conceptName : String, codeList : List[String]): ConceptCallParseTree =
      ConceptCallParseTree(LabelLeaf(conceptName), codeList map makeCallArg)
    
    def makeConcept(conceptType : ConceptTypeParseTree, codeList : List[String]): ConceptParseTree =
      codeList.headOption match {
        case Some(name) =>
          codeList.tail.headOption match {
            case Some(args) =>
              codeList.tail.tail.headOption match {
                case Some(body) => ConceptParseTree(conceptType, LabelLeaf(name), makeArgs(args), makeParseTree(body))
                case _ => throw new ParseErrorException("")
              }
            case _ => throw new ParseErrorException("")
          }
        case _ => throw new ParseErrorException("")
      }

  def makeArgs(args : String): List[ArgParseTree] =
    Cutter.lispSplit(args) map makeArg
  
  def makeArg(arg : String): ArgParseTree =
    if (Syntax.isLispBlock(arg)) {
      val cut = Cutter.lispSplit(arg)
      cut.headOption match {
        case Some(labelName) =>
          cut.tail.headOption match {
            case Some(":") =>
              cut.tail.tail.headOption match {
                case Some(typeName) => ArgParseTree(LabelLeaf(labelName), Some(Tag(typeName)))
                case None => throw new ParseErrorException("type is null")
              }
            case Some(str) => throw new ParseErrorException(": is expected but " + str + " is found")
            case None => throw new ParseErrorException(": is excected but not found")
          }
        case None => throw new ParseErrorException("arg name is null")
      }
    }
    else
      ArgParseTree(LabelLeaf(arg), None)
  
  def makeCallArg(arg : String): CallArgParseTree =
    CallArgParseTree(makeParseTree(arg), None)
  }
  
  def makeLeaf(code : String): ParseTree =
    code match {
      case ivalue if (Syntax.isIntLeaf(code)) => IntLeaf(ivalue.toInt)
      case dvalue if (Syntax.isDoubleLeaf(code)) => DoubleLeaf(dvalue.toDouble)
      case symbol if (Syntax.isSymbolLeaf(code)) => StringLeaf(symbol)
      case bvalue if (Syntax.isBooleanLeaf(code)) => BooleanLeaf(bvalue.toBoolean)
      case label if (Syntax.isLabelLeaf(code)) => LabelLeaf(label)
      case _ => throw new ParseErrorException(code + " is not Literal")
  }
}

object Cutter {
  
  def removeFirstLast(code : String): String =
    code.trim.substring(1, code.length - 1).trim
  
  def lispSplit(code : String): List[String] = {
    def doLispSplit(code : String, result : List[String]): List[String] = {
      if (code.length == 0)
        result.reverse
      else {
        val split = if (code.startsWith("(")) cutLispBlock(code) else cutLispLiteral(code)
        doLispSplit(split._2.trim, split._1 :: result)
      }
    }
    doLispSplit(Cutter.removeFirstLast(code), List.empty[String])
  }
  
  def cutLispBlock(code : String): Tuple2[String, String] = {
    def rec(code : String, count : Int, head : String): Tuple2[String, String] =
      if (count == 0 || code.length == 0)
        (head, code)
      else {
        val c = code.charAt(0)
        rec(code.drop(1), Parser.countParenthesis(c, count), head + c)
      }
    rec(code.drop(1), 1, code.substring(0,1))
  }
  
  def cutLispLiteral(code : String): Tuple2[String, String] = {
    val cut = code.trim.split(" ")
    (cut.head, code.trim.drop(cut.head.length).trim)
  }
}

object Syntax {
  
  val arithmeticOperators = List("+", "-", "*", "/", "%")
  val logicOperators = List("not", "and", "or", "eq")
  val conditionOperators = List("<", ">", "=")
  val operators = arithmeticOperators ++ logicOperators ++ conditionOperators
  
  val codeKeywords = List("let", "if", "for", "set")
  
  val gamePositions = List("piece", "position", "role", "message")
  val gameOperators = List("arg", "get-role", "size", "unificate")
  
  val conceptKeywords = List("concept", "pattern", "update", "transition", "heuristics", "recognition", "representation")
  
  val keywords = operators ++ codeKeywords ++ gamePositions ++ gameOperators ++ conceptKeywords
  
  def isLispBlock(code : String): Boolean = code.startsWith("(") && code.endsWith(")")
  
  def isLispBody(code : String): Boolean = {
    if (isLispBlock(code)) {
      val cut = Cutter.lispSplit(code)
      isLispBlock(cut.head)
    }
    else
      false
  }
  
  def isIntLeaf(code : String): Boolean = !code.contains(".") && isInt(code)
  
  def isInt(code : String) = {
    try {
      code.toInt
      true
    } catch {
      case e : Exception => false
    }
  }
  
  def isDoubleLeaf(code : String): Boolean = code.contains(".") && isDouble(code)
  
  def isDouble(code : String): Boolean = {
    try {
      code.toDouble
      true
    } catch {
      case e : Exception => false
    }
  }
  
  def isEvalToBoolTag(code : String): Boolean = isBooleanLeaf(code) || isLabelLeaf(code) || isLogicTag(code) || isConditionTag(code)
  def isEvalToIntTag(code : String): Boolean = isIntLeaf(code) || isLabelLeaf(code) || isArithmeticTag(code)
  
  def isSymbolLeaf(code : String): Boolean = code.startsWith("\"") && code.endsWith("\"")
  def isBooleanLeaf(code : String): Boolean = code == "true" || code == "false"
  def isLabelLeaf(code : String): Boolean = !keywords.contains(code)
  
  def isStatementTag(code : String): Boolean = operators.contains(code)
  def isArithmeticTag(code : String): Boolean = arithmeticOperators.contains(code)
  def isLogicTag(code : String): Boolean = logicOperators.contains(code)
  def isConditionTag(code : String): Boolean = conditionOperators.contains(code)
  
  def isCodeKeywordTag(code : String): Boolean = codeKeywords.contains(code)
  
  def isGamePositionTag(code : String): Boolean = gamePositions.contains(code)
  def isGameOperationTag(code : String): Boolean = gameOperators.contains(code)
 
  def isConcpetTag(code : String): Boolean = conceptKeywords.contains(code)
  
  def isKeywords(code : String): Boolean = keywords.contains(code)
}

class ParseErrorException(message :String = null, cause :Throwable = null) extends RuntimeException(message, cause)