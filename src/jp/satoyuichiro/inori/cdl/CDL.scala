package jp.satoyuichiro.inori.cdl

import java.io.BufferedReader
import java.io.InputStreamReader
import scala.io.Source
import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.nio.charset.Charset

object CLD {
  
  def main(args : Array[String]): Unit = {
    println("Concept Description Language Interpreter\n")
    val input = Source.fromInputStream(System.in)
    val interpreter = new Interpreter
    var code = ""
    
    for(line <- input.getLines) {
      if (line == "exit") {
        input.close()
        return
      }
      else {
        code += line
        if (Parser.checkParenthesis(code)) {
          try {
            interpreter.debug(code)
            code = ""
          } catch {
            case e : Exception =>
              println(code)
              e.printStackTrace()
              code = ""
          }
        }
      }
    }
  }
}

class Interpreter {
  
  val env = new Environment(Environment.empty)
  
  def debug(str : String): Unit = {
    val tree = Parser.parse(str)
    println("ParseTree " + tree)
    val code = Compiler.compile(tree)
    println("Code " + code)
    println("Result " + code.eval(env))
  }
  
  def interpretPrint(code : String): Unit =
    println(interpret(code))
  
  def interpretPrint(exp : Exp): Unit =
    println(interpret(exp))
  
  def interpret(code : String): Exp =
    Compiler.compile(Parser.parse(code)).eval(env)
  
  def interpret(exp : Exp): Exp =
    exp.eval(env)
  
  def interpret(path: Path, charset: Charset): Exp = {
    val lines = Files.readAllLines(path, charset).iterator()
    var res = null: Exp
    while(lines.hasNext()) {
      res = interpret(lines.next())
    }
    res
  }
  
  def refresh(): Unit = env.refresh()
}