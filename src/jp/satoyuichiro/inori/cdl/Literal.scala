package jp.satoyuichiro.inori.cdl

trait LeafExp extends Exp

abstract class Literal extends Exp with LeafExp {
  override def eval(env : Environment): Exp = this
}

object Literal {
  
  implicit def intToInteger(i : Int): Integer = Integer(i)
  implicit def doubleToReal(d : Double): Real = Real(d)
  implicit def stringToSymbol(s : String): Symbol = Symbol(s)
  implicit def booleanToBool(b : Boolean): Bool = Bool(b)
}

case class Label(label : String) extends Exp with LeafExp with EvalInteger with EvalNumber with EvalBool with EvalGameElement with EvalPiece with EvalPosition{
  
  override def eval(env : Environment): Exp = env.getValue(this)
  def evalInteger(env : Environment): Integer = env.getValue(this).asInstanceOf[Integer]
  def evalNumber(env : Environment): Number = env.getValue(this).asInstanceOf[Number]
  def evalBool(env : Environment): Bool = env.getValue(this).asInstanceOf[Bool]
  def evalGameElement(env : Environment): GameElement = env.getValue(this).asInstanceOf[GameElement]
  def evalPiece(env : Environment): Piece = env.getValue(this).asInstanceOf[Piece]
  def evalPosition(env : Environment): Position = env.getValue(this).asInstanceOf[Position]
  
  override def toLisp: String = label
}

abstract class Number extends Literal with EvalInteger with EvalNumber{
  
  def evalNumber(env : Environment): Number = this
  def evalInteger(env : Environment): Integer =
    this match {
      case i : Integer => i
      case r : Real => Integer(r.value.toInt)
    }
  
  def + (n : Number): Number
  def - (n : Number): Number
  def * (n : Number): Number
  def / (n : Number): Number
  def % (n : Number): Number
  
  def < (n : Number): Boolean
  def > (n : Number): Boolean
}

case class Integer(value : Int) extends Number {
  
  def + (n : Number): Number =
    n match {
      case i : Integer => Integer(value + i.value)
      case r : Real => Real(value.toDouble + r.value)
    }
  
  def - (n : Number): Number =
    n match {
      case i : Integer => Integer(value - i.value)
      case r : Real => Real(value.toDouble - r.value)
    }

  def * (n : Number): Number =
    n match {
      case i : Integer => Integer(value * i.value)
      case r : Real => Real(value.toDouble * r.value)
    }

  def / (n : Number): Number =
    n match {
      case i : Integer => Real(value.toDouble / i.value.toDouble)
      case r : Real => Real(value.toDouble / r.value)
    }
  
  def % (n : Number): Number =
    n match {
      case i : Integer => Integer(value % i.value)
      case r : Real => Real(value.toDouble % r.value)
    }
  
  def < (n : Number): Boolean =
    n match {
    case i : Integer => value < i.value
    case r : Real => value < r.value
  }

  def > (n : Number): Boolean =
    n match {
    case i : Integer => value > i.value
    case r : Real => value > r.value
  }
  
  override def toLisp: String = value.toString
}

case class Real(value : Double) extends Number {
  
  def + (n : Number): Number =
    n match {
      case i : Integer => Real(value + i.value.toDouble)
      case r : Real => Real(value + r.value)
    }
  
  def - (n : Number): Number =
    n match {
      case i : Integer => Real(value - i.value.toDouble)
      case r : Real => Real(value - r.value)
    }

  def * (n : Number): Number =
    n match {
      case i : Integer => Real(value * i.value.toDouble)
      case r : Real => Real(value * r.value)
    }

  def / (n : Number): Number =
    n match {
      case i : Integer => Real(value / i.value.toDouble)
      case r : Real => Real(value / r.value)
    }

  def % (n : Number): Number =
    n match {
      case i : Integer => Real(value % i.value.toDouble)
      case r : Real => Real(value % r.value)
    }
  
  def < (n : Number): Boolean =
    n match {
    case i : Integer => value < i.value
    case r : Real => value < r.value
  }

  def > (n : Number): Boolean =
    n match {
    case i : Integer => value > i.value
    case r : Real => value > r.value
  }
  
  override def toLisp = value.toString
}

case class Bool(value : Boolean) extends Literal with EvalBool{
  
  def evalBool(env : Environment): Bool = this
  
  def &&(b : Bool): Bool = Bool(value && b.value)
  def ||(b : Bool): Bool = Bool(value || b.value)
  def ==(b : Bool): Bool = Bool(value == b.value)
  
  override def toLisp: String = value.toString
}

case class Symbol(value : String) extends Literal {
  
  override def toLisp: String = "\"" + value + "\""
}

case class NullExp() extends Literal {
  override def toLisp: String = ""
}