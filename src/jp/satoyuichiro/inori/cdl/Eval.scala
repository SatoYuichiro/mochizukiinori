package jp.satoyuichiro.inori.cdl

trait Lisp {
  def toLisp: String = toString
}

trait Eval extends Lisp {
  
  def eval(env : Environment): Exp
}

trait EvalBool extends Lisp {
  
  def evalBool(env : Environment): Bool
}

trait EvalInteger extends Lisp {
  
  def evalInteger(env : Environment): Integer
}

trait EvalNumber extends Lisp {
  
  def evalNumber(env : Environment): Number
}

trait EvalSymbol extends Lisp {
  
  def evalSymbol(env : Environment): Symbol
}

trait EvalGameElement extends Lisp {
  
  def evalGameElement(env : Environment): GameElement
}

trait EvalPiece extends Lisp {
  
  def evalPiece(env : Environment): Piece
}

trait EvalPosition extends Lisp {
  
  def evalPosition(env : Environment): Position
}

abstract class Exp extends Eval
