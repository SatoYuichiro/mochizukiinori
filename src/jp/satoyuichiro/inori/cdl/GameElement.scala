package jp.satoyuichiro.inori.cdl

abstract class GameElement extends Exp with EvalGameElement {
  
  def arg(env : Environment, index : Integer): Exp
  def size: Int
}

case class Piece(args : List[Exp]) extends GameElement with EvalPiece {
  
  def eval(env : Environment): Exp = evalPiece(env)
  def evalGameElement(env : Environment): GameElement = evalPiece(env)
  def evalPiece(env : Environment): Piece = Piece(args map (_.eval(env)))
  
  def arg(env : Environment, index : Integer): Exp = (args map (_.eval(env))).take(index.value + 1).last
  def size: Int = args.size
  
  override def toLisp: String = {
    val argsstr = (for (exp <- args) yield " " + exp.toLisp).fold("")(_ + _)
    "(piece" + argsstr + ")"
  }
}

object Piece {
  
  def apply(exps : Exp*): Piece = Piece(exps.toList)
}

case class Position(position : List[EvalPiece], role : Option[Role]) extends GameElement with EvalPosition {
  
  def eval(env : Environment): Position = Position(position map (_.evalPiece(env)), role flatMap (r => Some(r.eval(env))))
  def evalGameElement(env : Environment): GameElement = eval(env)
  def evalPosition(env : Environment): Position = eval(env)
  
  def arg(env : Environment, index : Integer): Exp = (position map (_.evalPiece(env))).take(index.value + 1).last
  def size: Int = position.size
  
  override def toLisp: String = {
    val positionstr = (for (exp <- position) yield " " + exp.toLisp).fold("")(_ + _)
    val rolestr =
      role match {
      case Some(r) => " " + r.toLisp
      case None => ""
    }
    "(position" + positionstr + rolestr + ")"
  }
}

object Position {
  
  def apply(pieces : Piece*): Position = Position(pieces.toList, None)
  def apply(role: Role, pieces : Piece*): Position = Position(pieces.toList, Some(role))
}

case class Role(role : Exp) extends GameElement {
  
  def eval(env : Environment): Role = Role(role.eval(env))
  def evalGameElement(env : Environment): GameElement = eval(env)
  
  def arg(env : Environment, index : Integer): Exp = if (index.value == 1) role.eval(env) else throw new Exception()
  def size: Int = 1
  
  override def toLisp: String = "(role " + role.toLisp + ")"
}

case class Message(messages : List[Exp]) extends GameElement {
  
  def eval(env : Environment): Message = Message(messages map (_.eval(env)))
  def evalGameElement(env : Environment): GameElement = eval(env)
  
  def arg(env : Environment, index : Integer): Exp = (messages map (_.eval(env))).take(index.value + 1).last
  def size: Int = messages.size
  
  override def toLisp: String = {
    val messagesstr = (for (exp <- messages) yield " " + exp.toLisp).fold("")(_ + _)
    "(message" + messagesstr + ")"
  }
}