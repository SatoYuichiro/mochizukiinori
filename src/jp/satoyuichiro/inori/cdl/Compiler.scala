package jp.satoyuichiro.inori.cdl

object Compiler {
  
  def compile(code : String): Exp = compile(Parser.parse(code))
  
  def compile(tree : ParseTree): Exp = {
    tree match {
      case leaf : Leaf[_] => makeLeaf(leaf)
      case stm : StatementParseTree => makeStatement(stm)
      case code : CodeParseTree => makeCode(code)
      case game : GameParseTree => makeGame(game)
      case gameop : GameOperationParseTree => makeGameOP(gameop)
      case con : ConceptParseTree => makeConcept(con)
      case call : ConceptCallParseTree => makeConceptCall(call)
      case _ => throw new CompileErrorException(tree + " is not suppoted")
    }
  }
  
  def makeLeaf(leaf : Leaf[_]) : Exp = {
    leaf match {
      case i : IntLeaf => Integer(i.value)
      case d : DoubleLeaf => Real(d.value)
      case s : StringLeaf => Symbol(s.value)
      case b : BooleanLeaf => Bool(b.value)
      case l : LabelLeaf => Label(l.value)
    }
  }
  
  def makeStatement(stm : StatementParseTree): Exp =
    stm match {
    case ari : ArithmeticParseTree => makeArithmetic(ari)
    case log : LogicParseTree => makeLogic(log)
    case con : ConditionParseTree => makeCondition(con)
  }
  
  def makeArithmetic(ari : ArithmeticParseTree): Arithmetic =
    ari.operator match {
      case Tag("+") => Addition(ari.args map compile)
      case Tag("-") => Subtraction(ari.args map compile)
      case Tag("*") => Multiplication(ari.args map compile)
      case Tag("/") => Division(ari.args map compile)
      case Tag("%") => Modulo(ari.args map compile)
      case _ => throw new CompileErrorException(ari.operator + " is not an arithmetic operator")
    }
  
  def makeLogic(log : LogicParseTree): Logic =
    log.operator match {
    case Tag("not") => Not(log.args map compile)
    case Tag("and") => And(log.args map compile)
    case Tag("or") => Or(log.args map compile)
    case Tag("eq") => Eq(log.args map compile)
    case _ => throw new CompileErrorException(log.operator + " is not a logic operator")
  }
  
  def makeCondition(con : ConditionParseTree): Condition =
    con.operator match {
    case Tag("<") => Greater(makeEvalNumber(con.arg1), makeEvalNumber(con.arg2))
    case Tag(">") => Less(makeEvalNumber(con.arg1), makeEvalNumber(con.arg2))
    case Tag("=") => Equals(compile(con.arg1), compile(con.arg2))
    case _ => throw new CompileErrorException(con.operator + " is not a condition operator")
  }
  
  def makeEvalNumber(pt : ParseTree): EvalNumber =
    pt match {
    case ari : ArithmeticParseTree => makeArithmetic(ari)
    case i : IntLeaf => Integer(i.value)
    case r : DoubleLeaf => Real(r.value)
    case label : LabelLeaf => Label(label.value)
    case size : GetSizeParseTree => Size(makeEvalToGameElement(size.gameObject))
    case call : ConceptCallParseTree => makeConceptCall(call)
  }
  
  def makeEvalInteger(pt : ParseTree): EvalInteger =
    pt match {
    case i : IntLeaf => Integer(i.value)
    case ari : ArithmeticParseTree => makeArithmetic(ari)
    case label : LabelLeaf => Label(label.value)
    case size : GetSizeParseTree => Size(makeEvalToGameElement(size.gameObject))
  }
  
  def makeCode(code : CodeParseTree): Code =
    code match {
    case letpt : LetParseTree => Let(makeSubstitution(letpt.env), makeBody(letpt.body))
    case ifpt : IfParseTree => If(makeEvalToBool(ifpt.condition), makeBody(ifpt.trueBody), makeBody(ifpt.falseBody))
    case forpt : ForParseTree => For(makeSubstitution(forpt.index.indexs), makeEvalToBool(forpt.condition), makeBody(forpt.incriment), makeBody(forpt.body))
    case setpt : SetParseTree => Set(Label(setpt.label.value), compile(setpt.value))
    case _ => throw new CompileErrorException(code + " is not top level CodeParseTree")
  }
  
  def makeEvalToBool(toBool : EvalToBoolParseTree): EvalBool =
    toBool match {
    case log : LogicParseTree => makeLogic(log)
    case con : ConditionParseTree => makeCondition(con)
    case label : LabelLeaf => Label(label.value)
  }
  
  def makeSubstitution(tls : List[SubstitutionParseTree]): List[Substitution] =
    tls map (t => Substitution(Label(t.label.value), compile(t.value)))
  
  def makeBody(body : BodyParseTree): Body = Body(body.body map compile)
  
  def makeGame(game : GameParseTree): GameElement =
    game match {
    case piece : PieceParseTree => makePiece(piece)
    case position : PositionParseTree => makePosition(position)
    case role : RoleParseTree => Role(compile(role.role))
    case message : MessageParseTree => Message(message.messages map compile)
    case _ => throw new CompileErrorException(game + " is not Game Element")
    }
  
  def makePiece(p : PieceParseTree): Piece = Piece(p.args map compile)
  
  def makePosition(pos : PositionParseTree): Position = Position(pos.pieces map makeEvalToPiece, makeRole(pos.role))
  
  def makeRole(r : Option[RoleParseTree]): Option[Role] = r flatMap (v => Some(Role(compile(v.role))))
  
  def makeGameOP(gameop : GameOperationParseTree): Exp =
    gameop match {
    case arg : GetArgParseTree => Arg(makeEvalInteger(arg.index), makeEvalToGameElement(arg.gameObject))
    case getrole : GetRoleParseTree => GetRole(makeEvalToPosition(getrole.position))
    case size : GetSizeParseTree => Size(makeEvalToGameElement(size.gameObject))
    case uni : UnificateParseTree => Unificate(makeEvalToPiece(uni.piece1), makeEvalToPiece(uni.piece2))
    case _ => throw new CompileErrorException(gameop + " is not Game Operation")  }
  
  def makeEvalToGameElement(game : EvalToGameElementParseTree): EvalGameElement =
    game match {
    case piece : PieceParseTree => makePiece(piece)
    case position : PositionParseTree => makePosition(position)
    case role : RoleParseTree => Role(compile(role.role))
    case message : MessageParseTree => Message(message.messages map compile)
    case label : LabelLeaf => Label(label.value)
    case _ => throw new CompileErrorException(game + " is not Game Element")
    }
  
  def makeEvalToPosition(ppt : EvalToPositionParseTree): EvalPosition =
    ppt match {
    case pt : PositionParseTree => makePosition(pt)
    case label : LabelLeaf => Label(label.value)
    case _ => throw new CompileErrorException(ppt + " is not Game Element")
    }
    
  def makeEvalToPiece(piece : EvalToPieceParseTree): EvalPiece =
    piece match {
    case p : PieceParseTree => makePiece(p)
    case arg : GetArgParseTree => Arg(makeEvalInteger(arg.index), makeEvalToGameElement(arg.gameObject))
    case label : LabelLeaf => Label(label.value)
    case _ => throw new CompileErrorException(piece + " is not Piece")
  }
  
  def makeConcept(con : ConceptParseTree): DefineConcept =
    con.conceptType match {
    case pt : PatternParseTree => makeDefineConceptWithType(con, PositionType())
    case pt : UpdateParseTree => makeDefineConceptWithType(con, PieceType()) 
    case pt : HeuristicsParseTree => makeDefineConceptWithType(con, PositionType())
    case pt : TransitionParseTree => makeDefineConceptWithType(con, PositionType())
    case pt : RecognitionParseTree => makeDefineConceptWithType(con, MessageType())
    case pt : RepresentationParseTree => makeDefineConceptWithType(con, ConceptType())
    case pt : HigherParseTree => DefineConcept(Label(con.conceptName.value), Concept(con.args map makeTypedLabel, compile(con.body)))
  }
  
  def makeDefineConceptWithType(con : ConceptParseTree, t : Type): DefineConcept =
    DefineConcept(Label(con.conceptName.value), Concept(con.args map (a => TypedLabel(Label(a.valueLabel.value), t)), compile(con.body)))
  
  def makeTypedLabel(apt : ArgParseTree): TypedLabel = TypedLabel(Label(apt.valueLabel.value), makeType(apt.typeLabel))
  
  def makeType(t : Option[Tag]): Type =
    t match {
    case Some(Tag("Integer")) => IntegerType()
    case Some(Tag("Number")) => NumberType()
    case Some(Tag("Symbol")) => SymbolType()
    case Some(Tag("Bool")) => BoolType()
    case Some(Tag("Piece")) => PieceType()
    case Some(Tag("Position")) => PositionType()
    case Some(Tag("Role")) => RoleType()
    case Some(Tag("Message")) => MessageType()
    case None => NullType()
    case _ => throw new CompileErrorException(t.get.name + " is not a type")
  }
  
  def makeConceptCall(callpt : ConceptCallParseTree): ConceptCall =
    ConceptCall(Label(callpt.conceptName.value), callpt.args map makeTypedExp)

  def makeTypedExp(apt : CallArgParseTree): TypedExp = TypedExp(compile(apt.value), makeType(apt.typeLabel))
}

class CompileErrorException(message :String = null, cause :Throwable = null) extends RuntimeException(message, cause)