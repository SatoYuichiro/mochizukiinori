package jp.satoyuichiro.inori.cdl

abstract class Arithmetic(val args : List[Exp]) extends Exp with EvalInteger with EvalNumber with Serializable {

  def eval(env : Environment): Exp = evalNumber(env)
  def evalInteger(env : Environment): Integer =
    evalNumber(env) match {
      case i : Integer => i
      case r : Real => Integer(r.value.toInt)
    }
  
  def evalAll(env : Environment): List[Exp] = args map (_.eval(env))
  def hasIntegerArgs : Boolean = (args map (_.isInstanceOf[Integer])).fold(true)(_ && _)
  def hasRealArgs : Boolean = (args map (_.isInstanceOf[Integer])).fold(true)(_ && _)
  def isNumber(els : List[Exp]): Boolean = (args map (_.isInstanceOf[Number])).fold(true)(_ && _)
}

case class Addition(override val args : List[Exp]) extends Arithmetic(args) with Serializable {
  
  def evalNumber(env : Environment): Number = {
    try {
      val nums = args map (_.eval(env).asInstanceOf[Number])
      nums.tail.fold(nums.head)(_ + _)
    } catch {
      case e : Exception => throw new StatementEvalErrorException("In " + this.toString + "\n" + env.toString)
    }
  }
  
  override def toLisp: String = {
    val argsstr = (for (exp <- args) yield " " + exp.toLisp).fold("")(_ + _)
    "(+" + argsstr + ")"
  }
}

object Addition {
  
  def apply(exps : Exp*): Addition = Addition(exps.toList)
}

case class Subtraction(override val args : List[Exp]) extends Arithmetic(args) with Serializable {
  
  def evalNumber(env : Environment): Number = {
    try {
      val nums = args map (_.eval(env).asInstanceOf[Number])
      nums.tail.fold(nums.head)(_ - _)
    } catch {
      case e : Exception =>
//        e.printStackTrace()
        throw new StatementEvalErrorException("In " + this.toString)
    }
  }
  
  override def toLisp: String = {
    val argsstr = (for (exp <- args) yield " " + exp.toLisp).fold("")(_ + _)
    "(-" + argsstr + ")"
  }
}

object Subtraction {
  
  def apply(exps : Exp*): Subtraction = Subtraction(exps.toList)
}

case class Multiplication(override val args : List[Exp]) extends Arithmetic(args) with Serializable {
  
  def evalNumber(env : Environment): Number = {
    try {
      val nums = args map (_.eval(env).asInstanceOf[Number])
      nums.tail.fold(nums.head)(_ * _)
    } catch {
      case e : Exception => throw new StatementEvalErrorException("In " + this.toString)
    }
  }
  
  override def toLisp: String = {
    val argsstr = (for (exp <- args) yield " " + exp.toLisp).fold("")(_ + _)
    "(*" + argsstr + ")"
  }
}

object Multiplication {
  
  def apply(exps : Exp*): Multiplication = Multiplication(exps.toList)
}

case class Division(override val args : List[Exp]) extends Arithmetic(args) with Serializable {
  
  def evalNumber(env : Environment): Number = {
    try {
      val nums = args map (_.eval(env).asInstanceOf[Number])
      nums.tail.fold(nums.head)(_ / _)
    } catch {
      case e : Exception => throw new StatementEvalErrorException("In " + this.toString)
    }
  }
  
  override def toLisp: String = {
    val argsstr = (for (exp <- args) yield " " + exp.toLisp).fold("")(_ + _)
    "(/" + argsstr + ")"
  }
}

object Division {
  
  def apply(exps : Exp*): Division = Division(exps.toList)
}

case class Modulo(override val args : List[Exp]) extends Arithmetic(args) with Serializable {
  
  def evalNumber(env : Environment): Number = {
    try {
      val nums = args map (_.eval(env).asInstanceOf[Number])
      nums.tail.fold(nums.head)(_ % _)
    } catch {
      case e : Exception => throw new StatementEvalErrorException("In " + this.toString)
    }
  }
  
  override def toLisp: String = {
    val argsstr = (for (exp <- args) yield " " + exp.toLisp).fold("")(_ + _)
    "(%" + argsstr + ")"
  }
}

object Modulo {
  
  def apply(exps : Exp*): Modulo = Modulo(exps.toList)
}

abstract class Logic(val args : List[Exp]) extends Exp with EvalBool {
  
  def eval(env : Environment): Exp = evalBool(env)
}

case class Not(override val args : List[Exp]) extends Logic(args) with Serializable {
  
  def evalBool(env : Environment): Bool =
    try {
      val bool = args.head.eval(env)
      bool match {
        case b : Bool => Bool(!b.value)
        case _ => throw new StatementEvalErrorException("In " + this.toString)
      }
    } catch {
      case e : Exception => throw new StatementEvalErrorException("In " + this.toString)
    }
  
  override def toLisp: String = {
    val argsstr = (for (exp <- args) yield " " + exp.toLisp).fold("")(_ + _)
    "(not" + argsstr + ")"
  }
}

object Not {
  
  def apply(exp : Exp): Not = Not(List(exp))
}

case class And(override val args : List[Exp]) extends Logic(args) with Serializable {
  
  def evalBool(env : Environment): Bool =
    try {
      val bools = args map (_.eval(env).asInstanceOf[Bool])
      bools.tail.fold(bools.head)(_ && _)
    } catch {
      case e : Exception =>
//        e.printStackTrace()
        throw new StatementEvalErrorException("In " + this.toString)
    }
  
  override def toLisp: String = {
    val argsstr = (for (exp <- args) yield " " + exp.toLisp).fold("")(_ + _)
    "(and" + argsstr + ")"
  }
}

object And {
  
  def apply(exps : Exp*): And = And(exps.toList)
}

case class Or(override val args : List[Exp]) extends Logic(args) with Serializable {
  
  def evalBool(env : Environment): Bool =
    try {
      val bools = args map (_.eval(env).asInstanceOf[Bool])
      bools.tail.fold(bools.head)(_ || _)
    } catch {
      case e : Exception => throw new StatementEvalErrorException("In " + this.toString)
    }
  
  override def toLisp: String = {
    val argsstr = (for (exp <- args) yield " " + exp.toLisp).fold("")(_ + _)
    "(or" + argsstr + ")"
  }
}

object Or {
  
  def apply(exps : Exp*): Or = Or(exps.toList)
}

case class Eq(override val args : List[Exp]) extends Logic(args) with Serializable {
  
  def evalBool(env : Environment): Bool =
    try {
      val bools = args map (_.eval(env).asInstanceOf[Bool])
      bools.tail.fold(bools.head)(_ == _)
    } catch {
      case e : Exception => throw new StatementEvalErrorException("In " + this.toString)
    }
  
  override def toLisp: String = {
    val argsstr = (for (exp <- args) yield " " + exp.toLisp).fold("")(_ + _)
    "(eq" + argsstr + ")"
  }
}

object Eq {
  
  def apply(exps : Exp*): Eq = Eq(exps.toList)
}

abstract class Condition extends Exp with EvalBool {
  
  def eval(env : Environment): Exp = evalBool(env)
}

case class Greater(arg1 : EvalNumber, arg2 : EvalNumber) extends Condition with Serializable {
  
  def evalBool(env : Environment): Bool = Bool(arg1.evalNumber(env) < arg2.evalNumber(env))
  
  override def toLisp: String = "(< " + arg1.toLisp + " " + arg2.toLisp + ")"
}

case class Less(arg1 : EvalNumber, arg2 : EvalNumber) extends Condition with Serializable {
  
  def evalBool(env : Environment): Bool = Bool(arg1.evalNumber(env) > arg2.evalNumber(env))
  
  override def toLisp: String = "(> " + arg1.toLisp + " " + arg2.toLisp + ")"
}

case class Equals(arg1 : Exp, arg2 : Exp) extends Condition with Serializable {
  
  def evalBool(env : Environment): Bool = Bool(arg1.eval(env) == arg2.eval(env))
  
  override def toLisp: String = "(= " + arg1.toLisp + " " + arg2.toLisp + ")"
}

class StatementEvalErrorException(message : String, cause : Throwable = null) extends RuntimeException(message, cause)