package jp.satoyuichiro.inori.cdl

case class Concept(args : List[TypedLabel], body : Exp) extends Exp {
  
  var values = List.empty[Exp]
  
  def eval(env : Environment): Exp = {
    val env2 = new Environment(env)
    args.zip(values) foreach { t => env2.put(t._1.label, t._2) }
    body.eval(env2)
  }
    
  def setArgs(values : List[Exp]): Unit = {
    this.values = values
  }
  
  override def toLisp: String = {
    val argsstr = (for (exp <- args) yield exp.toLisp + " ").fold("")(_ + _)
    "(" + argsstr.substring(0, argsstr.length - 1) + ") " + body.toLisp
  }
}

case class DefineConcept(conceptName : Label, concept : Concept) extends Exp {
  
  def eval(env : Environment): Exp = {
    env.put(conceptName, concept)
    Bool(true)
  }
  
  override def toLisp: String = "(concept " + conceptName.toLisp + " " + concept.toLisp + ")"
}

case class ConceptCall(concept : Label, args : List[TypedExp]) extends Exp with EvalInteger with EvalNumber with EvalBool with EvalPosition with EvalPiece {
  
  def eval(env : Environment): Exp = {
    val evaluated = args map (
        arg => {
          val e = arg.exp.eval(env)
          if (arg.t.typeCheck(e)) e else throw new Exception("Run time type error")
        })
    
    env.getValue(concept) match {
      case func : Concept =>
        func.setArgs(evaluated)
        func.eval(env)
      case _ => throw new Exception(concept.label + " is not defined")
    }
  }
  def evalInteger(env : Environment): Integer = eval(env).asInstanceOf[Integer]
  def evalNumber(env : Environment): Number = eval(env).asInstanceOf[Number]
  def evalBool(env : Environment): Bool = eval(env).asInstanceOf[Bool]
  def evalGameElement(env : Environment): GameElement = eval(env).asInstanceOf[GameElement]
  def evalPiece(env : Environment): Piece = eval(env).asInstanceOf[Piece]
  def evalPosition(env : Environment): Position = eval(env).asInstanceOf[Position]

  override def toLisp: String = {
    val argsstr = (for (exp <- args) yield exp.toLisp + " ").fold("")(_ + _)
    "(" + concept.toLisp + " " + argsstr.substring(0, argsstr.length - 1) + ")"
  }
}

case class TypedLabel(label : Label, t : Type) extends Lisp {
  
  override def toLisp: String = label.toLisp
}
case class TypedExp(exp : Exp, t : Type) extends Lisp {
  
  override def toLisp: String = exp.toLisp
}

trait Type { def typeCheck(exp : Exp): Boolean }
case class NullType() extends Type { def typeCheck(exp : Exp): Boolean = true }
case class IntegerType() extends Type { def typeCheck(exp : Exp): Boolean = exp.isInstanceOf[Integer] }
case class NumberType() extends Type { def typeCheck(exp : Exp): Boolean = exp.isInstanceOf[Number] }
case class SymbolType() extends Type { def typeCheck(exp : Exp): Boolean = exp.isInstanceOf[Symbol] }
case class BoolType() extends Type { def typeCheck(exp : Exp): Boolean = exp.isInstanceOf[Bool] }
case class PieceType() extends Type { def typeCheck(exp : Exp): Boolean = exp.isInstanceOf[Piece] }
case class PositionType() extends Type { def typeCheck(exp : Exp): Boolean = exp.isInstanceOf[Position] }
case class RoleType() extends Type { def typeCheck(exp : Exp): Boolean = exp.isInstanceOf[Role] }
case class MessageType() extends Type { def typeCheck(exp : Exp): Boolean = exp.isInstanceOf[Message] }
case class ConceptType() extends Type { def typeCheck(exp : Exp): Boolean = exp.isInstanceOf[Concept] }