package jp.satoyuichiro.inori.cdl

class Environment(val previous : Environment) {

  var env = Map.empty[Label, Exp]
  
  def getValue(label : Label): Exp = {
    env.getOrElse(label, None) match {
      case exp : Exp => exp
      case None => previous.getOption(label) match {
        case Some(exp : Exp) => exp
        case None => throw new NoVariableException(label.label + " is not defined")
      }
    }
  }
  
  def getOption(label : Label): Option[Exp] =
    env.getOrElse(label, None) match {
      case exp : Exp => Some(exp)
      case None => previous.getOption(label)
    }
  
  def set(labelList : List[Substitution]): Environment = {
    val newEnv = new Environment(this)
    labelList foreach {
      s => newEnv.put(s.label, s.value)
    }
    newEnv
  }
  
  def put(label : Label, value : Exp): Unit = {
    val builder = Map.newBuilder[Label, Exp]
    env foreach {
      t =>
        builder += t
    }
    builder += (label -> value)
    env = builder.result
  }

  def contains(label : Label): Boolean = previous.contains(label)
  
  def put(label : String, value : Exp): Unit = {
    put(Label(label), value)
  }
  
  def refresh(): Unit = {
    env = Map.empty[Label, Exp]
    previous.refresh()
  }
  
  override def toString: String = {
    env.toString
  }
}

object Environment {
  
  def empty: Environment = new Environment(new NullEnvironment())
}

class NullEnvironment extends Environment(null) {
  
  override def getValue(label : Label): Exp = throw new NoVariableException("NullEnvironment has no variable")
  override def getOption(label : Label): Option[Exp] = None
}

class NoVariableException(message : String, cause : Throwable = null) extends RuntimeException(message, cause)