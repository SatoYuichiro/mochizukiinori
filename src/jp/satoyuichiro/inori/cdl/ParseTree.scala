package jp.satoyuichiro.inori.cdl

class ParseTree
case class Tag(name : String) extends ParseTree
trait EvalToBoolParseTree extends ParseTree
trait EvalToIntParseTree extends ParseTree
trait EvalToNumberParseTree extends ParseTree
trait EvalToGameElementParseTree extends ParseTree
trait EvalToPieceParseTree extends ParseTree
trait EvalToPositionParseTree extends ParseTree

class StatementParseTree extends ParseTree
case class ArithmeticParseTree(operator : Tag, args : List[ParseTree]) extends StatementParseTree with EvalToNumberParseTree with EvalToIntParseTree
case class LogicParseTree(operator : Tag, args : List[ParseTree]) extends StatementParseTree with EvalToBoolParseTree
case class ConditionParseTree(operator : Tag, arg1 : ParseTree, arg2 : ParseTree) extends StatementParseTree with EvalToBoolParseTree

class CodeParseTree extends ParseTree
case class IfParseTree(condition : EvalToBoolParseTree, trueBody : BodyParseTree, falseBody : BodyParseTree) extends CodeParseTree
case class ForParseTree(index : IndexParseTree, condition : EvalToBoolParseTree, incriment : BodyParseTree, body : BodyParseTree) extends CodeParseTree
case class IndexParseTree(indexs : List[SubstitutionParseTree]) extends CodeParseTree
case class SubstitutionParseTree(label : LabelLeaf, value : ParseTree) extends CodeParseTree
case class LetParseTree(env : List[SubstitutionParseTree], body : BodyParseTree) extends CodeParseTree
case class SetParseTree(label : LabelLeaf, value : ParseTree) extends CodeParseTree
case class BodyParseTree(body : List[ParseTree]) extends CodeParseTree

trait Leaf[A] extends ParseTree { def value : A }
case class IntLeaf(value : Int) extends Leaf[Int] with EvalToIntParseTree with EvalToNumberParseTree
case class DoubleLeaf(value : Double) extends Leaf[Double] with EvalToNumberParseTree
case class StringLeaf(value : String) extends Leaf[String]
case class BooleanLeaf(value : Boolean) extends Leaf[Boolean] with EvalToBoolParseTree
case class LabelLeaf(value : String) extends Leaf[String] with EvalToBoolParseTree with EvalToIntParseTree
  with EvalToGameElementParseTree with EvalToPieceParseTree with EvalToPositionParseTree

class GameParseTree extends ParseTree
case class PieceParseTree(args : List[ParseTree]) extends GameParseTree with EvalToGameElementParseTree with EvalToPieceParseTree
case class RoleParseTree(role : ParseTree) extends GameParseTree with EvalToGameElementParseTree
case class MessageParseTree(messages : List[ParseTree]) extends GameParseTree with EvalToGameElementParseTree
case class PositionParseTree(pieces : List[EvalToPieceParseTree], role : Option[RoleParseTree]) extends GameParseTree with EvalToGameElementParseTree with EvalToPositionParseTree

class GameOperationParseTree extends ParseTree
case class GetArgParseTree(index : EvalToIntParseTree, gameObject : EvalToGameElementParseTree) extends GameOperationParseTree with EvalToPieceParseTree
case class GetRoleParseTree(position : EvalToPositionParseTree) extends GameOperationParseTree
case class GetSizeParseTree(gameObject : EvalToGameElementParseTree) extends GameOperationParseTree with EvalToIntParseTree
case class UnificateParseTree(piece1 : EvalToPieceParseTree, piece2 : EvalToPieceParseTree) extends GameOperationParseTree

case class ConceptParseTree(conceptType : ConceptTypeParseTree, conceptName : LabelLeaf, args : List[ArgParseTree], body : ParseTree) extends ParseTree
case class ConceptCallParseTree(conceptName : LabelLeaf, args : List[CallArgParseTree]) extends ParseTree with EvalToBoolParseTree with EvalToIntParseTree
  with EvalToGameElementParseTree with EvalToPieceParseTree with EvalToPositionParseTree
case class ArgParseTree(valueLabel : LabelLeaf, typeLabel : Option[Tag]) extends ParseTree
case class CallArgParseTree(value : ParseTree, typeLabel : Option[Tag]) extends ParseTree
class ConceptTypeParseTree extends ParseTree
case class PatternParseTree() extends ConceptTypeParseTree
case class UpdateParseTree() extends ConceptTypeParseTree
case class HeuristicsParseTree() extends ConceptTypeParseTree
case class TransitionParseTree() extends ConceptTypeParseTree
case class RecognitionParseTree() extends ConceptTypeParseTree
case class RepresentationParseTree() extends ConceptTypeParseTree
case class HigherParseTree() extends ConceptTypeParseTree