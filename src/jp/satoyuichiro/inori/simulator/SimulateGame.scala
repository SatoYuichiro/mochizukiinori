package jp.satoyuichiro.inori.simulator

import scala.io.Source
import java.io.File
import java.util.ArrayList
import java.util.{List => JList}
import java.lang.{Integer => JInteger}
import org.ggp.base.util.statemachine.StateMachine
import org.ggp.base.util.statemachine.implementation.prover.ProverStateMachine
import org.ggp.base.util.gdl.factory.GdlFactory
import jp.satoyuichiro.inori.player.Player
import jp.satoyuichiro.inori.player.RandomPlayer
import jp.satoyuichiro.inori.player.PlayerUtil
import jp.satoyuichiro.inori.player.AlphaBetaPlayer
import org.ggp.base.util.statemachine.MachineState
import org.ggp.base.util.gdl.grammar.GdlSentence
import org.ggp.base.util.gdl.grammar.GdlFunction
import org.ggp.base.util.gdl.grammar.GdlConstant
import org.ggp.base.util.gdl.grammar.GdlTerm
import jp.satoyuichiro.inori.learning.concept.generator.CompositeHeuristics
import jp.satoyuichiro.inori.player.HeuristicsPlayer
import jp.satoyuichiro.inori.player.Heuristics
import jp.satoyuichiro.inori.player.Heuristics
import jp.satoyuichiro.inori.learning.concept.generator.PrimeConjunction
import jp.satoyuichiro.inori.learning.concept.generator.CompositeDisjunction
import jp.satoyuichiro.inori.learning.concept.generator.PrimeHeuristics
import jp.satoyuichiro.inori.cdl.Integer
import jp.satoyuichiro.inori.cdl.Label
import jp.satoyuichiro.inori.cdl.Piece
import jp.satoyuichiro.inori.cdl.Symbol
import jp.satoyuichiro.inori.cdl.Addition

object SimulateGame {

//  val tem = Array[String]("./games/twoplayers/breakthrough.gdl")
  val tem = Array[String]("./games/twoplayers/connect4.gdl")
//  val tem = Array[String]("./games/twoplayers/tictactoe.gdl")
  def main(args: Array[String]): Unit = {
    val args = tem
    val gameRules = SimulatorUtil.readGdl(args(0))
    val stateMachine = new ProverStateMachine
    val discription = gameRules map (r => GdlFactory.create(r))
//    gameRules foreach println
//    discription foreach println
    stateMachine.initialize(PlayerUtil.toJavaList(discription))
    val roles = PlayerUtil.toScalaList(stateMachine.getRoles())
    val startTime = System.currentTimeMillis()
    var results = List.empty[List[Int]]
    for (i <- 0 to 99) {
//      print(i + " ")
      results ::= simulateHeuristicsAlphaBetaGame(stateMachine)
    }
    val endTime = System.currentTimeMillis()
    println((startTime - endTime)/1000 + "sec")
    println((startTime - endTime)/60000 + "min")
    println(roles.head + " win " + (results filter (r => r.head > r.tail.head)).size)
    println(roles.tail.head + " win " + (results filter (r => r.head < r.tail.head)).size)
    println("draw " + (results filter (r => r.head == r.tail.head)).size)
  }
  
  def simulateRandomGame(stateMachine: StateMachine): List[Int] = {
    val roles = PlayerUtil.toScalaList(stateMachine.getRoles())
    val players = roles map (role => new RandomPlayer(stateMachine, role))
    var currentState = stateMachine.getInitialState
    while(!stateMachine.isTerminal(currentState)) {
      val moves = players map (_.chooseMove(currentState))
      currentState = stateMachine.getNextState(currentState, PlayerUtil.toJavaList(moves))
    }
    PlayerUtil.toScalaList(stateMachine.getGoals(currentState)) map (_.asInstanceOf[Int])
  }
  
  def printConnect4Position(machineState: MachineState): Unit = {
    val set = machineState.getContents()
    var list = List.empty[GdlFunction]
    val i = set.iterator()
    while(i.hasNext()) {
      list ::= i.next().getBody().get(0).asInstanceOf[GdlFunction]
    }
//    println(list)
    val control = list filter (_.getName().getValue() == "control")
    list = list filter (_.getName().getValue() != "control")
    val position = list map (_.getBody())
//    println(position)
    def gdlToInt(c: GdlTerm): Int = JInteger.parseInt(c.asInstanceOf[GdlConstant].getValue())
    
    println(control)
    for (i <- 1 to 7) {
      val line = position filter (c => gdlToInt(c.get(0)) == i)
      val sorted = line.sortWith((t1, t2) => gdlToInt(t1.get(1)) < gdlToInt(t2.get(1)))
      sorted foreach {c => print(c + " ")}
      println()
    }
    println()
  }
  
  def simulateAlphaBetaRandomGame(stateMachine: StateMachine): List[Int] = {
    val roles = PlayerUtil.toScalaList(stateMachine.getRoles())
    val players = new AlphaBetaPlayer(stateMachine, roles.head) :: (roles.tail map (role => new RandomPlayer(stateMachine, role)))
    println(players map (p => p.getClass() + " " + p.role))
    var currentState = stateMachine.getInitialState
    while(!stateMachine.isTerminal(currentState)) {
      printConnect4Position(currentState)
      val moves = players map (_.chooseMove(currentState))
      currentState = stateMachine.getNextState(currentState, PlayerUtil.toJavaList(moves))
    }
    PlayerUtil.toScalaList(stateMachine.getGoals(currentState)) map (_.asInstanceOf[Int])
  }
  
  def simulateAlphaBetaGame(stateMachine: StateMachine): List[Int] = {
    val roles = PlayerUtil.toScalaList(stateMachine.getRoles())
    val players = roles map (role => new AlphaBetaPlayer(stateMachine, role))
    var currentState = stateMachine.getInitialState
    while(!stateMachine.isTerminal(currentState)) {
      printConnect4Position(currentState)
      val moves = players map (_.chooseMove(currentState))
      currentState = stateMachine.getNextState(currentState, PlayerUtil.toJavaList(moves))
    }
    PlayerUtil.toScalaList(stateMachine.getGoals(currentState)) map (_.asInstanceOf[Int])
  }
  
  def simulateHeuristicsGame(stateMachine: StateMachine): List[Int] = {
    val roles = PlayerUtil.toScalaList(stateMachine.getRoles())
    val players = new HeuristicsPlayer(handMadeHeuristics2, stateMachine, roles.head) :: (roles.tail map (role => new RandomPlayer(stateMachine, role)))
//    println(compositeDisjunction.toConcept.toLisp)
//    println(composite.primeHeuristics.head.toConcept.toLisp)
    println(players map (p => p.getClass() + " " + p.role))
    var currentState = stateMachine.getInitialState
    while(!stateMachine.isTerminal(currentState)) {
      printConnect4Position(currentState)
      val moves = players map (_.chooseMove(currentState))
      currentState = stateMachine.getNextState(currentState, PlayerUtil.toJavaList(moves))
    }
    PlayerUtil.toScalaList(stateMachine.getGoals(currentState)) map (_.asInstanceOf[Int])
  }
  
  def simulateHeuristicsAlphaBetaGame(stateMachine: StateMachine): List[Int] = {
    val roles = PlayerUtil.toScalaList(stateMachine.getRoles())
    val players = new HeuristicsPlayer(handMadeHeuristics2, stateMachine, roles.head) :: (roles.tail map (role => new AlphaBetaPlayer(stateMachine, role)))
//    println(compositeDisjunction.toConcept.toLisp)
//    println(composite.primeHeuristics.head.toConcept.toLisp)
    println(players map (p => p.getClass() + " " + p.role))
    var currentState = stateMachine.getInitialState
    while(!stateMachine.isTerminal(currentState)) {
      printConnect4Position(currentState)
      val moves = players map (_.chooseMove(currentState))
      currentState = stateMachine.getNextState(currentState, PlayerUtil.toJavaList(moves))
    }
    PlayerUtil.toScalaList(stateMachine.getGoals(currentState)) map (_.asInstanceOf[Int])
  }
  
  
  def simulateAlphaBetaHeuristicsGame(stateMachine: StateMachine): List[Int] = {
    val roles = PlayerUtil.toScalaList(stateMachine.getRoles())
    val players = new AlphaBetaPlayer(stateMachine, roles.head) :: (roles.tail map (role => new HeuristicsPlayer(handMadeHeuristics2, stateMachine, role)))
//    println(compositeDisjunction.toConcept.toLisp)
//    println(composite.primeHeuristics.head.toConcept.toLisp)
    println(players map (p => p.getClass() + " " + p.role))
    var currentState = stateMachine.getInitialState
    while(!stateMachine.isTerminal(currentState)) {
      printConnect4Position(currentState)
      val moves = players map (_.chooseMove(currentState))
      currentState = stateMachine.getNextState(currentState, PlayerUtil.toJavaList(moves))
    }
    PlayerUtil.toScalaList(stateMachine.getGoals(currentState)) map (_.asInstanceOf[Int])
  }
  
  val x = Label("x")
  val y = Label("y")
  val z = Label("z")
  val w = Label("w")
  val r = Symbol("r")
  val ww = Symbol("w")
  val one = Integer(1)
  val piece1 = Piece(x, y, z, ww)
  val piece2 = Piece(x, Addition(y, one), z, ww)
  val piece3 = Piece(x, y, Addition(z, one), ww)
  val piece4 = Piece(x, Addition(y, one), Addition(z, one), ww)
  
  val primeConjunction1 = PrimeConjunction(List(piece1, piece2)).putLabel(Label("pattern1"))
  val primeConjunction2 = PrimeConjunction(List(piece1, piece3)).putLabel(Label("pattern2"))
  val primeConjunction3 = PrimeConjunction(List(piece1, piece4)).putLabel(Label("pattern3"))
  val conList = List(primeConjunction1, primeConjunction2, primeConjunction3)
  val compositeDisjunction = CompositeDisjunction(conList).putLabel(Label("disjunction1"))
  val primeHeuristics1 = PrimeHeuristics(compositeDisjunction, 1.0).putLabel(Label("hand-made-heuristics"))
  
  val primeH2 = PrimeHeuristics(primeConjunction1, 1.0).putLabel(Label("hand-made1"))
  val primeH3 = PrimeHeuristics(primeConjunction1, 1.0).putLabel(Label("hand-made2"))
  val primeH4 = PrimeHeuristics(primeConjunction1, 1.0).putLabel(Label("hand-made3"))
  
  val composite = new CompositeHeuristics(List(primeHeuristics1))
  val handMadeHeuristics = Heuristics(composite)
  val handMadeHeuristics2 = Heuristics(new CompositeHeuristics(List(primeH2, primeH3, primeH4)))
  

}