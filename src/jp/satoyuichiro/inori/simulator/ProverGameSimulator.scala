package jp.satoyuichiro.inori.simulator

import java.io.File
import org.ggp.base.util.statemachine.implementation.prover.ProverStateMachine
import jp.satoyuichiro.inori.gdl.Description
import jp.satoyuichiro.inori.gdl.GdlUtil
import jp.satoyuichiro.inori.player.RandomPlayer
import jp.satoyuichiro.inori.gdl.GdlConverter
import jp.satoyuichiro.inori.cdl.Position

class ProverGameSimulator(val gdl: File) {

  def simulateRandomPlayout(n: Int): List[Position] = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(gdl).getDescription)
    val roles = stateMachine.getRoles()
    val players = GdlUtil.toScalaList(roles) map (r => new RandomPlayer(stateMachine, r))
    val tictactoeSimulator = GameSimulator(stateMachine, players)
    tictactoeSimulator.simulatePlayout(n) map (p => GdlConverter.gdlTocdl(p))
  }
  
  def simulateFirstWinEpisode(n: Int): List[List[Position]] = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(gdl).getDescription)
    val roles = stateMachine.getRoles()
    val players = GdlUtil.toScalaList(roles) map (r => new RandomPlayer(stateMachine, r))
    val tictactoeSimulator = GameSimulator(stateMachine, players)
    tictactoeSimulator.simulateWinEpisode(players.head, n) map (pls => pls map (p => GdlConverter.gdlTocdl(p)))
  }

  def simulateFirstWinPlayout(n: Int): List[Position] = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(gdl).getDescription)
    val roles = stateMachine.getRoles()
    val players = GdlUtil.toScalaList(roles) map (r => new RandomPlayer(stateMachine, r))
    val tictactoeSimulator = GameSimulator(stateMachine, players)
    tictactoeSimulator.simulateWinner(players.head, n) map (p => GdlConverter.gdlTocdl(p))
  }

  def simulateSecondWinPlayout(n: Int): List[Position] = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(gdl).getDescription)
    val roles = stateMachine.getRoles()
    val players = GdlUtil.toScalaList(roles) map (r => new RandomPlayer(stateMachine, r))
    val tictactoeSimulator = GameSimulator(stateMachine, players)
    tictactoeSimulator.simulateWinner(players.tail.head, n) map (p => GdlConverter.gdlTocdl(p))
  }
}