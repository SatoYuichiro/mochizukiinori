package jp.satoyuichiro.inori.simulator

import org.ggp.base.util.statemachine.MachineState
import org.ggp.base.util.statemachine.StateMachine
import jp.satoyuichiro.inori.player.Player
import org.ggp.base.util.statemachine.Move
import jp.satoyuichiro.inori.gdl.GdlUtil
import java.util.{ List => JList }
import java.util.{ Map => JMap }
import org.ggp.base.util.statemachine.Role
import jp.satoyuichiro.inori.gdl.PlayerInfo


class GameSimulator(val stateMachine: StateMachine, val players: List[Player], val playerInfo: PlayerInfo) {

  def simulate(n: Int): List[List[MachineState]] = {
    var result = List.empty[List[MachineState]]
    for (i <- 1 to n) {
      result ::= simulate(stateMachine)
    }
    result
  }
  
  def simulate(stateMachine: StateMachine): List[MachineState] = {
    var currentState = stateMachine.getInitialState()
    var result = List(currentState)
    while(!stateMachine.isTerminal(currentState)) {
      val player_move = players.par.map(p => (p, p.chooseMove(currentState)))
      var sorted = new Array[Move](players.size)
      player_move foreach {
        pm => sorted(playerInfo.roleIndex(pm._1)) = pm._2
      }
      currentState = stateMachine.getNextState(currentState, GdlUtil.toJavaList(sorted.toList))
      result ::= currentState
    }
    result.reverse
  }
  
  def simulateMatch(n: Int): List[Map[Player, Boolean]] = {
    var result = List.empty[Map[Player, Boolean]]
    for (i <- 1 to n) {
      result ::= simulateMatch(stateMachine)
    }
    result
  }
  
  def simulateMatchPar(n: Int): List[Map[Player, Boolean]] = {
    var result = List.empty[Map[Player, Boolean]]
    (1 to n).toList.par.map(i => simulateMatch(stateMachine)).foreach { r =>
      result ::= r
    }
    result
  }
  
  def simulateMatch(stateMachine: StateMachine): Map[Player, Boolean] = {
    val result = simulate2(stateMachine)
    val goals = result._2.getGoals(result._1)
    players map (player => (player, playerInfo.isWinner(player, goals))) toMap
  }

  def simulate2(stateMachine: StateMachine): Tuple2[MachineState, StateMachine] = {
    var currentState = stateMachine.getInitialState()
    while(!stateMachine.isTerminal(currentState)) {
      val player_move = players.par.map(p => (p, p.chooseMove(currentState)))
      var sorted = new Array[Move](players.size)
      player_move foreach {
        pm => sorted(playerInfo.roleIndex(pm._1)) = pm._2
      }
      currentState = stateMachine.getNextState(currentState, GdlUtil.toJavaList(sorted.toList))
    }
    (currentState, stateMachine)
  }

  def simulatePlayout(n: Int): List[MachineState] = {
    var result = List.empty[MachineState]
    for (i <- 1 to n) {
      result ::= simulate(stateMachine).last
    }
    result
  }
  
  def simulateWinner(player: Player, n: Int): List[MachineState] = {
    var result = List.empty[MachineState]
    while(result.size < n) {
      val exp = simulate2(stateMachine)
      val expResult = exp._2.getGoals(exp._1)
      if (playerInfo.isWinner(player, expResult))
        result ::= exp._1
    }
    result
  }
  
  def simulate3(stateMachine: StateMachine): Tuple2[List[MachineState], StateMachine] = {
    var result = List.empty[MachineState]
    var currentState = stateMachine.getInitialState()
    while(!stateMachine.isTerminal(currentState)) {
      val player_move = players.par.map(p => (p, p.chooseMove(currentState)))
      var sorted = new Array[Move](players.size)
      player_move foreach {
        pm => sorted(playerInfo.roleIndex(pm._1)) = pm._2
      }
      currentState = stateMachine.getNextState(currentState, GdlUtil.toJavaList(sorted.toList))
      result ::= currentState
    }
    (result.reverse, stateMachine)
  }

  def simulateWinEpisode(player: Player, n: Int): List[List[MachineState]] = {
    var result = List.empty[List[MachineState]]
    while(result.size < n) {
      val exp = simulate3(stateMachine)
      val expResult = exp._2.getGoals(exp._1.last)
      if (playerInfo.isWinner(player, expResult)) {
        result ::= exp._1
      }
    }
    result
  }
}

object GameSimulator {
  
  def apply(stateMachine: StateMachine, players: List[Player]): GameSimulator = {
    val roleIndices = GdlUtil.toScalaMap(stateMachine.getRoleIndices())
    def findPlayer(role: Role): Option[Player] = {
      players foreach {
        p => if (p.role == role) return Some(p)
      }
      None
    }
    val builder = Map.newBuilder[Player, Int]
    roleIndices foreach {
      ri => findPlayer(ri._1) match {
        case Some(p) => builder += p -> ri._2
        case None =>
      } 
    }
    new GameSimulator(stateMachine, players, new PlayerInfo(builder.result))
  }
}