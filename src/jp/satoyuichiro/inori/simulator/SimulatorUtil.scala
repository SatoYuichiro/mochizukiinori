package jp.satoyuichiro.inori.simulator

import scala.io.Source
import java.io.File
import org.ggp.base.util.gdl.factory.GdlFactory
import org.ggp.base.util.gdl.grammar.Gdl
import org.ggp.base.util.statemachine.MachineState
import org.ggp.base.util.statemachine.Role
import jp.satoyuichiro.inori.cdl.Position

case class Episode(val positions: List[Position]) {
  
  def playout: Position = positions.last
  
  def at(n: Int): Position = positions.drop(n - 1).head
}

object SimulatorUtil {
  
  def getRules(filePath: String): List[Gdl] = readGdl(filePath) map (r => GdlFactory.create(r))
  
  def readGdl(filePath: String): List[String] = {
    var lines = List.empty[String]
    val input = Source.fromFile(new File(filePath))
    for (line <- input.getLines) {
        lines = cutComment(line) :: lines
    }
    cutter(lines.reverse.fold("")(_ + _))
  }

  def cutter(code: String): List[String] = {
    var lines = List.empty[String]
    var subCode = code
    while (1 <= subCode.size) {
//      println("subCode " + subCode)
      var i = 1
      var aCode = subCode.substring(0, 1)
      subCode = subCode.substring(1)
      while (i != 0 && !subCode.isEmpty()) {
        aCode += subCode.substring(0, 1)
        if (subCode.substring(0, 1) == "(") {
          i += 1
        } else if (subCode.substring(0, 1) == ")") {
          i -= 1
        }
        subCode = subCode.substring(1)
      }
//      println("aCode" + aCode)
      lines = aCode :: lines
//      println(lines)
    }
//    println(lines)
    lines.reverse
  }

  val spacer = List(" ", "\n", "\t")

  def cutComment(line: String): String = {
    val str = cutSpacer(line)
    if (str.startsWith(";"))
      cutTillEndOfLine(str)
    else
      str
  }

  def cutSpacer(line: String): String = {
    var subCode = line
    while (1 <= subCode.size && spacer.contains(subCode.substring(0, 1))) {
      subCode = subCode.substring(1)
    }
    subCode
  }

  def cutTillEndOfLine(line: String): String = {
    line.split("\n").tail.fold("")(_ + _)
  }
  
  def getControl(machineState: MachineState): Role = {
    null
  }
}