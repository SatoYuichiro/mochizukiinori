package jp.satoyuichiro.inori.simulator

import org.ggp.base.util.statemachine.implementation.prover.ProverStateMachine
import jp.satoyuichiro.inori.player.PlayerUtil
import jp.satoyuichiro.inori.player.AlphaBetaPlayer
import org.ggp.base.util.statemachine.Role
import jp.satoyuichiro.inori.player.Player
import org.ggp.base.util.gdl.factory.GdlFactory
import org.ggp.base.util.gdl.grammar.Gdl
import org.ggp.base.util.statemachine.Move
import org.ggp.base.util.gdl.grammar.GdlConstant
import jp.satoyuichiro.inori.gdl.Description
import jp.satoyuichiro.inori.learning.concept.adapter.SuccRelationGenerator
import jp.satoyuichiro.inori.gdl.GdlUtil
import jp.satoyuichiro.inori.learning.concept.adapter.SuccAdapter
import jp.satoyuichiro.inori.player.RandomPlayer

object VSHuman {

  def main(args: Array[String]): Unit = {
    if (args.size < 1) {
      println("file path is none")
      sys.exit(-1)
    }
    val description = Description(args(0)).getDescription
    val stateMachine = new ProverStateMachine
    stateMachine.initialize(description)
    val adapter = new SuccAdapter((new SuccRelationGenerator()).generate(GdlUtil.toScalaList(description)))
    GdlUtil.toScalaList(description) foreach println
    
    val roles = stateMachine.getRoles()
    println("choose your role")
    println(roles)
    val choosenRoleStr = readLine
    println("you are " + choosenRoleStr)
    var myRole : Role = null
    val roleIndices = stateMachine.getRoleIndices()
    PlayerUtil.toScalaList(roles) foreach {
      r =>
        if (r.toString == choosenRoleStr)
          myRole = r
    }
    
    var aiList = List.empty[Tuple2[Player, Role]]
    PlayerUtil.toScalaList(roles) foreach {
      r => if (r.toString != choosenRoleStr) aiList ::= (new RandomPlayer(stateMachine, r), r)//(new AlphaBetaPlayer(stateMachine, r), r)
    }
    var currentState = stateMachine.getInitialState()
    println("game start")
    while(!stateMachine.isTerminal(currentState)) {
      println("\ncurrent state")
      PlayerUtil.printMachineState(adapter.convert(currentState))
      val moves = stateMachine.getLegalMoves(currentState, myRole)
      var myMove : Move = null
      while(myMove == null) {
        println("choose move")
        println(moves)
        val choosenStr = readLine
        if (choosenStr == "exit") sys.exit(0)
        PlayerUtil.toScalaList(moves) foreach {
          m =>
            if (m.toString == choosenStr)
              myMove = m
        }
      }
      val aiMoves = aiList map (ai => {println(ai._2 + " choosing a move"); (ai._1.chooseMove(currentState), ai._2)})
      var moveArray = new Array[Move](aiMoves.size + 1)
      moveArray(roleIndices.get(myRole)) = myMove
      aiMoves foreach {
        move =>
          println("player " + move._2 + " choosen " + move._1)
          val i = roleIndices.get(move._2)
          moveArray(i) = move._1
      }
      currentState = stateMachine.getNextState(currentState, PlayerUtil.toJavaList(moveArray.toList))
    }
    PlayerUtil.printMachineState(currentState)
    println("Your score " + stateMachine.getGoal(currentState, myRole))
    aiList foreach {ai => println("score of " + ai._2 + " " + stateMachine.getGoal(currentState, ai._2))}
  }
}