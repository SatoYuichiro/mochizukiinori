package jp.satoyuichiro.inori.playertest

import org.junit._
import org.junit.Assert._
import junit.framework.TestCase
import jp.satoyuichiro.inori.player._

class TypedPieceTest extends TestCase {

  def testArgument {
    val argument1 = Argument(Array(IntArgument(), IntArgument(), StringArgument()), 3)
    assertEquals(Some(0), argument1.getFirstIntIndex)
    assertEquals(Some(1), argument1.getSecondIntIndex)
    assertEquals(Some(2), argument1.getFirstStringIndex)
  }
}