package jp.satoyuichiro.inori.playertest

import org.junit._
import org.junit.Assert._
import jp.satoyuichiro.inori.player.PlayerUtil
import junit.framework.TestCase
import org.ggp.base.util.gdl.factory.GdlFactory
import jp.satoyuichiro.inori.player.PlayerUtil._
import jp.satoyuichiro.inori.player.TypedPiece
import org.ggp.base.util.gdl.grammar.GdlRelation
import org.ggp.base.util.gdl.grammar.GdlFunction
import org.ggp.base.util.gdl.grammar.GdlSentence
import org.ggp.base.util.statemachine.implementation.prover.ProverStateMachine
import org.ggp.base.util.statemachine.MachineState

class PlayerUtilTest extends TestCase {

  def testcheckArgumentType {
    val input1 = List("(init (cell 1 2 o))")
    val gdl1 = makeInput(input1).getContents().iterator().next()
//    assertEquals(true, gdl1.asInstanceOf[Gdl])
    println(gdl1)
//    val result1 = TypedPiece.checkArgumentType(gdl1)
//    val expected1 = Argument(Array(IntArgument(), IntArgument(), StringArgument()), 3)
//    assertEquals(expected1.argumentType.deep, result1.argumentType.deep)
//    assertEquals(expected1.arity, result1.arity)

    val input2 = List("(init (cell blanck 3 2 hoge))")
    val gdl2 = makeInput(input2).getContents().iterator().next()
    println(gdl2)
//    val result2 = TypedPiece.checkArgumentType(gdl2)
//    val expected2 = Argument(Array(StringArgument(), IntArgument(), IntArgument(), StringArgument()), 4)
//    assertEquals(expected2.argumentType.deep, result2.argumentType.deep)
//    assertEquals(expected2.arity, result2.arity)
  }
  
  def makeInput(str: List[String]): MachineState = {
    val stateMachine = new ProverStateMachine
    stateMachine.initialize(PlayerUtil.toJavaList(str map (l => GdlFactory.create(l))))
    stateMachine.getInitialState()
  }

  def testtruesToGdlFunctions {
    val input1 = List("(init (cell 1 1 b))", "(init (cell 1 2 b))", "(init (cell 2 3 o))", "(init (control player1))")
    val result1 = PlayerUtil.machineStateToGdlRelations(makeInput(input1))
    val output1 = List("( cell 1 1 b )", "( cell 1 2 b )", "( cell 2 3 o )", "( control player1 )")
    //    val expected1 = output1 map (l => GdlFactory.create(l).asInstanceOf[GdlRelation])
    assertEquals(1, (result1 map (_.toString()) filter (_ == output1.head)).size)
    assertEquals(1, (result1 map (_.toString()) filter (_ == output1.tail.head)).size)
    assertEquals(1, (result1 map (_.toString()) filter (_ == output1.drop(2).head)).size)
    assertEquals(1, (result1 map (_.toString()) filter (_ == output1.drop(3).head)).size)
  }
}