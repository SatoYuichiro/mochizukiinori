package jp.satoyuichiro.inori.simulatortest

import jp.satoyuichiro.inori.cdl._
import jp.satoyuichiro.inori.learning.concept.generator.PrimeConjunction
import jp.satoyuichiro.inori.learning.concept.generator.CompositeDisjunction
import jp.satoyuichiro.inori.learning.concept.specifier.Specialization
import org.ggp.base.util.statemachine.implementation.prover.ProverStateMachine
import jp.satoyuichiro.inori.gdl.Description
import jp.satoyuichiro.inori.gdl.GdlConverter
import jp.satoyuichiro.inori.gdl.GdlUtil
import jp.satoyuichiro.inori.simulator.GameSimulator
import jp.satoyuichiro.inori.player.RandomPlayer
import jp.satoyuichiro.inori.learning.concept.generator.HeuristicsGenerator
import jp.satoyuichiro.inori.player.PlayerUtil
import jp.satoyuichiro.inori.learning.concept.specifier.SpecifiedConcept
import jp.satoyuichiro.inori.learning.concept.specifier.SpecifiedHeuristics
import jp.satoyuichiro.inori.learning.concept.generator.PrimeHeuristics
import jp.satoyuichiro.inori.player.HeuristicsPlayer
import jp.satoyuichiro.inori.learning.concept.generator.CompositeHeuristics
import jp.satoyuichiro.inori.player.Heuristics
import jp.satoyuichiro.inori.player.AlphaBetaPlayer
import jp.satoyuichiro.inori.learning.concept.adapter.SuccAdapter
import jp.satoyuichiro.inori.learning.concept.adapter.SuccRelationGenerator

object SpecifiedHeuristicsTest {
  
  val x0 = Label("x0")
  val x1 = Label("x1")
  val x2 = Label("x2")
  val x3 = Label("x3")
  
  val one = Integer(1)
  val two = Integer(2)
  val mone = Integer(-1)
  val mtwo = Integer(-2)
  
  val piece1 = Piece(x0, x1, x2, x3)
  val piece2 = Piece(x0, x1, Addition(x2, mone), x3)
  val piece3 = Piece(x0, x1, Addition(x2, one), x3)
  
  val piece4 = Piece(x0, Addition(x1, one), Addition(x2, mtwo), x3)
  val piece5 = Piece(x0, Addition(x1, mone), Addition(x2, mone), x3)
  
  val piece6 = Piece(x0, Addition(x1, one), x2, x3)
  val piece7 = Piece(x0, Addition(x1, mone), x2, x3)
  
  val piece8 = Piece(x0, Addition(x1, mtwo), Addition(x2, mtwo), x3)
  
  val piece9 = Piece(x0, x1, Addition(x2, two), x3)
  
  val prime1 = PrimeConjunction(List(piece1, piece2, piece3)).putLabel(Label("p1"))
  val prime2 = PrimeConjunction(List(piece1, piece4, piece5)).putLabel(Label("p2"))
  val prime3 = PrimeConjunction(List(piece1, piece6, piece7)).putLabel(Label("p3"))
  val prime4 = PrimeConjunction(List(piece1, piece5, piece8)).putLabel(Label("p4"))
  val prime5 = PrimeConjunction(List(piece1, piece3, piece9)).putLabel(Label("p5"))
  val prime6 = PrimeConjunction(List(piece1, piece8, piece5)).putLabel(Label("p6"))

  val comp = CompositeDisjunction(List(prime1, prime2, prime3, prime4, prime5, prime6)).putLabel(Label("composit"))
  
  val sp1 = Specialization(Map(x3 -> Symbol("r")))
  val sp2 = Specialization(Map(x3 -> Symbol("o")))
  val sp3 = Specialization(Map(x1 -> Integer(4), x3 -> Symbol("o")))
  
  val connect4Gdl = "./games/twoplayers/connect4.gdl"
  val fullConnect5Gdl = "./games/twoplayers/connect5.gdl"
  val connect5Gdl = "./games/twoplayers/mini-connect5.gdl"
  
  def main(args: Array[String]): Unit = {
//    val startTime = System.currentTimeMillis()
//    val redwin = doConnect4RedWin(100) flatMap PlayerUtil.makeAllTrinaryPosition
//    val endSimulation = System.currentTimeMillis()
//    println((endSimulation - startTime).toDouble / 1000.0 + " end Simulation")
//    
//    val heuristics1 = PrimeHeuristics(comp, (new HeuristicsGenerator()).generate(List(comp), redwin).head.evaluation)
//    val spcomp1 = (new SpecifiedConcept(comp, sp1)).toRichConcept
//    val heuristics2 = PrimeHeuristics(spcomp1, (new HeuristicsGenerator()).generate(List(spcomp1), redwin).head.evaluation)
//    val endMakeHeuristics = System.currentTimeMillis()
//    println((endMakeHeuristics - endSimulation).toDouble / 1000.0 + " end make Heuristics")
//    println(heuristics1.evaluation)
//    println(heuristics2.evaluation)
//    
//    val heuristicsSp1 = new SpecifiedHeuristics(comp, sp2, heuristics2.evaluation).toHeuristics
//    val heuristicsSp2 = new SpecifiedHeuristics(comp, sp3, heuristics2.evaluation).toHeuristics
//    
//    val simulationTimes = 100
//    println("simulation " + simulationTimes)
//    println("connect4 simulation")
//    doSimulateNoHeuristics(connect4Gdl, simulationTimes, 1)
//    doSimulate(connect4Gdl, heuristics1, simulationTimes, 1)
//    doSimulate(connect4Gdl, heuristics2, simulationTimes, 1)
//    
//    println("vs AlphaBeta")
//    doSimulateNoHeuristicsAlphaBeta(connect4Gdl, simulationTimes, 1)
//    doSimulateAlphaBeta(connect4Gdl, heuristics1, simulationTimes, 1)
//    doSimulateAlphaBeta(connect4Gdl, heuristics2, simulationTimes, 1)

//    println("mini-connect5 simulation")
//    doSimulateNoHeuristicsAlphaBetaC5(simulationTimes, 1)
//    doSimulateAlphaBetaC5(heuristicsSp1, simulationTimes, 1)
//    doSimulateAlphaBetaC5(heuristicsSp2, simulationTimes, 1)
    
    doFullConnect5()
 }
  
  def doConnect4WhiteWin(n:Int): List[Position] = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(connect4Gdl).getDescription)
    val roles = stateMachine.getRoles()
    val initPosition = GdlConverter.gdlTocdl(stateMachine.getInitialState()).position
    val players = GdlUtil.toScalaList(roles) map (r => new RandomPlayer(stateMachine, r))
    val tictactoeSimulator = GameSimulator(stateMachine, players)
    tictactoeSimulator.simulateWinner(players.head, n) map (p => GdlConverter.gdlTocdl(p)) map (p => Position(p.position filter (pi => !initPosition.contains(pi)), None))
  }

  def doConnect4RedWin(n:Int): List[Position] = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(connect4Gdl).getDescription)
    val roles = stateMachine.getRoles()
    val initPosition = GdlConverter.gdlTocdl(stateMachine.getInitialState()).position
    val players = GdlUtil.toScalaList(roles) map (r => new RandomPlayer(stateMachine, r))
    val tictactoeSimulator = GameSimulator(stateMachine, players)
    tictactoeSimulator.simulateWinner(players.tail.head, n) map (p => GdlConverter.gdlTocdl(p)) map (p => Position(p.position filter (pi => !initPosition.contains(pi)), None))
  }
  
  def doSimulateNoHeuristics(gdl: String, n: Int, depth: Int): Unit = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(gdl).getDescription)
    val roles = GdlUtil.toScalaList(stateMachine.getRoles())
    val players = List(new RandomPlayer(stateMachine, roles.head),
        new AlphaBetaPlayer(stateMachine, roles.tail.head, depth))
    val simulator = GameSimulator(stateMachine, players)
    
    val results = simulator.simulateMatch(n)
    
    val firstWin = results filter (_.getOrElse(players.head, false)) size
    val secondWin = results filter (_.getOrElse(players.tail.head, false)) size
    val drawN = n - firstWin - secondWin
    
    print(players.head.role + " " + firstWin + " ")
    print(players.tail.head.role + " " + secondWin + " ")
    println("draw " + drawN + " n " + n)
   }
  
  def doSimulate(gdl: String, heuristics: PrimeHeuristics, n: Int, depth: Int): Unit = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(gdl).getDescription)
    val roles = GdlUtil.toScalaList(stateMachine.getRoles())
    val players = List(new RandomPlayer(stateMachine, roles.head),
        new HeuristicsPlayer(Heuristics(CompositeHeuristics(List(heuristics))), stateMachine, roles.tail.head, depth))
    val simulator = GameSimulator(stateMachine, players)
    
    val results = simulator.simulateMatch(n)
    
    val firstWin = results filter (_.getOrElse(players.head, false)) size
    val secondWin = results filter (_.getOrElse(players.tail.head, false)) size
    val drawN = n - firstWin - secondWin
    
    print(players.head.role + " " + firstWin + " ")
    print(players.tail.head.role + " " + secondWin + " ")
    println("draw " + drawN + " n " + n)
  }
  
  def doSimulateNoHeuristicsAlphaBeta(gdl: String, n: Int, depth: Int): Unit = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(gdl).getDescription)
    val roles = GdlUtil.toScalaList(stateMachine.getRoles())
    val players = List(new AlphaBetaPlayer(stateMachine, roles.head, depth),
        new AlphaBetaPlayer(stateMachine, roles.tail.head, depth))
    val simulator = GameSimulator(stateMachine, players)
    
    val results = simulator.simulateMatch(n)
    
    val firstWin = results filter (_.getOrElse(players.head, false)) size
    val secondWin = results filter (_.getOrElse(players.tail.head, false)) size
    val drawN = n - firstWin - secondWin
    
    print(players.head.role + " " + firstWin + " ")
    print(players.tail.head.role + " " + secondWin + " ")
    println("draw " + drawN + " n " + n)
   }
  
  def doSimulateAlphaBeta(gdl: String, heuristics: PrimeHeuristics, n: Int, depth: Int): Unit = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(gdl).getDescription)
    val roles = GdlUtil.toScalaList(stateMachine.getRoles())
    val players = List(new AlphaBetaPlayer(stateMachine, roles.head, depth),
        new HeuristicsPlayer(Heuristics(CompositeHeuristics(List(heuristics))), stateMachine, roles.tail.head, depth))
    val simulator = GameSimulator(stateMachine, players)
    
    val results = simulator.simulateMatch(n)
    
    val firstWin = results filter (_.getOrElse(players.head, false)) size
    val secondWin = results filter (_.getOrElse(players.tail.head, false)) size
    val drawN = n - firstWin - secondWin
    
    print(players.head.role + " " + firstWin + " ")
    print(players.tail.head.role + " " + secondWin + " ")
    println("draw " + drawN + " n " + n)
  }
  
  def doSimulateNoHeuristicsAlphaBetaC5(connect5Gdl: String, n: Int, depth: Int): Unit = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(connect5Gdl).getDescription)
    val roles = GdlUtil.toScalaList(stateMachine.getRoles())
    
    val players = List(new AlphaBetaPlayer(stateMachine, roles.head, depth),
        new AlphaBetaPlayer(stateMachine, roles.tail.head, depth))
    val simulator = GameSimulator(stateMachine, players)
    
    val results = simulator.simulateMatch(n)
    
    val firstWin = results filter (_.getOrElse(players.head, false)) size
    val secondWin = results filter (_.getOrElse(players.tail.head, false)) size
    val drawN = n - firstWin - secondWin
    
    print(players.head.role + " " + firstWin + " ")
    print(players.tail.head.role + " " + secondWin + " ")
    println("draw " + drawN + " n " + n)
   }
  
  def doSimulateAlphaBetaC5(connect5Gdl: String, heuristics: PrimeHeuristics, n: Int, depth: Int): Unit = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(connect5Gdl).getDescription)
    val roles = GdlUtil.toScalaList(stateMachine.getRoles())

    val succAdapter = new SuccAdapter((new SuccRelationGenerator()).generate(GdlUtil.toScalaList(Description(connect5Gdl).getDescription)))
    val conn5Adapter = new Connect5Adapter(succAdapter)
    
    val players = List(new AlphaBetaPlayer(stateMachine, roles.head, depth),
        new HeuristicsPlayer(Heuristics(CompositeHeuristics(List(heuristics)), conn5Adapter), stateMachine, roles.tail.head, depth))
    val simulator = GameSimulator(stateMachine, players)
    
    val results = simulator.simulateMatch(n)
    
    val firstWin = results filter (_.getOrElse(players.head, false)) size
    val secondWin = results filter (_.getOrElse(players.tail.head, false)) size
    val drawN = n - firstWin - secondWin
    
    print(players.head.role + " " + firstWin + " ")
    print(players.tail.head.role + " " + secondWin + " ")
    println("draw " + drawN + " n " + n)
  }
  
  def doFullConnect5() {
    val times = 100
    val depth = 1
    val evaluation = 4.361648185954659E-4
    val heuristicsSp1 = new SpecifiedHeuristics(comp, sp2, evaluation).toHeuristics
    val heuristicsSp2 = new SpecifiedHeuristics(comp, sp3, evaluation).toHeuristics
    println("full connect5   search depth " + depth)
//    doSimulateNoHeuristicsAlphaBetaC5(fullConnect5Gdl, times, depth)
    doSimulateAlphaBetaC5(fullConnect5Gdl, heuristicsSp1, times, depth)
    doSimulateAlphaBetaC5(fullConnect5Gdl, heuristicsSp2, times, depth)
  }
}