package jp.satoyuichiro.inori.simulatortest

import jp.satoyuichiro.inori.cdl._
import jp.satoyuichiro.inori.learning.concept.generator._
import org.ggp.base.util.statemachine.StateMachine
import org.ggp.base.util.statemachine.implementation.prover.ProverStateMachine
import jp.satoyuichiro.inori.player.PlayerUtil
import jp.satoyuichiro.inori.player.RandomPlayer

object Benchmark {

  def main(args: Array[String]): Unit = {
    val generator = new ArithmeticConjunctionGenerator
    val positions = getPositions
    var concepts = List.empty[PrimeConjunction]
    positions map (
      p => PlayerUtil.makeBinaryPosition(p) map (
        pair => 
          try {
            concepts ::= generator.generate(pair).get
          } catch {
            case _ : Throwable =>
        }))
    concepts = concepts 
    val labeler = GeneratorUtil.Labeler.makeLabeler("pattern")
    concepts foreach {c => println(c.putLabel(labeler()).toDefineConcept.get.toLisp) }
  }
  
  def getPositions: List[Position] = {
    var positions = List.empty[Position]
    for (i <- 0 to 999) {
      positions ::= simulateTictactoe
    }
    positions
  }

  def simulateTictactoe: Position = {
    val rules = PlayerUtil.readGdl("./games/twoplayers/tictactoe.gdl")
    val description = PlayerUtil.toDescription(rules)
    val stateMachine = new ProverStateMachine
    stateMachine.initialize(PlayerUtil.toJavaList(description))
    
    val roles = PlayerUtil.toScalaList(stateMachine.getRoles())
    val players = roles map (role => new RandomPlayer(stateMachine, role))
    var currentState = stateMachine.getInitialState
    while(!stateMachine.isTerminal(currentState)) {
      val moves = players map (_.chooseMove(currentState))
      currentState = stateMachine.getNextState(currentState, PlayerUtil.toJavaList(moves))
    }
    PlayerUtil.gdlTocdl(currentState)
  }
}