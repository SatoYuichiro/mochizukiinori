package jp.satoyuichiro.inori.simulatortest

import org.ggp.base.util.statemachine.implementation.prover.ProverStateMachine
import jp.satoyuichiro.inori.gdl.Description
import jp.satoyuichiro.inori.gdl.GdlUtil
import jp.satoyuichiro.inori.player.RandomPlayer
import jp.satoyuichiro.inori.simulator.GameSimulator
import jp.satoyuichiro.inori.cdl._
import jp.satoyuichiro.inori.gdl.GdlConverter
import jp.satoyuichiro.inori.learning.concept.generator.ArithmeticConjunctionGenerator
import jp.satoyuichiro.inori.learning.concept.generator.GeneratorUtil
import jp.satoyuichiro.inori.player.PlayerUtil
import jp.satoyuichiro.inori.learning.concept.generator.RichConcept
import jp.satoyuichiro.inori.learning.concept.generator.HeuristicsGenerator
import jp.satoyuichiro.inori.learning.concept.generator.PrimeConcept
import jp.satoyuichiro.inori.learning.concept.generator.DisjunctionGenerator

object ConceptGenerationTest {

  val tictactoeGdl = "./games/twoplayers/tictactoe.gdl"
  val connect4Gdl = "./games/twoplayers/connect4.gdl"
  
  def main(args: Array[String]): Unit = {
    // tictactoe 1000 simulation
    val simulateResult = doTicTacToeSimulate(1000)
    
    // make concepts from simulation
    val biConcepts = makeBiConcepts(simulateResult)
    val labeler1 = GeneratorUtil.Labeler.makeLabeler("bi")
    biConcepts foreach { c => println(c.putLabel(labeler1()).toDefineConcept.get.toLisp) }

    val triConcepts = makeTriConcepts(simulateResult)
    val labeler2 = GeneratorUtil.Labeler.makeLabeler("tri")
    triConcepts foreach { c => println(c.putLabel(labeler2()).toDefineConcept.get.toLisp) }

    val labeler = GeneratorUtil.Labeler.makeLabeler("pattern")
    val allConcepts = (biConcepts ++ triConcepts) map(_.putLabel(labeler())) map (_.asInstanceOf[PrimeConcept])
    
    val disjunctionGenerator = new DisjunctionGenerator()
    val triPrimeConcepts = triConcepts map (_.putLabel(labeler())) map (_.asInstanceOf[PrimeConcept])
    
    def containsInteger(piece: Piece): Boolean = (piece.args map (_.isInstanceOf[Integer])).fold(false)(_ || _)
    def filterNonVariabled(pieces: List[Piece]): Boolean =
      pieces.head.args.contains(Label("x3")) && !containsInteger(pieces.tail.head) && !containsInteger(pieces.tail.tail.head)
    
    val variabled = triPrimeConcepts filter (c => filterNonVariabled(c.pieces))
//    val disjunction = disjunctionGenerator.generate(triConcepts map (_.putLabel(labeler())) map (_.asInstanceOf[PrimeConcept]), simulateResult)
    val disjunction = disjunctionGenerator.generate(variabled, simulateResult)
    println(disjunction)
    disjunction.get.primes foreach {p => println(p.toConcept.toLisp)}
    // make heuristics from simulation
    //val heuGenerator = new HeuristicsGenerator()
    //val heuristics = heuGenerator.generate(allConcepts, doConnect4RedWin(100))
    
    //heuristics foreach println
    // extend heuristics structure
    // apply it to connect4
  }

  def doTicTacToeSimulate(n: Int): List[Position] = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(tictactoeGdl).getDescription)
    val roles = stateMachine.getRoles()
    val players = GdlUtil.toScalaList(roles) map (r => new RandomPlayer(stateMachine, r))
    val tictactoeSimulator = GameSimulator(stateMachine, players)
//    tictactoeSimulator.simulateWinner(players.head, n) foreach {p => PlayerUtil.printMachineState(p)}
    tictactoeSimulator.simulateWinner(players.head, n) map (p => GdlConverter.gdlTocdl(p))
  }
  
  def doConnect4RedWin(n:Int): List[Position] = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(connect4Gdl).getDescription)
    val roles = stateMachine.getRoles()
    val players = GdlUtil.toScalaList(roles) map (r => new RandomPlayer(stateMachine, r))
    val tictactoeSimulator = GameSimulator(stateMachine, players)
    tictactoeSimulator.simulateWinner(players.head, n) map (p => GdlConverter.gdlTocdl(p))
  }

  def doConnect4WhiteWin(n:Int): List[Position] = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(connect4Gdl).getDescription)
    val roles = stateMachine.getRoles()
    val players = GdlUtil.toScalaList(roles) map (r => new RandomPlayer(stateMachine, r))
    val tictactoeSimulator = GameSimulator(stateMachine, players)
    tictactoeSimulator.simulateWinner(players.tail.head, n) map (p => GdlConverter.gdlTocdl(p))
  }

  def makeBiConcepts(positions: List[Position]): List[RichConcept] = {
    val generator = new ArithmeticConjunctionGenerator()
    val cells = positions map (
        position => new Position(
            position.position.filter(piece => piece.isInstanceOf[Piece]).filter(piece => piece.asInstanceOf[Piece].args.size > 2), None))
    
    val biPositions = cells flatMap (p => PlayerUtil.makeAllBinaryPosition(p))
    GeneratorUtil.generateUniquConcept(generator, biPositions)
  }
  
  def makeTriConcepts(positions: List[Position]): List[RichConcept] = {
    val generator = new ArithmeticConjunctionGenerator()
    val cells = positions map (
        position => new Position(
            position.position.filter(piece => piece.isInstanceOf[Piece]).filter(piece => piece.asInstanceOf[Piece].args.size > 2), None))
    
    val triPositions = cells flatMap (p => PlayerUtil.makeTrinaryPosition(p))
    GeneratorUtil.generateUniquConcept(generator, triPositions)
  }
}