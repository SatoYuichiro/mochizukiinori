package jp.satoyuichiro.inori.simulatortest

import jp.satoyuichiro.inori.cdl._
import jp.satoyuichiro.inori.learning.concept.generator.PrimeConjunction
import jp.satoyuichiro.inori.learning.concept.generator.CompositeDisjunction
import org.ggp.base.util.statemachine.implementation.prover.ProverStateMachine
import jp.satoyuichiro.inori.gdl.Description
import jp.satoyuichiro.inori.gdl.GdlUtil
import jp.satoyuichiro.inori.player.RandomPlayer
import jp.satoyuichiro.inori.simulator.GameSimulator
import jp.satoyuichiro.inori.gdl.GdlConverter
import jp.satoyuichiro.inori.learning.concept.specifier.ConceptSpecifier
import jp.satoyuichiro.inori.learning.concept.specifier.ConceptSpecifier
import jp.satoyuichiro.inori.learning.concept.generator.RichPattern
import jp.satoyuichiro.inori.learning.concept.generator.GeneratorUtil
import jp.satoyuichiro.inori.player.PlayerUtil
import jp.satoyuichiro.inori.learning.concept.generator.HeuristicsGenerator
import jp.satoyuichiro.inori.learning.concept.adapter.SuccRelationGenerator
import jp.satoyuichiro.inori.learning.concept.adapter.SuccAdapter

object KnowledgeTransferTest {

  val concept1 = "(concept tri1 (pos) (and " + 
    "(unificate (piece x0 x1 x2 x3) (arg 0 pos))" +
    "(= (piece x0 x1 (+ x2 -1) x3) (arg 1 pos))" +
    "(= (piece x0 x1 (+ x2 1) x3) (arg 2 pos))))"

  val concept2 = "(concept tri2 (pos) (and " +
    "(unificate (piece x0 x1 x2 x3) (arg 0 pos))" +
    "(= (piece x0 (+ x1 1) (+ x2 -2) x3) (arg 1 pos))" +
    "(= (piece x0 (+ x1 -1) (+ x2 -1) x3) (arg 2 pos))))"

  val concept3 = "(concept tri3 (pos) (and " +
    "(unificate (piece x0 x1 x2 x3) (arg 0 pos))" +
    "(= (piece x0 (+ x1 1) x2 x3) (arg 1 pos))" +
    "(= (piece x0 (+ x1 -1) x2 x3) (arg 2 pos))))"

  val concept4 = "(concept tri4 (pos) (and " +
    "(unificate (piece x0 x1 x2 x3) (arg 0 pos))" +
    "(= (piece x0 (+ x1 -1) (+ x2 -1) x3) (arg 1 pos))" +
    "(= (piece x0 (+ x1 -2) (+ x2 -2) x3) (arg 2 pos))))"

  val concept5 = "(concept tri5 (pos) (and " +
    "(unificate (piece x0 x1 x2 x3) (arg 0 pos))" +
    "(= (piece x0 x1 (+ x2 1) x3) (arg 1 pos))" +
    "(= (piece x0 x1 (+ x2 2) x3) (arg 2 pos))))"

  val concept6 = "(concept tri6 (pos) (and " +
    "(unificate (piece x0 x1 x2 x3) (arg 0 pos))" +
    "(= (piece x0 (+ x1 -2) x2 x3) (arg 1 pos))" +
    "(= (piece x0 (+ x1 -1) x2 x3) (arg 2 pos))))"

  val line = "(concept disjunction (pos) " +
    "(or (tri1 pos) (tri2 pos) (tri3 pos) (tri4 pos) (tri5 pos) (tri6 pos)))"

  val x0 = Label("x0")
  val x1 = Label("x1")
  val x2 = Label("x2")
  val x3 = Label("x3")
  
  val one = Integer(1)
  val two = Integer(2)
  val mone = Integer(-1)
  val mtwo = Integer(-2)
  
  val piece1 = Piece(x0, x1, x2, x3)
  val piece2 = Piece(x0, x1, Addition(x2, mone), x3)
  val piece3 = Piece(x0, x1, Addition(x2, one), x3)
  
  val piece4 = Piece(x0, Addition(x1, one), Addition(x2, mtwo), x3)
  val piece5 = Piece(x0, Addition(x1, mone), Addition(x2, mone), x3)
  
  val piece6 = Piece(x0, Addition(x1, one), x2, x3)
  val piece7 = Piece(x0, Addition(x1, mone), x2, x3)
  
  val piece8 = Piece(x0, Addition(x1, mtwo), Addition(x2, mtwo), x3)
  
  val piece9 = Piece(x0, x1, Addition(x2, two), x3)
  
  val prime1 = PrimeConjunction(List(piece1, piece2, piece3)).putLabel(Label("p1"))
  val prime2 = PrimeConjunction(List(piece1, piece4, piece5)).putLabel(Label("p2"))
  val prime3 = PrimeConjunction(List(piece1, piece6, piece7)).putLabel(Label("p3"))
  val prime4 = PrimeConjunction(List(piece1, piece5, piece8)).putLabel(Label("p4"))
  val prime5 = PrimeConjunction(List(piece1, piece3, piece9)).putLabel(Label("p5"))
  val prime6 = PrimeConjunction(List(piece1, piece8, piece5)).putLabel(Label("p6"))

  val comp = CompositeDisjunction(List(prime1, prime2, prime3, prime4, prime5, prime6)).putLabel(Label("composit"))
  
  val connect4Gdl = "./games/twoplayers/connect4.gdl"
  val connect5Gdl = "./games/twoplayers/mini-connect5.gdl"
  
  def main(args: Array[String]): Unit = {
    
    // make playout 1000 of connect4
    val startTime = System.currentTimeMillis()
    val redwin = doConnect4RedWin(50)
    val whitewin = doConnect4WhiteWin(50)
    val endSimulation = System.currentTimeMillis()
    println((endSimulation - startTime).toDouble / 1000.0 + " end of simulation")
    
    // make specified concept
    val specifier = new ConceptSpecifier()
    val positiveExample = makeMatchableExample(comp, redwin.take(10))
    val negativeExample = makeMatchableExample(comp, whitewin.take(10))
    val endMakeExample = System.currentTimeMillis()
//    positiveExample foreach println
//    println
//    negativeExample foreach println
    println((endMakeExample - endSimulation).toDouble / 1000.0 + " end of making examples")
    val specified = specifier.specify(comp, positiveExample, negativeExample)
    val endSpecialization = System.currentTimeMillis()
    println((endSpecialization - endMakeExample).toDouble / 1000.0 + " end of specialization")
    println(specified.specialization)
    
    // make heuristics with specified concept
    val heuristicsGenerator = new HeuristicsGenerator()
//	    val connect4Heuristics = heuristicsGenerator.generate(List(specified.toRichConcept.asInstanceOf[RichPattern]), redwin flatMap PlayerUtil.makeAllTrinaryPosition)
//	    println(connect4Heuristics)
    
    // make playout 10 of connect5
    val succAdapter = new SuccAdapter((new SuccRelationGenerator()).generate(GdlUtil.toScalaList(Description(connect5Gdl).getDescription)))
    println(succAdapter.succRelations.head.succ)
    
    val startSimulationC5 = System.currentTimeMillis()
    val c5xwin = doConnect5XWin(50) sortBy(_.position.size) take(10)
    val c5owin = doConnect5OWin(50) sortBy(_.position.size) take(10)
    val endSimulationC5 = System.currentTimeMillis()
    println((endSimulationC5 - startSimulationC5).toDouble / 1000.0 + " end of simulation")
    
    // make specified concept
    val connect5Pos = makeMatchableExample(comp, c5owin map (p => succAdapter.convert(p)))
    val connect5Neg = makeMatchableExample(comp, c5xwin map (p => succAdapter.convert(p)))
    println(connect5Pos.size + " " + connect5Neg.size)
    val endMakeExampleC5 = System.currentTimeMillis()
    println((endMakeExampleC5 - endSimulationC5).toDouble / 1000.0 + " end of making Examples")
    val connect5Specify = specifier.specify(comp, connect5Pos, connect5Neg)
    val endSpecializationC5 = System.currentTimeMillis()
    println((endSpecializationC5 - endMakeExampleC5).toDouble / 1000.0 + " end of specialization")
    println(connect5Specify.specialization)
    
    // replace specification to connect4 to connect5
  }
  
  def doConnect4WhiteWin(n:Int): List[Position] = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(connect4Gdl).getDescription)
    val roles = stateMachine.getRoles()
    val initPosition = GdlConverter.gdlTocdl(stateMachine.getInitialState()).position
    val players = GdlUtil.toScalaList(roles) map (r => new RandomPlayer(stateMachine, r))
    val tictactoeSimulator = GameSimulator(stateMachine, players)
    tictactoeSimulator.simulateWinner(players.head, n) map (p => GdlConverter.gdlTocdl(p)) map (p => Position(p.position filter (pi => !initPosition.contains(pi)), None))
  }

  def doConnect4RedWin(n:Int): List[Position] = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(connect4Gdl).getDescription)
    val roles = stateMachine.getRoles()
    val initPosition = GdlConverter.gdlTocdl(stateMachine.getInitialState()).position
    val players = GdlUtil.toScalaList(roles) map (r => new RandomPlayer(stateMachine, r))
    val tictactoeSimulator = GameSimulator(stateMachine, players)
    tictactoeSimulator.simulateWinner(players.tail.head, n) map (p => GdlConverter.gdlTocdl(p)) map (p => Position(p.position filter (pi => !initPosition.contains(pi)), None))
  }

  def doConnect5XWin(n:Int): List[Position] = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(connect5Gdl).getDescription)
    val roles = stateMachine.getRoles()
    val initPosition = GdlConverter.gdlTocdl(stateMachine.getInitialState()).position
    val players = GdlUtil.toScalaList(roles) map (r => new RandomPlayer(stateMachine, r))
    val tictactoeSimulator = GameSimulator(stateMachine, players)
    tictactoeSimulator.simulateWinner(players.head, n) map (p => GdlConverter.gdlTocdl(p)) map (p => Position(p.position filter (pi => !initPosition.contains(pi)), None))
  }

  def doConnect5OWin(n:Int): List[Position] = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(connect5Gdl).getDescription)
    val roles = stateMachine.getRoles()
    val initPosition = GdlConverter.gdlTocdl(stateMachine.getInitialState()).position
    val players = GdlUtil.toScalaList(roles) map (r => new RandomPlayer(stateMachine, r))
    val tictactoeSimulator = GameSimulator(stateMachine, players)
    tictactoeSimulator.simulateWinner(players.tail.head, n) map (p => GdlConverter.gdlTocdl(p)) map (p => Position(p.position filter (pi => !initPosition.contains(pi)), None))
  }
  def makeMatchableExample(concept: RichPattern, positions: List[Position]): List[Position] = {
    var result = List.empty[Position]
    val i = interpreter(concept)
    positions foreach {
      position =>
        PlayerUtil.makeAllTrinaryPosition(position) foreach {
          sub => if (i(sub)) result ::= sub
        }
    }
    result.reverse
  }
  
  def interpreter(concept: RichPattern): Position => Boolean = {
    val i = concept.load(new Interpreter)
    val makeQuery : Position => ConceptCall = (position: Position) => {
      ConceptCall(concept.conceptName.get, List(TypedExp(position, PositionType())))
    }
    (position: Position) => {
      i.interpret(makeQuery(position)) == Bool(true)
    }
  }
}