package jp.satoyuichiro.inori.simulatortest

import jp.satoyuichiro.inori.learning.concept.adapter.Adapter
import jp.satoyuichiro.inori.cdl.Position
import jp.satoyuichiro.inori.cdl.Piece
import jp.satoyuichiro.inori.cdl.Symbol
import org.ggp.base.util.statemachine.MachineState
import jp.satoyuichiro.inori.player.PlayerUtil
import jp.satoyuichiro.inori.learning.concept.adapter.SuccAdapter

class Connect5Adapter(val succ: SuccAdapter) extends Adapter {

  val b = Symbol("b")
  
  def convert(machineState: MachineState): Position = succ.convert(convert(PlayerUtil.gdlTocdl(machineState)))
  def convert(position: Position): Position = {
    Position(position.position collect { case p: Piece if p.args.last != b => p }, None)
  }
}