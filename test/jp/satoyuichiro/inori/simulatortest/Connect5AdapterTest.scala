package jp.satoyuichiro.inori.simulatortest

import org.junit._
import org.junit.Assert._
import junit.framework.TestCase
import org.ggp.base.util.statemachine.implementation.prover.ProverStateMachine
import jp.satoyuichiro.inori.gdl.Description
import jp.satoyuichiro.inori.gdl.GdlUtil
import jp.satoyuichiro.inori.learning.concept.adapter.SuccAdapter
import jp.satoyuichiro.inori.learning.concept.adapter.SuccRelationGenerator
import jp.satoyuichiro.inori.cdl._

class Connect5AdapterTest extends TestCase {

  val connect5Gdl = "./games/twoplayers/mini-connect5.gdl"
    
  val cell = Symbol("cell")
  val b = Symbol("b")
  val o = Symbol("o")
  val x = Symbol("x")
  val one = Integer(1)
  
  def test {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(connect5Gdl).getDescription)
    val roles = GdlUtil.toScalaList(stateMachine.getRoles())

    val succAdapter = new SuccAdapter((new SuccRelationGenerator()).generate(GdlUtil.toScalaList(Description(connect5Gdl).getDescription)))
    val conn5 = new Connect5Adapter(succAdapter)
    val init = stateMachine.getInitialState()
    println(conn5.convert(init))
    println(conn5.convert(Position(Piece(cell, one, one, b))))
    println(conn5.convert(Position(Piece(cell, one, one, o))))
    println(conn5.convert(Position(Piece(cell, one, one, x))))
  }
}