package jp.satoyuichiro.inori.cdltest

import org.junit._
import org.junit.Assert._

import jp.satoyuichiro.inori.cdl._
import junit.framework.TestCase

class ParserTest extends TestCase {

  def test {
    
  }
  
  def testmakeLet {
    val code1 = Cutter.lispSplit("(let (x 1 y 2) (+ x y))")
    val result1 = LetParseTree(
        List(SubstitutionParseTree(LabelLeaf("x"),IntLeaf(1)), SubstitutionParseTree(LabelLeaf("y"),IntLeaf(2))),
        BodyParseTree(List(ArithmeticParseTree(Tag("+"),List(LabelLeaf("x"), LabelLeaf("y"))))))
    assertEquals(result1, Parser.CodeMaker.makeLet(code1.tail))
  }
  
  def testmakeFor {
    val code1 = Cutter.lispSplit("(for (i 0) (< i 10) (set i (+ i 1)) (+ i 10))")
    val result1 = ForParseTree(
        IndexParseTree(List(SubstitutionParseTree(LabelLeaf("i"),IntLeaf(0)))),
        ConditionParseTree(Tag("<"),LabelLeaf("i"),IntLeaf(10)),
        BodyParseTree(List(SetParseTree(LabelLeaf("i"),ArithmeticParseTree(Tag("+"),List(LabelLeaf("i"), IntLeaf(1)))))),
            BodyParseTree(List(ArithmeticParseTree(Tag("+"),List(LabelLeaf("i"), IntLeaf(10))))))
    assertEquals(result1, Parser.CodeMaker.makeFor(code1.tail))
    
    val code2 = Cutter.lispSplit("(for (i 0) (< i 10) (set i (+ i 1)) ((+ i 10) (- 1 9))))")
    val result2 = ForParseTree(
        IndexParseTree(List(SubstitutionParseTree(LabelLeaf("i"),IntLeaf(0)))),
        ConditionParseTree(Tag("<"),LabelLeaf("i"),IntLeaf(10)),
        BodyParseTree(List(SetParseTree(LabelLeaf("i"),ArithmeticParseTree(Tag("+"),List(LabelLeaf("i"), IntLeaf(1)))))),
        BodyParseTree(List(
            ArithmeticParseTree(Tag("+"),List(LabelLeaf("i"), IntLeaf(10))),
            ArithmeticParseTree(Tag("-"),List(IntLeaf(1), IntLeaf(9))))))
    assertEquals(result2, Parser.CodeMaker.makeFor(code2.tail))
  }
  
  def testmakeIf {
    val code1 = Cutter.lispSplit("(if (true) 1 0)")
    val result1 = IfParseTree(BooleanLeaf(true),BodyParseTree(List(IntLeaf(1))),BodyParseTree(List(IntLeaf(0))))
    assertEquals(result1, Parser.CodeMaker.makeIf(code1.tail))

    val code2 = Cutter.lispSplit("(if (< 1 4) (+ 3 1) (- 3 1))")
    val result2 = IfParseTree(
        ConditionParseTree(Tag("<"),IntLeaf(1),IntLeaf(4)),
        BodyParseTree(List(ArithmeticParseTree(Tag("+"),List(IntLeaf(3), IntLeaf(1))))),
        BodyParseTree(List(ArithmeticParseTree(Tag("-"),List(IntLeaf(3), IntLeaf(1))))))
    assertEquals(result2, Parser.CodeMaker.makeIf(code2.tail))
  }
  
  def testisLispBody {
    val code1 = "((+ 1 2) (- 1 2))"
    assertEquals(true, Syntax.isLispBody(code1))
    val code2 = "(+ 1 2)"
    assertEquals(false, Syntax.isLispBody(code2))
    val code3 = "+"
    assertEquals(false, Syntax.isLispBody(code3))
  }
}