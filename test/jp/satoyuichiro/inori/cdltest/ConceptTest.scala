package jp.satoyuichiro.inori.cdltest

import org.junit._
import org.junit.Assert._

import jp.satoyuichiro.inori.cdl._
import junit.framework.TestCase

class ConceptTest extends TestCase {
  
  def test {
    val con1 = Compiler.compile("(concept con1 (x y) (+ x y))")
    val call1 = Compiler.compile("(con1 100 104)")
    val con2 = Compiler.compile("(concept con2 (con) (con 1 3))")
    val call2 = Compiler.compile("(con2 con1)")
    val con3 = Compiler.compile("(concept con3 (x y) (con1 x y))")
    val call3 = Compiler.compile("(con3 1 10)")
    val con4 = Compiler.compile("(concept con4 (x y) (if (< 2 (* x y (con1 3 4))) (con2 con1) (con3 1 1)))")
    val call4 = Compiler.compile("(con4 1 4)")
    val con5 = Compiler.compile("(concept con5 (con x y) (if (> 2 (con x y)) (con2 con1) (con3 1 1)))")
    val call5 = Compiler.compile("(con5 con1 1 4)")
    val i = new Interpreter
    i.interpret(con1)
    i.interpret(con2)
    i.interpret(con3)
    i.interpret(con4)
    i.interpret(con5)
    assertEquals(Integer(11), i.interpret(call3))
    assertEquals(Integer(4), i.interpret(call4))
    assertEquals(Integer(2), i.interpret(call5))
  }

  def testSubPosition {
    val makePair = Compiler.compile("(concept makePair (pos x y) (position (arg x pos) (arg y pos)))")
    val callMakePair = Compiler.compile("(makePair (position (piece 1) (piece 2) (piece 3) (piece 4) (piece 5)) 2 3)")
    val i = new Interpreter
    i.interpret(makePair)
    assertEquals(Position(Piece(Integer(3)), Piece(Integer(4))), i.interpret(callMakePair))
    val pattern0 = Compiler.compile("(pattern pattern0 (pos) (and (unificate (piece x y w) (arg 0 pos)) (= (piece (+ x 1) y w) (arg 1 pos))))")
    val evalPair = Compiler.compile("(heuristics heu1 (pos) (if (pattern0 pos) (0.1) (0.0)))")
    val evaluation = Compiler.compile("(heuristics f (pos) " + 
                                        "(let (result 0.0) " +
                                          "((for (i 0) (< i (size pos)) (set i (+ i 1)) " +
                                            "(for (j (+ i 1)) (< j (size pos)) (set j (+ j 1)) " +
                                              "(set result (+ result (heu1 (makePair pos i j)))) " +
                                           "result)))))");
    val call = Compiler.compile("(f (position (piece 1 1 1) (piece 1 2 1) (piece 1 3 1)))")
    i.interpret(pattern0)
    i.interpret(evalPair)
    i.interpret(evaluation)
//    println(evaluation)
    assertEquals(Real(0.1), i.interpret(call))
  }
}