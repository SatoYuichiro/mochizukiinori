package jp.satoyuichiro.inori.cdltest

import org.junit._
import org.junit.Assert._

import jp.satoyuichiro.inori.cdl._

import junit.framework.TestCase

class EnvironmentTest extends TestCase {

  val i = Label("i")
  val j = Label("j")
  
  val one = Integer(1)
  val two = Integer(2)
  val three = Integer(3)
  
  def testgetValue {
    val env = Environment.empty
    try {
      env.getValue(i)
      fail()
    } catch {
      case e : NoVariableException => ;
      case e : Exception => fail()
    }
    
    env.put(i, one)
    assertEquals(one, env.getValue(i))
    
    env.put(i, two)
    assertEquals(two, env.getValue(i))
    
    env.put(j, one)
    assertEquals(two, env.getValue(i))
    assertEquals(one, env.getValue(j))
  }
  
  def testgetOption {
    val env = Environment.empty
    
    assertEquals(None, env.getOption(i))
    
    env.put(i, one)
    assertEquals(Some(one), env.getOption(i))
  }
  
  def testgetPrevios {
    val env = Environment.empty
    env.put(i, one)
    env.put(j, two)
    
    val env2 = new Environment(env)
    assertEquals(one, env2.getValue(i))
    assertEquals(two, env2.getValue(j))
    
    env2.put(i, three)
    assertEquals(three, env2.getValue(i))
    assertEquals(two, env2.getValue(j))
    
    val env3 = new Environment(env2)
    assertEquals(three, env3.getValue(i))
    assertEquals(two, env3.getValue(j))
  }
  
  def testput {
    val env = Environment.empty
    env.put(i, two)
    env.put(j, three)
    val env2 = new Environment(env)
    env2.put(i, one)
    assertEquals(one, env2.getValue(i))
    assertEquals(three, env2.getValue(j))
  }
}