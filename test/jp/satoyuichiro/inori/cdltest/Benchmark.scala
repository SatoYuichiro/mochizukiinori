package jp.satoyuichiro.inori.cdltest

import jp.satoyuichiro.inori.cdl._

object Benchmark {

  def main(args: Array[String]): Unit = {
    val i = new Interpreter()
    i.interpret(makePattern)
    i.interpret(makeHeuristics)
    
    val makePositionStart = System.currentTimeMillis()
   
    val startTime = System.currentTimeMillis()
    val times = 10000
    for(n <- 0 to times) {
      makeCall.par foreach {c =>
        i.interpret(c)
      }
    }
    val endTime = System.currentTimeMillis()
    println((endTime - startTime) / 1000.0)
    println(makeCall.size * times + " pairs evaluated")
  }
  
  def makeCall: List[ConceptCall] = makePair(makePosition) map (p => ConceptCall(Label("heu1"), List(TypedExp(p, PositionType()))))
  
  def makePattern: DefineConcept = {
    Compiler.compile("(pattern pattern1 (pos) " +
        "(and (unificate (piece x y z w) (arg 0 pos))" +
        "(= (piece x (+ y 1) z w) (arg 1 pos))))").asInstanceOf[DefineConcept]
  }

  def makeHeuristics: DefineConcept = {
    Compiler.compile("(heuristics heu1 (pos) " +
        "(if (= true (pattern1 pos)) 0.1 0.0))").asInstanceOf[DefineConcept]
  }
  
  def makePosition: Position = {
    def makeSymbol: Symbol = Symbol((Math.random() * 3).toInt.toString)
    
    var pls = List.empty[Piece]
    for (i <- 0 to 6) {
      for (j <- 0 to 7) {
        pls ::= Piece(Symbol("cell"), Integer(i), Integer(j), makeSymbol)
      }
    }
    Position(pls,None)
  }
  
  def makePair(pos : Position): List[Position] = {
    var pls = List.empty[Position]
    val pieces = pos.position map (_.asInstanceOf[Piece])
    
    pieces foreach {
      p1 => pieces foreach {
        p2 =>
          val i1 = p1.arg(null, 1).asInstanceOf[Integer]
          val i2 = p2.arg(null, 1).asInstanceOf[Integer]
          if ((i1.value + 1) == i2.value) pls ::= Position(p1,p2)
      }
    }
    
    pieces foreach {
      p1 => pieces foreach {
        p2 =>
          val i1 = p1.arg(null, 2).asInstanceOf[Integer]
          val i2 = p2.arg(null, 2).asInstanceOf[Integer]
          if ((i1.value + 1) == i2.value) pls ::= Position(p1,p2)
      }
    }

    pls
  }
}