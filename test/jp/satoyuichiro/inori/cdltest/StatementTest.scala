package jp.satoyuichiro.inori.cdltest

import org.junit._
import org.junit.Assert._

import jp.satoyuichiro.inori.cdl._
import junit.framework.TestCase


class StatementTest extends TestCase {

  val x = Label("x")
  val y = Label("y")
  val z = Label("z")
  
  val one = Integer(1)
  val two = Integer(2)
  val three = Integer(3)
  
  val rone = Real(1.0)
  val rtwo = Real(2.0)
  val twopthree = Real(2.3)
  
  def testAddition {
    assertEquals(rtwo, Addition(one, rone).eval(Environment.empty))
    
    val env = Environment.empty
    env.put(x, one)
    env.put(y, two)
    val add = Addition(x,y)
    assertEquals(List(one, two), add.args map (_.eval(env).asInstanceOf[Number]))
    assertEquals(three, add.eval(env))
  }
  
  def testSubtraction {
    assertEquals(rtwo, Subtraction(three, rone).eval(Environment.empty))
    
    val env = Environment.empty
    env.put(x, one)
    env.put(y, two)
    assertEquals(Integer(-1), Subtraction(x,y).eval(env))
  }
  
  def testMultiplication {
    assertEquals(Real(24.0), Multiplication(Real(3.0), Real(4.0), Integer(2)).eval(Environment.empty))
    
    val env = Environment.empty
    env.put(x, one)
    env.put(y, two)
    assertEquals(two, Multiplication(x,y).eval(env))
  }
  
  def testDivision {
    assertEquals(Real(3.0), Division(Real(24.0), Real(4.0), Integer(2)).eval(Environment.empty))
    
    val env = Environment.empty
    env.put(x, one)
    env.put(y, two)
    assertEquals(Real(0.5), Division(x,y).eval(env))
  }
  
  def testModulo {
    assertEquals(Real(0.0), Modulo(Real(24.0), Integer(2)).eval(Environment.empty))
    assertEquals(Real(12.3 % 4.0), Modulo(Real(12.3), Real(4.0)).eval(Environment.empty))
    
    val env = Environment.empty
    env.put(x, three)
    env.put(y, two)
    assertEquals(one, Modulo(x,y).eval(env))
  }
  
  def testNot {
    
  }
  
  def testAnd {
    
  }
  
  def testOr {
    
  }
  
  def testEq {
    
  }
  
  def testGreater {
    assertEquals(Bool(true), Greater(one, two).eval(Environment.empty))
    assertEquals(Bool(false), Greater(two, one).eval(Environment.empty))
  }
  
  def testLess {
    assertEquals(Bool(false), Less(one, two).eval(Environment.empty))
    assertEquals(Bool(true), Less(two, one).eval(Environment.empty))
  }
  
  def testEquals {
    assertEquals(Bool(false), Equals(one, rone).eval(Environment.empty))
    assertEquals(Bool(true), Equals(one, one).eval(Environment.empty))    
  }
}