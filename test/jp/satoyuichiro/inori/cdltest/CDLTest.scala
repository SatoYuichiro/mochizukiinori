package jp.satoyuichiro.inori.cdltest

import org.junit._
import org.junit.Assert._

import jp.satoyuichiro.inori.cdl._
import junit.framework.TestCase

class CDLTest extends TestCase {
  
  def testcheckParenthesis {
    assertEquals(true, Parser.checkParenthesis("(+ 2 3)"))
    assertEquals(false, Parser.checkParenthesis("( ( )"))
    assertEquals(true, Parser.checkParenthesis("( () ( ( ( ))) () )"))
  }
  
  def testcountParenthesis {
    assertEquals(4, Parser.countParenthesis('(', 3))
    assertEquals(2, Parser.countParenthesis(')', 3))
    assertEquals(3, Parser.countParenthesis(' ', 3))
  }
  
  def testlispSplit {
    val res = Cutter.lispSplit("(let (env x 1 y 2) (+ x y))")
    assertEquals(3, res.size)
    assertEquals("let", res.head)
    assertEquals("(env x 1 y 2)", res.tail.head)
    assertEquals("(+ x y)", res.tail.tail.head)
  }
  
  def testcutListBlck {
    val res = Cutter.cutLispBlock("(+ 1 2 3) 2 3 4")
    assertEquals("(+ 1 2 3)", res._1)
    assertEquals(" 2 3 4", res._2)
  }
  
  def testcutListLiteral {
    val res = Cutter.cutLispLiteral(" 2 3 4")
    assertEquals("2", res._1)
    assertEquals("3 4", res._2)
    val res2 = Cutter.cutLispLiteral(" let (env x 1 y 2) (+ x y))")
    assertEquals("let", res2._1)
    assertEquals("(env x 1 y 2) (+ x y))", res2._2)
  }
  
}