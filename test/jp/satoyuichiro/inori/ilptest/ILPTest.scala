package jp.satoyuichiro.inori.ilptest

import jp.satoyuichiro.inori.cdl._
import jp.satoyuichiro.inori.ilp.AlephEncoder

object ILPTest {
  
  val cell = Symbol("cell")
  val o = Symbol("o")
  val x = Symbol("x")
  val blank = Symbol("blank")
  val control = Symbol("control")
  
  val one = Integer(1)
  val two = Integer(2)
  val thr = Integer(3)
  val four = Integer(4)
  val five = Integer(5)
  val six = Integer(6)
  
  val piece1b = Piece(cell, one, one, blank)
  val piece1x = Piece(cell, one, one, x)
  val piece1o = Piece(cell, one, one, o)
  
  val piece2b = Piece(cell, two, one, blank)
  val piece2x = Piece(cell, two, one, x)
  val piece2o = Piece(cell, two, one, o)
  
  val piece3b = Piece(cell, thr, one, blank)
  val piece3x = Piece(cell, thr, one, x)
  val piece3o = Piece(cell, thr, one, o)
  
  val piece4b = Piece(cell, one, two, blank)
  val piece4x = Piece(cell, one, two, x)
  val piece4o = Piece(cell, one, two, o)
  
  val piece5b = Piece(cell, two, two, blank)
  val piece5x = Piece(cell, two, two, x)
  val piece5o = Piece(cell, two, two, o)
  
  val piece6b = Piece(cell, thr, two, blank)
  val piece6x = Piece(cell, thr, two, x)
  val piece6o = Piece(cell, thr, two, o)
  
  val piece7b = Piece(cell, one, thr, blank)
  val piece7x = Piece(cell, one, thr, x)
  val piece7o = Piece(cell, one, thr, o)
  
  val piece8b = Piece(cell, two, thr, blank)
  val piece8x = Piece(cell, two, thr, x)
  val piece8o = Piece(cell, two, thr, o)
  
  val piece9b = Piece(cell, thr, thr, blank)
  val piece9x = Piece(cell, thr, thr, x)
  val piece9o = Piece(cell, thr, thr, o)
    
  val position_bbb = Position(piece1b, piece2b, piece3b)
  val position_xbb = Position(piece1x, piece2b, piece3b)
  val position_obb = Position(piece1o, piece2b, piece3b)
  
  val position_xxb = Position(piece1x, piece2x, piece3b)
  val position_oob = Position(piece1o, piece2o, piece3b)
  val position_xbo = Position(piece1x, piece2b, piece3o)
  
  val position_xbx = Position(piece1x, piece2b, piece3x)
  val position_obo = Position(piece1o, piece2b, piece3o)
  val position_bxo = Position(piece1b, piece2x, piece3o)
  
  val position_xxx = Position(piece1x, piece2x, piece3x)
  val position_xxo = Position(piece1x, piece2x, piece3o)
  val position_xox = Position(piece1x, piece2o, piece3x)
  
  val position_oxx = Position(piece1o, piece2x, piece3x)
  val position_ooo = Position(piece1o, piece2o, piece3o)
  val position_xoo = Position(piece1x, piece2o, piece3o)
  
  val position_oox = Position(piece1o, piece2o, piece3x)    
  
  val positions1 = List(position_oox, position_xoo, position_xbb)
  val positions2 = List(position_xbx, position_bxo)
  //val positions1 = List(position_oox, position_oox, position_oxx, position_xxx, position_ooo, position_xxo)
  //val positions2 = List(position_xbx, position_bxo, position_obo, position_xbb, position_xox, position_bbb)
  
  def main(args: Array[String]): Unit = {
    val alephEncoder = new AlephEncoder()
    alephEncoder.encode(positions1, positions2, "hoge")
  }

}