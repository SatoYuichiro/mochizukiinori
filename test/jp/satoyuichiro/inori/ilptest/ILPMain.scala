package jp.satoyuichiro.inori.ilptest

import jp.satoyuichiro.inori.cdl._
import jp.satoyuichiro.inori.ilp.AlephEncoder

object ILPMain {

  val interpreter = new Interpreter()
  
  val cell = Symbol("cell")
  val o = Symbol("o")
  val x = Symbol("x")
  val blank = Symbol("blank")
  val control = Symbol("control")
  
  val one = Integer(1)
  val two = Integer(2)
  val three = Integer(3)
  val four = Integer(4)
  
  val piece1 = Piece(cell, one, one, x)
  val piece2 = Piece(cell, one, two, x)
  val piece3 = Piece(cell, one, three, o)
  val piece4 = Piece(cell, one, four, o)
  val piece5 = Piece(control, x)
  val piece6 = Piece(control, o)
  
  val position1 = Position(piece1, piece2, piece5)
  val position2 = Position(piece3, piece4, piece6)
  val position3 = Position(piece2, piece4, piece5)
  val position4 = Position(piece1, piece4, piece6)
  val positions1 = List(position1, position2)
  val positions2 = List(position3, position4)
  
  def main(args: Array[String]): Unit = {
    val alephEncoder = new AlephEncoder()
//    println(alephEncoder.encode(piece1, 1))
//    println(alephEncoder.encode(position1, 1))
//    println(alephEncoder.encodeSymbols(List(position1)))
//    println(alephEncoder.getArg(3, positions1))
    alephEncoder.encode(positions1, positions2, "hoge")
  }
}