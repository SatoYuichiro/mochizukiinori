package jp.satoyuichiro.inori.ilptest

import jp.satoyuichiro.inori.ilp.AlephUtil
import java.io.File
import jp.satoyuichiro.inori.ilp.AlephConceptEncoder
import jp.satoyuichiro.inori.cdl._
import jp.satoyuichiro.inori.learning.concept.generator.RichConcept
import jp.satoyuichiro.inori.learning.concept.generator.PrimeConcept
import jp.satoyuichiro.inori.learning.concept.generator.GeneratorUtil

object ConceptEncodeTest {

  def main(args: Array[String]): Unit = {
    val concepts = putName(AlephUtil.conceptDeserialize(new File("./concepts/concepts-all.txt")))
    val encoder = new AlephConceptEncoder(Array(Symbol("cell"), Integer(1), Integer(1), Symbol("blank")))
    encoder.encode(concepts, "hoge")
  }

  def putName(rich: List[RichConcept]): List[PrimeConcept] = {
    val labeler = GeneratorUtil.Labeler.makeLabeler("concept")
    rich map (_.putLabel(labeler())) collect { case c: PrimeConcept => c }
  }
}