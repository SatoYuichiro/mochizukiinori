package jp.satoyuichiro.inori.learning.concept.generatortest

import org.junit._
import org.junit.Assert._

import jp.satoyuichiro.inori.cdl._
import jp.satoyuichiro.inori.learning.concept.generator._

import junit.framework.TestCase

class RecursiveGeneratorTest extends TestCase {
  
  val cell = Symbol("cell")
  val one = Integer(1)
  val two = Integer(2)
  val three = Integer(3)
  val blank = Symbol("blank")
  
  val piece1 = Piece(cell, one, one, blank)
  val piece2 = Piece(cell, one, two, blank)
  val piece3 = Piece(cell, one, one, blank)
  
  val testPosition1 = Position(piece1, piece1, piece1)
  val testPosition2 = Position(piece1, piece1, piece1, piece2, piece1, piece1, piece1, piece1)
  
  def test {
    val recursiveGenerator = new RecursiveGenerator
    val conjunctionGenerator = new ConjunctionGenerator
    val concept = conjunctionGenerator.generate(testPosition1).putLabel(Label("con1"))
    val rec = recursiveGenerator.generate(concept).toConcept
    println(concept.toConcept.toLisp)
    println(rec.toLisp)
    val interpreter = new Interpreter
    interpreter.interpret(DefineConcept(Label("con1"), concept.toConcept))
    interpreter.interpret(DefineConcept(Label("rec"), rec))
    assertEquals(Bool(true), interpreter.interpret(ConceptCall(Label("rec"), List(TypedExp(testPosition1, PositionType())))))
    assertEquals(Bool(false), interpreter.interpret(ConceptCall(Label("rec"), List(TypedExp(testPosition2, PositionType())))))
  }
}