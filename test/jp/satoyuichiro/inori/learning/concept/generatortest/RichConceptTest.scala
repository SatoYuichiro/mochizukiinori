package jp.satoyuichiro.inori.learning.concept.generatortest

import org.junit._
import org.junit.Assert._

import jp.satoyuichiro.inori.cdl._
import jp.satoyuichiro.inori.learning.concept.generator._

import junit.framework.TestCase

class RichConceptTest extends TestCase {

  val cell = Symbol("cell")

  val zero = Integer(0)
  val one = Integer(1)
  val two = Integer(2)
  val three = Integer(3)

  val o = Symbol("o")
  val x = Symbol("x")
  val b = Symbol("blank")

  val role1 = Role(o)
  val role2 = Role(x)

  val x0 = Label("x0")
  val x1 = Label("x1")
  val x2 = Label("x2")
  val x3 = Label("x3")

  val pos = Label("pos")

  val x1addone = Addition(x1, one)
  val x1addtwo = Addition(x1, two)
  val x2addone = Addition(x2, one)
  val x2addtwo = Addition(x2, two)

  val piece1 = Piece(x0, x1, x2, x3)
  val piece2 = Piece(x0, x1addone, x2, x3)
  val piece3 = Piece(x0, x1addtwo, x2, x3)

  val piece4 = Piece(x0, x1, x2addone, x3)
  val piece5 = Piece(x0, x1, x2addtwo, x3)

  val vrole = Role(x3)
  
  val arg0 = Arg(zero, pos)
  val arg1 = Arg(one, pos)
  val arg2 = Arg(two, pos)

  val uni = Unificate(piece1, arg0)
  val eq1 = Equals(piece2, arg1)
  val eq2 = Equals(piece3, arg2)
  val eq3 = Equals(piece4, arg1)
  val eq4 = Equals(piece5, arg2)
  val eqRole = Equals(vrole, GetRole(pos))

  val argument = List(TypedLabel(pos, PositionType()))

  val con1 = PrimeConjunction(List(piece1, piece2, piece3))
  val con2 = PrimeConjunction(List(piece1, piece4, piece5))
  
  val con1Label = Label("con1")
  val con2Label = Label("con2")

  val call1 = ConceptCall(con1Label, List(TypedExp(pos, PositionType())))
  val call2 = ConceptCall(con2Label, List(TypedExp(pos, PositionType())))

  val queryPiece1 = Piece(cell, one, one, x)
  val queryPiece2 = Piece(cell, two, one, x)
  val queryPiece3 = Piece(cell, three, one, x)
  val queryPosition1 = Position(queryPiece1, queryPiece2, queryPiece3)
  
  def testPrimeConjunction {
    val primeConjunction = PrimeConjunction(List(piece1, piece2, piece3))
    val expected = Concept(argument, And(uni, eq1, eq2))
    assertEquals(expected, primeConjunction.toConcept)

    val conceptName = Label("con")
    val expected2 = DefineConcept(conceptName, expected)
    assertEquals(Some(expected2), primeConjunction.putLabel(conceptName).toDefineConcept)
    assertEquals(None, primeConjunction.toDefineConcept)
    
    val primeConjunction2 = PrimeConjunction(List(piece1, piece2, piece3), vrole)
    val expected3 = Concept(argument, And(uni, eqRole, eq1, eq2))
    assertEquals(expected3, primeConjunction2.putLabel(conceptName).toConcept)
    
    val interpreter = primeConjunction.putLabel(conceptName).load(new Interpreter())
    val result = interpreter.interpret(new ConceptCall(conceptName, List(TypedExp(queryPosition1, new PositionType()))))
    assertEquals(Bool(true), result)
  }

  def testPrimeDisjunction {
    val primeDisjunction = PrimeDisjunction(List(piece1, piece2, piece3))
    val expected = Concept(argument, Or(uni, eq1, eq2))
    assertEquals(expected, primeDisjunction.toConcept)

    val conceptName = Label("con")
    val expected2 = DefineConcept(conceptName, expected)
    assertEquals(Some(expected2), primeDisjunction.putLabel(conceptName).toDefineConcept)
    assertEquals(None, primeDisjunction.toDefineConcept)
    
    val primeDisjunction2 = PrimeDisjunction(List(piece1, piece2, piece3), vrole)
    val expected3 = Concept(argument, Or(uni, eqRole, eq1, eq2))
    assertEquals(expected3, primeDisjunction2.putLabel(conceptName).toConcept)
    
    val interpreter = primeDisjunction.putLabel(conceptName).load(new Interpreter())
    val result = interpreter.interpret(new ConceptCall(conceptName, List(TypedExp(queryPosition1, new PositionType()))))
    assertEquals(Bool(true), result)
  }

  def testCompositeConjunction {
    val compositeConjunction = CompositeConjunction(List(con1.putLabel(con1Label), con2.putLabel(con2Label)))
    val expected = Concept(argument, And(call1, call2))
    assertEquals(expected, compositeConjunction.toConcept)
    assertEquals(3, compositeConjunction.arity)

    val conceptName = Label("con")
    val expected2 = DefineConcept(conceptName, expected)
    assertEquals(Some(expected2), compositeConjunction.putLabel(conceptName).toDefineConcept)
    assertEquals(None, compositeConjunction.toDefineConcept)
    
    val interpreter = compositeConjunction.putLabel(conceptName).load(new Interpreter())
    val result = interpreter.interpret(new ConceptCall(conceptName, List(TypedExp(queryPosition1, new PositionType()))))
    assertEquals(Bool(false), result)
  }

  def testCompositeDisjunction {
    val compositeDisjunction = CompositeDisjunction(List(con1.putLabel(con1Label), con2.putLabel(con2Label)))
    val expected = Concept(argument, Or(call1, call2))
    assertEquals(expected, compositeDisjunction.toConcept)

    val conceptName = Label("con")
    val expected2 = DefineConcept(conceptName, expected)
    assertEquals(Some(expected2), compositeDisjunction.putLabel(conceptName).toDefineConcept)
    assertEquals(None, compositeDisjunction.toDefineConcept)
    
    val interpreter = compositeDisjunction.putLabel(conceptName).load(new Interpreter())
    val result = interpreter.interpret(new ConceptCall(conceptName, List(TypedExp(queryPosition1, new PositionType()))))
    assertEquals(Bool(true), result)
  }

  def testprimeRecursive {
    val primeRecursive = PrimeRecursive(PrimeConjunction(List(piece1,piece2,piece3)).putLabel(Label("pat1")))
    val expected = "(pos) " + 
    "(for (i 0 result true) (< i (- (size pos) 2)) (set i (+ i 1)) " +
    "((set result " + 
    "(and result " +
    "(pat1 (position (arg (+ 0 i) pos) (arg (+ 1 i) pos) (arg (+ 2 i) pos))))) " +
    "result))"
    assertEquals(expected, primeRecursive.toConcept.toLisp)

    val conceptName = Label("con1")
    val expected2 = "(concept con1 " + expected + ")"
    assertEquals(expected2, primeRecursive.putLabel(conceptName).toDefineConcept.get.toLisp)
    assertEquals(None, primeRecursive.toDefineConcept)
  }

  def testPrimeHeuristics {
    val con = PrimeConjunction(List(piece1, piece2, piece3)).putLabel(con1Label)
    val primeHeuristics = PrimeHeuristics(con, 0.1)
    val expected = Concept(argument, If(call1, Body(Real(0.1)), Body(Real(0.0))))
    assertEquals(expected, primeHeuristics.toConcept)
    
    val label = Label("heu")
    val expected2 = DefineConcept(label, expected)
    assertEquals(Some(expected2), primeHeuristics.putLabel(label).toDefineConcept)
    assertEquals(None, primeHeuristics.toDefineConcept)
    
    val interpreter = primeHeuristics.putLabel(label).load(new Interpreter())
    val result = interpreter.interpret(new ConceptCall(label, List(TypedExp(queryPosition1, new PositionType()))))
    assertEquals(Real(0.1), result)
  }
  
  def testCompositeHeuristics {
    val heu1 = PrimeHeuristics(con1.putLabel(con1Label), 0.2).putLabel(new Label("heu1"))
    val heu2 = PrimeHeuristics(con2.putLabel(con2Label), 0.3).putLabel(new Label("heu2"))
    val compositeHeuristics = CompositeHeuristics(List(heu1, heu2))
    
    val interpreter = compositeHeuristics.load(new Interpreter())
    compositeHeuristics.primeHeuristics foreach {
      h => assertEquals(true, interpreter.interpret(h.toQuery(queryPosition1)).isInstanceOf[Real])
    }
    val querys = compositeHeuristics.primeHeuristics map (_.toQuery(queryPosition1))
    assertEquals(Real(0.2), interpreter.interpret(querys.head))
    assertEquals(Real(0.0), interpreter.interpret(querys.tail.head))
  }
  
}