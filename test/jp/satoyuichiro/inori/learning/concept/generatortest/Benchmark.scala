package jp.satoyuichiro.inori.learning.concept.generatortest

import jp.satoyuichiro.inori.cdl._
import jp.satoyuichiro.inori.learning.concept.generator._

object Benchmark {

  val tcell = Symbol("tic-tac-toe-cell")
  val ccell = Symbol("connect4-cell")

  val one = Integer(1)
  val two = Integer(2)
  val three = Integer(3)
  val four = Integer(4)
  val five = Integer(5)
  val six = Integer(6)
  val seven = Integer(7)

  val o = Symbol("o")
  val x = Symbol("x")
  val blank = Symbol("blank")

  val red = Symbol("red")
  val white = Symbol("white")

  val conjunctionGenerator = new ConjunctionGenerator
  val arithmeticGenerator = new ArithmeticConjunctionGenerator
  val disjunctionGenerator = new DisjunctionGenerator
  val heuristicsGenerator = new HeuristicsGenerator

  def main(args: Array[String]): Unit = {
    val patternLabelMaker = GeneratorUtil.Labeler.makeLabeler("pattern")
    val binaryPrimeConcepts = makeBinaryPrimeConcepts filter (_ != null) map (_.putLabel(patternLabelMaker()).asInstanceOf[PrimeConjunction])
    val trinaryPrimeConcepts = makeTrinaryPrimeConcepts filter (_ != null) map (_.putLabel(patternLabelMaker()).asInstanceOf[PrimeConjunction])

    println("Binary Concepts")
    binaryPrimeConcepts foreach { p => println(p.toDefineConcept.get.toLisp) }
    println("\nTrinary Concepts")
    trinaryPrimeConcepts foreach { p => println(p.toDefineConcept.get.toLisp) }

    val arithmeticBinaryPrimeConcepts = makeArithmeticBinaryPrimeConcepts filter (_ != null) map (_.putLabel(patternLabelMaker()).asInstanceOf[PrimeConjunction])
    val arithmeticTrinaryPrimeConcepts = makeArithmeticTrinaryPrimeConcepts filter (_ != null) map (_.putLabel(patternLabelMaker()).asInstanceOf[PrimeConjunction])

    println("\nArithmetic Binary Concepts")
    arithmeticBinaryPrimeConcepts foreach { p => println(p.toDefineConcept.get.toLisp) }
    println("\nArithmetic Trinary Concepts")
    arithmeticTrinaryPrimeConcepts foreach { p => println(p.toDefineConcept.get.toLisp) }

    val disjunctionConcepts = (makeDisjunctionConcepts(binaryPrimeConcepts) ++
      makeDisjunctionConcepts(trinaryPrimeConcepts) ++
      makeDisjunctionConcepts(arithmeticBinaryPrimeConcepts) ++
      makeDisjunctionConcepts(arithmeticTrinaryPrimeConcepts)) filter (_ != null) map (_.putLabel(patternLabelMaker()).asInstanceOf[CompositeDisjunction])

    println("\nDisjunction Concepts")
    disjunctionConcepts foreach { p => println(p.toDefineConcept.get.toLisp) }

    val allPatterns = binaryPrimeConcepts ++ trinaryPrimeConcepts ++
      arithmeticBinaryPrimeConcepts ++ arithmeticTrinaryPrimeConcepts ++
          disjunctionConcepts

    val heuristicsLabelMaker = GeneratorUtil.Labeler.makeLabeler("heuristics")
    val heuristics = heuristicsGenerator.generate(allPatterns, tictactoePositions) filter (_ != null) map (_.putLabel(heuristicsLabelMaker()).asInstanceOf[PrimeHeuristics])

    println("\nGenerated Heuristics")
    heuristics foreach { h => println(h.toDefineConcept.get.toLisp) }

    val loadableHeuristics = new CompositeHeuristics(heuristics)

    println()
    heuristicsTest(loadableHeuristics)
    println()
    evaluationSpead(loadableHeuristics)
  }

  def makeBinaryPrimeConcepts(): List[PrimeConcept] = {
    var binaryPrimeConcepts = List.empty[PrimeConcept]
    tictactoePositions foreach {
      position =>
        makeBinaryPosition(position) foreach {
          binaryPosition =>
            try {
              val binaryPrimeConcept = conjunctionGenerator.generate(binaryPosition)
              if (!binaryPrimeConcepts.contains(binaryPrimeConcept))
                binaryPrimeConcepts ::= binaryPrimeConcept
            } catch {
              case _: Throwable =>
            }

        }
    }
    binaryPrimeConcepts
  }

  def makeTrinaryPrimeConcepts(): List[PrimeConcept] = {
    var binaryPrimeConcepts = List.empty[PrimeConcept]
    tictactoePositions foreach {
      position =>
        makeTrinaryPosition(position) foreach {
          binaryPosition =>
            try {
              val binaryPrimeConcept = conjunctionGenerator.generate(binaryPosition)
              if (!binaryPrimeConcepts.contains(binaryPrimeConcept))
                binaryPrimeConcepts ::= binaryPrimeConcept
            } catch {
              case _: Throwable =>
            }
        }
    }
    binaryPrimeConcepts
  }

  def makeArithmeticBinaryPrimeConcepts(): List[PrimeConcept] = {
    var binaryPrimeConcepts = List.empty[PrimeConcept]
    tictactoePositions foreach {
      position =>
        makeBinaryPosition(position) foreach {
          binaryPosition =>
            try {
              val binaryPrimeConcept = arithmeticGenerator.generate(binaryPosition).get
              if (!binaryPrimeConcepts.contains(binaryPrimeConcept))
                binaryPrimeConcepts ::= binaryPrimeConcept
            } catch {
              case _: Throwable =>
            }

        }
    }
    binaryPrimeConcepts
  }

  def makeArithmeticTrinaryPrimeConcepts(): List[PrimeConcept] = {
    var binaryPrimeConcepts = List.empty[PrimeConcept]
    tictactoePositions foreach {
      position =>
        makeTrinaryPosition(position) foreach {
          binaryPosition =>
            try {
              val binaryPrimeConcept = arithmeticGenerator.generate(binaryPosition).get
              if (!binaryPrimeConcepts.contains(binaryPrimeConcept))
                binaryPrimeConcepts ::= binaryPrimeConcept
            } catch {
              case _: Throwable =>
            }
        }
    }
    binaryPrimeConcepts
  }

  def makeDisjunctionConcepts(primeConcepts: List[PrimeConcept]): List[CompositeDisjunction] = {
    (makeBinaryConceptPair(primeConcepts) map (p => CompositeDisjunction(p))) ++
      (makeTrinaryConceptPair(primeConcepts) map (p => CompositeDisjunction(p)))
  }

  def makeBinaryConceptPair(primeConcepts: List[PrimeConcept]): List[List[PrimeConcept]] = {
    var result = List.empty[List[PrimeConcept]]
    val arrayConcepts = primeConcepts.toArray
    for (i <- 0 to arrayConcepts.length - 2) {
      result ::= List(arrayConcepts(i), arrayConcepts(i + 1))
    }
    result
  }

  def makeTrinaryConceptPair(primeConcepts: List[PrimeConcept]): List[List[PrimeConcept]] = {
    var result = List.empty[List[PrimeConcept]]
    val arrayConcepts = primeConcepts.toArray
    for (i <- 0 to arrayConcepts.length - 3) {
      result ::= List(arrayConcepts(i), arrayConcepts(i + 1), arrayConcepts(i + 2))
    }
    result
  }

  def makeBinaryPosition(position: Position): List[Position] = {
    var result = List.empty[Position]
    val arrayPieces = position.position.toArray
    for (i <- 0 to arrayPieces.length - 2) {
      result ::= Position(List(arrayPieces(i), arrayPieces(i + 1)), None)
    }
    result
  }

  def makeTrinaryPosition(position: Position): List[Position] = {
    var result = List.empty[Position]
    val arrayPieces = position.position.toArray
    for (i <- 0 to arrayPieces.length - 3) {
      result ::= Position(List(arrayPieces(i), arrayPieces(i + 1), arrayPieces(i + 2)), None)
    }
    result
  }

  def heuristicsTest(heuristics: CompositeHeuristics): Unit = {
    var count = 0
    val loadedInterpreter = heuristics.load(new Interpreter)
    connect4Positions foreach {
      testPosition =>
        var evaluation = 0.0
        val binaryPositions = makeBinaryPosition(testPosition)
        val trinaryPositions = makeTrinaryPosition(testPosition)
        heuristics.primeHeuristics foreach {
          prime =>
            binaryPositions foreach {
              position =>
                evaluation += loadedInterpreter.interpret(prime.toQuery(position)).asInstanceOf[Real].value
            }
            trinaryPositions foreach {
              position =>
                evaluation += loadedInterpreter.interpret(prime.toQuery(position)).asInstanceOf[Real].value
            }
        }
        println("evaluation test " + count)
        println(evaluation)
        count += 1
    }
  }

  def evaluationSpead(heuristics: CompositeHeuristics): Unit = {
    val loadedInterpreter = heuristics.load(new Interpreter)
    val start = System.currentTimeMillis()
    var n = 0
    for (i <- 0 to 332) {
      connect4Positions foreach {
        testPosition =>
          var evaluation = 0.0
          val binaryPositions = makeBinaryPosition(testPosition)
          val trinaryPositions = makeTrinaryPosition(testPosition)
          heuristics.primeHeuristics.par foreach {
            prime =>
              binaryPositions.par foreach {
                position =>
                  evaluation += loadedInterpreter.interpret(prime.toQuery(position)).asInstanceOf[Real].value
              }
              trinaryPositions.par foreach {
                position =>
                  evaluation += loadedInterpreter.interpret(prime.toQuery(position)).asInstanceOf[Real].value
              }
          }
          n += 1
      }
    }
    val end = System.currentTimeMillis()
    println("Evaluation Spead")
    println(n + " of Connect4 positions are evaluated in " + ((end - start).toDouble / 1000.0) + " seconds")
  }

  val tictactoePositions = List(
    // position1
    Position(
      Piece(tcell, one, one, blank),
      Piece(tcell, one, two, blank),
      Piece(tcell, one, three, blank),

      Piece(tcell, two, one, o),
      Piece(tcell, two, two, o),
      Piece(tcell, two, three, blank),

      Piece(tcell, three, one, x),
      Piece(tcell, three, two, x),
      Piece(tcell, three, three, x)),

    // position2
    Position(
      Piece(tcell, one, one, x),
      Piece(tcell, one, two, o),
      Piece(tcell, one, three, x),

      Piece(tcell, two, one, o),
      Piece(tcell, two, two, x),
      Piece(tcell, two, three, o),

      Piece(tcell, three, one, o),
      Piece(tcell, three, two, x),
      Piece(tcell, three, three, x)),

    // position3
    Position(
      Piece(tcell, one, one, x),
      Piece(tcell, one, two, x),
      Piece(tcell, one, three, x),

      Piece(tcell, two, one, o),
      Piece(tcell, two, two, o),
      Piece(tcell, two, three, blank),

      Piece(tcell, three, one, blank),
      Piece(tcell, three, two, blank),
      Piece(tcell, three, three, blank)),

    // position4
    Position(
      Piece(tcell, one, one, x),
      Piece(tcell, one, two, blank),
      Piece(tcell, one, three, o),

      Piece(tcell, two, one, x),
      Piece(tcell, two, two, o),
      Piece(tcell, two, three, blank),

      Piece(tcell, three, one, x),
      Piece(tcell, three, two, blank),
      Piece(tcell, three, three, o)),

    // position5
    Position(
      Piece(tcell, one, one, blank),
      Piece(tcell, one, two, x),
      Piece(tcell, one, three, blank),

      Piece(tcell, two, one, x),
      Piece(tcell, two, two, x),
      Piece(tcell, two, three, o),

      Piece(tcell, three, one, o),
      Piece(tcell, three, two, x),
      Piece(tcell, three, three, o)),

    // position6
    Position(
      Piece(tcell, one, one, x),
      Piece(tcell, one, two, blank),
      Piece(tcell, one, three, blank),

      Piece(tcell, two, one, blank),
      Piece(tcell, two, two, x),
      Piece(tcell, two, three, blank),

      Piece(tcell, three, one, o),
      Piece(tcell, three, two, o),
      Piece(tcell, three, three, x)),

    // position7
    Position(
      Piece(tcell, one, one, x),
      Piece(tcell, one, two, x),
      Piece(tcell, one, three, o),

      Piece(tcell, two, one, x),
      Piece(tcell, two, two, o),
      Piece(tcell, two, three, x),

      Piece(tcell, three, one, o),
      Piece(tcell, three, two, x),
      Piece(tcell, three, three, o)),

    // position8
    Position(
      Piece(tcell, one, one, x),
      Piece(tcell, one, two, x),
      Piece(tcell, one, three, x),

      Piece(tcell, two, one, o),
      Piece(tcell, two, two, x),
      Piece(tcell, two, three, o),

      Piece(tcell, three, one, o),
      Piece(tcell, three, two, o),
      Piece(tcell, three, three, x)),

    // position9
    Position(
      Piece(tcell, one, one, blank),
      Piece(tcell, one, two, blank),
      Piece(tcell, one, three, blank),

      Piece(tcell, two, one, blank),
      Piece(tcell, two, two, blank),
      Piece(tcell, two, three, blank),

      Piece(tcell, three, one, blank),
      Piece(tcell, three, two, blank),
      Piece(tcell, three, three, blank)),

    // position10
    Position(
      Piece(tcell, one, one, x),
      Piece(tcell, one, two, o),
      Piece(tcell, one, three, o),

      Piece(tcell, two, one, x),
      Piece(tcell, two, two, x),
      Piece(tcell, two, three, o),

      Piece(tcell, three, one, x),
      Piece(tcell, three, two, x),
      Piece(tcell, three, three, o)))

  val connect4Positions = List(
    // position1
    Position(
      Piece(ccell, one, one, red),
      Piece(ccell, one, two, red),
      Piece(ccell, one, three, white),
      Piece(ccell, one, four, white),
      Piece(ccell, one, five, blank),
      Piece(ccell, one, six, red),

      Piece(ccell, two, one, white),
      Piece(ccell, two, two, red),
      Piece(ccell, two, three, blank),
      Piece(ccell, two, four, red),
      Piece(ccell, two, five, white),
      Piece(ccell, two, six, white),

      Piece(ccell, three, one, red),
      Piece(ccell, three, two, red),
      Piece(ccell, three, three, red),
      Piece(ccell, three, four, blank),
      Piece(ccell, three, five, red),
      Piece(ccell, three, six, white),

      Piece(ccell, four, one, blank),
      Piece(ccell, four, two, red),
      Piece(ccell, four, three, white),
      Piece(ccell, four, four, red),
      Piece(ccell, four, five, white),
      Piece(ccell, four, six, red),

      Piece(ccell, five, one, white),
      Piece(ccell, five, two, red),
      Piece(ccell, five, three, red),
      Piece(ccell, five, four, blank),
      Piece(ccell, five, five, red),
      Piece(ccell, five, six, white),

      Piece(ccell, six, one, blank),
      Piece(ccell, six, two, white),
      Piece(ccell, six, three, red),
      Piece(ccell, six, four, white),
      Piece(ccell, six, five, blank),
      Piece(ccell, six, six, red),

      Piece(ccell, seven, one, red),
      Piece(ccell, seven, two, red),
      Piece(ccell, seven, three, blank),
      Piece(ccell, seven, four, red),
      Piece(ccell, seven, five, white),
      Piece(ccell, seven, six, red)),

    // position2
    Position(
      Piece(ccell, one, one, red),
      Piece(ccell, one, two, blank),
      Piece(ccell, one, three, white),
      Piece(ccell, one, four, red),
      Piece(ccell, one, five, red),
      Piece(ccell, one, six, blank),

      Piece(ccell, two, one, red),
      Piece(ccell, two, two, red),
      Piece(ccell, two, three, white),
      Piece(ccell, two, four, white),
      Piece(ccell, two, five, red),
      Piece(ccell, two, six, blank),

      Piece(ccell, three, one, red),
      Piece(ccell, three, two, blank),
      Piece(ccell, three, three, white),
      Piece(ccell, three, four, red),
      Piece(ccell, three, five, blank),
      Piece(ccell, three, six, red),

      Piece(ccell, four, one, white),
      Piece(ccell, four, two, red),
      Piece(ccell, four, three, red),
      Piece(ccell, four, four, blank),
      Piece(ccell, four, five, red),
      Piece(ccell, four, six, white),

      Piece(ccell, five, one, red),
      Piece(ccell, five, two, white),
      Piece(ccell, five, three, red),
      Piece(ccell, five, four, blank),
      Piece(ccell, five, five, red),
      Piece(ccell, five, six, white),

      Piece(ccell, six, one, blank),
      Piece(ccell, six, two, red),
      Piece(ccell, six, three, white),
      Piece(ccell, six, four, red),
      Piece(ccell, six, five, red),
      Piece(ccell, six, six, blank),

      Piece(ccell, seven, one, red),
      Piece(ccell, seven, two, white),
      Piece(ccell, seven, three, white),
      Piece(ccell, seven, four, blank),
      Piece(ccell, seven, five, blank),
      Piece(ccell, seven, six, red)),

    // position3
    Position(
      Piece(ccell, one, one, blank),
      Piece(ccell, one, two, red),
      Piece(ccell, one, three, white),
      Piece(ccell, one, four, red),
      Piece(ccell, one, five, white),
      Piece(ccell, one, six, red),

      Piece(ccell, two, one, white),
      Piece(ccell, two, two, red),
      Piece(ccell, two, three, blank),
      Piece(ccell, two, four, red),
      Piece(ccell, two, five, red),
      Piece(ccell, two, six, white),

      Piece(ccell, three, one, red),
      Piece(ccell, three, two, blank),
      Piece(ccell, three, three, red),
      Piece(ccell, three, four, white),
      Piece(ccell, three, five, red),
      Piece(ccell, three, six, red),

      Piece(ccell, four, one, white),
      Piece(ccell, four, two, blank),
      Piece(ccell, four, three, blank),
      Piece(ccell, four, four, red),
      Piece(ccell, four, five, white),
      Piece(ccell, four, six, red),

      Piece(ccell, five, one, white),
      Piece(ccell, five, two, blank),
      Piece(ccell, five, three, white),
      Piece(ccell, five, four, white),
      Piece(ccell, five, five, blank),
      Piece(ccell, five, six, white),

      Piece(ccell, six, one, red),
      Piece(ccell, six, two, red),
      Piece(ccell, six, three, blank),
      Piece(ccell, six, four, red),
      Piece(ccell, six, five, white),
      Piece(ccell, six, six, blank),

      Piece(ccell, seven, one, red),
      Piece(ccell, seven, two, red),
      Piece(ccell, seven, three, white),
      Piece(ccell, seven, four, blank),
      Piece(ccell, seven, five, blank),
      Piece(ccell, seven, six, blank)))

}