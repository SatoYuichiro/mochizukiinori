package jp.satoyuichiro.inori.learning.concept.generatortest

import org.junit._
import org.junit.Assert._

import jp.satoyuichiro.inori.cdl._
import jp.satoyuichiro.inori.learning.concept.generator._

import junit.framework.TestCase

class GeneratorUtilTest extends TestCase {

  val x0 = Label("x0")
  val x1 = Label("x1")
  val x2 = Label("x2")

  val y0 = Label("y0")
  val y1 = Label("y1")
  val y2 = Label("y2")

  val one = Integer(1)
  val two = Integer(2)
  val three = Integer(3)

  val cell = Symbol("cell")
  val blank = Symbol("blank")

  val piece1 = Piece(cell, one, one, blank)
  val piece2 = Piece(cell, one, two, blank)
  val piece3 = Piece(cell, one, one, blank)

  val testPosition1 = Position(piece1, piece1, piece1)
  val testPosition2 = Position(piece1, piece1, piece1, piece2, piece1, piece1, piece1, piece1)

  val pos = Label("pos")
  val argpos = TypedLabel(pos, PositionType())

  def testLabeler {
    val labeler = GeneratorUtil.Labeler.makeLabeler("x")
    val res = (0 to 2) map (i => labeler())
    assertEquals(List(x0, x1, x2), res)

    val labeler2 = GeneratorUtil.Labeler.makeLabeler("y")
    val res2 = (0 to 1) map (i => labeler2())
    assertEquals(List(y0, y1), res2)
  }

  def testReplace {
    val builder = Map.newBuilder[LeafExp, Exp]
    builder += cell -> x0
    builder += one -> x1
    builder += blank -> x2
    println(GeneratorUtil.Manipulator.replace(testPosition2, builder.result))
  }
  def testFormat {
    val con = Concept(List(argpos), And(y0, y2, y2, y1, pos))
    val res = Concept(List(argpos), And(x0, x1, x1, x2, pos))
    assertEquals(res, GeneratorUtil.Formatter.format(con))

    val con2 = Concept(List(argpos),
      And(Unificate(Piece(y1, y2, y0), Arg(one, pos)), Equals(Piece(y1, Addition(y2, one), y0), Arg(two, pos))))
    val res2 = Concept(List(argpos),
      And(Unificate(Piece(x0, x1, x2), Arg(one, pos)), Equals(Piece(x0, Addition(x1, one), x2), Arg(two, pos))))
    assertEquals(res2, GeneratorUtil.Formatter.format(con2))
  }

  def testFormatter {
    val formatter = GeneratorUtil.Formatter.formatterMaker("x")
    assertEquals(x0, formatter(y1))
    assertEquals(x1, formatter(y2))
    assertEquals(x0, formatter(y1))
  }

  def testrelabel {
    val piece = Piece(x1, one, x2, two)
    val res = Piece(y0, one, y1, two)
    assertEquals(res, GeneratorUtil.relabel(piece, "y"))
  }

  def testrelabelWithout {
    val piece = Piece(x1, one, x2, two, x0)
    val res = Piece(y0, one, x2, two, y1)
    assertEquals(res, GeneratorUtil.relabelWithout(x2, piece, "y"))
  }

  def testcountArity {
    val conjunctionGenerator = new ConjunctionGenerator
    val concept = conjunctionGenerator.generate(testPosition1).toConcept
    assertEquals(2, GeneratorUtil.ArityCounter.count(concept))
  }

  def testcountMax {
    val recursiveGenerator = new RecursiveGenerator
    val counter = GeneratorUtil.ArityCounter.maxCounter
    assertEquals(2, counter(2))
    assertEquals(2, counter(1))
    assertEquals(4, counter(4))
  }

}