package jp.satoyuichiro.inori.learning.concept.generatortest

import org.junit._
import org.junit.Assert._

import jp.satoyuichiro.inori.cdl._
import jp.satoyuichiro.inori.learning.concept.generator._
import jp.satoyuichiro.inori.learning.concept.generator.GeneratorUtil.Manipulator._

import junit.framework.TestCase

class ConjunctionMakerTest extends TestCase {

  val cell = Symbol("cell")

  val one = Integer(1)
  val two = Integer(2)
  val three = Integer(3)

  val o = Symbol("o")
  val x = Symbol("x")
  val b = Symbol("blank")

  val role1 = Role(o)
  val role2 = Role(x)

  val x0 = Label("x0")
  val x1 = Label("x1")
  val x2 = Label("x2")
  val x3 = Label("x3")

  val y0 = Label("y0")
  val y1 = Label("y1")
  val y2 = Label("y2")
  val y3 = Label("y3")

  val piece1 = Piece(cell, one, one, o)
  val piece2 = Piece(cell, one, two, o)
  val piece3 = Piece(cell, one, three, o)

  val piece4 = Piece(cell, one, one, x)
  val piece5 = Piece(cell, one, two, x)
  val piece6 = Piece(cell, one, three, x)

  val piece7 = Piece(cell, one, one, b)
  val piece8 = Piece(cell, one, two, b)
  val piece9 = Piece(cell, one, three, b)

  val testPieces = List(piece1, piece2, piece3)
  val testPieces2 = List(piece4, piece5, piece6)
  val testPieces3 = List(piece7, piece8, piece9)

  val testPosition = Position(testPieces, None)
  val testPosition2 = Position(testPieces, Some(role1))
  val testPosition3 = Position(testPieces2, None)
  val testPosition4 = Position(testPieces2, Some(role2))

  val generator = new ConjunctionGenerator

  def testgenerate {
    assertEquals(generator.generate(testPosition), generator.generate(testPosition3))
    assertEquals(generator.generate(testPosition2), generator.generate(testPosition4))
    println(generator.generate(testPosition2).toConcept.toLisp)
    println(generator.generate(Position(testPieces, Some(role2))).toConcept.toLisp)
  }

  def testreplaceRoleToVariable {
    val maker = new ConjunctionGenerator()
    val res = maker.replaceRoleToVariable(testPieces, role1)
    assertEquals(Piece(cell, one, one, Label("role-val")), res.head)
    assertEquals(Piece(cell, one, two, Label("role-val")), res.tail.head)
    assertEquals(Piece(cell, one, three, Label("role-val")), res.tail.tail.head)
  }

  def testreplaceSameElementToVariable {
    val exp = And(Unificate(Piece(x0, x1,x1, x2), piece1),
      Equals(Piece(x0, x1, two, x2), piece2),
      Equals(Piece(x0, x1, three, x2), piece3))
    val res = generator.replaceSameElementToVariable(testPieces)
    //    println(res.toLisp)
    assertEquals(exp, res)

    val res2 = And(Unificate(Piece(x0, x1, x1, x2), piece4),
      Equals(Piece(x0, x1, two, x2), piece5),
      Equals(Piece(x0, x1, three, x2), piece6))
    assertEquals(res2, generator.replaceSameElementToVariable(testPieces2))
  }

  def testmakeQueryList {
    val ref = Piece(cell, x0, x1)
    val res = And(Unificate(ref, piece1), Equals(ref, piece2), Equals(ref, piece3))
    assertEquals(res, generator.makeQueryList(List(piece1, piece2, piece3), List(ref, ref, ref)))
  }

  def testmakeReplaceRule {
    val rules = generator.makeReplaceRule(piece1)
    //    rules foreach println
  }
  def testrecursiveMaker {
    val exps = List(cell, one, y1, o)
    val labels = List(x0, x1, x2)
    assertEquals(List(Map(cell -> x0, one -> x1, o -> x2)), generator.recursiveMaker(exps, labels, List.empty[Replace]))

    val exps2 = List(cell, one, two, o)
    val labels2 = List(x0, x1, x2)
    val ls2 = generator.recursiveMaker(exps2, labels2, List.empty[Replace])
    val res21 = Map(o -> x2, one -> x1, cell -> x0)
    val res22 = Map(two -> x1, o -> x2, cell -> x0)
    val res23 = Map(one -> x0, two -> x1, o -> x2)
    assertEquals(res21, ls2.head)
    assertEquals(res22, ls2.tail.head)
    assertEquals(res23, ls2.tail.tail.head)

    val exps3 = List(cell, one, y1, two, o)
    val labels3 = List(x0, x1, x2)
    val ls3 = generator.recursiveMaker(exps3, labels3, List.empty[Replace])
    val res31 = Map(o -> x2, one -> x1, cell -> x0)
    val res32 = Map(two -> x2, one -> x1, cell -> x0)
    val res33 = Map(one -> x0, two -> x1, o -> x2)
    assertEquals(res31, ls3.head)
    assertEquals(res22, ls3.tail.head)
    assertEquals(res23, ls3.tail.tail.head)
  }

  def testreplaceAll {
    val exps = List(cell, one, y1, o)
    val labels = List(x0, x1, x2)
    assertEquals(Map(cell -> x0, one -> x1, o -> x2), generator.replaceAll(exps, labels))
  }
  
  def testhasUnusedUnification {
    val p1 = Piece(x0, x1, x2, x3)
    val p2 = Piece(x0, x1, two, x3)
    val p3 = Piece(x0, x1, three, x3)
    val replaced = List(p1, p2, p3)
    assertEquals(true, generator.hasUnusedUnification(replaced, testPieces))
    
    val p4 = Piece(x0, x1, one, x3)
    val replaced2 = List(p4, p2, p3)
    assertEquals(false, generator.hasUnusedUnification(replaced2, testPieces))
  }
}