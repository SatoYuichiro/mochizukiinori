package jp.satoyuichiro.inori.learning.concept.generatortest

import org.junit._
import org.junit.Assert._

import jp.satoyuichiro.inori.cdl._
import jp.satoyuichiro.inori.learning.concept.generator._
import jp.satoyuichiro.inori.learning.concept.generator.GeneratorUtil.Manipulator._

import junit.framework.TestCase

class ArithmeticConjunctionGeneratorTest extends TestCase {

  val cell = Symbol("cell")

  val zero = Integer(0)
  val one = Integer(1)
  val two = Integer(2)
  val three = Integer(3)

  val o = Symbol("o")
  val x = Symbol("x")
  val b = Symbol("blank")

  val role1 = Role(o)
  val role2 = Role(x)

  val x0 = Label("x0")
  val x1 = Label("x1")
  val x2 = Label("x2")
  val x3 = Label("x3")

  val y0 = Label("y0")
  val y1 = Label("y1")
  val y2 = Label("y2")
  val y3 = Label("y3")
  
  val pos = Label("pos")
  
  val x2addone = Addition(x2, one)
  val x2addtwo = Addition(x2, two)

  val piece1 = Piece(cell, one, one, o)
  val piece2 = Piece(cell, one, two, o)
  val piece3 = Piece(cell, one, three, o)
  val piece10 = Piece(cell, one, Integer(4), o)

  val piece4 = Piece(cell, one, one, x)
  val piece5 = Piece(cell, one, two, x)
  val piece6 = Piece(cell, one, three, x)

  val piece7 = Piece(cell, one, one, b)
  val piece8 = Piece(cell, one, two, b)
  val piece9 = Piece(cell, one, three, b)

  val testPieces = List(piece1, piece2, piece3)
  val testPieces2 = List(piece4, piece5, piece6)
  val testPieces3 = List(piece7, piece8, piece9)

  val testPosition = Position(testPieces, None)
  val testPosition2 = Position(testPieces, Some(role1))
  val testPosition3 = Position(testPieces2, None)
  val testPosition4 = Position(testPieces2, Some(role2))

  val conjunctionGenerator = new ConjunctionGenerator
  val generator = new ArithmeticConjunctionGenerator

  def test {
    val result = generator.generate(testPosition).get.toConcept
    val and = And(Unificate(Piece(x0, x1, x2, x3), Arg(zero, pos)),
        Equals(Piece(x0, x1, x2addone, x3), Arg(one, pos)),
        Equals(Piece(x0, x1, x2addtwo, x3), Arg(two, pos)))
    assertEquals(and, result.body)
    assertEquals(Concept(List(TypedLabel(pos, PositionType())), and), result)
    
    assertEquals(generator.generate(testPosition3), generator.generate(testPosition))
    
    println(generator.generate(testPosition2).get.toConcept.toLisp)
  }

  def testgenerate {
    val and = And(Unificate(Piece(x0, x1, one, x2), Piece(cell, one, one, o)),
      Equals(Piece(x0, x1, two, x2), Piece(cell, one, two, o)),
      Equals(Piece(x0, x1, three, x2), Piece(cell, one, three, o)))
    val expected = And()
//    generator.generate(and, None)
  }

  def testisIntegerColumn {
    val column1 = List(one, two, three)
    assertEquals(true, generator.isIntegerColumn(column1))
    val column2 = List(one, two, x)
    assertEquals(false, generator.isIntegerColumn(column2))
  }

  def testcolumnToArithmetic {
    val gen = new ArithmeticConjunctionGenerator
    val column1 = List(one, two, three)
    assertEquals(Addition(Label("arithm0"), one), gen.columnToArithmetic(column1).tail.head)
  }

  def testaddColumn {
    val result = Array.empty[Array[Exp]]
    val column = List(cell, cell, cell)
    val expected = Array(Array[Exp](cell), Array[Exp](cell), Array[Exp](cell))
    val added = generator.addColumn(column, result)
    assertEquals(3, added.length)
    assertEquals(cell, added(0).head)
    assertEquals(cell, added(1).head)
    assertEquals(cell, added(2).head)

    val column2 = List(one, two, three)
    val expected21 = List(cell, one)
    val expected22 = List(cell, two)
    val expected23 = List(cell, three)
    val added2 = generator.addColumn(column2, expected)
    assertEquals(3, added2.length)
    assertEquals(expected21, added2(0).toList)
    assertEquals(expected22, added2(1).toList)
    assertEquals(expected23, added2(2).toList)
  }
}