package jp.satoyuichiro.inori.learning.concept.generatortest

import org.junit._
import org.junit.Assert._
import jp.satoyuichiro.inori.cdl._
import jp.satoyuichiro.inori.learning.concept.generator._
import junit.framework.TestCase
import jp.satoyuichiro.inori.learning.concept.generator.PrimeConcept

class DisjunctionGeneratorTest extends TestCase {

val cell = Symbol("cell")

  val zero = Integer(0)
  val one = Integer(1)
  val two = Integer(2)
  val three = Integer(3)

  val o = Symbol("o")
  val x = Symbol("x")
  val b = Symbol("blank")

  val role1 = Role(o)
  val role2 = Role(x)

  val x0 = Label("x0")
  val x1 = Label("x1")
  val x2 = Label("x2")
  val x3 = Label("x3")
  
  val pos = Label("pos")
  
  val x2addone = Addition(x2, one)
  val x2addtwo = Addition(x2, two)

  val piece1 = Piece(cell, one, one, o)
  val piece2 = Piece(cell, one, two, o)
  val piece3 = Piece(cell, one, three, o)

  val piece4 = Piece(cell, one, one, x)
  val piece5 = Piece(cell, two, one, x)
  val piece6 = Piece(cell, three, one, x)

  val piece7 = Piece(cell, one, one, b)
  val piece8 = Piece(cell, two, two, b)
  val piece9 = Piece(cell, three, three, b)
  
  val subPosition1 = List(piece1, piece2, piece3)
  val subPosition2 = List(piece4, piece5, piece6)
  val subPosition3 = List(piece7, piece8, piece9)
  
  val position1 = Position(subPosition1 ++ List(piece7, piece4), None)
  val position2 = Position(subPosition2 ++ List(piece5, piece6), None)
  val position3 = Position(subPosition3 ++ List(piece7, piece4), None)
  val position4 = Position(subPosition2 ++ List(piece5, piece6), None)
  val position5 = Position(subPosition1 ++ List(piece9, piece4), None)
  val position6 = Position(subPosition2 ++ List(piece3, piece6), None)
  val position7 = Position(subPosition1 ++ List(piece5, piece4), None)
  val position8 = Position(subPosition2 ++ List(piece9, piece4), None)
  val position9 = Position(subPosition1 ++ List(piece1, piece6), None)
  val position10 = Position(subPosition2 ++ List(piece3, piece5), None)
  
  val conjPiece1 = Piece(x0, x1, x2, x3)
  val conjPiece2 = Piece(x0, Addition(x1, one), x2, x3)
  val conjPiece3 = Piece(x0, Addition(x1, two), x2, x3)
  val conjPiece4 = Piece(x0, x1, Addition(x2, one), x3)
  val conjPiece5 = Piece(x0, x1, Addition(x2, two), x3)
  val conjPiece6 = Piece(x0, Addition(x1, one), Addition(x2, one), x3)
  val conjPiece7 = Piece(x0, Addition(x1, two), Addition(x2, two), x3)
  
  val primeConcept1 = PrimeConjunction(List(conjPiece1, conjPiece2, conjPiece3)).putLabel(Label("hoge"))
  val primeConcept2 = PrimeConjunction(List(conjPiece1, conjPiece4, conjPiece5)).putLabel(Label("foo"))
  val primeConcept3 = PrimeConjunction(List(conjPiece1, conjPiece6, conjPiece7)).putLabel(Label("baa"))
  
  def testGenerate {
    val disjunctionGenerator = new DisjunctionGenerator
//    println(disjunctionGenerator.generate(List(testPosition1, testPosition1, testPosition1)).toConcept.toLisp)
  }
  
  def testGenerateFromConcepts {
    val disjunctionGenerator = new DisjunctionGenerator
    val concepts = List(primeConcept1, primeConcept2, primeConcept3)
    val positions = List(position1, position2, position3, position4, position5, position6, position7, position8, position9, position10)
    val result = disjunctionGenerator.generate(concepts, positions)
    assertEquals(true, GeneratorUtil.isMatchable(primeConcept1.putLabel(Label("hoge")).toDefineConcept.get, position2))
    assertEquals(1, GeneratorUtil.countMatchable(primeConcept1.putLabel(Label("hoge")).toDefineConcept.get, List(position1, position2, position3)))
    println(result.get.toConcept.toLisp)
  }
}