package jp.satoyuichiro.inori.learning.concept.generatortest

import org.junit._
import org.junit.Assert._

import jp.satoyuichiro.inori.cdl._
import jp.satoyuichiro.inori.learning.concept.generator._
import jp.satoyuichiro.inori.learning.concept.generator.GeneratorUtil.Manipulator._

import junit.framework.TestCase

class HeuristicsGneratorTest extends TestCase {
  
  val cell = Symbol("cell")

  val zero = Integer(0)
  val one = Integer(1)
  val two = Integer(2)
  val three = Integer(3)

  val o = Symbol("o")
  val x = Symbol("x")
  val b = Symbol("blank")

  val role1 = Role(o)
  val role2 = Role(x)

  val x0 = Label("x0")
  val x1 = Label("x1")
  val x2 = Label("x2")
  val x3 = Label("x3")

  val y0 = Label("y0")
  val y1 = Label("y1")
  val y2 = Label("y2")
  val y3 = Label("y3")
  
  val pos = Label("pos")
  
  val x2addone = Addition(x2, one)
  val x2addtwo = Addition(x2, two)

  val piece1 = Piece(cell, one, one, o)
  val piece2 = Piece(cell, one, two, o)
  val piece3 = Piece(cell, one, three, o)
  val piece10 = Piece(cell, one, Integer(4), o)

  val piece4 = Piece(cell, one, one, x)
  val piece5 = Piece(cell, two, one, x)
  val piece6 = Piece(cell, three, one, x)

  val piece7 = Piece(cell, one, one, b)
  val piece8 = Piece(cell, one, two, b)
  val piece9 = Piece(cell, one, three, b)

  val testPieces1 = List(piece1, piece2, piece3)
  val testPieces2 = List(piece4, piece5, piece6)
  val testPieces3 = List(piece7, piece8, piece9)

  val biPosition1 = Position(piece1, piece2)
  val biPosition2 = Position(piece1, piece3)
  val biPosition3 = Position(piece1, piece10)
  
  val testPosition1 = Position(testPieces1, None)
  val testPosition2 = Position(testPieces1, Some(role1))
  val testPosition3 = Position(testPieces2, None)
  val testPosition4 = Position(testPieces2, Some(role2))

  val arithmeticGenerator = new ArithmeticConjunctionGenerator
  val generator = new HeuristicsGenerator
  
  def test {
    val biConcept1 = arithmeticGenerator.generate(biPosition1).get
    val biConcept2 = arithmeticGenerator.generate(biPosition2).get
    val biConcept3 = arithmeticGenerator.generate(biPosition3).get
    
    val triConcept1 = arithmeticGenerator.generate(testPosition1).get
    val triConcept2 = arithmeticGenerator.generate(testPosition3).get
    
    val concepts = List(biConcept1, biConcept2, triConcept1, biConcept3, triConcept2)
    
    val labelMaker = GeneratorUtil.Labeler.makeLabeler("pattern")
    
    val defConcepts = concepts map (c => c.putLabel(labelMaker()))
    
    val positions = List(testPosition1, testPosition3)
    generator.generate(defConcepts, positions) foreach {h => println(h.toConcept.toLisp) }
  }

}