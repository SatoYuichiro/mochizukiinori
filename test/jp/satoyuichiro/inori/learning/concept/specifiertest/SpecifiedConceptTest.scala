package jp.satoyuichiro.inori.learning.concept.specifiertest

import org.junit._
import org.junit.Assert._
import jp.satoyuichiro.inori.cdl._
import jp.satoyuichiro.inori.learning.concept.generator._
import jp.satoyuichiro.inori.learning.concept.generator.GeneratorUtil.Manipulator._
import junit.framework.TestCase
import jp.satoyuichiro.inori.learning.concept.specifier.SpecifiedConcept
import jp.satoyuichiro.inori.learning.concept.specifier.Specialization

class SpecifiedConceptTest extends TestCase {
  
  val one = new Integer(1)
  val two = new Integer(2)
  
  val red = new Symbol("r")
  
  val x0 = new Label("x0")
  val x1 = new Label("x1")
  val x2 = new Label("x2")
  val x3 = new Label("x3")
  
  val concept1 = PrimeConjunction(List(Piece(x0, x1, x2, x3), Piece(x0, x1, Addition(x2, one), x3), Piece(x0, Addition(x1, two), x2, x3))).putLabel(Label("con1"))
  val concept2 = PrimeConjunction(List(Piece(x0, x1, x2, red), Piece(x0, x1, Addition(x2, one), red), Piece(x0, Addition(x1, two), x2, red))).putLabel(Label("con2"))
  val concept3 = PrimeConjunction(List(Piece(x0, x1, one, red), Piece(x0, x1, Addition(one, one), red), Piece(x0, Addition(x1, two), one, red))).putLabel(Label("con3"))
  
  val comp = CompositeDisjunction(List(concept1, concept2, concept3)).putLabel(Label("hoge"))
  
  def testtoConcept {
    val specified = new SpecifiedConcept(concept1, new Specialization(Map(x3 -> red)))
    assertEquals(concept2.toConcept, specified.toConcept)
    
    val specified2 = new SpecifiedConcept(concept2, new Specialization(Map(x2 -> one)))
    assertEquals(concept3.toConcept, specified2.toConcept)
  }
  
  def testtoRichConcept {
    val specified = new SpecifiedConcept(comp, new Specialization(Map(x3 -> red, x2 -> one)))
    val results = specified.toRichConcept.asInstanceOf[CompositeDisjunction].primes
    results foreach println
    assertEquals(concept3.toConcept, results.head.toConcept)
    assertEquals(concept3.toConcept, results.drop(1).head.toConcept)
    assertEquals(concept3.toConcept, results.drop(2).head.toConcept)
    val resultstr = "(pos) (or (con1 pos) (con2 pos) (con3 pos))"
    assertEquals(resultstr, specified.toRichConcept.toConcept.toLisp)
  }

}