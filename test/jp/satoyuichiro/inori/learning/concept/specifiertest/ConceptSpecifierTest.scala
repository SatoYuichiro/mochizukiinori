package jp.satoyuichiro.inori.learning.concept.specifiertest

import org.junit._
import org.junit.Assert._
import jp.satoyuichiro.inori.cdl._
import jp.satoyuichiro.inori.learning.concept.generator._
import jp.satoyuichiro.inori.learning.concept.generator.GeneratorUtil.Manipulator._
import junit.framework.TestCase
import jp.satoyuichiro.inori.learning.concept.specifier.SpecifiedConcept
import jp.satoyuichiro.inori.learning.concept.specifier.Specialization
import jp.satoyuichiro.inori.learning.concept.specifier.ConceptSpecifier

class ConceptSpecifierTest extends TestCase {

  val one = new Integer(1)
  val two = new Integer(2)
  val three = new Integer(3)
  val four = new Integer(4)

  val red = new Symbol("red")
  val white = new Symbol("white")
  val blank = new Symbol("blank")

  val cell = new Symbol("cell")
  val control = new Symbol("control")

  val x0 = new Label("x0")
  val x1 = new Label("x1")
  val x2 = new Label("x2")
  val x3 = new Label("x3")

  val piece1 = Piece(cell, one, one, red)
  val piece2 = Piece(cell, one, one, white)
  val piece3 = Piece(cell, one, two, red)
  val piece4 = Piece(cell, one, two, white)
  val piece5 = Piece(cell, one, two, blank)
  val piece6 = Piece(cell, one, three, red)
  val piece7 = Piece(cell, one, three, white)
  val piece8 = Piece(cell, one, three, blank)
  val piece9 = Piece(cell, one, four, red)
  
  val piece10 = Piece(cell, two, one, red)
  val piece11 = Piece(cell, two, three, white)
  val piece12 = Piece(cell, two, four, blank)
  val piece13 = Piece(cell, three, one, red)
  val piece14 = Piece(cell, three, three, blank)
  val piece15 = Piece(cell, three, four, white)
  
  val control1 = Piece(control, red)
  val control2 = Piece(control, white)

  val position1 = Position(piece1, piece2, piece3)
  val position2 = Position(piece1, piece2, piece4)
  val position3 = Position(control1, piece9, piece8)
  val position4 = Position(piece5, piece6, piece7)
  
  val position5 = Position(piece1, piece3, piece6)
  val position6 = Position(piece3, piece6, piece9)
  val position7 = Position(piece2, piece4, piece7)

  val redLine1 = Position(piece1, piece3, piece6)
  val redLine2 = Position(piece3, piece6, piece9)
  val whiteLine1 = Position(piece2, piece4, piece7)
  
  val noise1 = Position(piece10, piece11, piece12)
  val noise2 = Position(piece10, piece12, piece13)
  val noise3 = Position(piece11, piece10, piece13)
  val noise4 = Position(piece11, piece15, piece14)
  val noise5 = Position(piece12, piece14, piece13)
  val noise6 = Position(piece12, piece10, piece15)
  val noise7 = Position(piece12, piece13, piece15)
  
  val concept1 = PrimeConjunction(List(Piece(x0, x1, x2, x3), Piece(x0, x1, Addition(x2, one), x3), Piece(x0, x1, Addition(x2, two), x3))).putLabel(Label("con1"))
  val concept2 = PrimeConjunction(List(Piece(x0, x1, x2, x3), Piece(x0, Addition(x1, one), x2, x3), Piece(x0, Addition(x1, two), x2, x3))).putLabel(Label("con2"))
  val concept3 = PrimeConjunction(List(Piece(x0, Addition(x1, one), Addition(x2, one), x3), Piece(x0, Addition(x1, one), Addition(x2, one), x3), Piece(x0, Addition(x1, two), Addition(x2, two), red))).putLabel(Label("con3"))
  val concept4 = PrimeConjunction(List(Piece(x0, x1, x2, x3), Piece(x0, Addition(x1, one), Addition(x2, one), x3), Piece(x0, Addition(x1, two), Addition(x2, two), x3))).putLabel(Label("con4"))
  val disjunction = CompositeDisjunction(List(concept1, concept2, concept3))

  val specifier = new ConceptSpecifier
  
  def testspecify {
    val concept = CompositeDisjunction(List(concept1, concept2, concept4))
    
    val p1 = Position(redLine1.position ++ noise1.position, None)
    val p2 = Position(redLine1.position ++ noise2.position, None)
    val p3 = Position(redLine1.position ++ noise3.position, None)
    val p4 = Position(redLine2.position ++ noise4.position, None)
    val p5 = Position(redLine2.position ++ noise5.position, None)
    val p6 = Position(redLine2.position ++ noise6.position, None)
    val positiveExample = List(p1, p2, p3, p4, p5, p6)
    
    val n1 = Position(whiteLine1.position ++ noise1.position, None)
    val n2 = Position(whiteLine1.position ++ noise2.position, None)
    val n3 = Position(whiteLine1.position ++ noise3.position, None)
    val n4 = Position(whiteLine1.position ++ noise4.position, None)
    val n5 = Position(whiteLine1.position ++ noise5.position, None)
    val n6 = Position(whiteLine1.position ++ noise6.position, None)
    val negativeExample = List(n1, n2, n3, n4, n5, n6)
    
    val result = specifier.specify(concept, positiveExample, negativeExample)
    assertEquals(Specialization(Map(x3 -> red)), result.specialization)
    assertEquals("(pos) (or (con1 pos) (con2 pos) (con4 pos))", result.toRichConcept.toConcept.toLisp)
    val primes = result.toRichConcept.asInstanceOf[CompositeDisjunction].primes
    
    val exp1 = PrimeConjunction(List(Piece(x0, x1, x2, red), Piece(x0, x1, Addition(x2, one), red), Piece(x0, x1, Addition(x2, two), red))).putLabel(Label("con1"))
    val exp2 = PrimeConjunction(List(Piece(x0, x1, x2, red), Piece(x0, Addition(x1, one), x2, red), Piece(x0, Addition(x1, two), x2, red))).putLabel(Label("con2"))
    val exp4 = PrimeConjunction(List(Piece(x0, x1, x2, red), Piece(x0, Addition(x1, one), Addition(x2, one), red), Piece(x0, Addition(x1, two), Addition(x2, two), red))).putLabel(Label("con4"))
    assertEquals(exp1, primes.head)
    assertEquals(exp2, primes.drop(1).head)
    assertEquals(exp4, primes.drop(2).head)
  }

  def testspecializationCandidate {
    val positiveExample = List(position1, position2, position3, position4)
//    val result = specifier.specalizationCandidate(positiveExample)
//    result foreach println
  }
  
  def testmakeSpecialization {
    val positiveExample = List(position1, position2, position3, position4)
    val substitutions = specifier.makeSpecialization(disjunction, specifier.specializationCandidate(positiveExample))
//    substitutions foreach println
  }
  
  def testmakeSpecifiedConcept {
    val concept = CompositeDisjunction(List(concept1, concept2, concept4))
    val positiveExample = List(position5, position6)
    val negativeExample = List(position7)
    
    val s1 = Specialization(Map(x0 -> cell))
    val s2 = Specialization(Map(x1 -> one))
    val s3 = Specialization(Map(x1 -> two))
    val s4 = Specialization(Map(x2 -> two))
    val s5 = Specialization(Map(x3 -> white))
    val s6 = Specialization(Map(x3 -> red))
    
    val candidates = List(s1, s2, s3, s4, s5, s6)
    
    val result1 = specifier.makeSpecifiedConcept(concept, positiveExample, negativeExample, candidates)
    assertEquals(s6, result1.specialization)
  }
  
  def testbenefit {
    val concept = CompositeDisjunction(List(concept1, concept2, concept4))
    val positiveExample = List(position5, position6)
    val negativeExample = List(position7)
    
    val specialization1 = Specialization(Map(x0 -> cell))
    val specialization2 = Specialization(Map(x3 -> red))
    
    assertEquals(1, specifier.benefit(List(specialization1), concept, positiveExample, negativeExample))
    assertEquals(2, specifier.benefit(List(specialization2), concept, positiveExample, negativeExample))
    assertEquals(2, specifier.benefit(List(specialization1, specialization2), concept, positiveExample, negativeExample))
  }
  
  def testcountMatch {
    val concept = CompositeDisjunction(List(concept1, concept2, concept4))
    val positiveExample = List(position5, position6)
    val negativeExample = List(position7)
    
    val specialization1 = Specialization(Map(x0 -> cell))
    val specialization2 = Specialization(Map(x3 -> red))
    
    assertEquals(2, specifier.countMatch(new SpecifiedConcept(concept, specialization1), positiveExample))
    assertEquals(2, specifier.countMatch(new SpecifiedConcept(concept, specialization2), positiveExample))
    assertEquals(1, specifier.countMatch(new SpecifiedConcept(concept, specialization1), negativeExample))
    assertEquals(0, specifier.countMatch(new SpecifiedConcept(concept, specialization2), negativeExample))
  }
}