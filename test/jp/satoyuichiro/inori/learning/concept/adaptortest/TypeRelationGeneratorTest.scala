package jp.satoyuichiro.inori.learning.concept.adaptortest

import org.junit._
import org.junit.Assert._
import junit.framework.TestCase
import jp.satoyuichiro.inori.cdl._
import jp.satoyuichiro.inori.learning.concept.adapter._
import org.ggp.base.util.statemachine.implementation.prover.ProverStateMachine
import org.ggp.base.util.gdl.factory.GdlFactory
import jp.satoyuichiro.inori.gdl.GdlUtil

class TypeRelationGeneratorTest extends TestCase {

  val one = Integer(1)
  val two = Integer(2)
  val three = Integer(3)
  val four = Integer(4)

  val a = Symbol("a")
  val b = Symbol("b")
  val c = Symbol("c")

  val o = Symbol("o")
  val x = Symbol("x")

  val cell = Symbol("cell")
  val hoge = Symbol("hoge")

  val piece1 = Piece(cell, one, two, o)
  val piece2 = Piece(cell, two, two, x)
  val piece3 = Piece(cell, o, a, b)
  val piece4 = Piece(cell, x, b, c)
  val piece5 = Piece(hoge, one, x)

  val position1 = Position(piece1, piece2)
  val position2 = Position(piece3, piece4)

  val succ = new SuccRelation("succ", Map(a -> one, b -> two, c -> three))

  val generator = new TypeRelationGenerator
  
  def testTypeRelationAdapter {
    val typeRelation = new TypeRelation(cell, Map(1 -> 3, 2 -> 1, 3 -> 2))
    val typeAdapter = TypeAdapter(List(typeRelation))
    
    val input = Position(Piece(cell, o, one, two), Piece(cell, x, two, two), Piece(hoge, x, one))
    val result = typeAdapter.convert(input).position
    assertEquals(true, result.contains(piece1))
    assertEquals(true, result.contains(piece2))
    assertEquals(true, result.contains(Piece(hoge, x, one)))
  }
  
  def testTypeRelationAdapterWithSucc {
    val typeRelation = new TypeRelation(cell, Map(0 -> 0, 1 -> 3, 2 -> 1, 3 -> 2))
    val typeAdapter = new TypeAdapter(List(typeRelation), Some(new SuccAdapter(List(succ))))
    
    val input = Position(Piece(cell, o, a, b), Piece(cell, x, b, b), Piece(hoge, x, a))
    val result = typeAdapter.convert(input).position
    assertEquals(true, result.contains(piece1))
    assertEquals(true, result.contains(piece2))
    assertEquals(true, result.contains(Piece(hoge, x, one)))
  }
  
  def testAdapter1 {
    val rules = List("(init (cell x 1 2))", "(init (cell o 2 2))", "(init (cell b 3 3))", "(init (hoge 1 2 b 4 5))", "(init (hoge 4 2 x 1 1))")
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(GdlUtil.toJavaList(rules map (r => GdlFactory.create(r))))
    val machineState = stateMachine.getInitialState()
    val result = generator.generate(machineState)
    assertEquals(2, result.size)
    
    val adapter = TypeAdapter(result)
//    println(adapter.convert(position1).toLisp)
  }
  
  def testAdapter2 {
    val rulestr = List("(init (cell x a b))", "(init (cell o b b))", "(init (cell o b c))", "(init (hoge 1 2 b 4 5))", "(init (hoge 4 2 x 1 1))")
    val succstr = List("(succ a b)", "(succ b c)")
    val rules = rulestr ++ succstr
    val description = rules map (r => GdlFactory.create(r))
    
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(GdlUtil.toJavaList(description))
    val machineState = stateMachine.getInitialState()
    
    val succGenerator = new SuccRelationGenerator()
    val succRelation = succGenerator.generate(description)
    val succAdapter = new SuccAdapter(succRelation)
    
    val typeGenerator = new TypeRelationGenerator()
    val result = generator.generate(succAdapter.convert(machineState))
//    assertEquals(2, result.size)
    
    val adapter = TypeAdapter(result)
//    println(adapter.convert(position1).toLisp)
  }
  
  def testTypeAdapter {
    val succRelation1 = new SuccRelation("succ", Map(a -> one, b -> two))
    val succRelation2 = new SuccRelation("tucc", Map(o -> three, x -> four))
    val succAdapter = new SuccAdapter(List(succRelation1, succRelation2))
    
    val typeRelation1 = new TypeRelation(cell, Map(1 -> 3, 2 -> 1, 3 ->2))
    val typeRelation2 = new TypeRelation(hoge, Map(1 -> 2, 2 -> 1))
    
    val adapter = new TypeAdapter(List(typeRelation1, typeRelation2), Some(succAdapter))
    
    val stateMachine = new ProverStateMachine()
    val description = List("(init (cell blank a b))", "(init (cell ocp a a))", "(init (hoge get x))", "(init (hoge do o))") map (r => GdlFactory.create(r))
    stateMachine.initialize(GdlUtil.toJavaList(description))
    val machineState = stateMachine.getInitialState()
    
    val result = adapter.convert(machineState).position filter (p => p.isInstanceOf[Piece]) map (p => p.asInstanceOf[Piece])
    val cells = result filter (_.args.head == cell)
    val hoges = result filter (_.args.head == hoge)
    assertEquals(2, cells.size)
    assertEquals(2, hoges.size)
    
    assertEquals(true, cells.contains(Piece(cell, one, two, Symbol("blank"))))
    assertEquals(true, cells.contains(Piece(cell, one, one, Symbol("ocp"))))
    
    assertEquals(true, hoges.contains(Piece(hoge, three, Symbol("do"))))
    assertEquals(true, hoges.contains(Piece(hoge, four, Symbol("get"))))
  }
  
  def testTypeAdapterGenerator {
    val succRelation1 = new SuccRelation("succ", Map(a -> one, b -> two))
    val succRelation2 = new SuccRelation("tucc", Map(o -> three, x -> four))
    val succAdapter = new SuccAdapter(List(succRelation1, succRelation2))
    
    val stateMachine = new ProverStateMachine()
    val description = List("(init (cell blank a b))", "(init (cell ocp a a))", "(init (hoge get x))", "(init (hoge do o))") map (r => GdlFactory.create(r))
    stateMachine.initialize(GdlUtil.toJavaList(description))
    val machineState = stateMachine.getInitialState()
    
    val adapter = new TypeAdapter(generator.generate(succAdapter.convert(machineState)), Some(succAdapter))
    val result = adapter.convert(machineState).position filter (p => p.isInstanceOf[Piece]) map (p => p.asInstanceOf[Piece])
    val cells = result filter (_.args.head == cell)
    val hoges = result filter (_.args.head == hoge)
    assertEquals(2, cells.size)
    assertEquals(2, hoges.size)
    
    assertEquals(true, cells.contains(Piece(cell, one, two, Symbol("blank"))))
    assertEquals(true, cells.contains(Piece(cell, one, one, Symbol("ocp"))))
    
    assertEquals(true, hoges.contains(Piece(hoge, three, Symbol("do"))))
    assertEquals(true, hoges.contains(Piece(hoge, four, Symbol("get"))))
  }
}