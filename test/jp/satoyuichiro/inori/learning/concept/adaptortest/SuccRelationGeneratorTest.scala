package jp.satoyuichiro.inori.learning.concept.adaptortest

import org.junit._
import org.junit.Assert._
import junit.framework.TestCase
import org.ggp.base.util.gdl.factory.GdlFactory
import org.ggp.base.util.gdl.grammar.GdlRelation
import jp.satoyuichiro.inori.learning.concept.adapter.SuccRelationGenerator
import org.ggp.base.util.gdl.grammar.Gdl
import jp.satoyuichiro.inori.cdl.Integer

class SuccRelationGeneratorTest extends TestCase {

  val generator = new SuccRelationGenerator
  val succ = List("(succ 1 2)", "(succ 2 3)", "(succ 3 4)") map (g => GdlFactory.create(g)) map (_.asInstanceOf[GdlRelation])
  val tucc = List("(tucc a b)", "(tucc b c)", "(tucc c d)", "(tucc d e)") map (g => GdlFactory.create(g)) map (_.asInstanceOf[GdlRelation])
  val relations = List(succ.head, succ.drop(1).head, tucc.head, succ.drop(2).head, tucc.drop(1).head, tucc.drop(2).head, tucc.drop(3).head)

  def testGdl {
    val input = List("(succ 1 2)", "(succ 2 3)", "(succ 3 4)")
    val result = input map (g => GdlFactory.create(g))
    assertEquals(true, result.head.isInstanceOf[GdlRelation])
    assertEquals(true, result.tail.head.isInstanceOf[GdlRelation])
    assertEquals(true, result.tail.tail.head.isInstanceOf[GdlRelation])
  }

  def testgenerate {
    val result = generator.generate(relations)
    assertEquals("succ", result.head.name)
    assertEquals("tucc", result.tail.head.name)
    assertEquals(Some(Integer(3)), result.tail.head.succ.get("c"))
  }

  def testmakeSameRelations {
    val result = generator.makeSameRelations(relations)
    assertEquals("succ", result.head.head.getName().getValue())
    assertEquals(3, result.head.size)
    assertEquals("tucc", result.tail.head.head.getName().getValue())
    assertEquals(4, result.tail.head.size)
  }
}