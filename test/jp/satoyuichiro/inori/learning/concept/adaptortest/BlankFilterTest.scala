package jp.satoyuichiro.inori.learning.concept.adaptortest

import org.junit._
import org.junit.Assert._
import junit.framework.TestCase
import jp.satoyuichiro.inori.learning.concept.adapter.BlankFilter
import jp.satoyuichiro.inori.cdl.Symbol
import jp.satoyuichiro.inori.cdl.Integer
import jp.satoyuichiro.inori.cdl.Position
import jp.satoyuichiro.inori.cdl.Piece

class BlankFilterTest extends TestCase {

  val cell = Symbol("cell")
  val b = Symbol("b")
  val r = Symbol("r")
  val one = Integer(1)
  val two = Integer(2)
  val thr = Integer(3)
  
  val p1 = Piece(cell, one, one, b)
  val p2 = Piece(cell, one, one, r)
  val p3 = Piece(cell, one, two, b)
  val p4 = Piece(cell, one, two, r)
  val p5 = Piece(cell, two, thr, r)
  val p6 = Piece(cell, two, thr, b)
  
  def test {
    val filter = BlankFilter(b)
    val position = Position(p1,p2,p3)
    val result = filter.convert(position)
    assertEquals(Position(p2), result)
  }

  def test2 {
    val filter = BlankFilter(b, (one,one), (two,one), (one, thr), (thr,thr))
    val position = Position(p1,p2,p3,p4,p5,p6)
    val result = filter.convert(position)
    assertEquals(Position(p1,p2,p4,p5), result)
  }
}