package jp.satoyuichiro.inori.connect4

import jp.satoyuichiro.inori.learning.concept.generator.RichConcept
import jp.satoyuichiro.inori.learning.concept.generator.GeneratorUtil
import jp.satoyuichiro.inori.learning.concept.generator.PrimeConcept
import jp.satoyuichiro.inori.ilp.AlephUtil
import java.io.File
import org.ggp.base.util.statemachine.implementation.prover.ProverStateMachine
import jp.satoyuichiro.inori.gdl.Description
import jp.satoyuichiro.inori.gdl.GdlUtil
import jp.satoyuichiro.inori.player.RandomPlayer
import jp.satoyuichiro.inori.simulator.GameSimulator
import jp.satoyuichiro.inori.simulator.Episode
import jp.satoyuichiro.inori.player.PlayerUtil
import jp.satoyuichiro.inori.weka.WekaData
import jp.satoyuichiro.inori.weka.WekaUtil
import jp.satoyuichiro.inori.cdl._
import jp.satoyuichiro.inori.learning.concept.specifier.Specialization
import jp.satoyuichiro.inori.learning.concept.specifier.SpecifiedConcept
import jp.satoyuichiro.inori.simulator.Episode
import java.nio.file.Paths

object Connect4Weka {
  
  val connect4gdl = "./games/twoplayers/connect4.gdl"
  val conceptPath = Paths.get("./concepts/concepts-define.txt")
  val dataSize = 50
  
  def main(args: Array[String]): Unit = {
    printConcepts()
//    binary()
//    trinary()
    noConcept()
  }
  
  def binary(): Unit = {
    number()
    trueOrFalse()
  }
  
  def trinary(): Unit = {
    numberEpisode(1)
    trueOrFalseEpisode(1)
    numberEpisode(3)
    trueOrFalseEpisode(3)
    numberEpisode(5)
    trueOrFalseEpisode(5)
    numberEpisode(10)
    trueOrFalseEpisode(10)
  }
  
  def noConcept(): Unit = {
    val winPlayout = toWekaDataWin(WekaUtil.simulateFirstPlayerWin(connect4gdl, dataSize) map (_.playout))
    val losePlayout = toWekaDataLose(WekaUtil.simulateSecondPlayerWin(connect4gdl, dataSize) map (_.playout))
    WekaUtil.makeArffXYCell("connect4no", winPlayout ++ losePlayout)
    
    noConceptEpisode(1)
    noConceptEpisode(3)
    noConceptEpisode(5)
    noConceptEpisode(10)
  }
  
  def printConcepts(): Unit = {
    val concepts = WekaUtil.getConceptFromDefineConcept(conceptPath)
    concepts foreach (c => println(c.toDefineConcept.get.toLisp))
  }
  
  def number(): Unit = {
    val concepts = WekaUtil.getConceptFromDefineConcept(conceptPath)
    val winPlayout = toWekaDataWin(WekaUtil.simulateFirstPlayerWin(connect4gdl, dataSize) map removeBlank1)
    val losePlayout = toWekaDataLose(WekaUtil.simulateSecondPlayerWin(connect4gdl, dataSize) map removeBlank1)
    WekaUtil.makeArff("connect4nm", concepts, winPlayout ++ losePlayout)
  }
  
  def trueOrFalse(): Unit = {
    val concepts = WekaUtil.getConceptFromDefineConcept(conceptPath)
    val winPlayout = toWekaDataWin(WekaUtil.simulateFirstPlayerWin(connect4gdl, dataSize) map removeBlank1)
    val losePlayout = toWekaDataLose(WekaUtil.simulateSecondPlayerWin(connect4gdl, dataSize) map removeBlank1)
    WekaUtil.makeArffTrueOrFalse("connect4tf", concepts, winPlayout ++ losePlayout)
  }
  
  def numberEpisode(n: Int): Unit = {
    val concepts = WekaUtil.getConceptFromDefineConcept(conceptPath)
    val winEpisode = toWekaWinEpisode(WekaUtil.simulateFirstPlayerWin(connect4gdl, dataSize) map removeBlank, n)
    val loseEpisode = toWekaLoseEpisode(WekaUtil.simulateSecondPlayerWin(connect4gdl, dataSize) map removeBlank, n)
    WekaUtil.makeArff("connect4nmep" + n, concepts, winEpisode ++ loseEpisode)
  }
  
  def trueOrFalseEpisode(n: Int): Unit = {
    val concepts = WekaUtil.getConceptFromDefineConcept(conceptPath)
    val winEpisode = toWekaWinEpisode(WekaUtil.simulateFirstPlayerWin(connect4gdl, dataSize) map removeBlank, n)
    val loseEpisode = toWekaLoseEpisode(WekaUtil.simulateSecondPlayerWin(connect4gdl, dataSize) map removeBlank, n)
    WekaUtil.makeArffTrueOrFalse("connect4tfep" + n, concepts, winEpisode ++ loseEpisode)    
  }
  
  def noConceptEpisode(n: Int): Unit = {
    val winEpisode = toWekaWinEpisode(WekaUtil.simulateFirstPlayerWin(connect4gdl, dataSize), n)
    val loseEpisode = toWekaLoseEpisode(WekaUtil.simulateSecondPlayerWin(connect4gdl, dataSize), n)
    WekaUtil.makeArffXYCell("connect4noep" + n, winEpisode ++ loseEpisode)    
  }
  
  def removeBlank1(episode: Episode): Position = {
    Position(episode.playout.position map (_.asInstanceOf[Piece]) filter (piece => piece.args.last != Symbol("b")), None)
  }

  def removeBlank(episode: Episode): Episode = {
    Episode(episode.positions map removeBlank)
  }
  
  def removeBlank(position: Position): Position = {
    Position(position.position map (_.asInstanceOf[Piece]) filter (piece => piece.args.last != Symbol("b")), None)
  }
  
  def toWekaDataWin(positions: List[Position]): List[WekaData] = {
    positions map (p => WekaData(p, "win"))
  }

  def toWekaDataLose(positions: List[Position]): List[WekaData] = {
    positions map (p => WekaData(p, "lose"))
  }
  
  def toWekaDataEven(positions: List[Position]): List[WekaData] = {
    positions map (p => WekaData(p, "even"))
  }
  
  def toWekaWinEpisode(episodes: List[Episode], n: Int): List[WekaData] = {
    val even = episodes flatMap (e => e.positions.dropRight(n))
    val win = episodes flatMap (e => e.positions.takeRight(n))
    toWekaDataEven(even) ++ toWekaDataWin(win)
  }
  
  def toWekaLoseEpisode(episodes: List[Episode], n: Int): List[WekaData] = {
    val even = episodes flatMap (e => e.positions.dropRight(n))
    val lose = episodes flatMap (e => e.positions.takeRight(n))
    toWekaDataEven(even) ++ toWekaDataLose(lose)
  }
}