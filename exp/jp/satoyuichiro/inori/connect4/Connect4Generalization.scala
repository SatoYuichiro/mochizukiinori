package jp.satoyuichiro.inori.connect4

import jp.satoyuichiro.inori.cdl._
import jp.satoyuichiro.inori.learning.concept.generator.CompositeDisjunction
import jp.satoyuichiro.inori.learning.concept.specifier.Specialization
import jp.satoyuichiro.inori.learning.concept.generator.PrimeHeuristics
import jp.satoyuichiro.inori.learning.concept.specifier.SpecifiedHeuristics
import jp.satoyuichiro.inori.learning.concept.generator.PrimeConcept
import jp.satoyuichiro.inori.ilp.AlephUtil
import jp.satoyuichiro.inori.learning.concept.generator.RichConcept
import jp.satoyuichiro.inori.learning.concept.generator.GeneratorUtil
import java.io.File
import jp.satoyuichiro.inori.player.Heuristics
import jp.satoyuichiro.inori.learning.concept.adapter.BlankFilter
import jp.satoyuichiro.inori.learning.concept.specifier.SpecifiedConcept
import org.ggp.base.util.statemachine.MachineState
import jp.satoyuichiro.inori.player.PlayerUtil
import org.ggp.base.util.statemachine.implementation.prover.ProverStateMachine
import jp.satoyuichiro.inori.gdl.Description
import jp.satoyuichiro.inori.gdl.GdlUtil
import jp.satoyuichiro.inori.player.RandomPlayer
import jp.satoyuichiro.inori.simulator.GameSimulator

object Connect4Generalization {

  val connect4gdl = "./games/twoplayers/connect4.gdl"
  var concepts = List.empty[PrimeConcept]
  val expSize = 1000

  def main(args: Array[String]): Unit = {
    concepts = putName(AlephUtil.conceptDeserialize(new File("./concepts/concepts-all.txt")))
    beginGameGeneralization()
  }
  
  def endGameGeneralization(): Unit = {
    val winPositions = simulatePlayoutWinner(expSize)
    val losePositions = simulatePlayoutLoser(expSize)
    val hogeWin = simulateBeforeWin(expSize)
    val hogeLose = simulateBeforeLose(expSize)
    println("expSize " + winPositions.length + " " + losePositions.length + " " + hogeWin.length + " " + hogeLose.length)
    for (i <- 3 to 20) {
      val testingConcept = makeConcept(i)
      print(i + " " + evaluateConcept(testingConcept, winPositions) + " " + evaluateConcept(testingConcept, losePositions))
      println(" " + evaluateConcept(testingConcept, hogeWin) + " " + evaluateConcept(testingConcept, hogeLose))
    }    
  }
  
  def beginGameGeneralization(): Unit = {
    val winEpisode = simulateWinEpisode(expSize)
    val loseEpisode = simulateLoseEpisode(expSize)
    
    val averageGameLength = (((winEpisode map (_.size)).fold(0)(_ + _)) + ((loseEpisode map (_.size)).fold(0)(_ + _))).toDouble / (expSize.toDouble * 2)
    println("average game length " + averageGameLength)
    
    val positionAt5Win = winEpisode map (ep => getPositionAt(ep, 5))
    val positionAt5Lose = loseEpisode map (ep => getPositionAt(ep, 5))

    val positionAt10Win = winEpisode map (ep => getPositionAt(ep, 10))
    val positionAt10Lose = loseEpisode map (ep => getPositionAt(ep, 10))

    val positionAt15Win = winEpisode map (ep => getPositionAt(ep, 15))
    val positionAt15Lose = loseEpisode map (ep => getPositionAt(ep, 15))

    for (i <- 3 to 20) {
      val testingConcept = makeConcept(i)
      print(i + " ")
      print(evaluateConcept(testingConcept, positionAt5Win) + " " + evaluateConcept(testingConcept, positionAt5Win) + " ")
      print(evaluateConcept(testingConcept, positionAt10Win) + " " + evaluateConcept(testingConcept, positionAt10Win) + " ")
      print(evaluateConcept(testingConcept, positionAt15Win) + " " + evaluateConcept(testingConcept, positionAt15Win) + " ")
      println
    }
  }
  
  def getPositionAt(episode: List[Position], n: Int): Position = {
    episode.drop(n - 1).head
  }
  
  def simulatePlayoutWinner(n: Int): List[Position] = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(connect4gdl).getDescription)
    val roles = GdlUtil.toScalaList(stateMachine.getRoles())
    val players = List(new RandomPlayer(stateMachine, roles.head),
        new RandomPlayer(stateMachine, roles.tail.head))
    val simulator = GameSimulator(stateMachine, players)
    
    simulator.simulateWinner(players.head, n) map gdlToCdl
  }
  
  def simulatePlayoutLoser(n: Int): List[Position] = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(connect4gdl).getDescription)
    val roles = GdlUtil.toScalaList(stateMachine.getRoles())
    val players = List(new RandomPlayer(stateMachine, roles.head),
        new RandomPlayer(stateMachine, roles.tail.head))
    val simulator = GameSimulator(stateMachine, players)
    
    simulator.simulateWinner(players.tail.head, n) map gdlToCdl
  }
  
  def simulateBeforeWin(n: Int): List[Position] = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(connect4gdl).getDescription)
    val roles = GdlUtil.toScalaList(stateMachine.getRoles())
    val players = List(new RandomPlayer(stateMachine, roles.head),
        new RandomPlayer(stateMachine, roles.tail.head))
    val simulator = GameSimulator(stateMachine, players)
    
    var episodes = List.empty[Position]
    while (episodes.size < n) {
      val tem = simulator.simulateWinEpisode(players.head, (n - episodes.size)) filter (ep => 2 < ep.size)
      episodes ++= (tem map (_.reverse.drop(2).head) map gdlToCdl)
    }
    episodes
  }
  
  def simulateBeforeLose(n: Int): List[Position] = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(connect4gdl).getDescription)
    val roles = GdlUtil.toScalaList(stateMachine.getRoles())
    val players = List(new RandomPlayer(stateMachine, roles.head),
        new RandomPlayer(stateMachine, roles.tail.head))
    val simulator = GameSimulator(stateMachine, players)
    
    var episodes = List.empty[Position]
    while (episodes.size < n) {
      val tem = simulator.simulateWinEpisode(players.tail.head, (n - episodes.size)) filter (ep => 2 < ep.size)
      episodes ++= (tem map (_.reverse.drop(2).head) map gdlToCdl)
    }
    episodes
  }
  
  def simulateWinEpisode(n: Int): List[List[Position]] = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(connect4gdl).getDescription)
    val roles = GdlUtil.toScalaList(stateMachine.getRoles())
    val players = List(new RandomPlayer(stateMachine, roles.head),
        new RandomPlayer(stateMachine, roles.tail.head))
    val simulator = GameSimulator(stateMachine, players)
    
    var episodes = List.empty[List[Position]]
    while (episodes.size < n) {
      val tem = simulator.simulateWinEpisode(players.head, (n - episodes.size)) filter (ep => 20 < ep.size)
      episodes ++= (tem map (pls => pls map gdlToCdl))
    }
    episodes
  }
  
  def simulateLoseEpisode(n: Int): List[List[Position]] = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(connect4gdl).getDescription)
    val roles = GdlUtil.toScalaList(stateMachine.getRoles())
    val players = List(new RandomPlayer(stateMachine, roles.head),
        new RandomPlayer(stateMachine, roles.tail.head))
    val simulator = GameSimulator(stateMachine, players)
    
    var episodes = List.empty[List[Position]]
    while (episodes.size < n) {
      val tem = simulator.simulateWinEpisode(players.tail.head, (n - episodes.size)) filter (ep => 20 < ep.size)
      episodes ++= (tem map (pls => pls map gdlToCdl))
    }
    episodes
  }
  
  def putName(rich: List[RichConcept]): List[PrimeConcept] = {
    val labeler = GeneratorUtil.Labeler.makeLabeler("concept")
    rich map (_.putLabel(labeler())) collect { case c: PrimeConcept => c }
  }
  
  def gdlToCdl(position: MachineState): Position = PlayerUtil.gdlTocdl(position)

  def evaluateConcept(concept: CompositeDisjunction, positions: List[Position]): Int = {
    val interpreter = new Interpreter
    val name = concept.conceptName.get
    interpreter.interpret(concept.toDefineConcept.get)
    concept.primes foreach { p =>
      interpreter.interpret(p.toDefineConcept.get)
    }
    positions.par.map(p => evaluatePosition(p, interpreter, name)) count (_ == true)
  }
  
  def evaluatePosition(position: Position, interpreter: Interpreter, name: Label): Boolean = {
    val subPos : List[Position] = PlayerUtil.makeBinaryPosition(position) ++
      PlayerUtil.makeAllTrinaryPosition(position)
    subPos.map(p => evaluateSubPosition(p, interpreter, name)).fold(false)(_ || _)
  }
  
  def evaluateSubPosition(subPosition: Position, interpreter: Interpreter, name: Label): Boolean = {
    interpreter.interpret(makeQuery(name, subPosition)).asInstanceOf[Bool].value
  }
  
  def makeQuery(name: Label, subPosition: Position): ConceptCall = ConceptCall(name, List(TypedExp(subPosition, PositionType())))
  
  def makeConcept(i: Int): CompositeDisjunction = {
    val cell = Symbol("cell")
    val w = Symbol("w")
    val r = Symbol("r")
    val b = Symbol("b")
    val one = Integer(1)
    val two = Integer(2)
    val thr = Integer(3)
    val fur = Integer(4)
    val fiv = Integer(5)
    val six = Integer(6)
    val sev = Integer(7)
    
    i match {
      case 1 => toOr()
      case 2 => toOr(makeSpecified(11, makeSp(cell, one, thr, w)))
      case 3 => toOr(makeSpecified(11, makeSp(cell, one, thr, w)))
      case 4 => toOr(
          makeSpecified(11, makeSp(cell, one, thr, w)),
          makeSpecified(9, makeSp(cell, one, one, w)))
      case 5 => toOr(
          makeSpecified(6, makeSp(cell, fiv, thr, w)),
          makeSpecified(20, makeSp(cell, fur, two, r)))
      
      case 6 => toOr(
          makeSpecified(4, makeSp(cell, fiv, thr, w)),
          makeSpecified(20, makeSp(cell, fur, two, r)))
      case 7 => toOr(
          makeSpecified(4, makeSp(cell, fiv, thr, w)),
          makeSpecified(20, makeSp(cell, fur, two, r)))
      case 8 => toOr(
          makeSpecified(20, makeSp(cell, fur, one, w)),
          makeSpecified(13, makeSp(cell, fiv, two, b)),
          makeSpecified(20, makeSp(cell, fur, fiv, w)))
      case 9 => toOr(
          makeSpecified(11, makeSp(cell, fur, one, w)),
          makeSpecified(13, makeSp(cell, fiv, two, b)),
          makeSpecified(4, makeSp(cell, six, two, r)))
      case 10 => toOr(
          makeSpecified(6, makeSp(cell, fiv, thr, w)),
          makeSpecified(13, makeSp(cell, fiv, two, b)),
          makeSpecified(8, makeSp(cell, thr, thr, w)))
      
      case 11 => toOr(
          makeSpecified(6, makeSp(cell, fiv, thr, w)),
          makeSpecified(13, makeSp(cell, fiv, two, b)),
          makeSpecified(8, makeSp(cell, thr, thr, w)))
      case 12 => toOr(
          makeSpecified(11, makeSp(cell, fiv, one, w)),
          makeSpecified(13, makeSp(cell, six, one, b)),
          makeSpecified(4, makeSp(cell, six, fur, w)))
      case 13 => toOr(
          makeSpecified(11, makeSp(cell, fiv, one, w)),
          makeSpecified(13, makeSp(cell, six, one, b)),
          makeSpecified(13, makeSp(cell, six, one, r)),
          makeSpecified(4, makeSp(cell, six, fur, w)))
      case 14 => toOr(
          makeSpecified(6, makeSp(cell, fiv, thr, w)),
          makeSpecified(13, makeSp(cell, fiv, two, b)),
          makeSpecified(9, makeSp(cell, one, two, r)),
          makeSpecified(8, makeSp(cell, two, two, w)),
          makeSpecified(4, makeSp(cell, sev, fur, r)))
      case 15 => toOr(
          makeSpecified(11, makeSp(cell, one, thr, w)),
          makeSpecified(13, makeSp(cell, six, one, r)),
          makeSpecified(6, makeSp(cell, fiv, thr, w)),
          makeSpecified(10, makeSp(cell, one, two, r)))
      
      case 16 => toOr(
          makeSpecified(11, makeSp(cell, one, thr, w)),
          makeSpecified(13, makeSp(cell, six, one, r)),
          makeSpecified(6, makeSp(cell, fiv, thr, w)),
          makeSpecified(10, makeSp(cell, one, two, r)))
      case 17 => toOr(
          makeSpecified(11, makeSp(cell, one, thr, w)),
          makeSpecified(13, makeSp(cell, six, one, r)),
          makeSpecified(70, makeSp(cell, two, two, r)),
          makeSpecified(10, makeSp(cell, fiv, two, r)))
      case 18 => toOr(
          makeSpecified(11, makeSp(cell, one, thr, w)),
          makeSpecified(13, makeSp(cell, six, one, r)),
          makeSpecified(6, makeSp(cell, fiv, thr, w)),
          makeSpecified(10, makeSp(cell, one, two, r)),
          makeSpecified(70, makeSp(cell, two, two, r)))
      case 19 => toOr(
          makeSpecified(11, makeSp(cell, one, thr, w)),
          makeSpecified(8, makeSp(cell, thr, two, b)),
          makeSpecified(6, makeSp(cell, fiv, thr, w)),
          makeSpecified(10, makeSp(cell, one, two, r)),
          makeSpecified(4, makeSp(cell, thr, two, r)))
      case 20 => toOr(
          makeSpecified(11, makeSp(cell, one, thr, w)),
          makeSpecified(8, makeSp(cell, thr, two, b)),
          makeSpecified(6, makeSp(cell, fiv, thr, w)),
          makeSpecified(10, makeSp(cell, one, two, r)),
          makeSpecified(4, makeSp(cell, thr, two, r)))
    }
  }
  
  def toOr(sps: SpecifiedConcept*): CompositeDisjunction = {
    CompositeDisjunction(sps.toList.map(_.toRichConcept.asInstanceOf[PrimeConcept])).putLabel(new Label("hoge"))
  }
  
  /*
   * concept8 = piece(x0, x1, x2, x3), piece(x0, (+ x1 -1), x2, x3)
   * concept13 = piece(x0,x1,x2,x3), piece(x0, x1, (+ x2 2), x3)
   * 
   */
  /*
  def makeDisjunctionHeuristics(i: Int): Heuristics = {
    val cell = Symbol("cell")
    val w = Symbol("w")
    val r = Symbol("r")
    val b = Symbol("b")
    val one = Integer(1)
    val two = Integer(2)
    val thr = Integer(3)
    val fur = Integer(4)
    val fiv = Integer(5)
    val six = Integer(6)
    val sev = Integer(7)
    
    i match {
      case 1 => Heuristics(BlankFilter(b))
      case 2 => Heuristics(BlankFilter(b),
          makeSpecifiedDisjunction(makeSp(cell, thr, fur, w)))
      case 3 => Heuristics(BlankFilter(b),
          makeSpecifiedDisjunction(makeSp(cell, one, thr, w)))
      case 4 => Heuristics(BlankFilter(b),
          makeSpecifiedDisjunction(makeSp(cell, one, thr, w)),
          makeSpecified(9, makeSp(cell, one, one, w)))
      case 5 => Heuristics(BlankFilter(b, 
          (thr,one), (thr,two), (thr,thr), (thr,fur),
          (fur,one), (fur,two), (fur,thr), (fur,fur),
          (fiv,one), (fiv,two), (fiv,thr), (fiv,fur),
          (six,one), (six,two), (six,thr), (six,fur),
          (sev,one), (sev,two), (sev,thr), (sev,fur)),
          makeSpecified(6, makeSp(cell, fiv, thr, w)),
          makeSpecifiedDisjunction(makeSp(cell, fiv, two, b)))     

      case 6 => Heuristics(BlankFilter(b),
          makeSpecified(22, makeSp(cell, fur, two, w)),
          makeSpecified(9, makeSp(cell, one, one, w)))
      case 7 => Heuristics(BlankFilter(b),
          makeSpecifiedDisjunction(makeSp(cell, one, thr, w)),
          makeSpecified(9, makeSp(cell, one, one, w)),
          makeSpecified(22, makeSp(cell, six, fur, w)))
      case 8 => Heuristics(BlankFilter(b),
          makeSpecified(22, makeSp(cell, fur, two, w)),
          makeSpecifiedDisjunction(makeSp(cell, one, thr, w)))
      case 9 => Heuristics(BlankFilter(b),
          makeSpecifiedDisjunction(makeSp(cell, one, thr, w)),
          makeSpecified(16, makeSp(cell, two, two, r)))
      case 10 => Heuristics(BlankFilter(b,
          (thr,one), (thr,two), (thr,thr), (thr,fur),
          (fur,one), (fur,two), (fur,thr), (fur,fur),
          (fiv,one), (fiv,two), (fiv,thr), (fiv,fur),
          (six,one), (six,two), (six,thr), (six,fur),
          (sev,one), (sev,two), (sev,thr), (sev,fur)),
          makeSpecified(6, makeSp(cell, fiv, thr, w)),
          makeSpecifiedDisjunction(makeSp(cell, fiv, two, b)),
          makeSpecified(22, makeSp(cell, thr, two, w)))
    }
  }
  */
  val disjunctionIndex = List(0, 1, 3, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16, 17, 18, 20, 22)
  
  def getDisjunction(): CompositeDisjunction = {
    CompositeDisjunction(disjunctionIndex map pickUpConcept).putLabel(Label("disj"))
  }
  
//  def makeSpecifiedDisjunction(sp: Specialization): PrimeHeuristics = (new SpecifiedHeuristics(getDisjunction(), sp, eval)).toHeuristics
  
 // def makePrimeHeuristics(i: Int): PrimeHeuristics = PrimeHeuristics(pickUpConcept(i), eval)

  val x0 = Label("x0")
  val x1 = Label("x1")
  val x2 = Label("x2")
  val x3 = Label("x3")
    
  def makeSp(s1: LeafExp, i1: LeafExp, i2: LeafExp, s2: LeafExp): Specialization = new Specialization(Map(x0 -> s1, x1 -> i1, x2 -> i2, x3 -> s2))
  
  def makeSpecified(i: Int, sp: Specialization): SpecifiedConcept = new SpecifiedConcept(pickUpConcept(i), sp)
  
  def pickUpConcept(i: Int): PrimeConcept = concepts.find(_.conceptName.getOrElse(Label("")).label == ("concept" + i.toString)).get
}