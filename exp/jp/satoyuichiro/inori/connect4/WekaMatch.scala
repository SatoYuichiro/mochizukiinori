package jp.satoyuichiro.inori.connect4

import jp.satoyuichiro.inori.player.Player
import org.ggp.base.util.statemachine.StateMachine
import org.ggp.base.util.statemachine.Role
import org.ggp.base.util.statemachine.Move
import org.ggp.base.util.statemachine.MachineState
import jp.satoyuichiro.inori.cdl._
import jp.satoyuichiro.inori.player.PlayerUtil
import jp.satoyuichiro.inori.player.AlphaBetaPlayer
import org.ggp.base.util.statemachine.implementation.prover.ProverStateMachine
import jp.satoyuichiro.inori.gdl.Description
import jp.satoyuichiro.inori.gdl.GdlUtil
import jp.satoyuichiro.inori.simulator.GameSimulator

object WekaMatch {

  val connect4gdl = "./games/twoplayers/connect4.gdl"
  val expSize = 100

  def main(args: Array[String]): Unit = {
    println("vs 1")
    doSimulateBase(connect4gdl, expSize, 3, 1)
    doSimulate(connect4gdl, expSize, 3, 1)
    println("vs 3")
    doSimulateBase(connect4gdl, expSize, 3, 3)
    doSimulate(connect4gdl, expSize, 3, 3)
  }
  
  def doSimulateBase(gdl: String, n:Int, depth1: Int, depth2: Int): Unit = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(gdl).getDescription)
    val roles = GdlUtil.toScalaList(stateMachine.getRoles())
    val players = List(new AlphaBetaPlayer(stateMachine, roles.head, depth1),
        new AlphaBetaPlayer(stateMachine, roles.tail.head, depth2))
    val simulator = GameSimulator(stateMachine, players)
    
    val results = simulator.simulateMatchPar(n)
    
    val firstWin = results filter (_.getOrElse(players.head, false)) size
    val secondWin = results filter (_.getOrElse(players.tail.head, false)) size
    val drawN = n - firstWin - secondWin
    
    println(players.head.role + " " + firstWin + " ")
    println(players.tail.head.role + " " + secondWin + " ")
    println("draw " + drawN + " n " + n + "\n")
  }
  
  def doSimulate(gdl: String, n: Int, depth1: Int, depth2: Int): Unit = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(gdl).getDescription)
    val roles = GdlUtil.toScalaList(stateMachine.getRoles())
    val players = List(new WekaPlayer(stateMachine, roles.head, depth1),
        new AlphaBetaPlayer(stateMachine, roles.tail.head, depth2))
    val simulator = GameSimulator(stateMachine, players)
    
    val results = simulator.simulateMatchPar(n)
    
    val firstWin = results filter (_.getOrElse(players.head, false)) size
    val secondWin = results filter (_.getOrElse(players.tail.head, false)) size
    val drawN = n - firstWin - secondWin
    
    println(players.head.role + " " + firstWin + " ")
    println(players.tail.head.role + " " + secondWin + " ")
    println("draw " + drawN + " n " + n + "\n")
   }

}

case class WekaPlayer(override val stateMachine : StateMachine, override val role: Role,
    override val searchDepth: Int = 3, override val debugMode: Boolean = false) extends AlphaBetaPlayer(stateMachine, role, searchDepth, debugMode) {
  
  val interpreter = Tree.loadTree(new Interpreter)
  
  override def evaluate(state: MachineState): Double = {
    if (stateMachine.isTerminal(state)) {
      val goals = stateMachine.getGoals(state)
      if (won(goals)) Double.MaxValue
      else if (lose(goals)) Double.MinValue
      else if (draw(goals)) 0.0
      else 0.0
    }
    else if (Tree.evaluate(state, interpreter)) 1.0 else -1.0
  }
}

object Tree {
  
  val addx1_m1 = Addition(Label("x1"), Integer(-1))
  val addx1_m2 = Addition(Label("x1"), Integer(-2))
  val addx2_m1 = Addition(Label("x2"), Integer(-1))
  val addx2_m2 = Addition(Label("x2"), Integer(-2))
  val addx1_1 = Addition(Label("x1"), Integer(1))
  val addx1_2 = Addition(Label("x1"), Integer(2))
  val addx2_1 = Addition(Label("x2"), Integer(1))
  val addx2_2 = Addition(Label("x2"), Integer(2))
  
  val piece_00r = Piece(Label("x0"), Label("x1"), Label("x2"), Symbol("r"))
  val piece_00w = Piece(Label("x0"), Label("x1"), Label("x2"), Symbol("w"))
  val piece_11w = Piece(Label("x0"), addx1_1, addx2_1, Symbol("w"))
  val piece_22w = Piece(Label("x0"), addx1_1, addx2_1, Symbol("w"))
  val piece_10w = Piece(Label("x0"), addx1_1, Label("x2"), Symbol("w"))
  val piece_m10w = Piece(Label("x0"), addx1_m1, Label("x2"), Symbol("w"))
  val piece_m12w = Piece(Label("x0"), addx1_m1, addx2_2, Symbol("w"))
  val piece_m1m2w = Piece(Label("x0"), addx1_m1, addx2_m2, Symbol("w"))
  val piece_m2m1w = Piece(Label("x0"), addx1_m1, addx2_2, Symbol("w"))
  val piece_0m1r = Piece(Label("x0"), Label("x1"), addx2_m1, Symbol("r"))
  val piece_01r = Piece(Label("x0"), Label("x1"), addx2_1, Symbol("r"))
  val piece_11r = Piece(Label("x0"), addx1_1, addx2_1, Symbol("r"))
  val piece_22r = Piece(Label("x0"), addx1_1, addx2_1, Symbol("r"))
  
  val arg0 = Arg(Integer(0), Label("pos"))
  val arg1 = Arg(Integer(1), Label("pos"))
  val arg2 = Arg(Integer(2), Label("pos"))
  
  val posLabel = List(TypedLabel(Label("pos"), PositionType()))

  //(concept concept37 (pos)
  //  (and 
  //    (unificate (piece x0 x1 x2 "w") (arg 0 pos))
  //    (= (piece x0 (+ x1 -1) (+ x2 2) "w") (arg 1 pos))))
  val concept37 = Concept(posLabel, And(Unificate(piece_00w, arg0), Equals(piece_m12w, arg1)))
  val defConcept37 = DefineConcept(Label("concept37"), concept37)
  
  //(concept concept55 (pos)
  //  (and 
  //    (unificate (piece x0 x1 x2 "w") (arg 0 pos))
  //    (= (piece x0 (+ x1 -2) (+ x2 -1) "w") (arg 1 pos))
  //    (= (piece x0 (+ x1 -1) (+ x2 -2) "w") (arg 2 pos))))
  val concept55 = Concept(posLabel, And(Unificate(piece_00w, arg0), Equals(piece_m2m1w, arg1), Equals(piece_m1m2w, arg2)))
  val defConcept55 = DefineConcept(Label("concept55"), concept55)

  //(concept concept56 (pos)
  //  (and 
  //    (unificate (piece x0 x1 x2 "r") (arg 0 pos))
  //    (= (piece x0 (+ x1 1) (+ x2 1) "r") (arg 1 pos))
  //     (= (piece x0 (+ x1 2) (+ x2 2) "r") (arg 2 pos))))
  val concept56 = Concept(posLabel, And(Unificate(piece_00r, arg0), Equals(piece_11r, arg1), Equals(piece_22r, arg2)))
  val defConcept56 = DefineConcept(Label("concept56"), concept56)

  //(concept concept57 (pos)
  //  (and 
  //    (unificate (piece x0 x1 x2 "w") (arg 0 pos))
  //    (= (piece x0 (+ x1 1) (+ x2 1) "w") (arg 1 pos))
  //    (= (piece x0 (+ x1 2) (+ x2 2) "w") (arg 2 pos))))
  val concept57 = Concept(posLabel, And(Unificate(piece_00w, arg0), Equals(piece_11w, arg1), Equals(piece_22w, arg2)))
  val defConcept57 = DefineConcept(Label("concept57"), concept57)

  //(concept concept62 (pos)
  //  (and 
  //    (unificate (piece x0 x1 x2 "r") (arg 0 pos))
  //    (= (piece x0 x1 (+ x2 -1) "r") (arg 1 pos))
  //    (= (piece x0 x1 (+ x2 1) "r") (arg 2 pos))))
  val concept62 = Concept(posLabel, And(Unificate(piece_00r, arg0), Equals(piece_0m1r, arg1), Equals(piece_01r, arg2)))
  val defConcept62 = DefineConcept(Label("concept62"), concept62)

  //(concept concept70 (pos)
  //  (and 
  //    (unificate (piece x0 x1 x2 "r") (arg 0 pos))
  //    (= (piece x0 (+ x1 1) x2 "r") (arg 1 pos))
  //    (= (piece x0 (+ x1 -1) x2 "r") (arg 2 pos))))
  val concept70 = Concept(posLabel, And(Unificate(piece_00r, arg0), Equals(piece_01r, arg1), Equals(piece_0m1r, arg2)))
  val defConcept70 = DefineConcept(Label("concept70"), concept70)

  //(concept concept71 (pos)
  //  (and 
  //    (unificate (piece x0 x1 x2 "w") (arg 0 pos))
  //    (= (piece x0 (+ x1 1) x2 "w") (arg 1 pos))
  //    (= (piece x0 (+ x1 -1) x2 "w") (arg 2 pos))))
  val concept71 = Concept(posLabel, And(Unificate(piece_00w, arg0), Equals(piece_10w, arg1), Equals(piece_m10w, arg2)))
  val defConcept71 = DefineConcept(Label("concept71"), concept71)

  val con37 = Label("concept37")
  val con55 = Label("concept55")
  val con56 = Label("concept56")
  val con57 = Label("concept57")
  val con62 = Label("concept62")
  val con70 = Label("concept70")
  val con71 = Label("concept71")
  
//concept70 <= 1
//|   concept62 <= 1
//|   |   concept56 <= 1
//|   |   |   concept37 <= 124: win (43.0)
//|   |   |   concept37 > 124: lose (3.0/1.0)
//|   |   concept56 > 1: lose (6.0/1.0)
//|   concept62 > 1
//|   |   concept57 <= 1: lose (17.0)
//|   |   concept57 > 1: win (2.0)
//concept70 > 1
//|   concept71 <= 1: lose (23.0)
//|   concept71 > 1
//|   |   concept55 <= 1: lose (4.0/1.0)
//|   |   concept55 > 1: win (2.0)  
  def evaluateTree(p: List[Position], i: Interpreter): Boolean = {
    if (!isMatch(p, i, con70)) {
      if (!isMatch(p, i, con62)) {
        if (!isMatch(p, i, con56)) {
          countMatch(p, i, con37) <= 124
        } else {
          false
        }
      } else {
        isMatch(p, i, con57)
      }
    } else {
      if (!isMatch(p, i, con71)) {
        false
      } else {
        !isMatch(p, i, con55)
      }
    }
  }

  def loadTree(interpreter: Interpreter): Interpreter = {
    interpreter.interpret(defConcept37)
    interpreter.interpret(defConcept55)
    interpreter.interpret(defConcept56)
    interpreter.interpret(defConcept57)
    interpreter.interpret(defConcept62)
    interpreter.interpret(defConcept70)
    interpreter.interpret(defConcept71)
    interpreter
  }
  
  var positionLookUpTable = Map.empty[Position, Boolean]
  var subPositionLookUpTable = Map.empty[(Label, Position), Boolean]
  
  def evaluate(position: MachineState, interpreter: Interpreter): Boolean = {
    val cdl = removeBlank(PlayerUtil.gdlTocdl(position))
    if (cdl.size < 10 && positionLookUpTable.contains(cdl)) {
      positionLookUpTable(cdl)
    } else {
      val evaluation = evaluatePosition(cdl, interpreter)
      positionLookUpTable += cdl -> evaluation
      evaluation
    }
  }

  def removeBlank(position: Position): Position = {
    new Position(position.position map (_.asInstanceOf[Piece]) filter (piece => (piece.args.last != Symbol("b") && piece.args.last != Symbol("dirt"))), None)
  }
  
  def evaluatePosition(position: Position, interpreter: Interpreter): Boolean = {
    val subPos : List[Position] = PlayerUtil.makeBinaryPosition(position) ++
      PlayerUtil.makeAllTrinaryPosition(position)
      evaluateTree(subPos, interpreter)
  }
  
  def isMatch(subPositions: List[Position], interpreter: Interpreter, name: Label): Boolean = {
    for (pos <- subPositions) {
      if(evaluateSubPosition(pos, interpreter, name)) {
        return true
      }
    }
    false
  }
  
  def countMatch(subPositions: List[Position], interpreter: Interpreter, name: Label): Int = {
    subPositions map (pos => interpreter.interpret(makeQuery(name, pos)).asInstanceOf[Bool].value) count (_ == true)
  }
  
  def makeQuery(name: Label, subPosition: Position): ConceptCall = ConceptCall(name, List(TypedExp(subPosition, PositionType())))
  
  def evaluateSubPositionBuf(subPosition: Position, interpreter: Interpreter, name: Label): Boolean = {
    if (subPositionLookUpTable.contains((name, subPosition))) {
      positionLookUpTable(subPosition)
    } else {
      val evaluation = evaluateSubPosition(subPosition, interpreter, name)
      subPositionLookUpTable += (name, subPosition) -> evaluation
      evaluation
    }
  }
  
  def evaluateSubPosition(subPosition: Position, interpreter: Interpreter, name: Label): Boolean = {
    if (subPosition.size == 2) {
      if (name == con37) {
        interpreter.interpret(makeQuery(name, subPosition)).asInstanceOf[Bool].value
      } else {
        false
      }
    } else {
      if (name == con37) {
        false
      } else {
        interpreter.interpret(makeQuery(name, subPosition)).asInstanceOf[Bool].value
      }
    }
  }
  
}
