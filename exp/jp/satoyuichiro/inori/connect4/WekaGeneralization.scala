package jp.satoyuichiro.inori.connect4

import jp.satoyuichiro.inori.cdl._
import org.ggp.base.util.statemachine.implementation.prover.ProverStateMachine
import jp.satoyuichiro.inori.gdl.Description
import jp.satoyuichiro.inori.gdl.GdlUtil
import jp.satoyuichiro.inori.player.RandomPlayer
import jp.satoyuichiro.inori.simulator.GameSimulator
import org.ggp.base.util.statemachine.MachineState
import jp.satoyuichiro.inori.player.PlayerUtil
import jp.satoyuichiro.inori.learning.concept.generator.CompositeDisjunction
import jp.satoyuichiro.inori.weka.NumberTree
import jp.satoyuichiro.inori.weka.NumberNode
import jp.satoyuichiro.inori.weka.NumberLeaf
import jp.satoyuichiro.inori.weka.GeneralizationUtil
import jp.satoyuichiro.inori.weka.WekaLabel
import jp.satoyuichiro.inori.weka.LoseLeaf
import jp.satoyuichiro.inori.weka.BoolNode
import jp.satoyuichiro.inori.weka.CompositeTree
import jp.satoyuichiro.inori.cdl.Interpreter
import jp.satoyuichiro.inori.weka.WekaUtil
import java.nio.file.Paths
import jp.satoyuichiro.inori.weka.BoolLeaf
import jp.satoyuichiro.inori.weka.WekaTree
import jp.satoyuichiro.inori.weka.BoolTree
import jp.satoyuichiro.inori.weka.WinLeaf
import jp.satoyuichiro.inori.weka.WekaParser
import jp.satoyuichiro.inori.weka.WekaTreeCompiler

object WekaGeneralization {

  val connect4gdl = "./games/twoplayers/connect4.gdl"
  val expSize = 100

  def main(args: Array[String]): Unit = {
    val nm = getTree(connect4nm)
    val nmep1 = getTree(connect4nmep1)
    val nmep3 = getTree(connect4nmep3)
    val nmep5 = getTree(connect4nmep5)
    val nmep10 = getTree(connect4nmep10)

    val tf = getTree(connect4tf)
    //println(getTree(connect4tfep1))
    val tfep3 = getTree(connect4tfep3)
    val tfep5 = getTree(connect4tfep5)
    val tfep10 = getTree(connect4tfep10)

    val no = getTriTree(connect4no)
    println(no)
//    experiment(nm)
//    experiment(nmep1)
//    experiment(nmep3)
//    experiment(nmep5)
//    experiment(nmep10)
//    
//    experiment(tf)
////    experiment(tfep1)
//    experiment(tfep3)
//    experiment(tfep5)
//    experiment(tfep10)
  }
  
  def experiment(tree: WekaTree): Unit = {
    val win = GeneralizationUtil.simulateWinEpisode(connect4gdl, expSize)
    val lose = GeneralizationUtil.simulateLoseEpisode(connect4gdl, expSize)
    
    val winLength = (win.map(_.size.toDouble)).fold(0.0)(_ + _) / win.size.toDouble
    val loseLength = (lose.map(_.size.toDouble)).fold(0.0)(_ + _) / win.size.toDouble
    
    println("average game length with win" + winLength)
    println("average game length with lose" + loseLength)
    
    val win5 = makeTestPosition(win, 5)
    val lose5 = makeTestPosition(lose, 5)
    
    val win10 = makeTestPosition(win, 10)
    val lose10 = makeTestPosition(lose, 10)

    val win15 = makeTestPosition(win, 15)
    val lose15 = makeTestPosition(lose, 15)
    
    val win20 = makeTestPosition(win, 20)
    val lose20 = makeTestPosition(lose, 20)
    
    val winLast = win map (p => GeneralizationUtil.getLast(p)) map removeBlank
    val loseLast = lose map (p => GeneralizationUtil.getLast(p)) map removeBlank
    
    println(evaluate(tree, win5) + " " + evaluate(tree, win10) + " " + evaluate(tree, win15) +  " " + evaluate(tree, win20) +  " " + evaluate(tree, winLast))
    println(evaluate(tree, lose5) + " " + evaluate(tree, lose10) + " " + evaluate(tree, lose15) +  " " + evaluate(tree, lose20) +  " " + evaluate(tree, loseLast))
  }
  
  def makeTestPosition(positions: List[List[Position]], n: Int): List[Position] = {
    positions map (p => GeneralizationUtil.getPositionAt(p, n)) map removeBlank
  }
  
  def removeBlank(position: Position): Position = {
    new Position(position.position map (_.asInstanceOf[Piece]) filter (piece => (piece.args.last != Symbol("b") && piece.args.last != Symbol("dirt"))), None)
  }
  
  def evaluate(tree: WekaTree, positions: List[Position]): Int = {
    val interpreter = new Interpreter
    val concepts = WekaUtil.getConceptFromDefineConcept(Paths.get("./concepts/concepts-define.txt"))
    concepts foreach { c => interpreter.interpret(c.toDefineConcept.get) }
    positions.par.map(p => tree.eval(interpreter, p)) count (_ == WekaLabel.Win)
  }
  
  def getTree(str: String): WekaTree = {
    WekaTreeCompiler.toBinaryWekaTree(WekaParser.parse(str).get)
  }
  
  def getTriTree(str: String): WekaTree = {
    println(str)
    println(WekaTreeCompiler.toAST(WekaParser.parse(str).get))
    WekaTreeCompiler.toClasifyWekaTree(WekaParser.parse(str).get, 4)
  }
////  concept52 <= 0
////  |   concept65 <= 0
////  |   |   concept64 <= 0
////  |   |   |   concept18 <= 0: lose (2.0)
////  |   |   |   concept18 > 0
////  |   |   |   |   concept5 <= 0
////  |   |   |   |   |   concept0 <= 0: win (2.0)
////  |   |   |   |   |   concept0 > 0: lose (2.0)
////  |   |   |   |   concept5 > 0: win (7.0)
////  |   |   concept64 > 0: lose (13.0/1.0)
////  |   concept65 > 0
////  |   |   concept56 <= 0: win (23.0/2.0)
////  |   |   concept56 > 0
////  |   |   |   concept9 <= 0: win (3.0)
////  |   |   |   concept9 > 0
////  |   |   |   |   concept8 <= 0: lose (4.0)
////  |   |   |   |   concept8 > 0
////  |   |   |   |   |   concept20 <= 0: lose (3.0/1.0)
////  |   |   |   |   |   concept20 > 0: win (7.0/1.0)
////  concept52 > 0: lose (34.0/9.0)
//  def tfTree: BoolTree = {
//    val leaf_0 = BoolLeaf(Label("concept0"), WekaLabel.Lose, WekaLabel.Win)
//    val leaf_20 = BoolLeaf(Label("concept20"), WekaLabel.Win, WekaLabel.Lose)
//    
//    val node_5 = BoolNode(Label("concept5"), WinLeaf(), leaf_0)
//    val node_18 = BoolNode(Label("concept18"), node_5, LoseLeaf())
//    val node_64 = BoolNode(Label("concept64"), node_18, LoseLeaf())
//    val node_8 = BoolNode(Label("concept8"), leaf_20, LoseLeaf())
//    val node_9 = BoolNode(Label("concept9"), node_8, WinLeaf())
//    val node_56 = BoolNode(Label("concept56"), node_9, WinLeaf())
//    
//    val falseCase = BoolNode(Label("concept65"), node_56, node_64)
//    val top = BoolNode(Label("concept52"), LoseLeaf(), falseCase)
//    BoolTree(top)
//  }
//  
//  val tfSource = 
//    """
//    concept52 <= 0
//    |   concept65 <= 0
//    |   |   concept64 <= 0
//    |   |   |   concept18 <= 0: lose (2.0)
//    |   |   |   concept18 > 0
//    |   |   |   |   concept5 <= 0
//    |   |   |   |   |   concept0 <= 0: win (2.0)
//    |   |   |   |   |   concept0 > 0: lose (2.0)
//    |   |   |   |   concept5 > 0: win (7.0)
//    |   |   concept64 > 0: lose (13.0/1.0)
//    |   concept65 > 0
//    |   |   concept56 <= 0: win (23.0/2.0)
//    |   |   concept56 > 0
//    |   |   |   concept9 <= 0: win (3.0)
//    |   |   |   concept9 > 0
//    |   |   |   |   concept8 <= 0: lose (4.0)
//    |   |   |   |   concept8 > 0
//    |   |   |   |   |   concept20 <= 0: lose (3.0/1.0)
//    |   |   |   |   |   concept20 > 0: win (7.0/1.0)
//    concept52 > 0: lose (34.0/9.0)
//    """
  
  val connect4nm =
    """
    concept6 <= 1
    |   concept2 <= 1
    |   |   concept3 <= 1
    |   |   |   concept7 <= 1
    |   |   |   |   concept1 <= 1
    |   |   |   |   |   concept41 <= 60: lose (9.0)
    |   |   |   |   |   concept41 > 60: win (2.0)
    |   |   |   |   concept1 > 1: win (4.0)
    |   |   |   concept7 > 1: win (15.0)
    |   |   concept3 > 1: win (22.0)
    |   concept2 > 1
    |   |   concept23 <= 185: lose (14.0)
    |   |   concept23 > 185: win (3.0)
    concept6 > 1
    |   concept1 <= 0: lose (23.0)
    |   concept1 > 0
    |   |   concept37 <= 160: lose (4.0)
    |   |   concept37 > 160: win (4.0)
    """
  
   val connect4nmep1 =
     """
concept2 <= 1
|   concept0 <= 1
|   |   concept6 <= 1
|   |   |   concept3 <= 1
|   |   |   |   concept7 <= 1
|   |   |   |   |   concept23 <= 78: even (1780.0/5.0)
|   |   |   |   |   concept23 > 78
|   |   |   |   |   |   concept55 <= 176
|   |   |   |   |   |   |   concept40 <= 58
|   |   |   |   |   |   |   |   concept63 <= 90: even (18.0/2.0)
|   |   |   |   |   |   |   |   concept63 > 90: win (4.0)
|   |   |   |   |   |   |   concept40 > 58: even (35.0)
|   |   |   |   |   |   concept55 > 176: lose (2.0)
|   |   |   |   concept7 > 1
|   |   |   |   |   concept3 <= 0
|   |   |   |   |   |   concept25 <= 64: win (3.0)
|   |   |   |   |   |   concept25 > 64: even (38.0/5.0)
|   |   |   |   |   concept3 > 0
|   |   |   |   |   |   concept19 <= 1: win (6.0/1.0)
|   |   |   |   |   |   concept19 > 1: even (2.0)
|   |   |   concept3 > 1
|   |   |   |   concept31 <= 32: win (13.0)
|   |   |   |   concept31 > 32
|   |   |   |   |   concept62 <= 84
|   |   |   |   |   |   concept22 <= 58
|   |   |   |   |   |   |   concept6 <= 0: even (18.0/2.0)
|   |   |   |   |   |   |   concept6 > 0: win (3.0)
|   |   |   |   |   |   concept22 > 58: even (43.0)
|   |   |   |   |   concept62 > 84: win (2.0)
|   |   concept6 > 1
|   |   |   concept6 <= 2
|   |   |   |   concept45 <= 42: lose (7.0)
|   |   |   |   concept45 > 42
|   |   |   |   |   concept7 <= 1
|   |   |   |   |   |   concept1 <= 0: even (33.0/2.0)
|   |   |   |   |   |   concept1 > 0: lose (3.0/1.0)
|   |   |   |   |   concept7 > 1
|   |   |   |   |   |   concept7 <= 2
|   |   |   |   |   |   |   concept18 <= 0: win (2.0)
|   |   |   |   |   |   |   concept18 > 0: even (6.0/1.0)
|   |   |   |   |   |   concept7 > 2: win (2.0)
|   |   |   concept6 > 2: lose (3.0)
|   concept0 > 1
|   |   concept1 <= 1
|   |   |   concept15 <= 0
|   |   |   |   concept40 <= 42: even (2.0)
|   |   |   |   concept40 > 42: lose (7.0)
|   |   |   concept15 > 0
|   |   |   |   concept18 <= 2
|   |   |   |   |   concept30 <= 102: lose (2.0)
|   |   |   |   |   concept30 > 102: even (21.0)
|   |   |   |   concept18 > 2: lose (2.0)
|   |   concept1 > 1: win (2.0/1.0)
concept2 > 1
|   concept58 <= 26: lose (10.0)
|   concept58 > 26
|   |   concept63 <= 54
|   |   |   concept58 <= 186: even (45.0/2.0)
|   |   |   concept58 > 186: lose (3.0)
|   |   concept63 > 54: lose (7.0)
     """
   
   val connect4nmep3 =
     """
concept36 <= 34
|   concept1 <= 0
|   |   concept6 <= 0
|   |   |   concept3 <= 0
|   |   |   |   concept67 <= 18: even (1047.0/13.0)
|   |   |   |   concept67 > 18
|   |   |   |   |   concept44 <= 0
|   |   |   |   |   |   concept30 <= 0: even (4.0/1.0)
|   |   |   |   |   |   concept30 > 0: win (4.0)
|   |   |   |   |   concept44 > 0: even (38.0/1.0)
|   |   |   concept3 > 0
|   |   |   |   concept3 <= 1
|   |   |   |   |   concept59 <= 14
|   |   |   |   |   |   concept59 <= 0
|   |   |   |   |   |   |   concept39 <= 22: even (69.0/6.0)
|   |   |   |   |   |   |   concept39 > 22: win (4.0/1.0)
|   |   |   |   |   |   concept59 > 0: win (4.0)
|   |   |   |   |   concept59 > 14: even (95.0/1.0)
|   |   |   |   concept3 > 1: win (10.0/1.0)
|   |   concept6 > 0
|   |   |   concept18 <= 0
|   |   |   |   concept6 <= 1
|   |   |   |   |   concept41 <= 50
|   |   |   |   |   |   concept45 <= 19
|   |   |   |   |   |   |   concept66 <= 17
|   |   |   |   |   |   |   |   concept40 <= 27
|   |   |   |   |   |   |   |   |   concept38 <= 15: lose (2.0)
|   |   |   |   |   |   |   |   |   concept38 > 15: even (22.0/1.0)
|   |   |   |   |   |   |   |   concept40 > 27: lose (2.0)
|   |   |   |   |   |   |   concept66 > 17: lose (2.0)
|   |   |   |   |   |   concept45 > 19: even (51.0)
|   |   |   |   |   concept41 > 50: lose (2.0)
|   |   |   |   concept6 > 1: lose (2.0)
|   |   |   concept18 > 0: even (16.0)
|   concept1 > 0
|   |   concept19 <= 0
|   |   |   concept22 <= 52
|   |   |   |   concept58 <= 42
|   |   |   |   |   concept36 <= 0: lose (3.0/1.0)
|   |   |   |   |   concept36 > 0: even (17.0)
|   |   |   |   concept58 > 42: lose (4.0)
|   |   |   concept22 > 52: win (3.0)
|   |   concept19 > 0: win (3.0)
concept36 > 34
|   concept2 <= 1
|   |   concept1 <= 1
|   |   |   concept7 <= 1
|   |   |   |   concept0 <= 1
|   |   |   |   |   concept58 <= 164
|   |   |   |   |   |   concept18 <= 1
|   |   |   |   |   |   |   concept6 <= 1
|   |   |   |   |   |   |   |   concept15 <= 1
|   |   |   |   |   |   |   |   |   concept26 <= 17
|   |   |   |   |   |   |   |   |   |   concept37 <= 80
|   |   |   |   |   |   |   |   |   |   |   concept27 <= 54: even (67.0/5.0)
|   |   |   |   |   |   |   |   |   |   |   concept27 > 54
|   |   |   |   |   |   |   |   |   |   |   |   concept7 <= 0: even (3.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept7 > 0: win (4.0)
|   |   |   |   |   |   |   |   |   |   concept37 > 80
|   |   |   |   |   |   |   |   |   |   |   concept59 <= 26: even (2.0)
|   |   |   |   |   |   |   |   |   |   |   concept59 > 26: win (8.0)
|   |   |   |   |   |   |   |   |   concept26 > 17
|   |   |   |   |   |   |   |   |   |   concept25 <= 152
|   |   |   |   |   |   |   |   |   |   |   concept26 <= 29
|   |   |   |   |   |   |   |   |   |   |   |   concept62 <= 52
|   |   |   |   |   |   |   |   |   |   |   |   |   concept45 <= 72
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept37 <= 9: lose (5.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept37 > 9
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept31 <= 54
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept7 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept26 <= 18
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept3 <= 0: even (6.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept3 > 0: win (3.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept26 > 18: even (68.0/4.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept7 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept0 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept59 <= 9: win (3.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept59 > 9
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept62 <= 23
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept25 <= 38: lose (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept25 > 38: even (13.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept62 > 23: lose (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept0 > 0: lose (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept31 > 54
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept15 <= 0: win (5.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept15 > 0: even (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept45 > 72
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept6 <= 0: win (8.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept6 > 0: even (9.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept62 > 52
|   |   |   |   |   |   |   |   |   |   |   |   |   concept22 <= 27: lose (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept22 > 27: win (4.0)
|   |   |   |   |   |   |   |   |   |   |   concept26 > 29
|   |   |   |   |   |   |   |   |   |   |   |   concept0 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   concept6 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept22 <= 72: even (118.0/9.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept22 > 72
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept10 <= 1
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept30 <= 57: even (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept30 > 57: lose (7.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept10 > 1: even (6.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept6 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept36 <= 44: lose (4.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept36 > 44
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept24 <= 108
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept38 <= 36: even (16.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept38 > 36
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept18 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept7 <= 0: lose (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept7 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept10 <= 0: even (5.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept10 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept40 <= 14: even (5.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept40 > 14
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept37 <= 29: even (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept37 > 29: lose (5.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept18 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept36 <= 51: even (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept36 > 51: lose (7.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept24 > 108: even (37.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept0 > 0: even (55.0/1.0)
|   |   |   |   |   |   |   |   |   |   concept25 > 152
|   |   |   |   |   |   |   |   |   |   |   concept41 <= 48: win (7.0)
|   |   |   |   |   |   |   |   |   |   |   concept41 > 48
|   |   |   |   |   |   |   |   |   |   |   |   concept37 <= 132
|   |   |   |   |   |   |   |   |   |   |   |   |   concept39 <= 108: lose (3.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept39 > 108: even (8.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept37 > 132: lose (3.0)
|   |   |   |   |   |   |   |   concept15 > 1
|   |   |   |   |   |   |   |   |   concept23 <= 93: win (7.0/1.0)
|   |   |   |   |   |   |   |   |   concept23 > 93: even (7.0)
|   |   |   |   |   |   |   concept6 > 1
|   |   |   |   |   |   |   |   concept40 <= 105: lose (10.0)
|   |   |   |   |   |   |   |   concept40 > 105
|   |   |   |   |   |   |   |   |   concept22 <= 34: even (7.0)
|   |   |   |   |   |   |   |   |   concept22 > 34: win (3.0)
|   |   |   |   |   |   concept18 > 1
|   |   |   |   |   |   |   concept2 <= 0: lose (6.0)
|   |   |   |   |   |   |   concept2 > 0
|   |   |   |   |   |   |   |   concept3 <= 1: even (17.0/1.0)
|   |   |   |   |   |   |   |   concept3 > 1: lose (3.0)
|   |   |   |   |   concept58 > 164
|   |   |   |   |   |   concept24 <= 124: even (3.0)
|   |   |   |   |   |   concept24 > 124
|   |   |   |   |   |   |   concept1 <= 0
|   |   |   |   |   |   |   |   concept22 <= 215: even (2.0)
|   |   |   |   |   |   |   |   concept22 > 215: lose (2.0)
|   |   |   |   |   |   |   concept1 > 0: lose (8.0)
|   |   |   |   concept0 > 1
|   |   |   |   |   concept3 <= 1
|   |   |   |   |   |   concept1 <= 0: lose (11.0)
|   |   |   |   |   |   concept1 > 0
|   |   |   |   |   |   |   concept3 <= 0: lose (2.0)
|   |   |   |   |   |   |   concept3 > 0: even (8.0/1.0)
|   |   |   |   |   concept3 > 1: win (2.0)
|   |   |   concept7 > 1
|   |   |   |   concept45 <= 102
|   |   |   |   |   concept1 <= 0
|   |   |   |   |   |   concept14 <= 0: win (9.0)
|   |   |   |   |   |   concept14 > 0
|   |   |   |   |   |   |   concept22 <= 22: even (2.0)
|   |   |   |   |   |   |   concept22 > 22: win (6.0)
|   |   |   |   |   concept1 > 0
|   |   |   |   |   |   concept22 <= 13: win (3.0)
|   |   |   |   |   |   concept22 > 13: lose (4.0)
|   |   |   |   concept45 > 102
|   |   |   |   |   concept31 <= 125
|   |   |   |   |   |   concept25 <= 152: even (17.0/1.0)
|   |   |   |   |   |   concept25 > 152: win (2.0/1.0)
|   |   |   |   |   concept31 > 125: win (7.0/1.0)
|   |   concept1 > 1
|   |   |   concept3 <= 0: even (7.0/1.0)
|   |   |   concept3 > 0: win (19.0/3.0)
|   concept2 > 1
|   |   concept15 <= 1: lose (20.0)
|   |   concept15 > 1
|   |   |   concept2 <= 2: win (2.0)
|   |   |   concept2 > 2: lose (2.0)
     """
   
   val connect4nmep5 =
     """
concept6 <= 0
|   concept41 <= 24
|   |   concept26 <= 29
|   |   |   concept10 <= 0
|   |   |   |   concept31 <= 16
|   |   |   |   |   concept30 <= 24
|   |   |   |   |   |   concept3 <= 0: even (974.0/18.0)
|   |   |   |   |   |   concept3 > 0
|   |   |   |   |   |   |   concept36 <= 34
|   |   |   |   |   |   |   |   concept23 <= 0: even (52.0/1.0)
|   |   |   |   |   |   |   |   concept23 > 0
|   |   |   |   |   |   |   |   |   concept24 <= 0: lose (3.0)
|   |   |   |   |   |   |   |   |   concept24 > 0: even (10.0)
|   |   |   |   |   |   |   concept36 > 34: lose (5.0)
|   |   |   |   |   concept30 > 24
|   |   |   |   |   |   concept7 <= 0
|   |   |   |   |   |   |   concept44 <= 25: even (13.0/1.0)
|   |   |   |   |   |   |   concept44 > 25
|   |   |   |   |   |   |   |   concept59 <= 10: lose (3.0)
|   |   |   |   |   |   |   |   concept59 > 10: even (4.0)
|   |   |   |   |   |   concept7 > 0
|   |   |   |   |   |   |   concept0 <= 0: win (4.0/1.0)
|   |   |   |   |   |   |   concept0 > 0: even (2.0)
|   |   |   |   concept31 > 16
|   |   |   |   |   concept0 <= 0
|   |   |   |   |   |   concept18 <= 0
|   |   |   |   |   |   |   concept45 <= 0: even (33.0/1.0)
|   |   |   |   |   |   |   concept45 > 0
|   |   |   |   |   |   |   |   concept26 <= 0
|   |   |   |   |   |   |   |   |   concept62 <= 31
|   |   |   |   |   |   |   |   |   |   concept67 <= 0
|   |   |   |   |   |   |   |   |   |   |   concept11 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   concept58 <= 17: even (27.0/2.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept58 > 17
|   |   |   |   |   |   |   |   |   |   |   |   |   concept63 <= 0: win (10.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept63 > 0: even (2.0)
|   |   |   |   |   |   |   |   |   |   |   concept11 > 0: win (2.0)
|   |   |   |   |   |   |   |   |   |   concept67 > 0: even (24.0)
|   |   |   |   |   |   |   |   |   concept62 > 31: win (3.0)
|   |   |   |   |   |   |   |   concept26 > 0
|   |   |   |   |   |   |   |   |   concept11 <= 0
|   |   |   |   |   |   |   |   |   |   concept40 <= 42: even (92.0/2.0)
|   |   |   |   |   |   |   |   |   |   concept40 > 42
|   |   |   |   |   |   |   |   |   |   |   concept7 <= 0: win (2.0)
|   |   |   |   |   |   |   |   |   |   |   concept7 > 0: even (3.0)
|   |   |   |   |   |   |   |   |   concept11 > 0
|   |   |   |   |   |   |   |   |   |   concept45 <= 28: win (3.0)
|   |   |   |   |   |   |   |   |   |   concept45 > 28: even (15.0)
|   |   |   |   |   |   concept18 > 0
|   |   |   |   |   |   |   concept22 <= 16: even (12.0)
|   |   |   |   |   |   |   concept22 > 16: win (5.0)
|   |   |   |   |   concept0 > 0: even (20.0)
|   |   |   concept10 > 0
|   |   |   |   concept31 <= 48
|   |   |   |   |   concept26 <= 11
|   |   |   |   |   |   concept23 <= 46: even (22.0/2.0)
|   |   |   |   |   |   concept23 > 46: lose (2.0)
|   |   |   |   |   concept26 > 11: lose (5.0/1.0)
|   |   |   |   concept31 > 48: win (6.0)
|   |   concept26 > 29
|   |   |   concept7 <= 1
|   |   |   |   concept11 <= 0
|   |   |   |   |   concept55 <= 57
|   |   |   |   |   |   concept10 <= 0
|   |   |   |   |   |   |   concept39 <= 0: even (23.0/1.0)
|   |   |   |   |   |   |   concept39 > 0
|   |   |   |   |   |   |   |   concept3 <= 1
|   |   |   |   |   |   |   |   |   concept40 <= 19
|   |   |   |   |   |   |   |   |   |   concept0 <= 0
|   |   |   |   |   |   |   |   |   |   |   concept54 <= 36
|   |   |   |   |   |   |   |   |   |   |   |   concept19 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   concept30 <= 16
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept59 <= 0: win (9.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept59 > 0: even (18.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept30 > 16: win (16.0/3.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept19 > 0: win (5.0)
|   |   |   |   |   |   |   |   |   |   |   concept54 > 36: even (6.0)
|   |   |   |   |   |   |   |   |   |   concept0 > 0: win (2.0)
|   |   |   |   |   |   |   |   |   concept40 > 19: even (16.0)
|   |   |   |   |   |   |   |   concept3 > 1: win (4.0)
|   |   |   |   |   |   concept10 > 0
|   |   |   |   |   |   |   concept38 <= 36: even (13.0)
|   |   |   |   |   |   |   concept38 > 36: lose (2.0)
|   |   |   |   |   concept55 > 57
|   |   |   |   |   |   concept67 <= 27: even (8.0)
|   |   |   |   |   |   concept67 > 27: lose (4.0)
|   |   |   |   concept11 > 0: even (19.0)
|   |   |   concept7 > 1: lose (6.0/1.0)
|   concept41 > 24
|   |   concept18 <= 1
|   |   |   concept30 <= 108
|   |   |   |   concept0 <= 1
|   |   |   |   |   concept66 <= 116
|   |   |   |   |   |   concept23 <= 24
|   |   |   |   |   |   |   concept1 <= 0
|   |   |   |   |   |   |   |   concept37 <= 36
|   |   |   |   |   |   |   |   |   concept0 <= 0
|   |   |   |   |   |   |   |   |   |   concept45 <= 31: lose (4.0)
|   |   |   |   |   |   |   |   |   |   concept45 > 31: even (2.0)
|   |   |   |   |   |   |   |   |   concept0 > 0: lose (2.0)
|   |   |   |   |   |   |   |   concept37 > 36: even (6.0)
|   |   |   |   |   |   |   concept1 > 0
|   |   |   |   |   |   |   |   concept10 <= 0: win (4.0)
|   |   |   |   |   |   |   |   concept10 > 0: lose (7.0/1.0)
|   |   |   |   |   |   concept23 > 24
|   |   |   |   |   |   |   concept27 <= 48
|   |   |   |   |   |   |   |   concept15 <= 1: even (42.0/1.0)
|   |   |   |   |   |   |   |   concept15 > 1: lose (3.0/1.0)
|   |   |   |   |   |   |   concept27 > 48
|   |   |   |   |   |   |   |   concept30 <= 12
|   |   |   |   |   |   |   |   |   concept10 <= 0: win (9.0/1.0)
|   |   |   |   |   |   |   |   |   concept10 > 0: lose (2.0)
|   |   |   |   |   |   |   |   concept30 > 12
|   |   |   |   |   |   |   |   |   concept7 <= 0
|   |   |   |   |   |   |   |   |   |   concept67 <= 12: win (6.0)
|   |   |   |   |   |   |   |   |   |   concept67 > 12
|   |   |   |   |   |   |   |   |   |   |   concept10 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   concept0 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   concept40 <= 52
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept23 <= 63: even (24.0/2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept23 > 63: win (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept40 > 52: win (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept0 > 0: even (12.0)
|   |   |   |   |   |   |   |   |   |   |   concept10 > 0
|   |   |   |   |   |   |   |   |   |   |   |   concept19 <= 0: win (7.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept19 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   concept15 <= 1: even (8.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept15 > 1
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept22 <= 56: even (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept22 > 56: win (2.0)
|   |   |   |   |   |   |   |   |   concept7 > 0
|   |   |   |   |   |   |   |   |   |   concept19 <= 0: even (14.0/1.0)
|   |   |   |   |   |   |   |   |   |   concept19 > 0
|   |   |   |   |   |   |   |   |   |   |   concept1 <= 0: win (2.0)
|   |   |   |   |   |   |   |   |   |   |   concept1 > 0: lose (3.0/1.0)
|   |   |   |   |   concept66 > 116: lose (5.0)
|   |   |   |   concept0 > 1: lose (9.0/1.0)
|   |   |   concept30 > 108: win (9.0)
|   |   concept18 > 1
|   |   |   concept39 <= 111: lose (9.0)
|   |   |   concept39 > 111: win (2.0)
concept6 > 0
|   concept6 <= 1
|   |   concept25 <= 234
|   |   |   concept7 <= 1
|   |   |   |   concept2 <= 0
|   |   |   |   |   concept26 <= 105
|   |   |   |   |   |   concept22 <= 124
|   |   |   |   |   |   |   concept1 <= 0
|   |   |   |   |   |   |   |   concept3 <= 1
|   |   |   |   |   |   |   |   |   concept62 <= 58
|   |   |   |   |   |   |   |   |   |   concept11 <= 0
|   |   |   |   |   |   |   |   |   |   |   concept14 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   concept15 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   concept40 <= 25
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept27 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept36 <= 38
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept54 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept62 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept58 <= 20
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept38 <= 13: lose (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept38 > 13
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept55 <= 0: even (12.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept55 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept63 <= 0: even (9.0/4.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept63 > 0: lose (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept58 > 20: lose (3.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept62 > 0: lose (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept54 > 0: lose (9.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept36 > 38
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept26 <= 42: win (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept26 > 42: lose (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept27 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept22 <= 66
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept19 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept44 <= 0: even (26.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept44 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept38 <= 20
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept3 <= 0: lose (7.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept3 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept23 <= 0: lose (3.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept23 > 0: even (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept38 > 20
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept44 <= 66: even (30.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept44 > 66: lose (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept19 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept3 <= 0: lose (3.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept3 > 0: even (3.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept22 > 66: lose (4.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept40 > 25: even (27.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept15 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   concept7 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept30 <= 33: even (13.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept30 > 33: win (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept7 > 0: even (3.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   concept14 > 0
|   |   |   |   |   |   |   |   |   |   |   |   concept25 <= 132: even (3.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept25 > 132: win (2.0)
|   |   |   |   |   |   |   |   |   |   concept11 > 0
|   |   |   |   |   |   |   |   |   |   |   concept3 <= 0: even (10.0)
|   |   |   |   |   |   |   |   |   |   |   concept3 > 0: win (5.0/1.0)
|   |   |   |   |   |   |   |   |   concept62 > 58: lose (8.0/1.0)
|   |   |   |   |   |   |   |   concept3 > 1
|   |   |   |   |   |   |   |   |   concept3 <= 2
|   |   |   |   |   |   |   |   |   |   concept18 <= 0: win (2.0)
|   |   |   |   |   |   |   |   |   |   concept18 > 0: even (10.0)
|   |   |   |   |   |   |   |   |   concept3 > 2: win (3.0)
|   |   |   |   |   |   |   concept1 > 0
|   |   |   |   |   |   |   |   concept0 <= 0
|   |   |   |   |   |   |   |   |   concept58 <= 20: even (11.0)
|   |   |   |   |   |   |   |   |   concept58 > 20
|   |   |   |   |   |   |   |   |   |   concept11 <= 0: win (12.0/1.0)
|   |   |   |   |   |   |   |   |   |   concept11 > 0
|   |   |   |   |   |   |   |   |   |   |   concept7 <= 0: even (5.0)
|   |   |   |   |   |   |   |   |   |   |   concept7 > 0: win (8.0/1.0)
|   |   |   |   |   |   |   |   concept0 > 0: even (9.0)
|   |   |   |   |   |   concept22 > 124: lose (7.0)
|   |   |   |   |   concept26 > 105
|   |   |   |   |   |   concept7 <= 0
|   |   |   |   |   |   |   concept38 <= 28: even (2.0)
|   |   |   |   |   |   |   concept38 > 28: win (16.0/1.0)
|   |   |   |   |   |   concept7 > 0: even (9.0)
|   |   |   |   concept2 > 0
|   |   |   |   |   concept58 <= 0: win (4.0)
|   |   |   |   |   concept58 > 0
|   |   |   |   |   |   concept38 <= 160
|   |   |   |   |   |   |   concept54 <= 42
|   |   |   |   |   |   |   |   concept67 <= 52
|   |   |   |   |   |   |   |   |   concept26 <= 23
|   |   |   |   |   |   |   |   |   |   concept37 <= 38: even (2.0)
|   |   |   |   |   |   |   |   |   |   concept37 > 38: lose (8.0)
|   |   |   |   |   |   |   |   |   concept26 > 23: even (14.0/1.0)
|   |   |   |   |   |   |   |   concept67 > 52: win (10.0)
|   |   |   |   |   |   |   concept54 > 42
|   |   |   |   |   |   |   |   concept18 <= 0
|   |   |   |   |   |   |   |   |   concept40 <= 78: lose (29.0)
|   |   |   |   |   |   |   |   |   concept40 > 78: win (3.0)
|   |   |   |   |   |   |   |   concept18 > 0
|   |   |   |   |   |   |   |   |   concept45 <= 81: even (22.0/2.0)
|   |   |   |   |   |   |   |   |   concept45 > 81
|   |   |   |   |   |   |   |   |   |   concept1 <= 0: lose (7.0)
|   |   |   |   |   |   |   |   |   |   concept1 > 0
|   |   |   |   |   |   |   |   |   |   |   concept14 <= 1: even (3.0)
|   |   |   |   |   |   |   |   |   |   |   concept14 > 1: lose (6.0)
|   |   |   |   |   |   concept38 > 160: even (7.0)
|   |   |   concept7 > 1
|   |   |   |   concept54 <= 132
|   |   |   |   |   concept23 <= 0: lose (4.0/1.0)
|   |   |   |   |   concept23 > 0
|   |   |   |   |   |   concept18 <= 0: win (10.0/1.0)
|   |   |   |   |   |   concept18 > 0
|   |   |   |   |   |   |   concept1 <= 0
|   |   |   |   |   |   |   |   concept0 <= 1
|   |   |   |   |   |   |   |   |   concept55 <= 69: even (5.0)
|   |   |   |   |   |   |   |   |   concept55 > 69: win (2.0)
|   |   |   |   |   |   |   |   concept0 > 1: win (2.0)
|   |   |   |   |   |   |   concept1 > 0: win (3.0)
|   |   |   |   concept54 > 132: lose (4.0)
|   |   concept25 > 234
|   |   |   concept10 <= 0: win (2.0)
|   |   |   concept10 > 0
|   |   |   |   concept31 <= 93: win (2.0)
|   |   |   |   concept31 > 93: lose (18.0)
|   concept6 > 1
|   |   concept3 <= 0: lose (16.0)
|   |   concept3 > 0
|   |   |   concept40 <= 99
|   |   |   |   concept27 <= 52: lose (6.0/1.0)
|   |   |   |   concept27 > 52
|   |   |   |   |   concept18 <= 0
|   |   |   |   |   |   concept26 <= 42: win (7.0)
|   |   |   |   |   |   concept26 > 42: lose (2.0)
|   |   |   |   |   concept18 > 0: win (28.0/1.0)
|   |   |   concept40 > 99: lose (6.0)
     """
     
   val connect4nmep10 =
     """
concept31 <= 48
|   concept7 <= 0
|   |   concept6 <= 0
|   |   |   concept3 <= 0
|   |   |   |   concept2 <= 0
|   |   |   |   |   concept40 <= 21
|   |   |   |   |   |   concept22 <= 22
|   |   |   |   |   |   |   concept24 <= 30
|   |   |   |   |   |   |   |   concept58 <= 29
|   |   |   |   |   |   |   |   |   concept59 <= 14
|   |   |   |   |   |   |   |   |   |   concept67 <= 12
|   |   |   |   |   |   |   |   |   |   |   concept24 <= 0: even (663.0/65.0)
|   |   |   |   |   |   |   |   |   |   |   concept24 > 0
|   |   |   |   |   |   |   |   |   |   |   |   concept39 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   concept45 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept27 <= 17: even (74.0/8.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept27 > 17: win (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept45 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept38 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept31 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept27 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept54 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept55 <= 12
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept25 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept30 <= 0: even (16.0/5.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept30 > 0: win (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept25 > 0: win (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept55 > 12: even (4.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept54 > 0: win (8.0/2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept27 > 0: even (6.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept31 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept30 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept25 <= 13: lose (3.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept25 > 13
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept26 <= 21: win (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept26 > 21: lose (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept30 > 0: win (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept38 > 0: even (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept39 > 0: even (13.0)
|   |   |   |   |   |   |   |   |   |   concept67 > 12: even (44.0)
|   |   |   |   |   |   |   |   |   concept59 > 14
|   |   |   |   |   |   |   |   |   |   concept45 <= 17
|   |   |   |   |   |   |   |   |   |   |   concept19 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   concept31 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   concept66 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept37 <= 23: even (20.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept37 > 23
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept26 <= 0: lose (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept26 > 0: even (4.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept66 > 0: lose (5.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept31 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   concept36 <= 14: lose (5.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept36 > 14: even (3.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   concept19 > 0: even (5.0)
|   |   |   |   |   |   |   |   |   |   concept45 > 17: even (76.0/4.0)
|   |   |   |   |   |   |   |   concept58 > 29
|   |   |   |   |   |   |   |   |   concept23 <= 0
|   |   |   |   |   |   |   |   |   |   concept27 <= 0: lose (3.0/1.0)
|   |   |   |   |   |   |   |   |   |   concept27 > 0: even (16.0/1.0)
|   |   |   |   |   |   |   |   |   concept23 > 0: lose (6.0/1.0)
|   |   |   |   |   |   |   concept24 > 30
|   |   |   |   |   |   |   |   concept10 <= 0
|   |   |   |   |   |   |   |   |   concept58 <= 17
|   |   |   |   |   |   |   |   |   |   concept66 <= 0
|   |   |   |   |   |   |   |   |   |   |   concept67 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   concept58 <= 0: even (4.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept58 > 0: win (3.0)
|   |   |   |   |   |   |   |   |   |   |   concept67 > 0: win (8.0)
|   |   |   |   |   |   |   |   |   |   concept66 > 0: even (4.0)
|   |   |   |   |   |   |   |   |   concept58 > 17
|   |   |   |   |   |   |   |   |   |   concept40 <= 19: even (26.0)
|   |   |   |   |   |   |   |   |   |   concept40 > 19: win (3.0/1.0)
|   |   |   |   |   |   |   |   concept10 > 0: win (2.0)
|   |   |   |   |   |   concept22 > 22
|   |   |   |   |   |   |   concept67 <= 0
|   |   |   |   |   |   |   |   concept59 <= 30
|   |   |   |   |   |   |   |   |   concept27 <= 18
|   |   |   |   |   |   |   |   |   |   concept25 <= 27: even (2.0)
|   |   |   |   |   |   |   |   |   |   concept25 > 27: lose (2.0)
|   |   |   |   |   |   |   |   |   concept27 > 18: even (9.0)
|   |   |   |   |   |   |   |   concept59 > 30: lose (5.0)
|   |   |   |   |   |   |   concept67 > 0: lose (7.0/1.0)
|   |   |   |   |   concept40 > 21
|   |   |   |   |   |   concept11 <= 0
|   |   |   |   |   |   |   concept26 <= 50
|   |   |   |   |   |   |   |   concept67 <= 44
|   |   |   |   |   |   |   |   |   concept18 <= 0
|   |   |   |   |   |   |   |   |   |   concept54 <= 23
|   |   |   |   |   |   |   |   |   |   |   concept54 <= 11: win (4.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   concept54 > 11: even (4.0)
|   |   |   |   |   |   |   |   |   |   concept54 > 23: win (6.0)
|   |   |   |   |   |   |   |   |   concept18 > 0: win (8.0)
|   |   |   |   |   |   |   |   concept67 > 44: even (6.0)
|   |   |   |   |   |   |   concept26 > 50: even (17.0/3.0)
|   |   |   |   |   |   concept11 > 0
|   |   |   |   |   |   |   concept1 <= 0: win (2.0)
|   |   |   |   |   |   |   concept1 > 0: lose (3.0)
|   |   |   |   concept2 > 0
|   |   |   |   |   concept23 <= 0
|   |   |   |   |   |   concept31 <= 29: win (19.0/2.0)
|   |   |   |   |   |   concept31 > 29: even (5.0)
|   |   |   |   |   concept23 > 0
|   |   |   |   |   |   concept14 <= 0
|   |   |   |   |   |   |   concept63 <= 17: lose (10.0)
|   |   |   |   |   |   |   concept63 > 17
|   |   |   |   |   |   |   |   concept55 <= 22: even (9.0/1.0)
|   |   |   |   |   |   |   |   concept55 > 22: lose (4.0)
|   |   |   |   |   |   concept14 > 0: win (2.0/1.0)
|   |   |   concept3 > 0
|   |   |   |   concept3 <= 1
|   |   |   |   |   concept62 <= 26
|   |   |   |   |   |   concept67 <= 40
|   |   |   |   |   |   |   concept63 <= 21
|   |   |   |   |   |   |   |   concept45 <= 54
|   |   |   |   |   |   |   |   |   concept40 <= 0
|   |   |   |   |   |   |   |   |   |   concept18 <= 0
|   |   |   |   |   |   |   |   |   |   |   concept27 <= 20
|   |   |   |   |   |   |   |   |   |   |   |   concept37 <= 40
|   |   |   |   |   |   |   |   |   |   |   |   |   concept62 <= 18
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept62 <= 16
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept41 <= 18
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept41 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept23 <= 14
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept23 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept55 <= 16
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept26 <= 23
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept24 <= 15
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept38 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept30 <= 13: even (28.0/11.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept30 > 13: lose (3.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept38 > 0: win (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept24 > 15: even (10.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept26 > 23: win (3.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept55 > 16
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept24 <= 0: win (5.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept24 > 0: lose (5.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept23 > 0: win (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept23 > 14: lose (5.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept41 > 0: win (3.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept41 > 18: even (5.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept62 > 16: even (5.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept62 > 18: lose (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept37 > 40: lose (7.0)
|   |   |   |   |   |   |   |   |   |   |   concept27 > 20: even (15.0/1.0)
|   |   |   |   |   |   |   |   |   |   concept18 > 0
|   |   |   |   |   |   |   |   |   |   |   concept63 <= 0: win (5.0)
|   |   |   |   |   |   |   |   |   |   |   concept63 > 0: lose (3.0)
|   |   |   |   |   |   |   |   |   concept40 > 0
|   |   |   |   |   |   |   |   |   |   concept23 <= 0
|   |   |   |   |   |   |   |   |   |   |   concept66 <= 0: even (4.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   concept66 > 0: lose (13.0/1.0)
|   |   |   |   |   |   |   |   |   |   concept23 > 0: even (8.0)
|   |   |   |   |   |   |   |   concept45 > 54: lose (5.0)
|   |   |   |   |   |   |   concept63 > 21: even (19.0/1.0)
|   |   |   |   |   |   concept67 > 40: lose (7.0)
|   |   |   |   |   concept62 > 26
|   |   |   |   |   |   concept66 <= 19: win (2.0)
|   |   |   |   |   |   concept66 > 19: lose (17.0)
|   |   |   |   concept3 > 1: win (7.0)
|   |   concept6 > 0
|   |   |   concept6 <= 1
|   |   |   |   concept67 <= 26
|   |   |   |   |   concept15 <= 0
|   |   |   |   |   |   concept19 <= 0
|   |   |   |   |   |   |   concept22 <= 0
|   |   |   |   |   |   |   |   concept37 <= 13: lose (7.0)
|   |   |   |   |   |   |   |   concept37 > 13
|   |   |   |   |   |   |   |   |   concept40 <= 0
|   |   |   |   |   |   |   |   |   |   concept62 <= 25
|   |   |   |   |   |   |   |   |   |   |   concept39 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   concept23 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   concept58 <= 24
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept25 <= 0: lose (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept25 > 0: even (24.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept58 > 24: lose (3.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept23 > 0: lose (3.0)
|   |   |   |   |   |   |   |   |   |   |   concept39 > 0: even (12.0)
|   |   |   |   |   |   |   |   |   |   concept62 > 25: lose (3.0)
|   |   |   |   |   |   |   |   |   concept40 > 0
|   |   |   |   |   |   |   |   |   |   concept44 <= 0: lose (3.0)
|   |   |   |   |   |   |   |   |   |   concept44 > 0
|   |   |   |   |   |   |   |   |   |   |   concept24 <= 32: win (2.0)
|   |   |   |   |   |   |   |   |   |   |   concept24 > 32: even (5.0/1.0)
|   |   |   |   |   |   |   concept22 > 0
|   |   |   |   |   |   |   |   concept11 <= 0
|   |   |   |   |   |   |   |   |   concept41 <= 0
|   |   |   |   |   |   |   |   |   |   concept38 <= 29: win (5.0/1.0)
|   |   |   |   |   |   |   |   |   |   concept38 > 29: lose (13.0)
|   |   |   |   |   |   |   |   |   concept41 > 0: win (3.0/1.0)
|   |   |   |   |   |   |   |   concept11 > 0
|   |   |   |   |   |   |   |   |   concept22 <= 44: even (3.0)
|   |   |   |   |   |   |   |   |   concept22 > 44: lose (2.0)
|   |   |   |   |   |   concept19 > 0: even (5.0)
|   |   |   |   |   concept15 > 0: lose (4.0/1.0)
|   |   |   |   concept67 > 26: lose (11.0)
|   |   |   concept6 > 1: lose (11.0)
|   concept7 > 0
|   |   concept2 <= 1
|   |   |   concept7 <= 1
|   |   |   |   concept31 <= 33
|   |   |   |   |   concept37 <= 0
|   |   |   |   |   |   concept55 <= 30
|   |   |   |   |   |   |   concept26 <= 0
|   |   |   |   |   |   |   |   concept24 <= 0
|   |   |   |   |   |   |   |   |   concept59 <= 14
|   |   |   |   |   |   |   |   |   |   concept36 <= 0: lose (6.0/2.0)
|   |   |   |   |   |   |   |   |   |   concept36 > 0: even (7.0)
|   |   |   |   |   |   |   |   |   concept59 > 14: lose (2.0)
|   |   |   |   |   |   |   |   concept24 > 0: even (12.0/1.0)
|   |   |   |   |   |   |   concept26 > 0: lose (5.0/1.0)
|   |   |   |   |   |   concept55 > 30: win (4.0)
|   |   |   |   |   concept37 > 0
|   |   |   |   |   |   concept45 <= 102
|   |   |   |   |   |   |   concept10 <= 0
|   |   |   |   |   |   |   |   concept37 <= 16: win (12.0)
|   |   |   |   |   |   |   |   concept37 > 16
|   |   |   |   |   |   |   |   |   concept24 <= 93
|   |   |   |   |   |   |   |   |   |   concept30 <= 60
|   |   |   |   |   |   |   |   |   |   |   concept36 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   concept40 <= 22: win (8.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept40 > 22: even (3.0)
|   |   |   |   |   |   |   |   |   |   |   concept36 > 0
|   |   |   |   |   |   |   |   |   |   |   |   concept66 <= 20
|   |   |   |   |   |   |   |   |   |   |   |   |   concept55 <= 23
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept62 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept27 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept2 <= 0: lose (8.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept2 > 0: win (3.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept27 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept58 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept6 <= 0: even (5.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept6 > 0: win (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept58 > 0: win (14.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept62 > 0: lose (12.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept55 > 23: win (13.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept66 > 20
|   |   |   |   |   |   |   |   |   |   |   |   |   concept26 <= 52: lose (28.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept26 > 52: win (3.0)
|   |   |   |   |   |   |   |   |   |   concept30 > 60: even (4.0)
|   |   |   |   |   |   |   |   |   concept24 > 93
|   |   |   |   |   |   |   |   |   |   concept15 <= 0: win (18.0/1.0)
|   |   |   |   |   |   |   |   |   |   concept15 > 0: lose (3.0/1.0)
|   |   |   |   |   |   |   concept10 > 0
|   |   |   |   |   |   |   |   concept25 <= 112: win (19.0)
|   |   |   |   |   |   |   |   concept25 > 112
|   |   |   |   |   |   |   |   |   concept2 <= 0: win (4.0/1.0)
|   |   |   |   |   |   |   |   |   concept2 > 0: lose (2.0)
|   |   |   |   |   |   concept45 > 102: lose (7.0)
|   |   |   |   concept31 > 33
|   |   |   |   |   concept22 <= 23
|   |   |   |   |   |   concept40 <= 34: even (32.0/5.0)
|   |   |   |   |   |   concept40 > 34: win (2.0)
|   |   |   |   |   concept22 > 23: lose (7.0/1.0)
|   |   |   concept7 > 1: win (17.0)
|   |   concept2 > 1: lose (9.0)
concept31 > 48
|   concept54 <= 44
|   |   concept18 <= 1
|   |   |   concept23 <= 58
|   |   |   |   concept44 <= 75
|   |   |   |   |   concept11 <= 1
|   |   |   |   |   |   concept30 <= 0
|   |   |   |   |   |   |   concept3 <= 0
|   |   |   |   |   |   |   |   concept25 <= 108: even (8.0)
|   |   |   |   |   |   |   |   concept25 > 108: win (2.0)
|   |   |   |   |   |   |   concept3 > 0: lose (2.0)
|   |   |   |   |   |   concept30 > 0
|   |   |   |   |   |   |   concept1 <= 1
|   |   |   |   |   |   |   |   concept31 <= 52
|   |   |   |   |   |   |   |   |   concept22 <= 12: lose (6.0)
|   |   |   |   |   |   |   |   |   concept22 > 12: win (8.0/1.0)
|   |   |   |   |   |   |   |   concept31 > 52
|   |   |   |   |   |   |   |   |   concept0 <= 0
|   |   |   |   |   |   |   |   |   |   concept41 <= 0: lose (2.0/1.0)
|   |   |   |   |   |   |   |   |   |   concept41 > 0: win (50.0/2.0)
|   |   |   |   |   |   |   |   |   concept0 > 0
|   |   |   |   |   |   |   |   |   |   concept7 <= 0: even (7.0)
|   |   |   |   |   |   |   |   |   |   concept7 > 0: win (3.0)
|   |   |   |   |   |   |   concept1 > 1: lose (3.0)
|   |   |   |   |   concept11 > 1: lose (3.0)
|   |   |   |   concept44 > 75: lose (9.0/1.0)
|   |   |   concept23 > 58: win (35.0)
|   |   concept18 > 1
|   |   |   concept7 <= 1
|   |   |   |   concept62 <= 12: even (6.0/1.0)
|   |   |   |   concept62 > 12
|   |   |   |   |   concept66 <= 99: lose (13.0)
|   |   |   |   |   concept66 > 99: win (4.0/1.0)
|   |   |   concept7 > 1: win (5.0)
|   concept54 > 44
|   |   concept67 <= 160
|   |   |   concept1 <= 0
|   |   |   |   concept37 <= 40: win (7.0)
|   |   |   |   concept37 > 40
|   |   |   |   |   concept55 <= 76
|   |   |   |   |   |   concept45 <= 26
|   |   |   |   |   |   |   concept11 <= 0: even (2.0)
|   |   |   |   |   |   |   concept11 > 0: lose (2.0)
|   |   |   |   |   |   concept45 > 26: lose (100.0/2.0)
|   |   |   |   |   concept55 > 76
|   |   |   |   |   |   concept45 <= 102: win (16.0)
|   |   |   |   |   |   concept45 > 102: lose (23.0)
|   |   |   concept1 > 0
|   |   |   |   concept25 <= 54
|   |   |   |   |   concept26 <= 66: even (4.0)
|   |   |   |   |   concept26 > 66: win (2.0)
|   |   |   |   concept25 > 54
|   |   |   |   |   concept2 <= 1
|   |   |   |   |   |   concept2 <= 0
|   |   |   |   |   |   |   concept10 <= 0
|   |   |   |   |   |   |   |   concept59 <= 100: win (8.0/1.0)
|   |   |   |   |   |   |   |   concept59 > 100: lose (23.0)
|   |   |   |   |   |   |   concept10 > 0
|   |   |   |   |   |   |   |   concept41 <= 31: lose (10.0/1.0)
|   |   |   |   |   |   |   |   concept41 > 31
|   |   |   |   |   |   |   |   |   concept6 <= 2: win (28.0)
|   |   |   |   |   |   |   |   |   concept6 > 2: lose (3.0)
|   |   |   |   |   |   concept2 > 0: win (16.0)
|   |   |   |   |   concept2 > 1: lose (6.0)
|   |   concept67 > 160: win (10.0)
     """
   
  val connect4tf =
    """
concept25 <= 0: win (6.0)
concept25 > 0
|   concept2 <= 0
|   |   concept23 <= 0
|   |   |   concept1 <= 0
|   |   |   |   concept58 <= 0: lose (4.0)
|   |   |   |   concept58 > 0
|   |   |   |   |   concept44 <= 0: lose (2.0)
|   |   |   |   |   concept44 > 0: win (5.0)
|   |   |   concept1 > 0: lose (5.0/1.0)
|   |   concept23 > 0
|   |   |   concept6 <= 0: win (11.0)
|   |   |   concept6 > 0
|   |   |   |   concept41 <= 0: win (2.0)
|   |   |   |   concept41 > 0
|   |   |   |   |   concept22 <= 0: lose (7.0/2.0)
|   |   |   |   |   concept22 > 0
|   |   |   |   |   |   concept19 <= 0
|   |   |   |   |   |   |   concept1 <= 0: lose (3.0)
|   |   |   |   |   |   |   concept1 > 0: win (2.0)
|   |   |   |   |   |   concept19 > 0: win (7.0)
|   concept2 > 0
|   |   concept6 <= 0
|   |   |   concept67 <= 0: lose (6.0)
|   |   |   concept67 > 0
|   |   |   |   concept59 <= 0: lose (3.0/1.0)
|   |   |   |   concept59 > 0: win (10.0/3.0)
|   |   concept6 > 0: lose (27.0/6.0)
    """
    
  val connect4tfep1 = 
    """
even (2216.0/100.0)
    """
  
  val connect4tfep3 =
    """
concept7 <= 0
|   concept6 <= 0
|   |   concept2 <= 0: even (1260.0/30.0)
|   |   concept2 > 0
|   |   |   concept11 <= 0
|   |   |   |   concept15 <= 0
|   |   |   |   |   concept14 <= 0
|   |   |   |   |   |   concept26 <= 0
|   |   |   |   |   |   |   concept24 <= 0
|   |   |   |   |   |   |   |   concept40 <= 0: even (38.0/3.0)
|   |   |   |   |   |   |   |   concept40 > 0
|   |   |   |   |   |   |   |   |   concept23 <= 0: even (5.0/1.0)
|   |   |   |   |   |   |   |   |   concept23 > 0: lose (2.0)
|   |   |   |   |   |   |   concept24 > 0
|   |   |   |   |   |   |   |   concept39 <= 0: even (11.0)
|   |   |   |   |   |   |   |   concept39 > 0
|   |   |   |   |   |   |   |   |   concept10 <= 0: win (5.0)
|   |   |   |   |   |   |   |   |   concept10 > 0: even (2.0)
|   |   |   |   |   |   concept26 > 0
|   |   |   |   |   |   |   concept45 <= 0
|   |   |   |   |   |   |   |   concept22 <= 0: lose (7.0/1.0)
|   |   |   |   |   |   |   |   concept22 > 0: even (4.0)
|   |   |   |   |   |   |   concept45 > 0: even (60.0/11.0)
|   |   |   |   |   concept14 > 0
|   |   |   |   |   |   concept3 <= 0: even (6.0)
|   |   |   |   |   |   concept3 > 0: win (2.0)
|   |   |   |   concept15 > 0
|   |   |   |   |   concept3 <= 0
|   |   |   |   |   |   concept63 <= 0: win (2.0/1.0)
|   |   |   |   |   |   concept63 > 0: even (4.0)
|   |   |   |   |   concept3 > 0: win (4.0)
|   |   |   concept11 > 0: even (57.0/1.0)
|   concept6 > 0
|   |   concept10 <= 0
|   |   |   concept15 <= 0
|   |   |   |   concept11 <= 0
|   |   |   |   |   concept2 <= 0: even (120.0/13.0)
|   |   |   |   |   concept2 > 0
|   |   |   |   |   |   concept23 <= 0: lose (3.0)
|   |   |   |   |   |   concept23 > 0
|   |   |   |   |   |   |   concept0 <= 0: win (2.0)
|   |   |   |   |   |   |   concept0 > 0
|   |   |   |   |   |   |   |   concept22 <= 0
|   |   |   |   |   |   |   |   |   concept66 <= 0: even (2.0)
|   |   |   |   |   |   |   |   |   concept66 > 0: lose (3.0)
|   |   |   |   |   |   |   |   concept22 > 0: even (15.0/3.0)
|   |   |   |   concept11 > 0
|   |   |   |   |   concept3 <= 0: even (3.0)
|   |   |   |   |   concept3 > 0: lose (3.0)
|   |   |   concept15 > 0
|   |   |   |   concept40 <= 0
|   |   |   |   |   concept19 <= 0
|   |   |   |   |   |   concept22 <= 0: lose (6.0)
|   |   |   |   |   |   concept22 > 0
|   |   |   |   |   |   |   concept58 <= 0: win (4.0/1.0)
|   |   |   |   |   |   |   concept58 > 0: lose (6.0/1.0)
|   |   |   |   |   concept19 > 0: even (2.0)
|   |   |   |   concept40 > 0: even (9.0)
|   |   concept10 > 0
|   |   |   concept26 <= 0: lose (6.0)
|   |   |   concept26 > 0
|   |   |   |   concept0 <= 0: even (22.0/6.0)
|   |   |   |   concept0 > 0
|   |   |   |   |   concept67 <= 0: win (3.0/1.0)
|   |   |   |   |   concept67 > 0: lose (9.0/1.0)
concept7 > 0
|   concept27 <= 0
|   |   concept26 <= 0: even (38.0/4.0)
|   |   concept26 > 0
|   |   |   concept55 <= 0: even (5.0)
|   |   |   concept55 > 0
|   |   |   |   concept31 <= 0: even (2.0)
|   |   |   |   concept31 > 0: lose (17.0/2.0)
|   concept27 > 0
|   |   concept45 <= 0
|   |   |   concept38 <= 0
|   |   |   |   concept30 <= 0: even (3.0)
|   |   |   |   concept30 > 0: lose (5.0)
|   |   |   concept38 > 0: even (20.0)
|   |   concept45 > 0
|   |   |   concept6 <= 0
|   |   |   |   concept55 <= 0: even (15.0)
|   |   |   |   concept55 > 0
|   |   |   |   |   concept66 <= 0
|   |   |   |   |   |   concept58 <= 0
|   |   |   |   |   |   |   concept11 <= 0: even (9.0/1.0)
|   |   |   |   |   |   |   concept11 > 0: win (3.0)
|   |   |   |   |   |   concept58 > 0: even (29.0/1.0)
|   |   |   |   |   concept66 > 0
|   |   |   |   |   |   concept2 <= 0
|   |   |   |   |   |   |   concept18 <= 0
|   |   |   |   |   |   |   |   concept40 <= 0: win (5.0/1.0)
|   |   |   |   |   |   |   |   concept40 > 0
|   |   |   |   |   |   |   |   |   concept19 <= 0: even (33.0/2.0)
|   |   |   |   |   |   |   |   |   concept19 > 0
|   |   |   |   |   |   |   |   |   |   concept22 <= 0: win (5.0)
|   |   |   |   |   |   |   |   |   |   concept22 > 0
|   |   |   |   |   |   |   |   |   |   |   concept41 <= 0: win (2.0)
|   |   |   |   |   |   |   |   |   |   |   concept41 > 0
|   |   |   |   |   |   |   |   |   |   |   |   concept15 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   concept62 <= 0: even (3.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept62 > 0: win (4.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept15 > 0: even (13.0/3.0)
|   |   |   |   |   |   |   concept18 > 0
|   |   |   |   |   |   |   |   concept41 <= 0: even (3.0)
|   |   |   |   |   |   |   |   concept41 > 0: win (9.0/1.0)
|   |   |   |   |   |   concept2 > 0
|   |   |   |   |   |   |   concept31 <= 0: lose (2.0)
|   |   |   |   |   |   |   concept31 > 0
|   |   |   |   |   |   |   |   concept15 <= 0
|   |   |   |   |   |   |   |   |   concept63 <= 0: even (8.0)
|   |   |   |   |   |   |   |   |   concept63 > 0
|   |   |   |   |   |   |   |   |   |   concept22 <= 0: lose (4.0/1.0)
|   |   |   |   |   |   |   |   |   |   concept22 > 0
|   |   |   |   |   |   |   |   |   |   |   concept11 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   concept10 <= 0: even (7.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept10 > 0: win (2.0)
|   |   |   |   |   |   |   |   |   |   |   concept11 > 0
|   |   |   |   |   |   |   |   |   |   |   |   concept3 <= 0: even (11.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept3 > 0: lose (6.0/1.0)
|   |   |   |   |   |   |   |   concept15 > 0
|   |   |   |   |   |   |   |   |   concept67 <= 0: even (5.0)
|   |   |   |   |   |   |   |   |   concept67 > 0: win (18.0/4.0)
|   |   |   concept6 > 0
|   |   |   |   concept58 <= 0
|   |   |   |   |   concept1 <= 0
|   |   |   |   |   |   concept55 <= 0: even (2.0)
|   |   |   |   |   |   concept55 > 0: win (5.0)
|   |   |   |   |   concept1 > 0: even (2.0)
|   |   |   |   concept58 > 0
|   |   |   |   |   concept30 <= 0
|   |   |   |   |   |   concept67 <= 0: even (8.0)
|   |   |   |   |   |   concept67 > 0: lose (5.0/2.0)
|   |   |   |   |   concept30 > 0
|   |   |   |   |   |   concept31 <= 0: even (4.0)
|   |   |   |   |   |   concept31 > 0
|   |   |   |   |   |   |   concept36 <= 0: even (3.0)
|   |   |   |   |   |   |   concept36 > 0
|   |   |   |   |   |   |   |   concept54 <= 0: lose (3.0)
|   |   |   |   |   |   |   |   concept54 > 0
|   |   |   |   |   |   |   |   |   concept59 <= 0
|   |   |   |   |   |   |   |   |   |   concept0 <= 0: win (2.0)
|   |   |   |   |   |   |   |   |   |   concept0 > 0: lose (3.0)
|   |   |   |   |   |   |   |   |   concept59 > 0
|   |   |   |   |   |   |   |   |   |   concept67 <= 0: win (14.0/3.0)
|   |   |   |   |   |   |   |   |   |   concept67 > 0
|   |   |   |   |   |   |   |   |   |   |   concept63 <= 0: lose (3.0)
|   |   |   |   |   |   |   |   |   |   |   concept63 > 0
|   |   |   |   |   |   |   |   |   |   |   |   concept22 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   concept1 <= 0: even (4.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept1 > 0: lose (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept22 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   concept23 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept40 <= 0: even (4.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept40 > 0: win (4.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept23 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept1 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept15 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept40 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept11 <= 0: lose (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept11 > 0: win (3.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept40 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept14 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept0 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept11 <= 0: even (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept11 > 0: lose (4.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept0 > 0: lose (6.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept14 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept0 <= 0: win (2.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept0 > 0: even (5.0/2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept15 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept19 <= 0: even (14.0/4.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept19 > 0: win (4.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept1 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept10 <= 0: win (9.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept10 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept0 <= 0: lose (7.0/2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept0 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept3 <= 0: lose (3.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept3 > 0: win (4.0)
    """
  
  val connect4tfep5 =
    """
concept63 <= 0
|   concept1 <= 0
|   |   concept2 <= 0
|   |   |   concept3 <= 0
|   |   |   |   concept6 <= 0: even (1055.0/39.0)
|   |   |   |   concept6 > 0
|   |   |   |   |   concept11 <= 0
|   |   |   |   |   |   concept7 <= 0
|   |   |   |   |   |   |   concept27 <= 0
|   |   |   |   |   |   |   |   concept66 <= 0: even (23.0/1.0)
|   |   |   |   |   |   |   |   concept66 > 0
|   |   |   |   |   |   |   |   |   concept31 <= 0: even (2.0)
|   |   |   |   |   |   |   |   |   concept31 > 0: win (2.0)
|   |   |   |   |   |   |   concept27 > 0
|   |   |   |   |   |   |   |   concept54 <= 0
|   |   |   |   |   |   |   |   |   concept26 <= 0: even (7.0)
|   |   |   |   |   |   |   |   |   concept26 > 0: lose (6.0/1.0)
|   |   |   |   |   |   |   |   concept54 > 0: even (27.0)
|   |   |   |   |   |   concept7 > 0
|   |   |   |   |   |   |   concept44 <= 0: even (17.0/2.0)
|   |   |   |   |   |   |   concept44 > 0
|   |   |   |   |   |   |   |   concept26 <= 0: win (13.0/2.0)
|   |   |   |   |   |   |   |   concept26 > 0: even (3.0)
|   |   |   |   |   concept11 > 0
|   |   |   |   |   |   concept19 <= 0: lose (2.0)
|   |   |   |   |   |   concept19 > 0: even (2.0)
|   |   |   concept3 > 0
|   |   |   |   concept30 <= 0
|   |   |   |   |   concept14 <= 0
|   |   |   |   |   |   concept31 <= 0
|   |   |   |   |   |   |   concept59 <= 0
|   |   |   |   |   |   |   |   concept66 <= 0: win (32.0/12.0)
|   |   |   |   |   |   |   |   concept66 > 0: even (4.0)
|   |   |   |   |   |   |   concept59 > 0: even (5.0)
|   |   |   |   |   |   concept31 > 0: even (21.0/1.0)
|   |   |   |   |   concept14 > 0: win (3.0)
|   |   |   |   concept30 > 0
|   |   |   |   |   concept54 <= 0
|   |   |   |   |   |   concept7 <= 0: even (4.0)
|   |   |   |   |   |   concept7 > 0: lose (5.0)
|   |   |   |   |   concept54 > 0: even (30.0/2.0)
|   |   concept2 > 0
|   |   |   concept19 <= 0
|   |   |   |   concept40 <= 0
|   |   |   |   |   concept55 <= 0
|   |   |   |   |   |   concept23 <= 0
|   |   |   |   |   |   |   concept59 <= 0
|   |   |   |   |   |   |   |   concept26 <= 0
|   |   |   |   |   |   |   |   |   concept27 <= 0
|   |   |   |   |   |   |   |   |   |   concept37 <= 0: lose (3.0/1.0)
|   |   |   |   |   |   |   |   |   |   concept37 > 0: even (2.0)
|   |   |   |   |   |   |   |   |   concept27 > 0: lose (4.0/1.0)
|   |   |   |   |   |   |   |   concept26 > 0: even (11.0/1.0)
|   |   |   |   |   |   |   concept59 > 0: lose (8.0/1.0)
|   |   |   |   |   |   concept23 > 0: even (8.0)
|   |   |   |   |   concept55 > 0: even (69.0/10.0)
|   |   |   |   concept40 > 0
|   |   |   |   |   concept24 <= 0: win (4.0)
|   |   |   |   |   concept24 > 0
|   |   |   |   |   |   concept31 <= 0: even (3.0)
|   |   |   |   |   |   concept31 > 0
|   |   |   |   |   |   |   concept66 <= 0: even (2.0)
|   |   |   |   |   |   |   concept66 > 0
|   |   |   |   |   |   |   |   concept14 <= 0
|   |   |   |   |   |   |   |   |   concept6 <= 0: even (13.0/1.0)
|   |   |   |   |   |   |   |   |   concept6 > 0: lose (4.0/1.0)
|   |   |   |   |   |   |   |   concept14 > 0: lose (23.0/2.0)
|   |   |   concept19 > 0: even (32.0)
|   concept1 > 0
|   |   concept36 <= 0: lose (6.0/1.0)
|   |   concept36 > 0
|   |   |   concept25 <= 0: lose (4.0/1.0)
|   |   |   concept25 > 0
|   |   |   |   concept19 <= 0
|   |   |   |   |   concept40 <= 0: even (5.0/1.0)
|   |   |   |   |   concept40 > 0: win (12.0)
|   |   |   |   concept19 > 0
|   |   |   |   |   concept62 <= 0: lose (4.0/1.0)
|   |   |   |   |   concept62 > 0
|   |   |   |   |   |   concept40 <= 0
|   |   |   |   |   |   |   concept18 <= 0: even (3.0/1.0)
|   |   |   |   |   |   |   concept18 > 0: win (4.0)
|   |   |   |   |   |   concept40 > 0: even (16.0)
concept63 > 0
|   concept45 <= 0: even (28.0)
|   concept45 > 0
|   |   concept26 <= 0
|   |   |   concept1 <= 0
|   |   |   |   concept2 <= 0: even (46.0/1.0)
|   |   |   |   concept2 > 0
|   |   |   |   |   concept22 <= 0: win (4.0)
|   |   |   |   |   concept22 > 0: even (12.0/2.0)
|   |   |   concept1 > 0: win (7.0/2.0)
|   |   concept26 > 0
|   |   |   concept38 <= 0
|   |   |   |   concept44 <= 0
|   |   |   |   |   concept0 <= 0
|   |   |   |   |   |   concept3 <= 0: win (3.0)
|   |   |   |   |   |   concept3 > 0: even (4.0)
|   |   |   |   |   concept0 > 0: lose (5.0)
|   |   |   |   concept44 > 0
|   |   |   |   |   concept7 <= 0: even (39.0)
|   |   |   |   |   concept7 > 0
|   |   |   |   |   |   concept11 <= 0
|   |   |   |   |   |   |   concept59 <= 0: win (4.0/1.0)
|   |   |   |   |   |   |   concept59 > 0: even (17.0/1.0)
|   |   |   |   |   |   concept11 > 0
|   |   |   |   |   |   |   concept1 <= 0: win (5.0)
|   |   |   |   |   |   |   concept1 > 0: even (3.0/1.0)
|   |   |   concept38 > 0
|   |   |   |   concept55 <= 0
|   |   |   |   |   concept62 <= 0: even (8.0)
|   |   |   |   |   concept62 > 0
|   |   |   |   |   |   concept67 <= 0
|   |   |   |   |   |   |   concept58 <= 0: lose (3.0)
|   |   |   |   |   |   |   concept58 > 0: even (10.0)
|   |   |   |   |   |   concept67 > 0: lose (15.0/1.0)
|   |   |   |   concept55 > 0
|   |   |   |   |   concept31 <= 0
|   |   |   |   |   |   concept2 <= 0: even (15.0)
|   |   |   |   |   |   concept2 > 0
|   |   |   |   |   |   |   concept66 <= 0: even (3.0/1.0)
|   |   |   |   |   |   |   concept66 > 0: win (10.0/2.0)
|   |   |   |   |   concept31 > 0
|   |   |   |   |   |   concept44 <= 0
|   |   |   |   |   |   |   concept62 <= 0: even (21.0/5.0)
|   |   |   |   |   |   |   concept62 > 0: win (3.0)
|   |   |   |   |   |   concept44 > 0
|   |   |   |   |   |   |   concept24 <= 0
|   |   |   |   |   |   |   |   concept10 <= 0: even (6.0/2.0)
|   |   |   |   |   |   |   |   concept10 > 0: win (4.0)
|   |   |   |   |   |   |   concept24 > 0
|   |   |   |   |   |   |   |   concept14 <= 0
|   |   |   |   |   |   |   |   |   concept11 <= 0
|   |   |   |   |   |   |   |   |   |   concept22 <= 0
|   |   |   |   |   |   |   |   |   |   |   concept15 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   concept27 <= 0: lose (3.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept27 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   concept36 <= 0: lose (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept36 > 0: even (37.0/10.0)
|   |   |   |   |   |   |   |   |   |   |   concept15 > 0: lose (6.0)
|   |   |   |   |   |   |   |   |   |   concept22 > 0
|   |   |   |   |   |   |   |   |   |   |   concept58 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   concept7 <= 0: even (6.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept7 > 0: win (11.0/2.0)
|   |   |   |   |   |   |   |   |   |   |   concept58 > 0
|   |   |   |   |   |   |   |   |   |   |   |   concept30 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   concept2 <= 0: even (7.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept2 > 0: win (4.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept30 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   concept67 <= 0: even (15.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept67 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept40 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept15 <= 0: win (10.0/2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept15 > 0: even (3.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept40 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept6 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept0 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept3 <= 0: lose (8.0/2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept3 > 0: even (23.0/4.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept0 > 0: lose (13.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept6 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept15 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept18 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept10 <= 0: win (17.0/3.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept10 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept1 <= 0: lose (4.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept1 > 0: even (5.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept18 > 0: lose (3.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept15 > 0: lose (14.0/3.0)
|   |   |   |   |   |   |   |   |   concept11 > 0
|   |   |   |   |   |   |   |   |   |   concept30 <= 0
|   |   |   |   |   |   |   |   |   |   |   concept6 <= 0: even (12.0/2.0)
|   |   |   |   |   |   |   |   |   |   |   concept6 > 0: lose (3.0)
|   |   |   |   |   |   |   |   |   |   concept30 > 0
|   |   |   |   |   |   |   |   |   |   |   concept62 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   concept15 <= 0: win (5.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept15 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   concept6 <= 0: win (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept6 > 0: lose (4.0)
|   |   |   |   |   |   |   |   |   |   |   concept62 > 0: win (42.0/7.0)
|   |   |   |   |   |   |   |   concept14 > 0
|   |   |   |   |   |   |   |   |   concept18 <= 0
|   |   |   |   |   |   |   |   |   |   concept0 <= 0
|   |   |   |   |   |   |   |   |   |   |   concept6 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   concept3 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   concept7 <= 0: lose (6.0/3.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept7 > 0: win (6.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept3 > 0: even (6.0/2.0)
|   |   |   |   |   |   |   |   |   |   |   concept6 > 0
|   |   |   |   |   |   |   |   |   |   |   |   concept2 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   concept1 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept11 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept62 <= 0: win (3.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept62 > 0: lose (3.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept11 > 0: win (4.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept1 > 0: win (6.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept2 > 0: lose (8.0)
|   |   |   |   |   |   |   |   |   |   concept0 > 0
|   |   |   |   |   |   |   |   |   |   |   concept67 <= 0: even (7.0)
|   |   |   |   |   |   |   |   |   |   |   concept67 > 0
|   |   |   |   |   |   |   |   |   |   |   |   concept6 <= 0: even (5.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept6 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   concept7 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept41 <= 0: even (3.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept41 > 0: lose (9.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept7 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept2 <= 0: win (3.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept2 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept3 <= 0: lose (7.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept3 > 0: win (2.0)
|   |   |   |   |   |   |   |   |   concept18 > 0
|   |   |   |   |   |   |   |   |   |   concept62 <= 0: even (3.0)
|   |   |   |   |   |   |   |   |   |   concept62 > 0
|   |   |   |   |   |   |   |   |   |   |   concept1 <= 0: lose (36.0/6.0)
|   |   |   |   |   |   |   |   |   |   |   concept1 > 0
|   |   |   |   |   |   |   |   |   |   |   |   concept3 <= 0: lose (11.0/2.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept3 > 0: win (3.0)
    """
  
  val connect4tfep10 =
    """
concept25 <= 0
|   concept22 <= 0
|   |   concept23 <= 0
|   |   |   concept58 <= 0: even (577.0/68.0)
|   |   |   concept58 > 0
|   |   |   |   concept44 <= 0: even (47.0/7.0)
|   |   |   |   concept44 > 0
|   |   |   |   |   concept39 <= 0
|   |   |   |   |   |   concept31 <= 0: win (13.0/1.0)
|   |   |   |   |   |   concept31 > 0: even (3.0)
|   |   |   |   |   concept39 > 0: even (11.0/1.0)
|   |   concept23 > 0
|   |   |   concept40 <= 0
|   |   |   |   concept6 <= 0
|   |   |   |   |   concept39 <= 0
|   |   |   |   |   |   concept38 <= 0: even (4.0/1.0)
|   |   |   |   |   |   concept38 > 0: win (3.0)
|   |   |   |   |   concept39 > 0
|   |   |   |   |   |   concept36 <= 0
|   |   |   |   |   |   |   concept38 <= 0
|   |   |   |   |   |   |   |   concept31 <= 0: lose (3.0)
|   |   |   |   |   |   |   |   concept31 > 0: even (2.0)
|   |   |   |   |   |   |   concept38 > 0: even (4.0)
|   |   |   |   |   |   concept36 > 0: even (22.0)
|   |   |   |   concept6 > 0: lose (2.0)
|   |   |   concept40 > 0: lose (6.0)
|   concept22 > 0
|   |   concept14 <= 0
|   |   |   concept45 <= 0: even (10.0/1.0)
|   |   |   concept45 > 0
|   |   |   |   concept39 <= 0: win (7.0)
|   |   |   |   concept39 > 0
|   |   |   |   |   concept15 <= 0: even (2.0)
|   |   |   |   |   concept15 > 0: win (4.0)
|   |   concept14 > 0: lose (3.0/1.0)
concept25 > 0
|   concept58 <= 0
|   |   concept6 <= 0
|   |   |   concept54 <= 0
|   |   |   |   concept2 <= 0
|   |   |   |   |   concept62 <= 0
|   |   |   |   |   |   concept63 <= 0
|   |   |   |   |   |   |   concept40 <= 0
|   |   |   |   |   |   |   |   concept36 <= 0: even (94.0/17.0)
|   |   |   |   |   |   |   |   concept36 > 0
|   |   |   |   |   |   |   |   |   concept67 <= 0
|   |   |   |   |   |   |   |   |   |   concept59 <= 0: even (40.0/9.0)
|   |   |   |   |   |   |   |   |   |   concept59 > 0
|   |   |   |   |   |   |   |   |   |   |   concept44 <= 0: win (8.0/4.0)
|   |   |   |   |   |   |   |   |   |   |   concept44 > 0: lose (6.0)
|   |   |   |   |   |   |   |   |   concept67 > 0: even (4.0)
|   |   |   |   |   |   |   concept40 > 0
|   |   |   |   |   |   |   |   concept44 <= 0
|   |   |   |   |   |   |   |   |   concept3 <= 0
|   |   |   |   |   |   |   |   |   |   concept24 <= 0: even (2.0)
|   |   |   |   |   |   |   |   |   |   concept24 > 0: win (2.0)
|   |   |   |   |   |   |   |   |   concept3 > 0: even (3.0)
|   |   |   |   |   |   |   |   concept44 > 0
|   |   |   |   |   |   |   |   |   concept22 <= 0: lose (2.0)
|   |   |   |   |   |   |   |   |   concept22 > 0: win (4.0)
|   |   |   |   |   |   concept63 > 0: even (15.0)
|   |   |   |   |   concept62 > 0
|   |   |   |   |   |   concept19 <= 0: even (32.0/6.0)
|   |   |   |   |   |   concept19 > 0: lose (3.0)
|   |   |   |   concept2 > 0
|   |   |   |   |   concept62 <= 0
|   |   |   |   |   |   concept31 <= 0: lose (19.0/2.0)
|   |   |   |   |   |   concept31 > 0: even (3.0/1.0)
|   |   |   |   |   concept62 > 0: even (6.0)
|   |   |   concept54 > 0
|   |   |   |   concept15 <= 0
|   |   |   |   |   concept23 <= 0
|   |   |   |   |   |   concept24 <= 0: even (17.0/1.0)
|   |   |   |   |   |   concept24 > 0
|   |   |   |   |   |   |   concept37 <= 0
|   |   |   |   |   |   |   |   concept2 <= 0: even (21.0/4.0)
|   |   |   |   |   |   |   |   concept2 > 0
|   |   |   |   |   |   |   |   |   concept38 <= 0: even (2.0)
|   |   |   |   |   |   |   |   |   concept38 > 0: win (2.0)
|   |   |   |   |   |   |   concept37 > 0
|   |   |   |   |   |   |   |   concept63 <= 0
|   |   |   |   |   |   |   |   |   concept40 <= 0
|   |   |   |   |   |   |   |   |   |   concept31 <= 0
|   |   |   |   |   |   |   |   |   |   |   concept27 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   concept45 <= 0: even (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept45 > 0: win (5.0/2.0)
|   |   |   |   |   |   |   |   |   |   |   concept27 > 0: lose (3.0)
|   |   |   |   |   |   |   |   |   |   concept31 > 0: even (19.0/4.0)
|   |   |   |   |   |   |   |   |   concept40 > 0: win (4.0)
|   |   |   |   |   |   |   |   concept63 > 0: win (14.0/3.0)
|   |   |   |   |   concept23 > 0
|   |   |   |   |   |   concept26 <= 0: even (8.0/1.0)
|   |   |   |   |   |   concept26 > 0: win (33.0/2.0)
|   |   |   |   concept15 > 0
|   |   |   |   |   concept66 <= 0: even (9.0)
|   |   |   |   |   concept66 > 0: lose (4.0/1.0)
|   |   concept6 > 0
|   |   |   concept40 <= 0: lose (21.0/1.0)
|   |   |   concept40 > 0
|   |   |   |   concept23 <= 0: win (10.0)
|   |   |   |   concept23 > 0: lose (6.0)
|   concept58 > 0
|   |   concept24 <= 0
|   |   |   concept36 <= 0
|   |   |   |   concept44 <= 0
|   |   |   |   |   concept62 <= 0: even (10.0/3.0)
|   |   |   |   |   concept62 > 0: win (2.0)
|   |   |   |   concept44 > 0: win (4.0)
|   |   |   concept36 > 0
|   |   |   |   concept55 <= 0
|   |   |   |   |   concept37 <= 0: win (3.0/1.0)
|   |   |   |   |   concept37 > 0: even (2.0)
|   |   |   |   concept55 > 0
|   |   |   |   |   concept37 <= 0
|   |   |   |   |   |   concept0 <= 0: even (6.0)
|   |   |   |   |   |   concept0 > 0: lose (2.0)
|   |   |   |   |   concept37 > 0
|   |   |   |   |   |   concept62 <= 0
|   |   |   |   |   |   |   concept66 <= 0
|   |   |   |   |   |   |   |   concept22 <= 0
|   |   |   |   |   |   |   |   |   concept27 <= 0
|   |   |   |   |   |   |   |   |   |   concept54 <= 0: lose (3.0)
|   |   |   |   |   |   |   |   |   |   concept54 > 0: even (6.0)
|   |   |   |   |   |   |   |   |   concept27 > 0: lose (10.0/1.0)
|   |   |   |   |   |   |   |   concept22 > 0: lose (9.0)
|   |   |   |   |   |   |   concept66 > 0: even (5.0/1.0)
|   |   |   |   |   |   concept62 > 0: lose (10.0)
|   |   concept24 > 0
|   |   |   concept1 <= 0
|   |   |   |   concept7 <= 0
|   |   |   |   |   concept14 <= 0
|   |   |   |   |   |   concept63 <= 0
|   |   |   |   |   |   |   concept11 <= 0
|   |   |   |   |   |   |   |   concept10 <= 0
|   |   |   |   |   |   |   |   |   concept19 <= 0
|   |   |   |   |   |   |   |   |   |   concept6 <= 0
|   |   |   |   |   |   |   |   |   |   |   concept2 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   concept15 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   concept45 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept54 <= 0: lose (12.0/2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept54 > 0: win (2.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept45 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept62 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept44 <= 0: even (16.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept44 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept37 <= 0: win (7.0/2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept37 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept54 <= 0: lose (4.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept54 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept30 <= 0: lose (6.0/2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept30 > 0: even (8.0/2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept62 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept27 <= 0: lose (8.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept27 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept40 <= 0: win (9.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept40 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept3 <= 0: even (3.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept3 > 0: win (4.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept15 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   concept38 <= 0: even (5.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept38 > 0: lose (3.0)
|   |   |   |   |   |   |   |   |   |   |   concept2 > 0
|   |   |   |   |   |   |   |   |   |   |   |   concept45 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   concept59 <= 0: even (3.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept59 > 0: lose (3.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept45 > 0: lose (12.0)
|   |   |   |   |   |   |   |   |   |   concept6 > 0
|   |   |   |   |   |   |   |   |   |   |   concept23 <= 0: win (10.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   concept23 > 0: lose (6.0)
|   |   |   |   |   |   |   |   |   concept19 > 0: even (8.0)
|   |   |   |   |   |   |   |   concept10 > 0: even (15.0/2.0)
|   |   |   |   |   |   |   concept11 > 0
|   |   |   |   |   |   |   |   concept2 <= 0: win (8.0)
|   |   |   |   |   |   |   |   concept2 > 0: even (5.0/1.0)
|   |   |   |   |   |   concept63 > 0
|   |   |   |   |   |   |   concept45 <= 0
|   |   |   |   |   |   |   |   concept41 <= 0: even (3.0)
|   |   |   |   |   |   |   |   concept41 > 0: win (2.0)
|   |   |   |   |   |   |   concept45 > 0
|   |   |   |   |   |   |   |   concept11 <= 0
|   |   |   |   |   |   |   |   |   concept66 <= 0
|   |   |   |   |   |   |   |   |   |   concept38 <= 0: lose (13.0/1.0)
|   |   |   |   |   |   |   |   |   |   concept38 > 0
|   |   |   |   |   |   |   |   |   |   |   concept44 <= 0: win (6.0)
|   |   |   |   |   |   |   |   |   |   |   concept44 > 0
|   |   |   |   |   |   |   |   |   |   |   |   concept3 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   concept10 <= 0: lose (10.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept10 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept0 <= 0: win (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept0 > 0: lose (4.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept3 > 0: win (4.0)
|   |   |   |   |   |   |   |   |   concept66 > 0
|   |   |   |   |   |   |   |   |   |   concept31 <= 0: lose (6.0/1.0)
|   |   |   |   |   |   |   |   |   |   concept31 > 0
|   |   |   |   |   |   |   |   |   |   |   concept6 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   concept15 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   concept26 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept19 <= 0: even (6.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept19 > 0: win (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept26 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept30 <= 0: even (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept30 > 0: win (24.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept15 > 0: even (9.0/2.0)
|   |   |   |   |   |   |   |   |   |   |   concept6 > 0
|   |   |   |   |   |   |   |   |   |   |   |   concept0 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   concept18 <= 0: win (12.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept18 > 0: lose (9.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept0 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   concept2 <= 0: lose (3.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept2 > 0: even (5.0)
|   |   |   |   |   |   |   |   concept11 > 0
|   |   |   |   |   |   |   |   |   concept18 <= 0
|   |   |   |   |   |   |   |   |   |   concept31 <= 0: lose (3.0/1.0)
|   |   |   |   |   |   |   |   |   |   concept31 > 0
|   |   |   |   |   |   |   |   |   |   |   concept27 <= 0: win (2.0)
|   |   |   |   |   |   |   |   |   |   |   concept27 > 0
|   |   |   |   |   |   |   |   |   |   |   |   concept6 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   concept10 <= 0: lose (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept10 > 0: win (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept6 > 0: lose (17.0)
|   |   |   |   |   |   |   |   |   concept18 > 0: win (2.0)
|   |   |   |   |   concept14 > 0
|   |   |   |   |   |   concept38 <= 0
|   |   |   |   |   |   |   concept3 <= 0: even (9.0)
|   |   |   |   |   |   |   concept3 > 0: win (4.0/1.0)
|   |   |   |   |   |   concept38 > 0
|   |   |   |   |   |   |   concept30 <= 0: even (2.0)
|   |   |   |   |   |   |   concept30 > 0
|   |   |   |   |   |   |   |   concept6 <= 0
|   |   |   |   |   |   |   |   |   concept41 <= 0
|   |   |   |   |   |   |   |   |   |   concept40 <= 0: even (6.0/1.0)
|   |   |   |   |   |   |   |   |   |   concept40 > 0
|   |   |   |   |   |   |   |   |   |   |   concept67 <= 0: even (6.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   concept67 > 0: lose (10.0/2.0)
|   |   |   |   |   |   |   |   |   concept41 > 0: lose (14.0)
|   |   |   |   |   |   |   |   concept6 > 0
|   |   |   |   |   |   |   |   |   concept18 <= 0: win (5.0/1.0)
|   |   |   |   |   |   |   |   |   concept18 > 0: lose (14.0)
|   |   |   |   concept7 > 0
|   |   |   |   |   concept10 <= 0
|   |   |   |   |   |   concept0 <= 0
|   |   |   |   |   |   |   concept19 <= 0
|   |   |   |   |   |   |   |   concept14 <= 0
|   |   |   |   |   |   |   |   |   concept45 <= 0
|   |   |   |   |   |   |   |   |   |   concept23 <= 0
|   |   |   |   |   |   |   |   |   |   |   concept37 <= 0: lose (4.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   concept37 > 0: win (8.0/2.0)
|   |   |   |   |   |   |   |   |   |   concept23 > 0: lose (14.0)
|   |   |   |   |   |   |   |   |   concept45 > 0
|   |   |   |   |   |   |   |   |   |   concept41 <= 0
|   |   |   |   |   |   |   |   |   |   |   concept18 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   concept2 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   concept59 <= 0: even (3.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept59 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept36 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept31 <= 0: win (7.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept31 > 0: even (3.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept36 > 0: win (18.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept2 > 0: win (3.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   concept18 > 0: lose (3.0/1.0)
|   |   |   |   |   |   |   |   |   |   concept41 > 0
|   |   |   |   |   |   |   |   |   |   |   concept15 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   concept54 <= 0: lose (9.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept54 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   concept67 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept22 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept55 <= 0: win (8.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept55 > 0
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept6 <= 0: lose (7.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   concept6 > 0: win (3.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   concept22 > 0: lose (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept67 > 0: win (25.0)
|   |   |   |   |   |   |   |   |   |   |   concept15 > 0: lose (14.0/1.0)
|   |   |   |   |   |   |   |   concept14 > 0
|   |   |   |   |   |   |   |   |   concept2 <= 0: win (7.0)
|   |   |   |   |   |   |   |   |   concept2 > 0: even (9.0/1.0)
|   |   |   |   |   |   |   concept19 > 0
|   |   |   |   |   |   |   |   concept59 <= 0: lose (3.0)
|   |   |   |   |   |   |   |   concept59 > 0
|   |   |   |   |   |   |   |   |   concept18 <= 0
|   |   |   |   |   |   |   |   |   |   concept40 <= 0
|   |   |   |   |   |   |   |   |   |   |   concept14 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   concept66 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   |   concept22 <= 0: even (3.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   concept22 > 0: win (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept66 > 0: even (20.0)
|   |   |   |   |   |   |   |   |   |   |   concept14 > 0: win (2.0)
|   |   |   |   |   |   |   |   |   |   concept40 > 0: win (6.0)
|   |   |   |   |   |   |   |   |   concept18 > 0: lose (4.0/1.0)
|   |   |   |   |   |   concept0 > 0
|   |   |   |   |   |   |   concept66 <= 0: win (3.0)
|   |   |   |   |   |   |   concept66 > 0
|   |   |   |   |   |   |   |   concept3 <= 0: lose (10.0)
|   |   |   |   |   |   |   |   concept3 > 0
|   |   |   |   |   |   |   |   |   concept6 <= 0: lose (4.0)
|   |   |   |   |   |   |   |   |   concept6 > 0
|   |   |   |   |   |   |   |   |   |   concept19 <= 0: lose (3.0)
|   |   |   |   |   |   |   |   |   |   concept19 > 0
|   |   |   |   |   |   |   |   |   |   |   concept23 <= 0: lose (2.0)
|   |   |   |   |   |   |   |   |   |   |   concept23 > 0: win (4.0)
|   |   |   |   |   concept10 > 0
|   |   |   |   |   |   concept26 <= 0: lose (3.0)
|   |   |   |   |   |   concept26 > 0: win (56.0/3.0)
|   |   |   concept1 > 0
|   |   |   |   concept31 <= 0
|   |   |   |   |   concept26 <= 0: even (5.0/1.0)
|   |   |   |   |   concept26 > 0: lose (17.0)
|   |   |   |   concept31 > 0
|   |   |   |   |   concept54 <= 0: lose (6.0)
|   |   |   |   |   concept54 > 0
|   |   |   |   |   |   concept63 <= 0: win (7.0/1.0)
|   |   |   |   |   |   concept63 > 0
|   |   |   |   |   |   |   concept2 <= 0
|   |   |   |   |   |   |   |   concept45 <= 0: even (3.0/1.0)
|   |   |   |   |   |   |   |   concept45 > 0
|   |   |   |   |   |   |   |   |   concept66 <= 0
|   |   |   |   |   |   |   |   |   |   concept40 <= 0: lose (8.0)
|   |   |   |   |   |   |   |   |   |   concept40 > 0
|   |   |   |   |   |   |   |   |   |   |   concept19 <= 0: lose (2.0)
|   |   |   |   |   |   |   |   |   |   |   concept19 > 0: win (5.0)
|   |   |   |   |   |   |   |   |   concept66 > 0
|   |   |   |   |   |   |   |   |   |   concept18 <= 0: win (41.0/2.0)
|   |   |   |   |   |   |   |   |   |   concept18 > 0
|   |   |   |   |   |   |   |   |   |   |   concept0 <= 0
|   |   |   |   |   |   |   |   |   |   |   |   concept6 <= 0: win (17.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept6 > 0: lose (2.0)
|   |   |   |   |   |   |   |   |   |   |   concept0 > 0
|   |   |   |   |   |   |   |   |   |   |   |   concept7 <= 0: lose (13.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   concept7 > 0: win (3.0)
|   |   |   |   |   |   |   concept2 > 0
|   |   |   |   |   |   |   |   concept15 <= 0: win (10.0/1.0)
|   |   |   |   |   |   |   |   concept15 > 0
|   |   |   |   |   |   |   |   |   concept18 <= 0
|   |   |   |   |   |   |   |   |   |   concept3 <= 0: lose (3.0)
|   |   |   |   |   |   |   |   |   |   concept3 > 0: win (2.0)
|   |   |   |   |   |   |   |   |   concept18 > 0: lose (21.0/1.0)
    """

    val connect4no =
      """
cell60 = dirt: win (0.0)
cell60 = r: lose (7.0/1.0)
cell60 = b
|   cell75 = dirt: win (0.0)
|   cell75 = r
|   |   cell51 = dirt: win (0.0)
|   |   cell51 = r: lose (1.0)
|   |   cell51 = b
|   |   |   cell30 = dirt: win (0.0)
|   |   |   cell30 = r: lose (3.0/1.0)
|   |   |   cell30 = b
|   |   |   |   cell10 = dirt: win (0.0)
|   |   |   |   cell10 = r: win (1.0)
|   |   |   |   cell10 = b
|   |   |   |   |   cell73 = dirt: win (0.0)
|   |   |   |   |   cell73 = r: win (9.0)
|   |   |   |   |   cell73 = b: win (9.0)
|   |   |   |   |   cell73 = w
|   |   |   |   |   |   cell25 = dirt: lose (0.0)
|   |   |   |   |   |   cell25 = r: win (2.0)
|   |   |   |   |   |   cell25 = b: lose (0.0)
|   |   |   |   |   |   cell25 = w: lose (3.0)
|   |   |   |   cell10 = w: lose (5.0/1.0)
|   |   |   cell30 = w: lose (3.0)
|   |   cell51 = w: win (7.0)
|   cell75 = b
|   |   cell53 = dirt: lose (0.0)
|   |   cell53 = r: lose (1.0)
|   |   cell53 = b: lose (2.0)
|   |   cell53 = w: win (2.0)
|   cell75 = w
|   |   cell33 = dirt: lose (0.0)
|   |   cell33 = r: lose (11.0)
|   |   cell33 = b
|   |   |   cell44 = dirt: win (0.0)
|   |   |   cell44 = r: lose (7.0/1.0)
|   |   |   cell44 = b: win (6.0/1.0)
|   |   |   cell44 = w
|   |   |   |   cell24 = dirt: win (0.0)
|   |   |   |   cell24 = r: win (5.0)
|   |   |   |   cell24 = b: win (2.0)
|   |   |   |   cell24 = w: lose (3.0)
|   |   cell33 = w
|   |   |   cell73 = dirt: lose (0.0)
|   |   |   cell73 = r: lose (3.0)
|   |   |   cell73 = b: lose (2.0)
|   |   |   cell73 = w: win (2.0)
cell60 = w
|   cell65 = dirt: win (0.0)
|   cell65 = r: lose (2.0)
|   cell65 = b: win (0.0)
|   cell65 = w: win (2.0)
      """
    
   val connect4noep1 =
     """

     """
   
   val connect4noep3 =
     """
cell41 = dirt: even (0.0)
cell41 = b
|   cell12 = dirt: even (0.0)
|   cell12 = b
|   |   cell53 = dirt: even (0.0)
|   |   cell53 = b
|   |   |   cell42 = dirt: even (0.0)
|   |   |   cell42 = b
|   |   |   |   cell13 = dirt: even (0.0)
|   |   |   |   cell13 = b
|   |   |   |   |   cell63 = dirt: even (0.0)
|   |   |   |   |   cell63 = b: even (1004.0/14.0)
|   |   |   |   |   cell63 = w
|   |   |   |   |   |   cell64 = dirt: even (0.0)
|   |   |   |   |   |   cell64 = b: even (0.0)
|   |   |   |   |   |   cell64 = w
|   |   |   |   |   |   |   cell54 = dirt: even (0.0)
|   |   |   |   |   |   |   cell54 = b: even (12.0)
|   |   |   |   |   |   |   cell54 = w: win (3.0)
|   |   |   |   |   |   |   cell54 = r
|   |   |   |   |   |   |   |   cell65 = dirt: even (0.0)
|   |   |   |   |   |   |   |   cell65 = b: even (0.0)
|   |   |   |   |   |   |   |   cell65 = w
|   |   |   |   |   |   |   |   |   cell32 = dirt: win (0.0)
|   |   |   |   |   |   |   |   |   cell32 = b: win (6.0)
|   |   |   |   |   |   |   |   |   cell32 = w: win (0.0)
|   |   |   |   |   |   |   |   |   cell32 = r
|   |   |   |   |   |   |   |   |   |   cell30 = dirt: win (0.0)
|   |   |   |   |   |   |   |   |   |   cell30 = b: even (3.0)
|   |   |   |   |   |   |   |   |   |   cell30 = w: win (3.0)
|   |   |   |   |   |   |   |   |   |   cell30 = r: win (0.0)
|   |   |   |   |   |   |   |   cell65 = r: even (21.0)
|   |   |   |   |   |   cell64 = r: even (65.0)
|   |   |   |   |   cell63 = r: even (77.0/3.0)
|   |   |   |   cell13 = w
|   |   |   |   |   cell61 = dirt: even (0.0)
|   |   |   |   |   cell61 = b
|   |   |   |   |   |   cell21 = dirt: even (0.0)
|   |   |   |   |   |   cell21 = b: even (88.0/9.0)
|   |   |   |   |   |   cell21 = w: even (1.0)
|   |   |   |   |   |   cell21 = r: win (3.0)
|   |   |   |   |   cell61 = w: even (0.0)
|   |   |   |   |   cell61 = r: win (2.0)
|   |   |   |   cell13 = r
|   |   |   |   |   cell63 = dirt: even (0.0)
|   |   |   |   |   cell63 = b: even (17.0/1.0)
|   |   |   |   |   cell63 = w: even (3.0)
|   |   |   |   |   cell63 = r: lose (3.0)
|   |   |   cell42 = w
|   |   |   |   cell65 = dirt: even (0.0)
|   |   |   |   cell65 = b
|   |   |   |   |   cell24 = dirt: win (0.0)
|   |   |   |   |   cell24 = b: even (2.0)
|   |   |   |   |   cell24 = w: win (3.0)
|   |   |   |   |   cell24 = r: win (0.0)
|   |   |   |   cell65 = w: even (30.0)
|   |   |   |   cell65 = r
|   |   |   |   |   cell44 = dirt: even (0.0)
|   |   |   |   |   cell44 = b: even (0.0)
|   |   |   |   |   cell44 = w
|   |   |   |   |   |   cell73 = dirt: even (0.0)
|   |   |   |   |   |   cell73 = b: even (9.0)
|   |   |   |   |   |   cell73 = w: even (0.0)
|   |   |   |   |   |   cell73 = r: lose (7.0/1.0)
|   |   |   |   |   cell44 = r: lose (3.0)
|   |   |   cell42 = r
|   |   |   |   cell73 = dirt: even (0.0)
|   |   |   |   cell73 = b
|   |   |   |   |   cell32 = dirt: even (0.0)
|   |   |   |   |   cell32 = b: even (21.0)
|   |   |   |   |   cell32 = w: even (6.0)
|   |   |   |   |   cell32 = r: lose (3.0)
|   |   |   |   cell73 = w: win (3.0/1.0)
|   |   |   |   cell73 = r
|   |   |   |   |   cell13 = dirt: even (0.0)
|   |   |   |   |   cell13 = b: even (3.0)
|   |   |   |   |   cell13 = w: win (2.0)
|   |   |   |   |   cell13 = r: even (0.0)
|   |   cell53 = w
|   |   |   cell30 = dirt: even (0.0)
|   |   |   cell30 = b
|   |   |   |   cell60 = dirt: even (0.0)
|   |   |   |   cell60 = b
|   |   |   |   |   cell21 = dirt: even (0.0)
|   |   |   |   |   cell21 = b
|   |   |   |   |   |   cell33 = dirt: even (0.0)
|   |   |   |   |   |   cell33 = b
|   |   |   |   |   |   |   cell42 = dirt: even (0.0)
|   |   |   |   |   |   |   cell42 = b
|   |   |   |   |   |   |   |   cell22 = dirt: even (0.0)
|   |   |   |   |   |   |   |   cell22 = b: even (103.0/7.0)
|   |   |   |   |   |   |   |   cell22 = w
|   |   |   |   |   |   |   |   |   cell55 = dirt: win (0.0)
|   |   |   |   |   |   |   |   |   cell55 = b: win (0.0)
|   |   |   |   |   |   |   |   |   cell55 = w: win (3.0)
|   |   |   |   |   |   |   |   |   cell55 = r: even (3.0/1.0)
|   |   |   |   |   |   |   |   cell22 = r: even (3.0)
|   |   |   |   |   |   |   cell42 = w: win (3.0)
|   |   |   |   |   |   |   cell42 = r: even (2.0)
|   |   |   |   |   |   cell33 = w
|   |   |   |   |   |   |   cell74 = dirt: even (0.0)
|   |   |   |   |   |   |   cell74 = b: lose (5.0/2.0)
|   |   |   |   |   |   |   cell74 = w: win (5.0)
|   |   |   |   |   |   |   cell74 = r: even (7.0)
|   |   |   |   |   |   cell33 = r
|   |   |   |   |   |   |   cell64 = dirt: lose (0.0)
|   |   |   |   |   |   |   cell64 = b: lose (6.0)
|   |   |   |   |   |   |   cell64 = w: lose (0.0)
|   |   |   |   |   |   |   cell64 = r: even (6.0)
|   |   |   |   |   cell21 = w: lose (6.0/1.0)
|   |   |   |   |   cell21 = r: even (3.0/1.0)
|   |   |   |   cell60 = w: lose (3.0/1.0)
|   |   |   |   cell60 = r: win (5.0/1.0)
|   |   |   cell30 = w: lose (4.0)
|   |   |   cell30 = r: win (2.0)
|   |   cell53 = r
|   |   |   cell42 = dirt: even (0.0)
|   |   |   cell42 = b
|   |   |   |   cell20 = dirt: even (0.0)
|   |   |   |   cell20 = b
|   |   |   |   |   cell32 = dirt: even (0.0)
|   |   |   |   |   cell32 = b
|   |   |   |   |   |   cell63 = dirt: even (0.0)
|   |   |   |   |   |   cell63 = b: even (124.0/9.0)
|   |   |   |   |   |   cell63 = w
|   |   |   |   |   |   |   cell73 = dirt: even (0.0)
|   |   |   |   |   |   |   cell73 = b
|   |   |   |   |   |   |   |   cell34 = dirt: lose (0.0)
|   |   |   |   |   |   |   |   cell34 = b
|   |   |   |   |   |   |   |   |   cell44 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   cell44 = b: even (4.0)
|   |   |   |   |   |   |   |   |   cell44 = w: even (0.0)
|   |   |   |   |   |   |   |   |   cell44 = r: lose (2.0)
|   |   |   |   |   |   |   |   cell34 = w: lose (9.0)
|   |   |   |   |   |   |   |   cell34 = r: even (4.0/1.0)
|   |   |   |   |   |   |   cell73 = w: even (0.0)
|   |   |   |   |   |   |   cell73 = r: even (6.0)
|   |   |   |   |   |   cell63 = r
|   |   |   |   |   |   |   cell50 = dirt: even (0.0)
|   |   |   |   |   |   |   cell50 = b: even (21.0/2.0)
|   |   |   |   |   |   |   cell50 = w: lose (5.0/2.0)
|   |   |   |   |   |   |   cell50 = r: even (0.0)
|   |   |   |   |   cell32 = w
|   |   |   |   |   |   cell74 = dirt: even (0.0)
|   |   |   |   |   |   cell74 = b: lose (2.0)
|   |   |   |   |   |   cell74 = w: even (4.0)
|   |   |   |   |   |   cell74 = r: win (1.0)
|   |   |   |   |   cell32 = r
|   |   |   |   |   |   cell31 = dirt: even (0.0)
|   |   |   |   |   |   cell31 = b: even (15.0/3.0)
|   |   |   |   |   |   cell31 = w: even (0.0)
|   |   |   |   |   |   cell31 = r: win (2.0)
|   |   |   |   cell20 = w: lose (3.0/1.0)
|   |   |   |   cell20 = r
|   |   |   |   |   cell52 = dirt: even (0.0)
|   |   |   |   |   cell52 = b: even (7.0/1.0)
|   |   |   |   |   cell52 = w: lose (4.0/1.0)
|   |   |   |   |   cell52 = r: even (0.0)
|   |   |   cell42 = w
|   |   |   |   cell71 = dirt: win (0.0)
|   |   |   |   cell71 = b
|   |   |   |   |   cell22 = dirt: win (0.0)
|   |   |   |   |   cell22 = b: win (6.0)
|   |   |   |   |   cell22 = w: lose (2.0)
|   |   |   |   |   cell22 = r: win (1.0)
|   |   |   |   cell71 = w: even (4.0)
|   |   |   |   cell71 = r: even (2.0)
|   |   |   cell42 = r: even (9.0/1.0)
|   cell12 = w
|   |   cell31 = dirt: even (0.0)
|   |   cell31 = b
|   |   |   cell65 = dirt: even (0.0)
|   |   |   cell65 = b: win (1.0)
|   |   |   cell65 = w: lose (7.0/2.0)
|   |   |   cell65 = r
|   |   |   |   cell55 = dirt: even (0.0)
|   |   |   |   cell55 = b
|   |   |   |   |   cell11 = dirt: lose (0.0)
|   |   |   |   |   cell11 = b: win (2.0/1.0)
|   |   |   |   |   cell11 = w: lose (0.0)
|   |   |   |   |   cell11 = r: lose (2.0)
|   |   |   |   cell55 = w
|   |   |   |   |   cell63 = dirt: even (0.0)
|   |   |   |   |   cell63 = b: even (46.0/1.0)
|   |   |   |   |   cell63 = w
|   |   |   |   |   |   cell71 = dirt: even (0.0)
|   |   |   |   |   |   cell71 = b
|   |   |   |   |   |   |   cell54 = dirt: even (0.0)
|   |   |   |   |   |   |   cell54 = b: even (0.0)
|   |   |   |   |   |   |   cell54 = w: win (3.0)
|   |   |   |   |   |   |   cell54 = r
|   |   |   |   |   |   |   |   cell22 = dirt: even (0.0)
|   |   |   |   |   |   |   |   cell22 = b: even (10.0)
|   |   |   |   |   |   |   |   cell22 = w: win (2.0)
|   |   |   |   |   |   |   |   cell22 = r: even (0.0)
|   |   |   |   |   |   cell71 = w: win (3.0)
|   |   |   |   |   |   cell71 = r: even (0.0)
|   |   |   |   |   cell63 = r: win (3.0)
|   |   |   |   cell55 = r
|   |   |   |   |   cell64 = dirt: lose (0.0)
|   |   |   |   |   cell64 = b: lose (0.0)
|   |   |   |   |   cell64 = w: lose (5.0/1.0)
|   |   |   |   |   cell64 = r: win (2.0)
|   |   cell31 = w: win (4.0)
|   |   cell31 = r: even (0.0)
|   cell12 = r
|   |   cell60 = dirt: even (0.0)
|   |   cell60 = b
|   |   |   cell42 = dirt: even (0.0)
|   |   |   cell42 = b
|   |   |   |   cell10 = dirt: even (0.0)
|   |   |   |   cell10 = b
|   |   |   |   |   cell43 = dirt: even (0.0)
|   |   |   |   |   cell43 = b
|   |   |   |   |   |   cell55 = dirt: even (0.0)
|   |   |   |   |   |   cell55 = b: even (0.0)
|   |   |   |   |   |   cell55 = w: even (37.0)
|   |   |   |   |   |   cell55 = r
|   |   |   |   |   |   |   cell13 = dirt: even (0.0)
|   |   |   |   |   |   |   cell13 = b: even (0.0)
|   |   |   |   |   |   |   cell13 = w
|   |   |   |   |   |   |   |   cell44 = dirt: even (0.0)
|   |   |   |   |   |   |   |   cell44 = b: even (14.0)
|   |   |   |   |   |   |   |   cell44 = w
|   |   |   |   |   |   |   |   |   cell22 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   cell22 = b: even (19.0/2.0)
|   |   |   |   |   |   |   |   |   cell22 = w
|   |   |   |   |   |   |   |   |   |   cell11 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   cell11 = b: even (4.0)
|   |   |   |   |   |   |   |   |   |   cell11 = w: even (0.0)
|   |   |   |   |   |   |   |   |   |   cell11 = r: lose (2.0)
|   |   |   |   |   |   |   |   |   cell22 = r: lose (1.0)
|   |   |   |   |   |   |   |   cell44 = r
|   |   |   |   |   |   |   |   |   cell64 = dirt: win (0.0)
|   |   |   |   |   |   |   |   |   cell64 = b
|   |   |   |   |   |   |   |   |   |   cell73 = dirt: win (0.0)
|   |   |   |   |   |   |   |   |   |   cell73 = b: even (1.0)
|   |   |   |   |   |   |   |   |   |   cell73 = w: win (3.0)
|   |   |   |   |   |   |   |   |   |   cell73 = r: even (2.0)
|   |   |   |   |   |   |   |   |   cell64 = w: win (0.0)
|   |   |   |   |   |   |   |   |   cell64 = r: lose (3.0)
|   |   |   |   |   |   |   cell13 = r: lose (3.0)
|   |   |   |   |   cell43 = w
|   |   |   |   |   |   cell34 = dirt: lose (0.0)
|   |   |   |   |   |   cell34 = b: lose (3.0)
|   |   |   |   |   |   cell34 = w: even (5.0)
|   |   |   |   |   |   cell34 = r: lose (6.0)
|   |   |   |   |   cell43 = r
|   |   |   |   |   |   cell44 = dirt: even (0.0)
|   |   |   |   |   |   cell44 = b: even (0.0)
|   |   |   |   |   |   cell44 = w: lose (4.0)
|   |   |   |   |   |   cell44 = r: even (13.0)
|   |   |   |   cell10 = w
|   |   |   |   |   cell54 = dirt: even (0.0)
|   |   |   |   |   cell54 = b: even (0.0)
|   |   |   |   |   cell54 = w: even (16.0/1.0)
|   |   |   |   |   cell54 = r: lose (3.0/1.0)
|   |   |   |   cell10 = r: win (4.0/1.0)
|   |   |   cell42 = w
|   |   |   |   cell71 = dirt: even (0.0)
|   |   |   |   cell71 = b: even (6.0/1.0)
|   |   |   |   cell71 = w: win (3.0)
|   |   |   |   cell71 = r: even (0.0)
|   |   |   cell42 = r: lose (4.0)
|   |   cell60 = w
|   |   |   cell11 = dirt: lose (0.0)
|   |   |   cell11 = b: even (2.0)
|   |   |   cell11 = w: lose (8.0/2.0)
|   |   |   cell11 = r: lose (0.0)
|   |   cell60 = r: win (2.0)
cell41 = w
|   cell70 = dirt: even (0.0)
|   cell70 = b
|   |   cell30 = dirt: even (0.0)
|   |   cell30 = b
|   |   |   cell62 = dirt: even (0.0)
|   |   |   cell62 = b
|   |   |   |   cell40 = dirt: even (0.0)
|   |   |   |   cell40 = b: even (18.0)
|   |   |   |   cell40 = w
|   |   |   |   |   cell74 = dirt: even (0.0)
|   |   |   |   |   cell74 = b: even (0.0)
|   |   |   |   |   cell74 = w: even (7.0/1.0)
|   |   |   |   |   cell74 = r: win (3.0)
|   |   |   |   cell40 = r: win (4.0/1.0)
|   |   |   cell62 = w
|   |   |   |   cell43 = dirt: win (0.0)
|   |   |   |   cell43 = b: win (0.0)
|   |   |   |   cell43 = w: win (12.0)
|   |   |   |   cell43 = r
|   |   |   |   |   cell32 = dirt: win (0.0)
|   |   |   |   |   cell32 = b: even (2.0)
|   |   |   |   |   cell32 = w: win (3.0)
|   |   |   |   |   cell32 = r: win (0.0)
|   |   |   cell62 = r: win (6.0/1.0)
|   |   cell30 = w: even (7.0)
|   |   cell30 = r: even (0.0)
|   cell70 = w: even (0.0)
|   cell70 = r
|   |   cell74 = dirt: lose (0.0)
|   |   cell74 = b: lose (0.0)
|   |   cell74 = w: win (2.0)
|   |   cell74 = r: lose (3.0)
cell41 = r
|   cell60 = dirt: even (0.0)
|   cell60 = b
|   |   cell30 = dirt: even (0.0)
|   |   cell30 = b: even (57.0/2.0)
|   |   cell30 = w: lose (7.0/1.0)
|   |   cell30 = r: lose (3.0)
|   cell60 = w: win (4.0/1.0)
|   cell60 = r: lose (9.0)
     """
   
   val connect4noep5 =
     """
cell70 = dirt: even (0.0)
cell70 = b
|   cell30 = dirt: even (0.0)
|   cell30 = b
|   |   cell10 = dirt: even (0.0)
|   |   cell10 = b
|   |   |   cell20 = dirt: even (0.0)
|   |   |   cell20 = b
|   |   |   |   cell62 = dirt: even (0.0)
|   |   |   |   cell62 = b
|   |   |   |   |   cell50 = dirt: even (0.0)
|   |   |   |   |   cell50 = b
|   |   |   |   |   |   cell63 = dirt: even (0.0)
|   |   |   |   |   |   cell63 = b
|   |   |   |   |   |   |   cell21 = dirt: even (0.0)
|   |   |   |   |   |   |   cell21 = b
|   |   |   |   |   |   |   |   cell23 = dirt: even (0.0)
|   |   |   |   |   |   |   |   cell23 = b
|   |   |   |   |   |   |   |   |   cell12 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   cell12 = b
|   |   |   |   |   |   |   |   |   |   cell52 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   cell52 = b: even (1076.0/74.0)
|   |   |   |   |   |   |   |   |   |   cell52 = w
|   |   |   |   |   |   |   |   |   |   |   cell64 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   |   cell64 = b: even (12.0)
|   |   |   |   |   |   |   |   |   |   |   cell64 = w: even (0.0)
|   |   |   |   |   |   |   |   |   |   |   cell64 = r: win (6.0/1.0)
|   |   |   |   |   |   |   |   |   |   cell52 = r
|   |   |   |   |   |   |   |   |   |   |   cell64 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   |   cell64 = b
|   |   |   |   |   |   |   |   |   |   |   |   cell54 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   |   |   cell54 = b: even (0.0)
|   |   |   |   |   |   |   |   |   |   |   |   cell54 = w
|   |   |   |   |   |   |   |   |   |   |   |   |   cell73 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   cell73 = b: win (4.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   cell73 = w: even (7.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   cell73 = r: even (0.0)
|   |   |   |   |   |   |   |   |   |   |   |   cell54 = r: even (13.0)
|   |   |   |   |   |   |   |   |   |   |   cell64 = w: even (16.0)
|   |   |   |   |   |   |   |   |   |   |   cell64 = r: win (3.0/1.0)
|   |   |   |   |   |   |   |   |   cell12 = w
|   |   |   |   |   |   |   |   |   |   cell64 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   cell64 = b: even (17.0)
|   |   |   |   |   |   |   |   |   |   cell64 = w: even (4.0/1.0)
|   |   |   |   |   |   |   |   |   |   cell64 = r: win (10.0)
|   |   |   |   |   |   |   |   |   cell12 = r
|   |   |   |   |   |   |   |   |   |   cell33 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   cell33 = b: even (21.0/1.0)
|   |   |   |   |   |   |   |   |   |   cell33 = w: win (6.0/1.0)
|   |   |   |   |   |   |   |   |   |   cell33 = r: even (0.0)
|   |   |   |   |   |   |   |   cell23 = w
|   |   |   |   |   |   |   |   |   cell73 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   cell73 = b
|   |   |   |   |   |   |   |   |   |   cell33 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   cell33 = b
|   |   |   |   |   |   |   |   |   |   |   cell42 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   |   cell42 = b
|   |   |   |   |   |   |   |   |   |   |   |   cell55 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   |   |   cell55 = b: win (4.0)
|   |   |   |   |   |   |   |   |   |   |   |   cell55 = w: even (15.0)
|   |   |   |   |   |   |   |   |   |   |   |   cell55 = r
|   |   |   |   |   |   |   |   |   |   |   |   |   cell75 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   cell75 = b
|   |   |   |   |   |   |   |   |   |   |   |   |   |   cell65 = dirt: win (0.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   cell65 = b: even (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   cell65 = w: win (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   |   cell65 = r: win (0.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   cell75 = w: even (5.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   cell75 = r: even (0.0)
|   |   |   |   |   |   |   |   |   |   |   cell42 = w: even (0.0)
|   |   |   |   |   |   |   |   |   |   |   cell42 = r: win (3.0)
|   |   |   |   |   |   |   |   |   |   cell33 = w: win (8.0)
|   |   |   |   |   |   |   |   |   |   cell33 = r: win (3.0)
|   |   |   |   |   |   |   |   |   cell73 = w
|   |   |   |   |   |   |   |   |   |   cell71 = dirt: lose (0.0)
|   |   |   |   |   |   |   |   |   |   cell71 = b: lose (7.0/1.0)
|   |   |   |   |   |   |   |   |   |   cell71 = w: win (2.0)
|   |   |   |   |   |   |   |   |   |   cell71 = r: lose (0.0)
|   |   |   |   |   |   |   |   |   cell73 = r: even (31.0/2.0)
|   |   |   |   |   |   |   |   cell23 = r
|   |   |   |   |   |   |   |   |   cell33 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   cell33 = b: even (81.0/6.0)
|   |   |   |   |   |   |   |   |   cell33 = w
|   |   |   |   |   |   |   |   |   |   cell75 = dirt: win (0.0)
|   |   |   |   |   |   |   |   |   |   cell75 = b: win (0.0)
|   |   |   |   |   |   |   |   |   |   cell75 = w: win (6.0)
|   |   |   |   |   |   |   |   |   |   cell75 = r: even (2.0)
|   |   |   |   |   |   |   |   |   cell33 = r: even (21.0)
|   |   |   |   |   |   |   cell21 = w
|   |   |   |   |   |   |   |   cell52 = dirt: win (0.0)
|   |   |   |   |   |   |   |   cell52 = b: win (8.0/1.0)
|   |   |   |   |   |   |   |   cell52 = w: win (0.0)
|   |   |   |   |   |   |   |   cell52 = r: even (2.0)
|   |   |   |   |   |   |   cell21 = r
|   |   |   |   |   |   |   |   cell75 = dirt: lose (0.0)
|   |   |   |   |   |   |   |   cell75 = b: lose (0.0)
|   |   |   |   |   |   |   |   cell75 = w: even (2.0)
|   |   |   |   |   |   |   |   cell75 = r: lose (2.0)
|   |   |   |   |   |   cell63 = w
|   |   |   |   |   |   |   cell22 = dirt: even (0.0)
|   |   |   |   |   |   |   cell22 = b
|   |   |   |   |   |   |   |   cell73 = dirt: even (0.0)
|   |   |   |   |   |   |   |   cell73 = b
|   |   |   |   |   |   |   |   |   cell13 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   cell13 = b
|   |   |   |   |   |   |   |   |   |   cell25 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   cell25 = b: even (11.0)
|   |   |   |   |   |   |   |   |   |   cell25 = w
|   |   |   |   |   |   |   |   |   |   |   cell53 = dirt: win (0.0)
|   |   |   |   |   |   |   |   |   |   |   cell53 = b: win (8.0)
|   |   |   |   |   |   |   |   |   |   |   cell53 = w: even (6.0)
|   |   |   |   |   |   |   |   |   |   |   cell53 = r: win (0.0)
|   |   |   |   |   |   |   |   |   |   cell25 = r
|   |   |   |   |   |   |   |   |   |   |   cell32 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   |   cell32 = b
|   |   |   |   |   |   |   |   |   |   |   |   cell53 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   |   |   cell53 = b: even (19.0)
|   |   |   |   |   |   |   |   |   |   |   |   cell53 = w: win (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   cell53 = r
|   |   |   |   |   |   |   |   |   |   |   |   |   cell75 = dirt: lose (0.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   cell75 = b: even (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   cell75 = w: lose (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   cell75 = r: lose (0.0)
|   |   |   |   |   |   |   |   |   |   |   cell32 = w: even (0.0)
|   |   |   |   |   |   |   |   |   |   |   cell32 = r: lose (5.0)
|   |   |   |   |   |   |   |   |   cell13 = w
|   |   |   |   |   |   |   |   |   |   cell33 = dirt: win (0.0)
|   |   |   |   |   |   |   |   |   |   cell33 = b: even (3.0)
|   |   |   |   |   |   |   |   |   |   cell33 = w: win (6.0)
|   |   |   |   |   |   |   |   |   |   cell33 = r: win (0.0)
|   |   |   |   |   |   |   |   |   cell13 = r: lose (6.0/1.0)
|   |   |   |   |   |   |   |   cell73 = w: even (1.0)
|   |   |   |   |   |   |   |   cell73 = r: even (17.0/1.0)
|   |   |   |   |   |   |   cell22 = w: lose (2.0)
|   |   |   |   |   |   |   cell22 = r: even (11.0/1.0)
|   |   |   |   |   |   cell63 = r
|   |   |   |   |   |   |   cell13 = dirt: even (0.0)
|   |   |   |   |   |   |   cell13 = b
|   |   |   |   |   |   |   |   cell34 = dirt: even (0.0)
|   |   |   |   |   |   |   |   cell34 = b
|   |   |   |   |   |   |   |   |   cell54 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   cell54 = b
|   |   |   |   |   |   |   |   |   |   cell14 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   cell14 = b: even (4.0)
|   |   |   |   |   |   |   |   |   |   cell14 = w: even (0.0)
|   |   |   |   |   |   |   |   |   |   cell14 = r: lose (2.0)
|   |   |   |   |   |   |   |   |   cell54 = w: even (9.0)
|   |   |   |   |   |   |   |   |   cell54 = r: lose (3.0)
|   |   |   |   |   |   |   |   cell34 = w
|   |   |   |   |   |   |   |   |   cell64 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   cell64 = b: even (0.0)
|   |   |   |   |   |   |   |   |   cell64 = w: win (4.0)
|   |   |   |   |   |   |   |   |   cell64 = r
|   |   |   |   |   |   |   |   |   |   cell53 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   cell53 = b: even (26.0/1.0)
|   |   |   |   |   |   |   |   |   |   cell53 = w: even (6.0)
|   |   |   |   |   |   |   |   |   |   cell53 = r: win (6.0/1.0)
|   |   |   |   |   |   |   |   cell34 = r
|   |   |   |   |   |   |   |   |   cell23 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   cell23 = b: even (18.0)
|   |   |   |   |   |   |   |   |   cell23 = w: even (0.0)
|   |   |   |   |   |   |   |   |   cell23 = r
|   |   |   |   |   |   |   |   |   |   cell54 = dirt: win (0.0)
|   |   |   |   |   |   |   |   |   |   cell54 = b: even (2.0)
|   |   |   |   |   |   |   |   |   |   cell54 = w: win (5.0)
|   |   |   |   |   |   |   |   |   |   cell54 = r: win (0.0)
|   |   |   |   |   |   |   cell13 = w
|   |   |   |   |   |   |   |   cell15 = dirt: even (0.0)
|   |   |   |   |   |   |   |   cell15 = b: even (0.0)
|   |   |   |   |   |   |   |   cell15 = w
|   |   |   |   |   |   |   |   |   cell32 = dirt: lose (0.0)
|   |   |   |   |   |   |   |   |   cell32 = b: lose (14.0)
|   |   |   |   |   |   |   |   |   cell32 = w: lose (0.0)
|   |   |   |   |   |   |   |   |   cell32 = r: even (2.0)
|   |   |   |   |   |   |   |   cell15 = r: even (13.0)
|   |   |   |   |   |   |   cell13 = r
|   |   |   |   |   |   |   |   cell74 = dirt: even (0.0)
|   |   |   |   |   |   |   |   cell74 = b: even (0.0)
|   |   |   |   |   |   |   |   cell74 = w: lose (3.0)
|   |   |   |   |   |   |   |   cell74 = r: even (4.0)
|   |   |   |   |   cell50 = w
|   |   |   |   |   |   cell25 = dirt: even (0.0)
|   |   |   |   |   |   cell25 = b: even (0.0)
|   |   |   |   |   |   cell25 = w
|   |   |   |   |   |   |   cell12 = dirt: even (0.0)
|   |   |   |   |   |   |   cell12 = b
|   |   |   |   |   |   |   |   cell55 = dirt: even (0.0)
|   |   |   |   |   |   |   |   cell55 = b: even (0.0)
|   |   |   |   |   |   |   |   cell55 = w: even (19.0/1.0)
|   |   |   |   |   |   |   |   cell55 = r
|   |   |   |   |   |   |   |   |   cell73 = dirt: lose (0.0)
|   |   |   |   |   |   |   |   |   cell73 = b: even (3.0)
|   |   |   |   |   |   |   |   |   cell73 = w: lose (0.0)
|   |   |   |   |   |   |   |   |   cell73 = r: lose (8.0)
|   |   |   |   |   |   |   cell12 = w: even (0.0)
|   |   |   |   |   |   |   cell12 = r: lose (4.0)
|   |   |   |   |   |   cell25 = r
|   |   |   |   |   |   |   cell73 = dirt: win (0.0)
|   |   |   |   |   |   |   cell73 = b: win (6.0)
|   |   |   |   |   |   |   cell73 = w: lose (2.0)
|   |   |   |   |   |   |   cell73 = r: win (0.0)
|   |   |   |   |   cell50 = r: even (15.0/1.0)
|   |   |   |   cell62 = w
|   |   |   |   |   cell23 = dirt: even (0.0)
|   |   |   |   |   cell23 = b
|   |   |   |   |   |   cell74 = dirt: even (0.0)
|   |   |   |   |   |   cell74 = b
|   |   |   |   |   |   |   cell44 = dirt: even (0.0)
|   |   |   |   |   |   |   cell44 = b: even (19.0/3.0)
|   |   |   |   |   |   |   cell44 = w: lose (10.0/1.0)
|   |   |   |   |   |   |   cell44 = r: win (1.0)
|   |   |   |   |   |   cell74 = w: win (3.0)
|   |   |   |   |   |   cell74 = r
|   |   |   |   |   |   |   cell65 = dirt: win (0.0)
|   |   |   |   |   |   |   cell65 = b: win (0.0)
|   |   |   |   |   |   |   cell65 = w
|   |   |   |   |   |   |   |   cell14 = dirt: win (0.0)
|   |   |   |   |   |   |   |   cell14 = b: even (2.0)
|   |   |   |   |   |   |   |   cell14 = w
|   |   |   |   |   |   |   |   |   cell11 = dirt: win (0.0)
|   |   |   |   |   |   |   |   |   cell11 = b: win (11.0)
|   |   |   |   |   |   |   |   |   cell11 = w: win (0.0)
|   |   |   |   |   |   |   |   |   cell11 = r
|   |   |   |   |   |   |   |   |   |   cell61 = dirt: win (0.0)
|   |   |   |   |   |   |   |   |   |   cell61 = b: even (2.0)
|   |   |   |   |   |   |   |   |   |   cell61 = w: win (3.0)
|   |   |   |   |   |   |   |   |   |   cell61 = r: win (0.0)
|   |   |   |   |   |   |   |   cell14 = r: win (0.0)
|   |   |   |   |   |   |   cell65 = r: lose (3.0/1.0)
|   |   |   |   |   cell23 = w: even (8.0)
|   |   |   |   |   cell23 = r: lose (9.0/1.0)
|   |   |   |   cell62 = r
|   |   |   |   |   cell13 = dirt: even (0.0)
|   |   |   |   |   cell13 = b
|   |   |   |   |   |   cell35 = dirt: even (0.0)
|   |   |   |   |   |   cell35 = b
|   |   |   |   |   |   |   cell15 = dirt: lose (0.0)
|   |   |   |   |   |   |   cell15 = b: even (2.0)
|   |   |   |   |   |   |   cell15 = w: even (1.0)
|   |   |   |   |   |   |   cell15 = r: lose (4.0)
|   |   |   |   |   |   cell35 = w
|   |   |   |   |   |   |   cell21 = dirt: even (0.0)
|   |   |   |   |   |   |   cell21 = b: even (33.0/2.0)
|   |   |   |   |   |   |   cell21 = w: lose (4.0)
|   |   |   |   |   |   |   cell21 = r: even (0.0)
|   |   |   |   |   |   cell35 = r
|   |   |   |   |   |   |   cell45 = dirt: lose (0.0)
|   |   |   |   |   |   |   cell45 = b
|   |   |   |   |   |   |   |   cell72 = dirt: lose (0.0)
|   |   |   |   |   |   |   |   cell72 = b: even (2.0)
|   |   |   |   |   |   |   |   cell72 = w: lose (0.0)
|   |   |   |   |   |   |   |   cell72 = r: lose (2.0)
|   |   |   |   |   |   |   cell45 = w: win (4.0)
|   |   |   |   |   |   |   cell45 = r: lose (5.0)
|   |   |   |   |   cell13 = w: lose (6.0)
|   |   |   |   |   cell13 = r: even (3.0)
|   |   |   cell20 = w
|   |   |   |   cell74 = dirt: even (0.0)
|   |   |   |   cell74 = b: even (6.0)
|   |   |   |   cell74 = w: win (5.0)
|   |   |   |   cell74 = r: even (0.0)
|   |   |   cell20 = r
|   |   |   |   cell55 = dirt: lose (0.0)
|   |   |   |   cell55 = b: lose (0.0)
|   |   |   |   cell55 = w
|   |   |   |   |   cell53 = dirt: lose (0.0)
|   |   |   |   |   cell53 = b: win (3.0/1.0)
|   |   |   |   |   cell53 = w
|   |   |   |   |   |   cell64 = dirt: lose (0.0)
|   |   |   |   |   |   cell64 = b: even (5.0/1.0)
|   |   |   |   |   |   cell64 = w: lose (4.0)
|   |   |   |   |   |   cell64 = r: lose (11.0/1.0)
|   |   |   |   |   cell53 = r: lose (0.0)
|   |   |   |   cell55 = r: win (2.0)
|   |   cell10 = w
|   |   |   cell22 = dirt: lose (0.0)
|   |   |   cell22 = b
|   |   |   |   cell31 = dirt: lose (0.0)
|   |   |   |   cell31 = b
|   |   |   |   |   cell65 = dirt: even (0.0)
|   |   |   |   |   cell65 = b: win (5.0)
|   |   |   |   |   cell65 = w: even (9.0/2.0)
|   |   |   |   |   cell65 = r: lose (3.0)
|   |   |   |   cell31 = w: lose (0.0)
|   |   |   |   cell31 = r: lose (4.0)
|   |   |   cell22 = w: lose (0.0)
|   |   |   cell22 = r: lose (9.0)
|   |   cell10 = r
|   |   |   cell24 = dirt: even (0.0)
|   |   |   cell24 = b
|   |   |   |   cell32 = dirt: lose (0.0)
|   |   |   |   cell32 = b: even (8.0/1.0)
|   |   |   |   cell32 = w: lose (9.0)
|   |   |   |   cell32 = r: lose (0.0)
|   |   |   cell24 = w: win (6.0)
|   |   |   cell24 = r
|   |   |   |   cell54 = dirt: even (0.0)
|   |   |   |   cell54 = b: even (7.0)
|   |   |   |   cell54 = w: win (5.0)
|   |   |   |   cell54 = r: even (10.0/2.0)
|   cell30 = w
|   |   cell75 = dirt: lose (0.0)
|   |   cell75 = b: lose (0.0)
|   |   cell75 = w: lose (16.0/2.0)
|   |   cell75 = r
|   |   |   cell53 = dirt: win (0.0)
|   |   |   cell53 = b: lose (4.0)
|   |   |   cell53 = w: win (6.0)
|   |   |   cell53 = r: win (0.0)
|   cell30 = r
|   |   cell54 = dirt: lose (0.0)
|   |   cell54 = b: even (4.0)
|   |   cell54 = w
|   |   |   cell43 = dirt: win (0.0)
|   |   |   cell43 = b
|   |   |   |   cell61 = dirt: even (0.0)
|   |   |   |   cell61 = b: even (6.0)
|   |   |   |   cell61 = w: win (3.0)
|   |   |   |   cell61 = r: even (0.0)
|   |   |   cell43 = w: win (5.0)
|   |   |   cell43 = r: win (2.0)
|   |   cell54 = r
|   |   |   cell73 = dirt: lose (0.0)
|   |   |   cell73 = b: even (8.0/1.0)
|   |   |   cell73 = w: lose (10.0/1.0)
|   |   |   cell73 = r: lose (9.0)
cell70 = w
|   cell53 = dirt: win (0.0)
|   cell53 = b: lose (12.0/1.0)
|   cell53 = w
|   |   cell33 = dirt: win (0.0)
|   |   cell33 = b: even (2.0)
|   |   cell33 = w: win (5.0)
|   |   cell33 = r: win (10.0)
|   cell53 = r
|   |   cell73 = dirt: win (0.0)
|   |   cell73 = b: win (0.0)
|   |   cell73 = w: win (5.0)
|   |   cell73 = r: lose (6.0/1.0)
cell70 = r
|   cell72 = dirt: win (0.0)
|   cell72 = b: win (0.0)
|   cell72 = w: lose (8.0)
|   cell72 = r
|   |   cell52 = dirt: win (0.0)
|   |   cell52 = b
|   |   |   cell34 = dirt: win (0.0)
|   |   |   cell34 = b: even (5.0)
|   |   |   cell34 = w
|   |   |   |   cell14 = dirt: win (0.0)
|   |   |   |   cell14 = b: even (3.0)
|   |   |   |   cell14 = w: win (5.0)
|   |   |   |   cell14 = r
|   |   |   |   |   cell60 = dirt: win (0.0)
|   |   |   |   |   cell60 = b: even (2.0)
|   |   |   |   |   cell60 = w: win (5.0)
|   |   |   |   |   cell60 = r: win (0.0)
|   |   |   cell34 = r: win (2.0)
|   |   cell52 = w: win (4.0)
|   |   cell52 = r: lose (5.0)
     """
   
   val connect4noep10 =
     """
cell31 = dirt: even (0.0)
cell31 = b
|   cell61 = dirt: even (0.0)
|   cell61 = b
|   |   cell52 = dirt: even (0.0)
|   |   cell52 = b
|   |   |   cell71 = dirt: even (0.0)
|   |   |   cell71 = b
|   |   |   |   cell64 = dirt: even (0.0)
|   |   |   |   cell64 = b
|   |   |   |   |   cell32 = dirt: even (0.0)
|   |   |   |   |   cell32 = b
|   |   |   |   |   |   cell12 = dirt: even (0.0)
|   |   |   |   |   |   cell12 = b
|   |   |   |   |   |   |   cell15 = dirt: even (0.0)
|   |   |   |   |   |   |   cell15 = b
|   |   |   |   |   |   |   |   cell73 = dirt: even (0.0)
|   |   |   |   |   |   |   |   cell73 = b
|   |   |   |   |   |   |   |   |   cell44 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   cell44 = b: even (319.0/27.0)
|   |   |   |   |   |   |   |   |   cell44 = w
|   |   |   |   |   |   |   |   |   |   cell23 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   cell23 = b
|   |   |   |   |   |   |   |   |   |   |   cell45 = dirt: win (0.0)
|   |   |   |   |   |   |   |   |   |   |   cell45 = b: win (0.0)
|   |   |   |   |   |   |   |   |   |   |   cell45 = w: win (10.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   cell45 = r: even (5.0)
|   |   |   |   |   |   |   |   |   |   cell23 = w: even (0.0)
|   |   |   |   |   |   |   |   |   |   cell23 = r: even (7.0)
|   |   |   |   |   |   |   |   |   cell44 = r
|   |   |   |   |   |   |   |   |   |   cell65 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   cell65 = b: even (13.0)
|   |   |   |   |   |   |   |   |   |   cell65 = w
|   |   |   |   |   |   |   |   |   |   |   cell74 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   |   cell74 = b: even (5.0)
|   |   |   |   |   |   |   |   |   |   |   cell74 = w: lose (2.0)
|   |   |   |   |   |   |   |   |   |   |   cell74 = r: even (0.0)
|   |   |   |   |   |   |   |   |   |   cell65 = r: win (3.0)
|   |   |   |   |   |   |   |   cell73 = w
|   |   |   |   |   |   |   |   |   cell24 = dirt: lose (0.0)
|   |   |   |   |   |   |   |   |   cell24 = b: even (2.0)
|   |   |   |   |   |   |   |   |   cell24 = w: lose (0.0)
|   |   |   |   |   |   |   |   |   cell24 = r: lose (6.0)
|   |   |   |   |   |   |   |   cell73 = r: even (17.0/2.0)
|   |   |   |   |   |   |   cell15 = w
|   |   |   |   |   |   |   |   cell13 = dirt: even (0.0)
|   |   |   |   |   |   |   |   cell13 = b
|   |   |   |   |   |   |   |   |   cell44 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   cell44 = b
|   |   |   |   |   |   |   |   |   |   cell45 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   cell45 = b
|   |   |   |   |   |   |   |   |   |   |   cell34 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   |   cell34 = b: even (99.0/9.0)
|   |   |   |   |   |   |   |   |   |   |   cell34 = w: even (0.0)
|   |   |   |   |   |   |   |   |   |   |   cell34 = r
|   |   |   |   |   |   |   |   |   |   |   |   cell65 = dirt: win (0.0)
|   |   |   |   |   |   |   |   |   |   |   |   cell65 = b
|   |   |   |   |   |   |   |   |   |   |   |   |   cell75 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   cell75 = b: even (1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   cell75 = w: lose (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   cell75 = r: even (3.0)
|   |   |   |   |   |   |   |   |   |   |   |   cell65 = w: win (6.0)
|   |   |   |   |   |   |   |   |   |   |   |   cell65 = r: even (1.0)
|   |   |   |   |   |   |   |   |   |   cell45 = w
|   |   |   |   |   |   |   |   |   |   |   cell65 = dirt: lose (0.0)
|   |   |   |   |   |   |   |   |   |   |   cell65 = b
|   |   |   |   |   |   |   |   |   |   |   |   cell14 = dirt: lose (0.0)
|   |   |   |   |   |   |   |   |   |   |   |   cell14 = b: even (9.0/3.0)
|   |   |   |   |   |   |   |   |   |   |   |   cell14 = w: lose (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   cell14 = r: lose (5.0)
|   |   |   |   |   |   |   |   |   |   |   cell65 = w
|   |   |   |   |   |   |   |   |   |   |   |   cell34 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   |   |   cell34 = b: even (6.0)
|   |   |   |   |   |   |   |   |   |   |   |   cell34 = w: lose (2.0)
|   |   |   |   |   |   |   |   |   |   |   |   cell34 = r: win (4.0)
|   |   |   |   |   |   |   |   |   |   |   cell65 = r: lose (0.0)
|   |   |   |   |   |   |   |   |   |   cell45 = r
|   |   |   |   |   |   |   |   |   |   |   cell34 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   |   cell34 = b
|   |   |   |   |   |   |   |   |   |   |   |   cell54 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   |   |   cell54 = b
|   |   |   |   |   |   |   |   |   |   |   |   |   cell24 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   cell24 = b: even (19.0/5.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   cell24 = w: even (1.0)
|   |   |   |   |   |   |   |   |   |   |   |   |   cell24 = r: lose (5.0)
|   |   |   |   |   |   |   |   |   |   |   |   cell54 = w: even (0.0)
|   |   |   |   |   |   |   |   |   |   |   |   cell54 = r: lose (3.0)
|   |   |   |   |   |   |   |   |   |   |   cell34 = w
|   |   |   |   |   |   |   |   |   |   |   |   cell55 = dirt: win (0.0)
|   |   |   |   |   |   |   |   |   |   |   |   cell55 = b: even (5.0/2.0)
|   |   |   |   |   |   |   |   |   |   |   |   cell55 = w: win (0.0)
|   |   |   |   |   |   |   |   |   |   |   |   cell55 = r: win (4.0)
|   |   |   |   |   |   |   |   |   |   |   cell34 = r: even (15.0)
|   |   |   |   |   |   |   |   |   cell44 = w: lose (16.0/2.0)
|   |   |   |   |   |   |   |   |   cell44 = r: even (28.0/5.0)
|   |   |   |   |   |   |   |   cell13 = w: lose (7.0)
|   |   |   |   |   |   |   |   cell13 = r
|   |   |   |   |   |   |   |   |   cell72 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   cell72 = b
|   |   |   |   |   |   |   |   |   |   cell75 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   cell75 = b: even (2.0)
|   |   |   |   |   |   |   |   |   |   cell75 = w
|   |   |   |   |   |   |   |   |   |   |   cell55 = dirt: win (0.0)
|   |   |   |   |   |   |   |   |   |   |   cell55 = b: even (2.0)
|   |   |   |   |   |   |   |   |   |   |   cell55 = w: win (0.0)
|   |   |   |   |   |   |   |   |   |   |   cell55 = r: win (3.0)
|   |   |   |   |   |   |   |   |   |   cell75 = r: even (20.0)
|   |   |   |   |   |   |   |   |   cell72 = w: lose (4.0/1.0)
|   |   |   |   |   |   |   |   |   cell72 = r: even (0.0)
|   |   |   |   |   |   |   cell15 = r
|   |   |   |   |   |   |   |   cell43 = dirt: even (0.0)
|   |   |   |   |   |   |   |   cell43 = b
|   |   |   |   |   |   |   |   |   cell73 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   cell73 = b: even (163.0/8.0)
|   |   |   |   |   |   |   |   |   cell73 = w
|   |   |   |   |   |   |   |   |   |   cell65 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   cell65 = b: win (5.0)
|   |   |   |   |   |   |   |   |   |   cell65 = w: even (0.0)
|   |   |   |   |   |   |   |   |   |   cell65 = r
|   |   |   |   |   |   |   |   |   |   |   cell24 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   |   cell24 = b: even (12.0)
|   |   |   |   |   |   |   |   |   |   |   cell24 = w: even (0.0)
|   |   |   |   |   |   |   |   |   |   |   cell24 = r: win (3.0)
|   |   |   |   |   |   |   |   |   cell73 = r: even (17.0/2.0)
|   |   |   |   |   |   |   |   cell43 = w: even (5.0/1.0)
|   |   |   |   |   |   |   |   cell43 = r
|   |   |   |   |   |   |   |   |   cell45 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   cell45 = b: even (0.0)
|   |   |   |   |   |   |   |   |   cell45 = w: even (15.0)
|   |   |   |   |   |   |   |   |   cell45 = r
|   |   |   |   |   |   |   |   |   |   cell75 = dirt: win (0.0)
|   |   |   |   |   |   |   |   |   |   cell75 = b: even (2.0)
|   |   |   |   |   |   |   |   |   |   cell75 = w: win (0.0)
|   |   |   |   |   |   |   |   |   |   cell75 = r: win (8.0)
|   |   |   |   |   |   cell12 = w: even (21.0/4.0)
|   |   |   |   |   |   cell12 = r: lose (5.0)
|   |   |   |   |   cell32 = w
|   |   |   |   |   |   cell55 = dirt: even (0.0)
|   |   |   |   |   |   cell55 = b: even (0.0)
|   |   |   |   |   |   cell55 = w: even (10.0)
|   |   |   |   |   |   cell55 = r
|   |   |   |   |   |   |   cell43 = dirt: lose (0.0)
|   |   |   |   |   |   |   cell43 = b: lose (8.0)
|   |   |   |   |   |   |   cell43 = w: lose (0.0)
|   |   |   |   |   |   |   cell43 = r: even (2.0)
|   |   |   |   |   cell32 = r
|   |   |   |   |   |   cell13 = dirt: even (0.0)
|   |   |   |   |   |   cell13 = b
|   |   |   |   |   |   |   cell14 = dirt: win (0.0)
|   |   |   |   |   |   |   cell14 = b
|   |   |   |   |   |   |   |   cell23 = dirt: even (0.0)
|   |   |   |   |   |   |   |   cell23 = b: even (9.0/1.0)
|   |   |   |   |   |   |   |   cell23 = w: even (0.0)
|   |   |   |   |   |   |   |   cell23 = r: win (2.0)
|   |   |   |   |   |   |   cell14 = w: even (2.0)
|   |   |   |   |   |   |   cell14 = r: win (8.0)
|   |   |   |   |   |   cell13 = w: lose (6.0/1.0)
|   |   |   |   |   |   cell13 = r
|   |   |   |   |   |   |   cell23 = dirt: even (0.0)
|   |   |   |   |   |   |   cell23 = b: even (3.0)
|   |   |   |   |   |   |   cell23 = w: lose (2.0)
|   |   |   |   |   |   |   cell23 = r: even (0.0)
|   |   |   |   cell64 = w
|   |   |   |   |   cell32 = dirt: even (0.0)
|   |   |   |   |   cell32 = b
|   |   |   |   |   |   cell15 = dirt: even (0.0)
|   |   |   |   |   |   cell15 = b: even (37.0/1.0)
|   |   |   |   |   |   cell15 = w
|   |   |   |   |   |   |   cell53 = dirt: win (0.0)
|   |   |   |   |   |   |   cell53 = b
|   |   |   |   |   |   |   |   cell44 = dirt: win (0.0)
|   |   |   |   |   |   |   |   cell44 = b
|   |   |   |   |   |   |   |   |   cell13 = dirt: win (0.0)
|   |   |   |   |   |   |   |   |   cell13 = b: even (9.0/1.0)
|   |   |   |   |   |   |   |   |   cell13 = w: win (2.0)
|   |   |   |   |   |   |   |   |   cell13 = r: win (8.0/1.0)
|   |   |   |   |   |   |   |   cell44 = w: win (0.0)
|   |   |   |   |   |   |   |   cell44 = r
|   |   |   |   |   |   |   |   |   cell45 = dirt: win (0.0)
|   |   |   |   |   |   |   |   |   cell45 = b: win (0.0)
|   |   |   |   |   |   |   |   |   cell45 = w: win (24.0)
|   |   |   |   |   |   |   |   |   cell45 = r
|   |   |   |   |   |   |   |   |   |   cell75 = dirt: win (0.0)
|   |   |   |   |   |   |   |   |   |   cell75 = b: lose (2.0)
|   |   |   |   |   |   |   |   |   |   cell75 = w
|   |   |   |   |   |   |   |   |   |   |   cell41 = dirt: win (0.0)
|   |   |   |   |   |   |   |   |   |   |   cell41 = b: even (3.0)
|   |   |   |   |   |   |   |   |   |   |   cell41 = w: win (0.0)
|   |   |   |   |   |   |   |   |   |   |   cell41 = r: win (3.0)
|   |   |   |   |   |   |   |   |   |   cell75 = r: win (6.0)
|   |   |   |   |   |   |   cell53 = w
|   |   |   |   |   |   |   |   cell35 = dirt: lose (0.0)
|   |   |   |   |   |   |   |   cell35 = b: lose (1.0)
|   |   |   |   |   |   |   |   cell35 = w: win (7.0)
|   |   |   |   |   |   |   |   cell35 = r: lose (7.0)
|   |   |   |   |   |   |   cell53 = r: lose (8.0)
|   |   |   |   |   |   cell15 = r
|   |   |   |   |   |   |   cell73 = dirt: even (0.0)
|   |   |   |   |   |   |   cell73 = b
|   |   |   |   |   |   |   |   cell33 = dirt: even (0.0)
|   |   |   |   |   |   |   |   cell33 = b
|   |   |   |   |   |   |   |   |   cell23 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   cell23 = b
|   |   |   |   |   |   |   |   |   |   cell34 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   cell34 = b
|   |   |   |   |   |   |   |   |   |   |   cell25 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   |   cell25 = b: win (6.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   cell25 = w: even (52.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   cell25 = r
|   |   |   |   |   |   |   |   |   |   |   |   cell42 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   |   |   cell42 = b: even (9.0/1.0)
|   |   |   |   |   |   |   |   |   |   |   |   cell42 = w: lose (3.0)
|   |   |   |   |   |   |   |   |   |   |   |   cell42 = r: even (0.0)
|   |   |   |   |   |   |   |   |   |   cell34 = w: win (7.0/2.0)
|   |   |   |   |   |   |   |   |   |   cell34 = r
|   |   |   |   |   |   |   |   |   |   |   cell75 = dirt: lose (0.0)
|   |   |   |   |   |   |   |   |   |   |   cell75 = b: lose (2.0)
|   |   |   |   |   |   |   |   |   |   |   cell75 = w: lose (0.0)
|   |   |   |   |   |   |   |   |   |   |   cell75 = r: even (2.0)
|   |   |   |   |   |   |   |   |   cell23 = w
|   |   |   |   |   |   |   |   |   |   cell12 = dirt: win (0.0)
|   |   |   |   |   |   |   |   |   |   cell12 = b: win (16.0/2.0)
|   |   |   |   |   |   |   |   |   |   cell12 = w: win (0.0)
|   |   |   |   |   |   |   |   |   |   cell12 = r: even (3.0)
|   |   |   |   |   |   |   |   |   cell23 = r
|   |   |   |   |   |   |   |   |   |   cell65 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   cell65 = b: even (0.0)
|   |   |   |   |   |   |   |   |   |   cell65 = w: even (8.0)
|   |   |   |   |   |   |   |   |   |   cell65 = r: win (4.0)
|   |   |   |   |   |   |   |   cell33 = w
|   |   |   |   |   |   |   |   |   cell74 = dirt: win (0.0)
|   |   |   |   |   |   |   |   |   cell74 = b: win (5.0)
|   |   |   |   |   |   |   |   |   cell74 = w: lose (2.0)
|   |   |   |   |   |   |   |   |   cell74 = r: win (0.0)
|   |   |   |   |   |   |   |   cell33 = r: lose (6.0)
|   |   |   |   |   |   |   cell73 = w: win (15.0/1.0)
|   |   |   |   |   |   |   cell73 = r
|   |   |   |   |   |   |   |   cell55 = dirt: lose (0.0)
|   |   |   |   |   |   |   |   cell55 = b: lose (0.0)
|   |   |   |   |   |   |   |   cell55 = w: even (4.0/1.0)
|   |   |   |   |   |   |   |   cell55 = r: lose (7.0)
|   |   |   |   |   cell32 = w
|   |   |   |   |   |   cell55 = dirt: win (0.0)
|   |   |   |   |   |   cell55 = b: win (0.0)
|   |   |   |   |   |   cell55 = w
|   |   |   |   |   |   |   cell73 = dirt: win (0.0)
|   |   |   |   |   |   |   cell73 = b: win (5.0/1.0)
|   |   |   |   |   |   |   cell73 = w: even (3.0)
|   |   |   |   |   |   |   cell73 = r: win (0.0)
|   |   |   |   |   |   cell55 = r: lose (3.0)
|   |   |   |   |   cell32 = r: lose (10.0)
|   |   |   |   cell64 = r
|   |   |   |   |   cell22 = dirt: even (0.0)
|   |   |   |   |   cell22 = b
|   |   |   |   |   |   cell13 = dirt: even (0.0)
|   |   |   |   |   |   cell13 = b
|   |   |   |   |   |   |   cell74 = dirt: even (0.0)
|   |   |   |   |   |   |   cell74 = b
|   |   |   |   |   |   |   |   cell25 = dirt: even (0.0)
|   |   |   |   |   |   |   |   cell25 = b
|   |   |   |   |   |   |   |   |   cell33 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   cell33 = b
|   |   |   |   |   |   |   |   |   |   cell62 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   |   cell62 = b: even (31.0/1.0)
|   |   |   |   |   |   |   |   |   |   cell62 = w: lose (2.0)
|   |   |   |   |   |   |   |   |   |   cell62 = r: even (4.0)
|   |   |   |   |   |   |   |   |   cell33 = w
|   |   |   |   |   |   |   |   |   |   cell63 = dirt: win (0.0)
|   |   |   |   |   |   |   |   |   |   cell63 = b: lose (2.0/1.0)
|   |   |   |   |   |   |   |   |   |   cell63 = w: win (0.0)
|   |   |   |   |   |   |   |   |   |   cell63 = r: win (2.0)
|   |   |   |   |   |   |   |   |   cell33 = r: even (0.0)
|   |   |   |   |   |   |   |   cell25 = w: even (38.0/5.0)
|   |   |   |   |   |   |   |   cell25 = r
|   |   |   |   |   |   |   |   |   cell65 = dirt: win (0.0)
|   |   |   |   |   |   |   |   |   cell65 = b: win (0.0)
|   |   |   |   |   |   |   |   |   cell65 = w: win (10.0/2.0)
|   |   |   |   |   |   |   |   |   cell65 = r: even (4.0/1.0)
|   |   |   |   |   |   |   cell74 = w
|   |   |   |   |   |   |   |   cell35 = dirt: lose (0.0)
|   |   |   |   |   |   |   |   cell35 = b: win (6.0)
|   |   |   |   |   |   |   |   cell35 = w: lose (5.0/1.0)
|   |   |   |   |   |   |   |   cell35 = r: lose (7.0)
|   |   |   |   |   |   |   cell74 = r
|   |   |   |   |   |   |   |   cell55 = dirt: even (0.0)
|   |   |   |   |   |   |   |   cell55 = b
|   |   |   |   |   |   |   |   |   cell33 = dirt: win (0.0)
|   |   |   |   |   |   |   |   |   cell33 = b
|   |   |   |   |   |   |   |   |   |   cell25 = dirt: win (0.0)
|   |   |   |   |   |   |   |   |   |   cell25 = b: even (6.0/1.0)
|   |   |   |   |   |   |   |   |   |   cell25 = w: win (12.0/1.0)
|   |   |   |   |   |   |   |   |   |   cell25 = r: win (0.0)
|   |   |   |   |   |   |   |   |   cell33 = w: win (0.0)
|   |   |   |   |   |   |   |   |   cell33 = r: lose (3.0)
|   |   |   |   |   |   |   |   cell55 = w
|   |   |   |   |   |   |   |   |   cell14 = dirt: lose (0.0)
|   |   |   |   |   |   |   |   |   cell14 = b: win (2.0/1.0)
|   |   |   |   |   |   |   |   |   cell14 = w: lose (5.0)
|   |   |   |   |   |   |   |   |   cell14 = r: lose (0.0)
|   |   |   |   |   |   |   |   cell55 = r: even (22.0/5.0)
|   |   |   |   |   |   cell13 = w
|   |   |   |   |   |   |   cell15 = dirt: even (0.0)
|   |   |   |   |   |   |   cell15 = b: even (0.0)
|   |   |   |   |   |   |   cell15 = w
|   |   |   |   |   |   |   |   cell14 = dirt: even (0.0)
|   |   |   |   |   |   |   |   cell14 = b: even (0.0)
|   |   |   |   |   |   |   |   cell14 = w: win (3.0)
|   |   |   |   |   |   |   |   cell14 = r
|   |   |   |   |   |   |   |   |   cell55 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   cell55 = b: even (15.0/1.0)
|   |   |   |   |   |   |   |   |   cell55 = w: even (0.0)
|   |   |   |   |   |   |   |   |   cell55 = r: lose (10.0)
|   |   |   |   |   |   |   cell15 = r: even (47.0)
|   |   |   |   |   |   cell13 = r
|   |   |   |   |   |   |   cell72 = dirt: win (0.0)
|   |   |   |   |   |   |   cell72 = b
|   |   |   |   |   |   |   |   cell45 = dirt: win (0.0)
|   |   |   |   |   |   |   |   cell45 = b: win (5.0)
|   |   |   |   |   |   |   |   cell45 = w: win (14.0)
|   |   |   |   |   |   |   |   cell45 = r: lose (17.0)
|   |   |   |   |   |   |   cell72 = w: even (2.0)
|   |   |   |   |   |   |   cell72 = r: win (0.0)
|   |   |   |   |   cell22 = w
|   |   |   |   |   |   cell65 = dirt: lose (0.0)
|   |   |   |   |   |   cell65 = b: lose (0.0)
|   |   |   |   |   |   cell65 = w
|   |   |   |   |   |   |   cell14 = dirt: lose (0.0)
|   |   |   |   |   |   |   cell14 = b: even (6.0)
|   |   |   |   |   |   |   cell14 = w
|   |   |   |   |   |   |   |   cell21 = dirt: lose (0.0)
|   |   |   |   |   |   |   |   cell21 = b
|   |   |   |   |   |   |   |   |   cell74 = dirt: even (0.0)
|   |   |   |   |   |   |   |   |   cell74 = b: lose (2.0)
|   |   |   |   |   |   |   |   |   cell74 = w: even (0.0)
|   |   |   |   |   |   |   |   |   cell74 = r: even (4.0)
|   |   |   |   |   |   |   |   cell21 = w: lose (7.0)
|   |   |   |   |   |   |   |   cell21 = r: lose (3.0)
|   |   |   |   |   |   |   cell14 = r: lose (0.0)
|   |   |   |   |   |   cell65 = r
|   |   |   |   |   |   |   cell73 = dirt: win (0.0)
|   |   |   |   |   |   |   cell73 = b: even (2.0)
|   |   |   |   |   |   |   cell73 = w: win (0.0)
|   |   |   |   |   |   |   cell73 = r: win (3.0)
|   |   |   |   |   cell22 = r
|   |   |   |   |   |   cell45 = dirt: lose (0.0)
|   |   |   |   |   |   cell45 = b: lose (0.0)
|   |   |   |   |   |   cell45 = w: lose (12.0)
|   |   |   |   |   |   cell45 = r: win (2.0)
|   |   |   cell71 = w
|   |   |   |   cell65 = dirt: win (0.0)
|   |   |   |   cell65 = b: win (0.0)
|   |   |   |   cell65 = w: lose (8.0/1.0)
|   |   |   |   cell65 = r
|   |   |   |   |   cell72 = dirt: win (0.0)
|   |   |   |   |   cell72 = b: win (0.0)
|   |   |   |   |   cell72 = w
|   |   |   |   |   |   cell23 = dirt: win (0.0)
|   |   |   |   |   |   cell23 = b
|   |   |   |   |   |   |   cell14 = dirt: win (0.0)
|   |   |   |   |   |   |   cell14 = b: even (3.0)
|   |   |   |   |   |   |   cell14 = w: win (0.0)
|   |   |   |   |   |   |   cell14 = r: win (4.0)
|   |   |   |   |   |   cell23 = w: win (0.0)
|   |   |   |   |   |   cell23 = r: win (12.0)
|   |   |   |   |   cell72 = r: even (9.0)
|   |   |   cell71 = r
|   |   |   |   cell54 = dirt: win (0.0)
|   |   |   |   cell54 = b
|   |   |   |   |   cell45 = dirt: win (0.0)
|   |   |   |   |   cell45 = b: even (4.0)
|   |   |   |   |   cell45 = w: win (4.0)
|   |   |   |   |   cell45 = r
|   |   |   |   |   |   cell63 = dirt: win (0.0)
|   |   |   |   |   |   cell63 = b
|   |   |   |   |   |   |   cell73 = dirt: win (0.0)
|   |   |   |   |   |   |   cell73 = b: win (0.0)
|   |   |   |   |   |   |   cell73 = w: win (8.0/2.0)
|   |   |   |   |   |   |   cell73 = r: even (2.0)
|   |   |   |   |   |   cell63 = w: win (0.0)
|   |   |   |   |   |   cell63 = r: win (14.0)
|   |   |   |   cell54 = w
|   |   |   |   |   cell20 = dirt: win (0.0)
|   |   |   |   |   cell20 = b
|   |   |   |   |   |   cell42 = dirt: win (0.0)
|   |   |   |   |   |   cell42 = b
|   |   |   |   |   |   |   cell70 = dirt: win (0.0)
|   |   |   |   |   |   |   cell70 = b: even (2.0)
|   |   |   |   |   |   |   cell70 = w: win (0.0)
|   |   |   |   |   |   |   cell70 = r: win (5.0)
|   |   |   |   |   |   cell42 = w: win (0.0)
|   |   |   |   |   |   cell42 = r: win (13.0)
|   |   |   |   |   cell20 = w: lose (2.0)
|   |   |   |   |   cell20 = r: win (0.0)
|   |   |   |   cell54 = r: lose (10.0)
|   |   cell52 = w
|   |   |   cell74 = dirt: even (0.0)
|   |   |   cell74 = b
|   |   |   |   cell41 = dirt: even (0.0)
|   |   |   |   cell41 = b
|   |   |   |   |   cell25 = dirt: even (0.0)
|   |   |   |   |   cell25 = b
|   |   |   |   |   |   cell33 = dirt: lose (0.0)
|   |   |   |   |   |   cell33 = b: even (2.0)
|   |   |   |   |   |   cell33 = w: lose (2.0)
|   |   |   |   |   |   cell33 = r: lose (0.0)
|   |   |   |   |   cell25 = w: even (21.0/1.0)
|   |   |   |   |   cell25 = r: lose (4.0)
|   |   |   |   cell41 = w: even (0.0)
|   |   |   |   cell41 = r
|   |   |   |   |   cell75 = dirt: lose (0.0)
|   |   |   |   |   cell75 = b: lose (0.0)
|   |   |   |   |   cell75 = w: win (6.0)
|   |   |   |   |   cell75 = r: lose (7.0)
|   |   |   cell74 = w
|   |   |   |   cell22 = dirt: lose (0.0)
|   |   |   |   cell22 = b: lose (25.0/3.0)
|   |   |   |   cell22 = w: lose (4.0)
|   |   |   |   cell22 = r: win (5.0)
|   |   |   cell74 = r
|   |   |   |   cell42 = dirt: even (0.0)
|   |   |   |   cell42 = b
|   |   |   |   |   cell21 = dirt: even (0.0)
|   |   |   |   |   cell21 = b
|   |   |   |   |   |   cell11 = dirt: even (0.0)
|   |   |   |   |   |   cell11 = b
|   |   |   |   |   |   |   cell70 = dirt: even (0.0)
|   |   |   |   |   |   |   cell70 = b: even (23.0/1.0)
|   |   |   |   |   |   |   cell70 = w: even (0.0)
|   |   |   |   |   |   |   cell70 = r
|   |   |   |   |   |   |   |   cell43 = dirt: win (0.0)
|   |   |   |   |   |   |   |   cell43 = b: even (2.0)
|   |   |   |   |   |   |   |   cell43 = w: win (0.0)
|   |   |   |   |   |   |   |   cell43 = r: win (5.0)
|   |   |   |   |   |   cell11 = w: win (1.0)
|   |   |   |   |   |   cell11 = r: win (2.0)
|   |   |   |   |   cell21 = w: win (3.0)
|   |   |   |   |   cell21 = r: even (0.0)
|   |   |   |   cell42 = w: win (4.0)
|   |   |   |   cell42 = r: even (0.0)
|   |   cell52 = r
|   |   |   cell55 = dirt: lose (0.0)
|   |   |   cell55 = b: lose (0.0)
|   |   |   cell55 = w
|   |   |   |   cell74 = dirt: lose (0.0)
|   |   |   |   cell74 = b
|   |   |   |   |   cell63 = dirt: lose (0.0)
|   |   |   |   |   cell63 = b: lose (12.0)
|   |   |   |   |   cell63 = w: win (2.0)
|   |   |   |   |   cell63 = r: lose (0.0)
|   |   |   |   cell74 = w: win (17.0)
|   |   |   |   cell74 = r: lose (20.0)
|   |   |   cell55 = r
|   |   |   |   cell25 = dirt: lose (0.0)
|   |   |   |   cell25 = b: lose (1.0)
|   |   |   |   cell25 = w: lose (35.0/4.0)
|   |   |   |   cell25 = r
|   |   |   |   |   cell24 = dirt: even (0.0)
|   |   |   |   |   cell24 = b: even (11.0)
|   |   |   |   |   cell24 = w: lose (3.0)
|   |   |   |   |   cell24 = r: even (0.0)
|   cell61 = w: lose (33.0/3.0)
|   cell61 = r
|   |   cell50 = dirt: win (0.0)
|   |   cell50 = b
|   |   |   cell24 = dirt: win (0.0)
|   |   |   cell24 = b
|   |   |   |   cell35 = dirt: win (0.0)
|   |   |   |   cell35 = b: even (7.0)
|   |   |   |   cell35 = w: win (0.0)
|   |   |   |   cell35 = r: win (8.0)
|   |   |   cell24 = w
|   |   |   |   cell65 = dirt: lose (0.0)
|   |   |   |   cell65 = b: lose (0.0)
|   |   |   |   cell65 = w: lose (10.0)
|   |   |   |   cell65 = r: win (4.0)
|   |   |   cell24 = r: win (50.0/4.0)
|   |   cell50 = w: lose (8.0)
|   |   cell50 = r: win (4.0)
cell31 = w
|   cell35 = dirt: lose (0.0)
|   cell35 = b: lose (0.0)
|   cell35 = w
|   |   cell15 = dirt: lose (0.0)
|   |   cell15 = b: even (1.0)
|   |   cell15 = w: lose (28.0)
|   |   cell15 = r
|   |   |   cell62 = dirt: lose (0.0)
|   |   |   cell62 = b
|   |   |   |   cell73 = dirt: lose (0.0)
|   |   |   |   cell73 = b: lose (20.0/2.0)
|   |   |   |   cell73 = w: lose (0.0)
|   |   |   |   cell73 = r
|   |   |   |   |   cell42 = dirt: even (0.0)
|   |   |   |   |   cell42 = b: even (16.0/1.0)
|   |   |   |   |   cell42 = w: even (0.0)
|   |   |   |   |   cell42 = r: lose (3.0)
|   |   |   cell62 = w: lose (16.0)
|   |   |   cell62 = r: lose (0.0)
|   cell35 = r
|   |   cell51 = dirt: win (0.0)
|   |   cell51 = b: win (22.0/1.0)
|   |   cell51 = w: win (0.0)
|   |   cell51 = r: lose (8.0)
cell31 = r
|   cell75 = dirt: win (0.0)
|   cell75 = b: win (0.0)
|   cell75 = w: win (42.0/4.0)
|   cell75 = r
|   |   cell73 = dirt: win (0.0)
|   |   cell73 = b: lose (2.0)
|   |   cell73 = w: win (16.0)
|   |   cell73 = r: lose (8.0)
     """
}