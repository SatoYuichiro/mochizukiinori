package jp.satoyuichiro.inori.connect4

import org.ggp.base.util.statemachine.implementation.prover.ProverStateMachine
import jp.satoyuichiro.inori.gdl.Description
import jp.satoyuichiro.inori.gdl.GdlUtil
import jp.satoyuichiro.inori.player.AlphaBetaPlayer
import jp.satoyuichiro.inori.player.HeuristicsPlayer
import jp.satoyuichiro.inori.player.Heuristics
import jp.satoyuichiro.inori.player.RandomPlayer
import jp.satoyuichiro.inori.simulator.GameSimulator
import jp.satoyuichiro.inori.learning.concept.generator._
import jp.satoyuichiro.inori.ilp.AlephUtil
import java.io.File
import jp.satoyuichiro.inori.cdl._
import jp.satoyuichiro.inori.learning.concept.specifier.Specialization
import jp.satoyuichiro.inori.learning.concept.specifier.SpecifiedHeuristics
import jp.satoyuichiro.inori.cdl.LeafExp
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardOpenOption
import jp.satoyuichiro.inori.learning.concept.adapter.BlankFilter
import jp.satoyuichiro.inori.player.Player
import jp.satoyuichiro.inori.connect4.Connect4Simulation._

object Connect4Simulation2 {

  def main(args: Array[String]): Unit = {
    fileName = "connect4VsAlpha3.txt"
    if (!args.isEmpty) {
      fileName = args.head + fileName
    }
    println("start simulation")
    alphaVsAlpha()
  }
  
  def alphaVsAlpha() = {
    val n = 100
    concepts = putName(AlephUtil.conceptDeserialize(new File("./concepts/concepts-all.txt")))
    Files.write(Paths.get(fileName), Array.empty[Byte])
    output("search depth 1 vs alpha1\n")
	doSimulateNoHeuristicsNoHeuristics(connect4gdl, n, 1)
	  for (i <- 1 to 20) {
	    doSimulateAlphaBetaNoHeuristics(connect4gdl, n, 1, i)
	}
    output("search depth 3 vs alpha1\n")
	doSimulateNoHeuristicsNoHeuristics(connect4gdl, n, 3)
    for (i <- 1 to 20) {
      doSimulateAlphaBetaNoHeuristics(connect4gdl, n, 3, i)
    }
  }
  
  def doSimulateNoHeuristicsNoHeuristics(gdl: String, n: Int, depth: Int): Unit = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(gdl).getDescription)
    val roles = GdlUtil.toScalaList(stateMachine.getRoles())
    val players = List(new AlphaBetaPlayer(stateMachine, roles.head, depth),
        new AlphaBetaPlayer(stateMachine, roles.tail.head, 1))
    val simulator = GameSimulator(stateMachine, players)
    
    val results = simulator.simulateMatch(n)
    
    val firstWin = results filter (_.getOrElse(players.head, false)) size
    val secondWin = results filter (_.getOrElse(players.tail.head, false)) size
    val drawN = n - firstWin - secondWin
    
    output(players.head.role + " " + firstWin + " ")
    output(players.tail.head.role + " " + secondWin + " ")
    output("draw " + drawN + " n " + n + "\n")
  }
  
  def doSimulateAlphaBetaNoHeuristics(gdl: String, n: Int, depth: Int, i: Int): Unit = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(gdl).getDescription)
    val roles = GdlUtil.toScalaList(stateMachine.getRoles())
    val players = List(new HeuristicsPlayer(makeHeuristics(i), stateMachine, roles.head, depth),
        new AlphaBetaPlayer(stateMachine, roles.tail.head, 1))
    val simulator = GameSimulator(stateMachine, players)
    
    val results = simulator.simulateMatch(n)
    
    val firstWin = results filter (_.getOrElse(players.head, false)) size
    val secondWin = results filter (_.getOrElse(players.tail.head, false)) size
    val drawN = n - firstWin - secondWin
    
    output(i + " ")
    output(players.head.role + " " + firstWin + " ")
    output(players.tail.head.role + " " + secondWin + " ")
    output("draw " + drawN + " n " + n + "\n")
   }
}