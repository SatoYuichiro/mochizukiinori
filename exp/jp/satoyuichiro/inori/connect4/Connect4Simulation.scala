package jp.satoyuichiro.inori.connect4

import org.ggp.base.util.statemachine.implementation.prover.ProverStateMachine
import jp.satoyuichiro.inori.gdl.Description
import jp.satoyuichiro.inori.gdl.GdlUtil
import jp.satoyuichiro.inori.player.AlphaBetaPlayer
import jp.satoyuichiro.inori.player.HeuristicsPlayer
import jp.satoyuichiro.inori.player.Heuristics
import jp.satoyuichiro.inori.player.RandomPlayer
import jp.satoyuichiro.inori.simulator.GameSimulator
import jp.satoyuichiro.inori.learning.concept.generator._
import jp.satoyuichiro.inori.ilp.AlephUtil
import java.io.File
import jp.satoyuichiro.inori.cdl._
import jp.satoyuichiro.inori.learning.concept.specifier.Specialization
import jp.satoyuichiro.inori.learning.concept.specifier.SpecifiedHeuristics
import jp.satoyuichiro.inori.cdl.LeafExp
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardOpenOption
import jp.satoyuichiro.inori.learning.concept.adapter.BlankFilter
import jp.satoyuichiro.inori.player.Player

object Connect4Simulation {

  val connect4gdl = "./games/twoplayers/connect4.gdl"
  val eval = 1.0
  var concepts = List.empty[PrimeConcept]
  var fileName = "connect4ConjSim.txt"
  
  def main(args: Array[String]): Unit = {
    if (!args.isEmpty) {
      fileName = args.head + fileName
    }
    println("start simulation")
    disjVsRandom()
  }
  
  def alphaVsRandom() = {
    val n = 100
    concepts = putName(AlephUtil.conceptDeserialize(new File("./concepts/concepts-all.txt")))
    Files.write(Paths.get(fileName), Array.empty[Byte])
    output("search depth 1 vs random\n")
    //doSimulateNoHeuristicsRandom(connect4gdl, n, 1)
//    for (i <- 1 to 20) {
//      doSimulateHeuristicsRandom(connect4gdl, n, 1, i)
//    }
    output("search depth 3 vs random\n")
    for (i <- 1 to 20) {
      doSimulateHeuristicsRandom(connect4gdl, n, 3, i)
    }
  }
  
  def disjVsRandom() = {
    val n = 100
    concepts = putName(AlephUtil.conceptDeserialize(new File("./concepts/concepts-all.txt")))
    Files.write(Paths.get(fileName), Array.empty[Byte])
    output("disjunction search depth 1 vs random\n")
//    for (i <- 1 to 10) {
//      doSimulateDisjunctionRandom(connect4gdl, n, 1, i)
//    }
    output("disjunction search depth 3 vs random\n")
    for (i <- 1 to 10) {
      doSimulateHeuristicsRandom(connect4gdl, n, 3, i)
    }
  }
  
  def alphaVsAlpha() = {
    val n = 100
    output("search depth 1 \n")
    doSimulateNoHeuristicsAlphaBeta(connect4gdl, n, 1)
    for (i <- 1 to 20) {
      doSimulateAlphaBeta(connect4gdl, n, 1, i)
    }
    output("search depth 3 \n")
    doSimulateNoHeuristicsAlphaBeta(connect4gdl, n, 3)
    for (i <- 1 to 20) {
      doSimulateAlphaBeta(connect4gdl, n, 3, i)
    }
  }
  
  def output(str: String): Unit = {
    Files.write(Paths.get(fileName), AlephUtil.toArrayChar(str), StandardOpenOption.APPEND)
  }
  
  def putName(rich: List[RichConcept]): List[PrimeConcept] = {
    val labeler = GeneratorUtil.Labeler.makeLabeler("concept")
    rich map (_.putLabel(labeler())) collect { case c: PrimeConcept => c }
  }
  
  def doSimulateNoHeuristicsRandom(gdl: String, n: Int, depth: Int): Unit = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(gdl).getDescription)
    val roles = GdlUtil.toScalaList(stateMachine.getRoles())
    val players = List(new AlphaBetaPlayer(stateMachine, roles.head, depth),
        new RandomPlayer(stateMachine, roles.tail.head))
    val simulator = GameSimulator(stateMachine, players)
    
    println("start simulation " + n)
    val dn = n / 10
    var results = List.empty[Map[Player, Boolean]]
    for (i <- 0 to 10) {
      println(i)
      results ++= simulator.simulateMatch(dn)
    }
    
    val firstWin = results filter (_.getOrElse(players.head, false)) size
    val secondWin = results filter (_.getOrElse(players.tail.head, false)) size
    val drawN = n - firstWin - secondWin
    
    output(players.head.role + " " + firstWin + " ")
    output(players.tail.head.role + " " + secondWin + " ")
    output("draw " + drawN + " n " + n + "\n")
   }
  
  def doSimulateHeuristicsRandom(gdl: String, n: Int, depth: Int, i: Int): Unit = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(gdl).getDescription)
    val roles = GdlUtil.toScalaList(stateMachine.getRoles())
    val players = List(new HeuristicsPlayer(makeHeuristics(i), stateMachine, roles.head, depth),
        new RandomPlayer(stateMachine, roles.tail.head))
    val simulator = GameSimulator(stateMachine, players)
    
    val start = System.currentTimeMillis()
    println("start simulation of " + i + " for " + n + " times")
    val dn = n / 10
    var results = List.empty[Map[Player, Boolean]]
    for (i <- 1 to 10) {
      results ++= simulator.simulateMatch(dn)
      print((i * 10) + "% ")
    }
    
    println("\n" + results.size + " times simulation is done, " + ((System.currentTimeMillis() - start) / (60 * 1000)) + "min")
    
    val firstWin = results filter (_.getOrElse(players.head, false)) size
    val secondWin = results filter (_.getOrElse(players.tail.head, false)) size
    val drawN = n - firstWin - secondWin
    
    output(i + " ")
    output(players.head.role + " " + firstWin + " ")
    output(players.tail.head.role + " " + secondWin + " ")
    output("draw " + drawN + " n " + n + "\n")
  }
    
  def doSimulateDisjunctionRandom(gdl: String, n: Int, depth: Int, i: Int): Unit = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(gdl).getDescription)
    val roles = GdlUtil.toScalaList(stateMachine.getRoles())
    val players = List(new HeuristicsPlayer(makeHeuristics(i), stateMachine, roles.head, depth),
        new RandomPlayer(stateMachine, roles.tail.head))
    val simulator = GameSimulator(stateMachine, players)
    
    val start = System.currentTimeMillis()
    println("start simulation of " + i + " for " + n + " times")
    val dn = n / 10
    var results = List.empty[Map[Player, Boolean]]
    for (i <- 1 to 10) {
      results ++= simulator.simulateMatch(dn)
      print((i * 10) + "% ")
    }
    
    println("\n" + results.size + " times simulation is done, " + ((System.currentTimeMillis() - start) / (60 * 1000)) + "min")
    
    val firstWin = results filter (_.getOrElse(players.head, false)) size
    val secondWin = results filter (_.getOrElse(players.tail.head, false)) size
    val drawN = n - firstWin - secondWin
    
    output(i + " ")
    output(players.head.role + " " + firstWin + " ")
    output(players.tail.head.role + " " + secondWin + " ")
    output("draw " + drawN + " n " + n + "\n")
  }
  
  def doSimulateNoHeuristicsAlphaBeta(gdl: String, n: Int, depth: Int): Unit = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(gdl).getDescription)
    val roles = GdlUtil.toScalaList(stateMachine.getRoles())
    val players = List(new AlphaBetaPlayer(stateMachine, roles.head, depth),
        new AlphaBetaPlayer(stateMachine, roles.tail.head, depth))
    val simulator = GameSimulator(stateMachine, players)
    
    val results = simulator.simulateMatch(n)
    
    val firstWin = results filter (_.getOrElse(players.head, false)) size
    val secondWin = results filter (_.getOrElse(players.tail.head, false)) size
    val drawN = n - firstWin - secondWin
    
    output(players.head.role + " " + firstWin + " ")
    output(players.tail.head.role + " " + secondWin + " ")
    output("draw " + drawN + " n " + n + "\n")
   }
  
  def doSimulateAlphaBeta(gdl: String, n: Int, depth: Int, i: Int): Unit = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(gdl).getDescription)
    val roles = GdlUtil.toScalaList(stateMachine.getRoles())
    val players = List(new HeuristicsPlayer(makeHeuristics(i), stateMachine, roles.head, depth),
        new AlphaBetaPlayer(stateMachine, roles.tail.head, depth))
    val simulator = GameSimulator(stateMachine, players)
    
    val results = simulator.simulateMatch(n)
    
    val firstWin = results filter (_.getOrElse(players.head, false)) size
    val secondWin = results filter (_.getOrElse(players.tail.head, false)) size
    val drawN = n - firstWin - secondWin
    
    output(i + " ")
    output(players.head.role + " " + firstWin + " ")
    output(players.tail.head.role + " " + secondWin + " ")
    output("draw " + drawN + " n " + n + "\n")
  }
  
  def makeHeuristics(i: Int): Heuristics = {
    val cell = Symbol("cell")
    val w = Symbol("w")
    val r = Symbol("r")
    val b = Symbol("b")
    val one = Integer(1)
    val two = Integer(2)
    val thr = Integer(3)
    val fur = Integer(4)
    val fiv = Integer(5)
    val six = Integer(6)
    val sev = Integer(7)
    
    i match {
      case 1 => Heuristics(BlankFilter(b))
      case 2 => Heuristics(BlankFilter(b),
          makeSpecified(11, makeSp(cell, one, thr, w)))
      case 3 => Heuristics(BlankFilter(b),
          makeSpecified(11, makeSp(cell, one, thr, w)))
      case 4 => Heuristics(BlankFilter(b),
          makeSpecified(11, makeSp(cell, one, thr, w)),
          makeSpecified(9, makeSp(cell, one, one, w)))
      case 5 => Heuristics(BlankFilter(b),
          makeSpecified(6, makeSp(cell, fiv, thr, w)),
          makeSpecified(20, makeSp(cell, fur, two, r)))
      
      case 6 => Heuristics(BlankFilter(b),
          makeSpecified(4, makeSp(cell, fiv, thr, w)),
          makeSpecified(20, makeSp(cell, fur, two, r)))
      case 7 => Heuristics(BlankFilter(b),
          makeSpecified(4, makeSp(cell, fiv, thr, w)),
          makeSpecified(20, makeSp(cell, fur, two, r)))
      case 8 => Heuristics(BlankFilter(b, (fiv, two), (fiv, fur)),
          makeSpecified(20, makeSp(cell, fur, one, w)),
          makeSpecified(13, makeSp(cell, fiv, two, b)),
          makeSpecified(20, makeSp(cell, fur, fiv, w)))
      case 9 => Heuristics(BlankFilter(b, (fiv, two), (fiv, fur)),
          makeSpecified(11, makeSp(cell, fur, one, w)),
          makeSpecified(13, makeSp(cell, fiv, two, b)),
          makeSpecified(4, makeSp(cell, six, two, r)))
      case 10 => Heuristics(BlankFilter(b, (fiv, two), (fiv, fur)),
          makeSpecified(6, makeSp(cell, fiv, thr, w)),
          makeSpecified(13, makeSp(cell, fiv, two, b)),
          makeSpecified(8, makeSp(cell, thr, thr, w)))
      
      case 11 => Heuristics(BlankFilter(b, (fiv, two), (fiv, fur)),
          makeSpecified(6, makeSp(cell, fiv, thr, w)),
          makeSpecified(13, makeSp(cell, fiv, two, b)),
          makeSpecified(8, makeSp(cell, thr, thr, w)))
      case 12 => Heuristics(BlankFilter(b, (six, one), (six, thr)),
          makeSpecified(11, makeSp(cell, fiv, one, w)),
          makeSpecified(13, makeSp(cell, six, one, b)),
          makeSpecified(4, makeSp(cell, six, fur, w)))
      case 13 => Heuristics(BlankFilter(b, (six, one), (six, thr)),
          makeSpecified(11, makeSp(cell, fiv, one, w)),
          makeSpecified(13, makeSp(cell, six, one, b)),
          makeSpecified(13, makeSp(cell, six, one, r)),
          makeSpecified(4, makeSp(cell, six, fur, w)))
      case 14 => Heuristics(BlankFilter(b, (fiv, two), (fiv, fur)),
          makeSpecified(6, makeSp(cell, fiv, thr, w)),
          makeSpecified(13, makeSp(cell, fiv, two, b)),
          makeSpecified(9, makeSp(cell, one, two, r)),
          makeSpecified(8, makeSp(cell, two, two, w)),
          makeSpecified(4, makeSp(cell, sev, fur, r)))
      case 15 => Heuristics(BlankFilter(b),
          makeSpecified(11, makeSp(cell, one, thr, w)),
          makeSpecified(13, makeSp(cell, six, one, r)),
          makeSpecified(6, makeSp(cell, fiv, thr, w)),
          makeSpecified(10, makeSp(cell, one, two, r)))
      
      case 16 => Heuristics(BlankFilter(b),
          makeSpecified(11, makeSp(cell, one, thr, w)),
          makeSpecified(13, makeSp(cell, six, one, r)),
          makeSpecified(6, makeSp(cell, fiv, thr, w)),
          makeSpecified(10, makeSp(cell, one, two, r)))
      case 17 => Heuristics(BlankFilter(b),
          makeSpecified(11, makeSp(cell, one, thr, w)),
          makeSpecified(13, makeSp(cell, six, one, r)),
          makeSpecified(70, makeSp(cell, two, two, r)),
          makeSpecified(10, makeSp(cell, fiv, two, r)))
      case 18 => Heuristics(BlankFilter(b),
          makeSpecified(11, makeSp(cell, one, thr, w)),
          makeSpecified(13, makeSp(cell, six, one, r)),
          makeSpecified(6, makeSp(cell, fiv, thr, w)),
          makeSpecified(10, makeSp(cell, one, two, r)),
          makeSpecified(70, makeSp(cell, two, two, r)))
      case 19 => Heuristics(BlankFilter(b, (thr, two), (two, two)),
          makeSpecified(11, makeSp(cell, one, thr, w)),
          makeSpecified(8, makeSp(cell, thr, two, b)),
          makeSpecified(6, makeSp(cell, fiv, thr, w)),
          makeSpecified(10, makeSp(cell, one, two, r)),
          makeSpecified(4, makeSp(cell, thr, two, r)))
      case 20 => Heuristics(BlankFilter(b, (thr, two), (two, two)),
          makeSpecified(11, makeSp(cell, one, thr, w)),
          makeSpecified(8, makeSp(cell, thr, two, b)),
          makeSpecified(6, makeSp(cell, fiv, thr, w)),
          makeSpecified(10, makeSp(cell, one, two, r)),
          makeSpecified(4, makeSp(cell, thr, two, r)))
    }
  }
  
  /*
   * concept8 = piece(x0, x1, x2, x3), piece(x0, (+ x1 -1), x2, x3)
   * concept13 = piece(x0,x1,x2,x3), piece(x0, x1, (+ x2 2), x3)
   * 
   */
  
  def makeDisjunctionHeuristics(i: Int): Heuristics = {
    val cell = Symbol("cell")
    val w = Symbol("w")
    val r = Symbol("r")
    val b = Symbol("b")
    val one = Integer(1)
    val two = Integer(2)
    val thr = Integer(3)
    val fur = Integer(4)
    val fiv = Integer(5)
    val six = Integer(6)
    val sev = Integer(7)
    
    i match {
      case 1 => Heuristics(BlankFilter(b))
      case 2 => Heuristics(BlankFilter(b),
          makeSpecifiedDisjunction(makeSp(cell, thr, fur, w)))
      case 3 => Heuristics(BlankFilter(b),
          makeSpecifiedDisjunction(makeSp(cell, one, thr, w)))
      case 4 => Heuristics(BlankFilter(b),
          makeSpecifiedDisjunction(makeSp(cell, one, thr, w)),
          makeSpecified(9, makeSp(cell, one, one, w)))
      case 5 => Heuristics(BlankFilter(b, 
          (thr,one), (thr,two), (thr,thr), (thr,fur),
          (fur,one), (fur,two), (fur,thr), (fur,fur),
          (fiv,one), (fiv,two), (fiv,thr), (fiv,fur),
          (six,one), (six,two), (six,thr), (six,fur),
          (sev,one), (sev,two), (sev,thr), (sev,fur)),
          makeSpecified(6, makeSp(cell, fiv, thr, w)),
          makeSpecifiedDisjunction(makeSp(cell, fiv, two, b)))     

      case 6 => Heuristics(BlankFilter(b),
          makeSpecified(22, makeSp(cell, fur, two, w)),
          makeSpecified(9, makeSp(cell, one, one, w)))
      case 7 => Heuristics(BlankFilter(b),
          makeSpecifiedDisjunction(makeSp(cell, one, thr, w)),
          makeSpecified(9, makeSp(cell, one, one, w)),
          makeSpecified(22, makeSp(cell, six, fur, w)))
      case 8 => Heuristics(BlankFilter(b),
          makeSpecified(22, makeSp(cell, fur, two, w)),
          makeSpecifiedDisjunction(makeSp(cell, one, thr, w)))
      case 9 => Heuristics(BlankFilter(b),
          makeSpecifiedDisjunction(makeSp(cell, one, thr, w)),
          makeSpecified(16, makeSp(cell, two, two, r)))
      case 10 => Heuristics(BlankFilter(b,
          (thr,one), (thr,two), (thr,thr), (thr,fur),
          (fur,one), (fur,two), (fur,thr), (fur,fur),
          (fiv,one), (fiv,two), (fiv,thr), (fiv,fur),
          (six,one), (six,two), (six,thr), (six,fur),
          (sev,one), (sev,two), (sev,thr), (sev,fur)),
          makeSpecified(6, makeSp(cell, fiv, thr, w)),
          makeSpecifiedDisjunction(makeSp(cell, fiv, two, b)),
          makeSpecified(22, makeSp(cell, thr, two, w)))
    }
  }
  
  val disjunctionIndex = List(0, 1, 3, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16, 17, 18, 20, 22)
  
  def getDisjunction(): CompositeDisjunction = {
    CompositeDisjunction(disjunctionIndex map pickUpConcept).putLabel(Label("disj"))
  }
  
  def makeSpecifiedDisjunction(sp: Specialization): PrimeHeuristics = (new SpecifiedHeuristics(getDisjunction(), sp, eval)).toHeuristics
  
  def makePrimeHeuristics(i: Int): PrimeHeuristics = PrimeHeuristics(pickUpConcept(i), eval)

  val x0 = Label("x0")
  val x1 = Label("x1")
  val x2 = Label("x2")
  val x3 = Label("x3")
    
  def makeSp(s1: LeafExp, i1: LeafExp, i2: LeafExp, s2: LeafExp): Specialization = new Specialization(Map(x0 -> s1, x1 -> i1, x2 -> i2, x3 -> s2))
  
  def makeSpecified(i: Int, sp: Specialization): PrimeHeuristics = (new SpecifiedHeuristics(pickUpConcept(i), sp, eval)).toHeuristics
  
  def pickUpConcept(i: Int): PrimeConcept = concepts.find(_.conceptName.getOrElse(Label("")).label == ("concept" + i.toString)).get
}