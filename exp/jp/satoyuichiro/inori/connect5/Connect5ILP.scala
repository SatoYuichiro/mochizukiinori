package jp.satoyuichiro.inori.connect5

import jp.satoyuichiro.inori.simulator.ProverGameSimulator
import jp.satoyuichiro.inori.ilp.AlephUtil
import java.io.File
import jp.satoyuichiro.inori.ilp.AlephEncoder
import jp.satoyuichiro.inori.cdl._
import jp.satoyuichiro.inori.ilp.AlephConceptEncoder
import jp.satoyuichiro.inori.learning.concept.generator.RichConcept
import jp.satoyuichiro.inori.learning.concept.generator.PrimeConcept
import jp.satoyuichiro.inori.learning.concept.generator.GeneratorUtil
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardOpenOption
import jp.satoyuichiro.inori.learning.concept.adapter.SuccAdapter
import jp.satoyuichiro.inori.learning.concept.adapter.SuccRelationGenerator
import jp.satoyuichiro.inori.gdl.Description

object Connect5ILP {
  
  val dir = "./ILPworkspace/connect5/"
  
  def main(args: Array[String]): Unit = {
    makeEncode()
    //encodeDisjunction()
  }
  
  def makePlayout(): Unit = {
    val connect5Gdl = "./games/twoplayers/connect5Copy.gdl"
    val proverSimulator = new ProverGameSimulator(new File(connect5Gdl))
    val desc = Description(connect5Gdl)
    val adapter = new SuccAdapter((new SuccRelationGenerator()).generate(desc.description))
    
    for (i <- 0 to 19) {
      println(i)
      val wins = proverSimulator.simulateFirstWinPlayout(1) map (p => adapter.convert(p))
      println(wins.size)
      val loses = proverSimulator.simulateSecondWinPlayout(1) map (p => adapter.convert(p))
      println(loses.size)
      println()
      println(wins)
      println(loses)
      AlephUtil.positionSerialize(wins, dir + "playout_wins_" + i.toString + ".txt")
      AlephUtil.positionSerialize(loses, dir + "playout_loses_" + i.toString + ".txt")
    }
  }
  
  def makeEncode(): Unit = {
    val output = "connect5"
    val alephEncoder = new AlephEncoder

    var wins = List.empty[Position]
    var loses = List.empty[Position]
    val all = putName(AlephUtil.conceptDeserialize(new File("./concepts/concepts-all.txt")))
    val concepts = List(pickUpConcept(4,all), pickUpConcept(6,all), pickUpConcept(8,all), pickUpConcept(9,all),
        pickUpConcept(10,all), pickUpConcept(11,all), pickUpConcept(13,all), pickUpConcept(20,all), pickUpConcept(70,all))
    
    for (i <- 0 to 19) {
      wins ++= AlephUtil.positionDeserialize(new File(dir + "playout_wins_" + i.toString + ".txt"))
      loses ++= AlephUtil.positionDeserialize(new File(dir + "playout_loses_" + i.toString + ".txt"))
      val fileName = output + "_" + (1 + i).toString
      alephEncoder.encode(wins, loses, fileName)
      val encoder = new AlephConceptEncoder(Array(Symbol("cell"), Integer(1), Integer(1), Symbol("blank")))
      encoder.encode(concepts, fileName)
      encodeSetting(fileName)
    }
  }
  
  def putName(rich: List[RichConcept]): List[PrimeConcept] = {
    val labeler = GeneratorUtil.Labeler.makeLabeler("concept")
    rich map (_.putLabel(labeler())) collect { case c: PrimeConcept => c }
  }

  def encodeSetting(fileName: String): Unit = {
    val setLimit = ":- set(nodes,500000).\n"
    val setLength = ":- set(clauselength,5).\n"
    val str = "\n" + setLimit + setLength
    Files.write(Paths.get("./ILPworkspace/" + fileName + ".b"), AlephUtil.toArrayChar(str), StandardOpenOption.APPEND)
  }
  
  def encodeDisjunction(): Unit = {
    /*
concept12 2 (piece x0 x1 x2 x3)
concept0 2 (piece x0 x1 x2 x3)
concept10 2 (piece x0 x1 x2 x3)
concept18 2 (piece x0 x1 x2 x3)
concept1 2 (piece x0 x1 x2 x3)
concept22 2 (piece x0 x1 x2 x3)
concept20 2 (piece x0 x1 x2 x3)
concept7 2 (piece x0 x1 x2 x3)
concept9 2 (piece x0 x1 x2 x3)
concept13 2 (piece x0 x1 x2 x3)
concept11 2 (piece x0 x1 x2 x3)
concept17 2 (piece x0 x1 x2 x3)
concept14 2 (piece x0 x1 x2 x3)
concept6 2 (piece x0 x1 x2 x3)
concept8 2 (piece x0 x1 x2 x3)
concept3 2 (piece x0 x1 x2 x3)
concept16 2 (piece x0 x1 x2 x3)
     */
    val output = "connect4dsj"
    val alephEncoder = new AlephEncoder

    var wins = List.empty[Position]
    var loses = List.empty[Position]
    val allConcepts = putName(AlephUtil.conceptDeserialize(new File("./concepts/concepts-all.txt")))
    
    val condition = (name: String) => name == "concept12" ||
name == "concept0" ||
name == "concept10" ||
name == "concept18" ||
name == "concept1" ||
name == "concept22" ||
name == "concept20" ||
name == "concept7" ||
name == "concept9" ||
name == "concept13" ||
name == "concept11" ||
name == "concept17" ||
name == "concept14" ||
name == "concept6" ||
name == "concept8" ||
name == "concept3" ||
name == "concept16"

    val concepts = allConcepts.filter(c => condition(c.conceptName.getOrElse(Label("hoge")).label))
    
    for (i <- 0 to 19) {
      wins ++= AlephUtil.positionDeserialize(new File(dir + "playout_wins_" + i.toString + ".txt"))
      loses ++= AlephUtil.positionDeserialize(new File(dir + "playout_loses_" + i.toString + ".txt"))
      val fileName = output + "_" + (1 + i).toString
      alephEncoder.encode(wins, loses, fileName)
      val encoder = new AlephConceptEncoder(Array(Symbol("cell"), Integer(1), Integer(1), Symbol("blank")))
      encoder.encode(concepts, fileName)
      encodeSetting(fileName)
      
      val head = ":- modeb(1, disj(+piece,+piece,#symbol,#int,#int,#symbol)). \n" +
        ":- determination(example/1, disj/6).\n"
      val disj = 
"disj(P1,P2,X0,X1,X2,X3):- concept12(P1,P2,X0,X1,X2,X3).\n" +
"disj(P1,P2,X0,X1,X2,X3):- concept0(P1,P2,X0,X1,X2,X3).\n" +
"disj(P1,P2,X0,X1,X2,X3):- concept10(P1,P2,X0,X1,X2,X3).\n" +
"disj(P1,P2,X0,X1,X2,X3):- concept18(P1,P2,X0,X1,X2,X3).\n" +
"disj(P1,P2,X0,X1,X2,X3):- concept1(P1,P2,X0,X1,X2,X3).\n" +
"disj(P1,P2,X0,X1,X2,X3):- concept22(P1,P2,X0,X1,X2,X3).\n" +
"disj(P1,P2,X0,X1,X2,X3):- concept20(P1,P2,X0,X1,X2,X3).\n" +
"disj(P1,P2,X0,X1,X2,X3):- concept7(P1,P2,X0,X1,X2,X3).\n" +
"disj(P1,P2,X0,X1,X2,X3):- concept9(P1,P2,X0,X1,X2,X3).\n" +
"disj(P1,P2,X0,X1,X2,X3):- concept13(P1,P2,X0,X1,X2,X3).\n" +
"disj(P1,P2,X0,X1,X2,X3):- concept11(P1,P2,X0,X1,X2,X3).\n" +
"disj(P1,P2,X0,X1,X2,X3):- concept17(P1,P2,X0,X1,X2,X3).\n" +
"disj(P1,P2,X0,X1,X2,X3):- concept14(P1,P2,X0,X1,X2,X3).\n" +
"disj(P1,P2,X0,X1,X2,X3):- concept6(P1,P2,X0,X1,X2,X3).\n" +
"disj(P1,P2,X0,X1,X2,X3):- concept8(P1,P2,X0,X1,X2,X3).\n" +
"disj(P1,P2,X0,X1,X2,X3):- concept3(P1,P2,X0,X1,X2,X3).\n" +
"disj(P1,P2,X0,X1,X2,X3):- concept16(P1,P2,X0,X1,X2,X3).\n"
      Files.write(Paths.get("./ILPworkspace/" + fileName + ".b"), AlephUtil.toArrayChar(head + disj), StandardOpenOption.APPEND)
    }
  }
  
  def pickUpConcept(i: Int, concepts: List[PrimeConcept]): PrimeConcept = concepts.find(_.conceptName.getOrElse(Label("")).label == ("concept" + i.toString)).get
}