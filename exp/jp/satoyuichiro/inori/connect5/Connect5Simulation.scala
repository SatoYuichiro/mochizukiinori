package jp.satoyuichiro.inori.connect5

import org.ggp.base.util.statemachine.implementation.prover.ProverStateMachine
import jp.satoyuichiro.inori.gdl.Description
import jp.satoyuichiro.inori.gdl.GdlUtil
import jp.satoyuichiro.inori.player.AlphaBetaPlayer
import jp.satoyuichiro.inori.player.RandomPlayer
import jp.satoyuichiro.inori.player.HeuristicsPlayer
import jp.satoyuichiro.inori.player.Heuristics
import jp.satoyuichiro.inori.simulator.GameSimulator
import jp.satoyuichiro.inori.learning.concept.generator._
import jp.satoyuichiro.inori.ilp.AlephUtil
import java.io.File
import jp.satoyuichiro.inori.cdl._
import jp.satoyuichiro.inori.learning.concept.specifier.Specialization
import jp.satoyuichiro.inori.learning.concept.specifier.SpecifiedHeuristics
import jp.satoyuichiro.inori.cdl.LeafExp
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardOpenOption
import jp.satoyuichiro.inori.learning.concept.adapter.BlankFilter
import jp.satoyuichiro.inori.player.Player
import jp.satoyuichiro.inori.learning.concept.adapter.SuccAdapter
import jp.satoyuichiro.inori.learning.concept.adapter.SuccRelationGenerator
import jp.satoyuichiro.inori.learning.concept.adapter.SuccBlankFilter

object Connect5Simulation {


  val connect5gdl = "./games/twoplayers/connect5Copy.gdl"
  val eval = 1.0
  var concepts = List.empty[PrimeConcept]
  var fileName = "connect5ConjSim.txt"
  
  val desc = Description(connect5gdl)
  val succ = new SuccAdapter((new SuccRelationGenerator()).generate(desc.description))
  
  def main(args: Array[String]): Unit = {
    if (!args.isEmpty) {
      fileName = args.head + fileName
    }
    concepts = putName(AlephUtil.conceptDeserialize(new File("./concepts/concepts-all.txt")))
    alphaVsRandom()
  }
  
  def output(str: String): Unit = {
    Files.write(Paths.get(fileName), AlephUtil.toArrayChar(str), StandardOpenOption.APPEND)
  }
  
  def putName(rich: List[RichConcept]): List[PrimeConcept] = {
    val labeler = GeneratorUtil.Labeler.makeLabeler("concept")
    rich map (_.putLabel(labeler())) collect { case c: PrimeConcept => c }
  }
  
  def alphaVsRandom() = {
    val n = 200
    Files.write(Paths.get(fileName), Array.empty[Byte])
    output("search depth 1 \n")
    doSimulateNoHeuristicsRandom(connect5gdl, n, 1)
//    for (i <- 1 to 20) {
//      doSimulateHeuristicsRandom(connect5gdl, n, 1, i)
//    }
//    output("search depth 3 \n")
//    doSimulateNoHeuristicsRandom(connect5gdl, n, 3)
//    for (i <- 1 to 20) {
//      doSimulateHeuristicsRandom(connect5gdl, n, 3, i)
//    }
  }
  
  def alphaVsAlpha() = {
    val n = 10
    Files.write(Paths.get(fileName), Array.empty[Byte])
    output("search depth 1 \n")
    doSimulateNoHeuristicsAlphaBeta(connect5gdl, n, 1)
    for (i <- 1 to 20) {
      doSimulateAlphaBeta(connect5gdl, n, 1, i)
    }
    output("search depth 3 \n")
    doSimulateNoHeuristicsAlphaBeta(connect5gdl, n, 3)
    for (i <- 1 to 20) {
      doSimulateAlphaBeta(connect5gdl, n, 3, i)
    }
  }
  
  def doSimulateNoHeuristicsRandom(gdl: String, n: Int, depth: Int): Unit = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(gdl).getDescription)
    val roles = GdlUtil.toScalaList(stateMachine.getRoles())
    val players = List(new AlphaBetaPlayer(stateMachine, roles.head, depth),
        new RandomPlayer(stateMachine, roles.tail.head))
    val simulator = GameSimulator(stateMachine, players)
    
    val start = System.currentTimeMillis()
    println("start simulation " + n)
    val dn = n / 10
    var results = List.empty[Map[Player, Boolean]]
    for (i <- 1 to 10) {
      results ++= simulator.simulateMatch(dn)
      print((i * 10) + "% ")
    }
    
    println("\n" + results.size + " times simulation is done, " + ((System.currentTimeMillis() - start) / (60 * 1000)) + "min")

    val firstWin = results filter (_.getOrElse(players.head, false)) size
    val secondWin = results filter (_.getOrElse(players.tail.head, false)) size
    val drawN = n - firstWin - secondWin
    
    output(players.head.role + " " + firstWin + " ")
    output(players.tail.head.role + " " + secondWin + " ")
    output("draw " + drawN + " n " + n + "\n")
   }
  
  def doSimulateHeuristicsRandom(gdl: String, n: Int, depth: Int, i: Int): Unit = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(gdl).getDescription)
    val roles = GdlUtil.toScalaList(stateMachine.getRoles())
    
    val players = List(new HeuristicsPlayer(makeHeuristics(i), stateMachine, roles.head, depth),
        new RandomPlayer(stateMachine, roles.tail.head))
    val simulator = GameSimulator(stateMachine, players)
    
    val start = System.currentTimeMillis()
    println("start simulation of " + i + " for " + n + " times")
    val dn = n / 10
    var results = List.empty[Map[Player, Boolean]]
    for (i <- 1 to 10) {
      results ++= simulator.simulateMatch(dn)
      print((i * 10) + "% ")
    }
    
    println("\n" + results.size + " times simulation is done, " + ((System.currentTimeMillis() - start) / (60 * 1000)) + "min")
    
    val firstWin = results filter (_.getOrElse(players.head, false)) size
    val secondWin = results filter (_.getOrElse(players.tail.head, false)) size
    val drawN = n - firstWin - secondWin
    
    output(i + " ")
    output(players.head.role + " " + firstWin + " ")
    output(players.tail.head.role + " " + secondWin + " ")
    output("draw " + drawN + " n " + n + "\n")
  }
  
  def doSimulateNoHeuristicsAlphaBeta(gdl: String, n: Int, depth: Int): Unit = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(gdl).getDescription)
    val roles = GdlUtil.toScalaList(stateMachine.getRoles())
    val players = List(new AlphaBetaPlayer(stateMachine, roles.head, depth),
        new AlphaBetaPlayer(stateMachine, roles.tail.head, depth))
    val simulator = GameSimulator(stateMachine, players)
    
    val results = simulator.simulateMatch(n)
    
    val firstWin = results filter (_.getOrElse(players.head, false)) size
    val secondWin = results filter (_.getOrElse(players.tail.head, false)) size
    val drawN = n - firstWin - secondWin
    
    output(players.head.role + " " + firstWin + " ")
    output(players.tail.head.role + " " + secondWin + " ")
    output("draw " + drawN + " n " + n + "\n")
   }
  
  def doSimulateAlphaBeta(gdl: String, n: Int, depth: Int, i: Int): Unit = {
    val stateMachine = new ProverStateMachine()
    stateMachine.initialize(Description(gdl).getDescription)
    val roles = GdlUtil.toScalaList(stateMachine.getRoles())
    val players = List(new HeuristicsPlayer(makeHeuristics(i), stateMachine, roles.head, depth),
        new AlphaBetaPlayer(stateMachine, roles.tail.head, depth))
    val simulator = GameSimulator(stateMachine, players)
    
    val results = simulator.simulateMatch(n)
    
    val firstWin = results filter (_.getOrElse(players.head, false)) size
    val secondWin = results filter (_.getOrElse(players.tail.head, false)) size
    val drawN = n - firstWin - secondWin
    
    output(i + " ")
    output(players.head.role + " " + firstWin + " ")
    output(players.tail.head.role + " " + secondWin + " ")
    output("draw " + drawN + " n " + n + "\n")
  }
  
  def makeHeuristics(i: Int): Heuristics = {
    val cell = Symbol("cell")
    val x = Symbol("x")
    val o = Symbol("o")
    val b = Symbol("blank")
    val one = Integer(1)
    val two = Integer(2)
    val thr = Integer(3)
    val fur = Integer(4)
    val fiv = Integer(5)
    val six = Integer(6)
    val sev = Integer(7)
    val eit = Integer(8)
    
    i match {
      case 1 => Heuristics(new SuccBlankFilter(succ, BlankFilter(b)))
      case 2 => Heuristics(new SuccBlankFilter(succ, BlankFilter(b, (one,one))),
          makeSpecified(70, makeSp(cell, one, one, b)))
      case 3 => Heuristics(new SuccBlankFilter(succ, BlankFilter(b, (one,one), (thr,one))),
          makeSpecified(9, makeSp(cell, one, one, b)))
      case 4 => Heuristics(new SuccBlankFilter(succ, BlankFilter(b, (one,one), (thr,one))),
          makeSpecified(9, makeSp(cell, one, one, b)))
      case 5 => Heuristics(new SuccBlankFilter(succ, BlankFilter(b, (one,one), (thr,one))),
          makeSpecified(9, makeSp(cell, one, one, b)))
      
      case 6 => Heuristics(new SuccBlankFilter(succ, BlankFilter(b, (one,one), (thr,one))),
          makeSpecified(9, makeSp(cell, one, one, b)),
          makeSpecified(6, makeSp(cell, fur, two, x)))
      case 7 => Heuristics(new SuccBlankFilter(succ, BlankFilter(b, (one,one), (thr,one))),
          makeSpecified(9, makeSp(cell, one, one, b)),
          makeSpecified(20, makeSp(cell, thr, two, o)))
      case 8 => Heuristics(new SuccBlankFilter(succ, BlankFilter(b, (one,one), (thr,one))),
          makeSpecified(9, makeSp(cell, one, one, b)),
          makeSpecified(6, makeSp(cell, fiv, thr, x)))
      case 9 => Heuristics(new SuccBlankFilter(succ, BlankFilter(b)),
          makeSpecified(10, makeSp(cell, fur, one, x)),
          makeSpecified(11, makeSp(cell, fur, fur, x)),
          makeSpecified(4, makeSp(cell, two, fur, o)))
      
      case 10 => Heuristics(new SuccBlankFilter(succ, BlankFilter(b)),
          makeSpecified(10, makeSp(cell, fur, one, x)),
          makeSpecified(11, makeSp(cell, fur, fur, x)),
          makeSpecified(4, makeSp(cell, two, fur, o)))    
      case 11 => Heuristics(new SuccBlankFilter(succ, BlankFilter(b, (thr,one), (fiv, one))),
          makeSpecified(10, makeSp(cell, fur, one, x)),
          makeSpecified(9, makeSp(cell, thr, one, b)),
          makeSpecified(4, makeSp(cell, two, fur, o)))
      case 12 => Heuristics(new SuccBlankFilter(succ, BlankFilter(b, (thr,one), (fiv, one))),
          makeSpecified(10, makeSp(cell, fur, one, x)),
          makeSpecified(9, makeSp(cell, thr, one, b)),
          makeSpecified(8, makeSp(cell, thr, sev, x)),
          makeSpecified(4, makeSp(cell, eit, thr, x)))
      case 13 => Heuristics(new SuccBlankFilter(succ, BlankFilter(b, (thr,one), (fiv, one))),
          makeSpecified(10, makeSp(cell, fur, one, x)),
          makeSpecified(9, makeSp(cell, thr, one, b)),
          makeSpecified(8, makeSp(cell, thr, sev, x)),
          makeSpecified(4, makeSp(cell, eit, thr, x)))
      case 14 => Heuristics(new SuccBlankFilter(succ, BlankFilter(b, (six,one), (eit, one), (eit,six), (eit,eit))),
          makeSpecified(9, makeSp(cell, six, one, b)),
          makeSpecified(13, makeSp(cell, eit, six, b)),
          makeSpecified(20, makeSp(cell, sev, fiv, x)),
          makeSpecified(13, makeSp(cell, fur, thr, o)))
          
      case 15 => Heuristics(new SuccBlankFilter(succ, BlankFilter(b, (six,one), (eit, one), (eit,six), (eit,eit))),
          makeSpecified(9, makeSp(cell, six, one, b)),
          makeSpecified(13, makeSp(cell, eit, six, b)),
          makeSpecified(20, makeSp(cell, sev, fiv, x)),
          makeSpecified(13, makeSp(cell, fur, thr, o)))
      case 16 => Heuristics(new SuccBlankFilter(succ, BlankFilter(b, (six,one), (eit,one), (eit,six), (eit,eit))),
          makeSpecified(9, makeSp(cell, six, one, b)),
          makeSpecified(13, makeSp(cell, eit, six, b)),
          makeSpecified(4, makeSp(cell, six, thr, o)),
          makeSpecified(10, makeSp(cell, fur, fiv, x)))
      case 17 => Heuristics(new SuccBlankFilter(succ, BlankFilter(b, (six,one), (eit, one), (eit,six), (eit,eit))),
          makeSpecified(9, makeSp(cell, six, one, b)),
          makeSpecified(13, makeSp(cell, eit, six, b)),
          makeSpecified(4, makeSp(cell, six, thr, o)),
          makeSpecified(10, makeSp(cell, fur, fiv, x)))
      case 18 => Heuristics(new SuccBlankFilter(succ, BlankFilter(b, (six,one), (eit,one), (eit,eit), (sev,eit))),
          makeSpecified(9, makeSp(cell, six, one, b)),
          makeSpecified(8, makeSp(cell, eit, eit, b)),
          makeSpecified(4, makeSp(cell, six, thr, o)),
          makeSpecified(10, makeSp(cell, fur, fiv, x)),
          makeSpecified(20, makeSp(cell, fiv, six, o)))
      case 19 => Heuristics(new SuccBlankFilter(succ, BlankFilter(b, (six,one), (eit,one), (eit,eit), (sev,eit))),
          makeSpecified(9, makeSp(cell, six, one, b)),
          makeSpecified(8, makeSp(cell, eit, eit, b)),
          makeSpecified(4, makeSp(cell, six, thr, o)),
          makeSpecified(10, makeSp(cell, fur, fiv, x)),
          makeSpecified(20, makeSp(cell, fiv, six, o)),
          makeSpecified(8, makeSp(cell, thr, one, o)))
      case 20 => Heuristics(new SuccBlankFilter(succ, BlankFilter(b, (six,one), (eit,one))),
          makeSpecified(9, makeSp(cell, six, one, b)),
          makeSpecified(20, makeSp(cell, six, one, o)),
          makeSpecified(8, makeSp(cell, sev, fiv, x)),
          makeSpecified(8, makeSp(cell, thr, sev, x)),
          makeSpecified(8, makeSp(cell, eit, sev, x)))
    }
  }
  
  /*
   * 
   *concept4 = piece(x0, x1, x2, x3), piece(x0, (+ x1, -1), (+ x2, -1), x3)
   *concept6 = piece(x0, x1, x2, x3), piece(x0, (+ x1, 2), (+ x2, -1), x3)
   *concpet8 = piece(x0, x1, x2, x3), piece(x0, (+ x1, -1), x2, x3)
   *concept9 = piece(x0, x1, x2, x3), piece(x0, (+ x1, 2), x2, x3)
   *concept10 = piece(x0, x1, x2, x3), piece(x0, (+ x1, 2), (+ x2, 2), x3)
   *concept11 = piece(x0, x1, x2, x3), piece(x0, (+ x1, 2), (+ x2, 1), x3)
   *concept13 = piece(x0, x1, x2, x3), piece(x0, x1, (+ x2, 2), x3)
   *concept14 = piece(x0, x1, x2, x3), piece(x0, (+ x1, -2), (+ x2, -2), x3)
   *concept20 = piece(x0, x1, x2, x3), piece(x0, x1, (+ x2, 1), x3)
   *concept70 = piece(x0, x1, x1, x2), piece(x0, 1, x1, x2), piece(x0, x1, 1, x2)
   * 
   */
  
  def makePrimeHeuristics(i: Int): PrimeHeuristics = PrimeHeuristics(pickUpConcept(i), eval)

  val x0 = Label("x0")
  val x1 = Label("x1")
  val x2 = Label("x2")
  val x3 = Label("x3")
    
  def makeSp(s1: LeafExp, i1: LeafExp, i2: LeafExp, s2: LeafExp): Specialization = new Specialization(Map(x0 -> s1, x1 -> i1, x2 -> i2, x3 -> s2))
  
  def makeSpecified(i: Int, sp: Specialization): PrimeHeuristics = (new SpecifiedHeuristics(pickUpConcept(i), sp, eval)).toHeuristics
  
  def pickUpConcept(i: Int): PrimeConcept = concepts.find(_.conceptName.getOrElse(Label("")).label == ("concept" + i.toString)).get
}