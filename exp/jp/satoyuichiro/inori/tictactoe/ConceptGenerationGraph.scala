package jp.satoyuichiro.inori.tictactoe

import jp.satoyuichiro.inori.simulator.ProverGameSimulator
import java.io.File
import java.io.FileInputStream
import java.io.ObjectInputStream
import jp.satoyuichiro.inori.cdl.Position
import java.io.FileOutputStream
import java.io.ObjectOutputStream
import jp.satoyuichiro.inori.learning.concept.generator.RichConcept
import jp.satoyuichiro.inori.learning.concept.generator.ArithmeticConjunctionGenerator
import jp.satoyuichiro.inori.cdl.Piece
import jp.satoyuichiro.inori.player.PlayerUtil
import jp.satoyuichiro.inori.learning.concept.generator.GeneratorUtil

object ConceptGenerationGraph {

  val n = 199
  
  def main(args: Array[String]): Unit = {
    //prepareData()
    //conceptMake()
    countConcepts()
  }
  
  val workingDir = "./ILPworkspace/graph/"
  val tictactoeGdl = "./games/twoplayers/tictactoe.gdl"
    
  def prepareData(): Unit = {
    val simulator = new ProverGameSimulator(new File(tictactoeGdl))
    for(i <- 0 to n) {
      positionSerialize(simulator.simulateRandomPlayout(10), i)
    }
    
    for(i <- 0 to n) {
      println(positionDeserialize(i))
    }
  }
  
  def positionSerialize(positions: List[Position], n: Int): Unit = {
    val fileName = workingDir + "positions"+ n + ".txt"
    try {
      val cFile = new FileOutputStream(fileName)
	  val cStream = new ObjectOutputStream(cFile)
	  cStream.writeObject(positions)

	  cStream.close()
	  cFile.close()
    } catch {
      case _: Throwable => println("something wrong!")
    }
  }
  
  def positionDeserialize(n: Int): List[Position] = {
    val cFileName = workingDir + "positions"+ n + ".txt"
    val cf = new File(cFileName)
    
    if (cf.exists()) {
      val cFile = new FileInputStream(cFileName)
	  val cStream = new ObjectInputStream(cFile)

      try {
        val cmap = cStream.readObject().asInstanceOf[List[Position]]
            
        cmap
      } catch {
        case _: Throwable => List.empty[Position]
      } finally {
        cStream.close()
        cFile.close()
      }
    } else {
      println("no file")
      List.empty[Position]
    }
  }

  def conceptMake(): Unit = {
    for (i <- 0 to n) {
      val positions = positionDeserialize(i)
      val concepts = makeBiConcepts(positions) ++ makeTriConcepts(positions)
      conceptSerialize(concepts, i)
    }
  }
  
  def makeBiConcepts(positions: List[Position]): List[RichConcept] = {
    val generator = new ArithmeticConjunctionGenerator()
    val cells = positions map (
        position => new Position(
            position.position.filter(piece => piece.isInstanceOf[Piece]).filter(piece => piece.asInstanceOf[Piece].args.size > 2), None))
    
    val biPositions = cells flatMap (p => PlayerUtil.makeAllBinaryPosition(p))
    GeneratorUtil.generateUniquConcept(generator, biPositions)
  }
  
  def makeTriConcepts(positions: List[Position]): List[RichConcept] = {
    val generator = new ArithmeticConjunctionGenerator()
    val cells = positions map (
        position => new Position(
            position.position.filter(piece => piece.isInstanceOf[Piece]).filter(piece => piece.asInstanceOf[Piece].args.size > 2), None))
    
    val triPositions = cells flatMap (p => PlayerUtil.makeTrinaryPosition(p))
    GeneratorUtil.generateUniquConcept(generator, triPositions)
  }
  
  def conceptSerialize(concepts: List[RichConcept], n: Int): Unit = {
    val fileName = workingDir + "concepts"+ n + ".txt"
    try {
      val cFile = new FileOutputStream(fileName)
	  val cStream = new ObjectOutputStream(cFile)
	  cStream.writeObject(concepts)

	  cStream.close()
	  cFile.close()
    } catch {
      case _: Throwable => println("something wrong!")
    }
  }
  
  def conceptDeserialize(n: Int): List[RichConcept] = {
    val cFileName = workingDir + "concepts"+ n + ".txt"
    val cf = new File(cFileName)
    
    if (cf.exists()) {
      val cFile = new FileInputStream(cFileName)
	  val cStream = new ObjectInputStream(cFile)

      try {
        val cmap = cStream.readObject().asInstanceOf[List[RichConcept]]
            
        cmap
      } catch {
        case e: Throwable =>
          e.printStackTrace()
          List.empty[RichConcept]
      } finally {
        cStream.close()
        cFile.close()
      }
    } else {
      println("no file")
      List.empty[RichConcept]
    }
  }
  
  def countConcepts(): Unit = {
    var concepts = List.empty[RichConcept]
    for (i <- 0 to n) {
      concepts ++= conceptDeserialize(i)
      concepts = concepts.distinct
      println((10 * (i + 1)).toString + " " + concepts.size)
    }
    
    val fileName = "concepts/concepts-all.txt"
      
    try {
      val cFile = new FileOutputStream(fileName)
	  val cStream = new ObjectOutputStream(cFile)
	  cStream.writeObject(concepts)

	  cStream.close()
	  cFile.close()
    } catch {
      case _: Throwable => println("something wrong!")
    }
  }
}