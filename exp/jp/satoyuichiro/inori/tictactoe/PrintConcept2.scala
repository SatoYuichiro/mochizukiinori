package jp.satoyuichiro.inori.tictactoe

import jp.satoyuichiro.inori.ilp.AlephUtil
import java.io.File

object PrintConcept2 {

  def main(args: Array[String]): Unit = {
    var count = 0
    val concepts = AlephUtil.conceptDeserialize(new File("./concepts/concepts-all.txt"))
    concepts.foreach { c =>
      println(count + " " + c.toConcept.toLisp)
      count += 1
    }}

}