package jp.satoyuichiro.inori.tictactoe

import jp.satoyuichiro.inori.ilp.AlephUtil
import java.io.File
import jp.satoyuichiro.inori.cdl.Label
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardOpenOption

object PrintConcept3 {

  def main(args: Array[String]): Unit = {
    val concepts = AlephUtil.conceptDeserialize(new File("./concepts/concepts-all.txt"))
    var i = 0
    val outputFile = Paths.get("./concepts/concepts-define.txt")
    Files.write(outputFile, "".getBytes(), StandardOpenOption.CREATE)
    concepts.foreach { c =>
      val lisp = c.putLabel(Label("concept" + i)).toDefineConcept.get.toLisp + "\n"
      println(lisp)
      Files.write(outputFile, lisp.getBytes(), StandardOpenOption.APPEND)
      i += 1
    }
  }
}