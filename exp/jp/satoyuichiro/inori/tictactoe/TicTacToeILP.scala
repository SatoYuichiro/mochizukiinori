package jp.satoyuichiro.inori.tictactoe

import jp.satoyuichiro.inori.ilp.AlephEncoder
import jp.satoyuichiro.inori.simulator.ProverGameSimulator
import java.io.File
import jp.satoyuichiro.inori.ilp.AlephUtil
import jp.satoyuichiro.inori.ilp.AlephConceptEncoder
import jp.satoyuichiro.inori.learning.concept.generator.RichConcept
import jp.satoyuichiro.inori.learning.concept.generator.PrimeConcept
import jp.satoyuichiro.inori.learning.concept.generator.GeneratorUtil
import jp.satoyuichiro.inori.cdl._

object TicTacToeILP {

  val tictactoeGdl = "./games/twoplayers/tictactoe.gdl"
  val alephEncoder = new AlephEncoder
  val proverSimulator = new ProverGameSimulator(new File(tictactoeGdl))
  
  def main(args: Array[String]): Unit = {
    val wins = proverSimulator.simulateFirstWinPlayout(10)
    println(wins.size)
    val loses = proverSimulator.simulateSecondWinPlayout(10)
    println(loses.size)
    alephEncoder.encode(wins, loses, "tictactoe")
    val concepts = putName(AlephUtil.conceptDeserialize(new File("./concepts/concepts-all.txt")))
    val encoder = new AlephConceptEncoder(Array(Symbol("cell"), Integer(1), Integer(1), Symbol("blank")))
    encoder.encode(concepts, "tictactoe")
  }
  
  def putName(rich: List[RichConcept]): List[PrimeConcept] = {
    val labeler = GeneratorUtil.Labeler.makeLabeler("concept")
    rich map (_.putLabel(labeler())) collect { case c: PrimeConcept => c }
  }
}