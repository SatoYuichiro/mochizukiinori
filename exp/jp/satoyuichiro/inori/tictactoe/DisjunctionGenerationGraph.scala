package jp.satoyuichiro.inori.tictactoe

import jp.satoyuichiro.inori.ilp.AlephUtil
import java.io.File
import jp.satoyuichiro.inori.simulator.ProverGameSimulator
import jp.satoyuichiro.inori.learning.concept.generator.DisjunctionGenerator
import jp.satoyuichiro.inori.learning.concept.generator.PrimeConcept
import jp.satoyuichiro.inori.learning.concept.generator.GeneratorUtil
import jp.satoyuichiro.inori.learning.concept.generator.CompositeDisjunction

object DisjunctionGenerationGraph {

  val dir = "./ILPworkspace/graph/disjunction/"
  
  def main(args: Array[String]): Unit = {
    //makePlayouts()
    //conceptMake()
    countConcepts()
  }

  def makePlayouts(): Unit = {
    val simulator = new ProverGameSimulator(new File("./games/twoplayers/tictactoe.gdl"))
    for (i <- 0 to 199) {
      val fileName = dir + "playout" + i + ".txt"
      val playouts = simulator.simulateFirstWinPlayout(10)
      AlephUtil.positionSerialize(playouts, fileName)
    }
  }
  
  def conceptMake(): Unit = {
    val rich = AlephUtil.conceptDeserialize(new File("./concepts/concepts-all.txt"))
    val labeler = GeneratorUtil.Labeler.makeLabeler("concept")
    val concepts = rich map (_.putLabel(labeler())) collect { case c: PrimeConcept => c }
    concepts foreach println
    for (i <- 0 to 199) {
      val generator = new DisjunctionGenerator()
      val pFileName = dir + "playout" + i + ".txt"
      val playouts = AlephUtil.positionDeserialize(pFileName)
      println(playouts)
      val disjunction = generator.generate(concepts, playouts)
      val dFileName = dir + "disjunction" + i + ".txt"
      println(i)
      println(disjunction)
      println(disjunction.flatMap(a => Some(a.arity)))
      disjunction match {
        case Some(d) => AlephUtil.conceptSerialize(d.primes, dFileName)
        case None => println("None")
      }
    }
  }
  
  def countConcepts(): Unit = {
    var primes = List.empty[PrimeConcept]
    for (i <- 0 to 199) {
      val dFileName = dir + "disjunction" + i + ".txt"
      val concepts = AlephUtil.conceptDeserialize(dFileName) collect { case c: PrimeConcept => c }
      primes ++= concepts
      primes = primes.distinct
      println((10 * (i + 1)).toString + " " + primes.size)
    }
    primes foreach println
    primes foreach { p => println(p.conceptName.get.label + " " + p.arity.toString + " " + p.pieces.head.toLisp) }
    outputConcept(primes)
  }
  
  def outputConcept(primes: List[PrimeConcept]): Unit = {
    val disj = List(CompositeDisjunction(primes))
    val fileName = "./concepts/disjunction.txt"
    AlephUtil.conceptSerialize(disj, fileName)
  }
}