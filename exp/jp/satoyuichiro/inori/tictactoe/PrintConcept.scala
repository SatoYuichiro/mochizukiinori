package jp.satoyuichiro.inori.tictactoe

import jp.satoyuichiro.inori.ilp.AlephUtil
import java.io.File

object PrintConcept {

  def main(args: Array[String]): Unit = {
    val concepts = AlephUtil.conceptDeserialize(new File("./concepts/concepts-all.txt"))
    concepts.foreach { c =>
      println(c.toConcept.toLisp)
    }
  }

}